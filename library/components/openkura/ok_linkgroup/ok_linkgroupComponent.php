<?php
	class ok_linkgroupComponent extends Component
	{

		public function createInstance($params = null)
		{
			$resManager = Managers::ResourceManager();
			$r = $resManager->queryAssoc("Instance()<Component('ok_linkgroup');");
			if(!$r)
				$r = 1;
			else
				$r = sizeof($r)+1;

			if(isset($params['title'])) {
				if(!$this->arrayInsert('kura_linkgroup', array(
									'value' => $params['title'],
									'inst' => $r,
									'item' => 0,
									'setting' => 0,
									'link' => "")))
					return null;
			}
			
			return $r;
		}

		public function initialize()
		{

		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getGroup($args);
			break;
			}

			if($args == null)
				echo $response;

			return $response;
		}

		private function getGroup($args)
		{
			$result = $this->db->sendQuery("SELECT * FROM kura_linkgroup WHERE inst='{$this->instanceId}' ORDER BY item;", false, false);
			return $result;
		}

	}
?>
