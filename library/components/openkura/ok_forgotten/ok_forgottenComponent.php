<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

 	/*
	*  component for managing requests for lost passwords.
	*  file a request, then user can regenerate their password
	*/
	class ok_forgottenComponent extends Component
	{
		public function initialize()
		{

		}

		public function createInstance($params = null)
		{
			// really there should only be one instance
			return 1;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->addRequest($args);
			break;

			case 2:
				$response = $this->resolveRequest($args);
			break;
			}

			if($args == null)
				echo $response;

			return $response;
		}

		private function addRequest($args)
		{
			include_once(LibLoad::shared('kura', 'forgotten'));
			$flib = new forgottenLibrary($this->db);

			$user = $_POST['user'];
			if(!$flib->addRequest($user))
				$this->addTrackerParam("fpcode", 0);
			else
				$this->addTrackerParam("fpcode", 1);
		}

		private function resolveRequest($args)
		{
			include_once(LibLoad::shared('kura', 'forgotten'));
			$flib = new forgottenLibrary($this->db);
			if(!$flib->regeneratePassword($args['hash'], $args['password']))
				return 104;

			$flib->removeHash($args['hash']);

			return 102;
		}

	}
?>
