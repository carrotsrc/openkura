<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_myprofileComponent extends Component
	{
		private $resManager;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
		}

		public function createInstance($params = null)
		{
			return 1;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel)
			{
			case 1:
				$response = $this->currentUser($args);
			break;

			case 2:
				$response = $this->getUserProfile($args);
			break;

			case 10:
				$response = $this->addProfileItem($args);
			break;
			
			case 11:
				$response = $this->removeProfileItem($args);
			break;

			case 12:
				$response = $this->fullUpdate($args);
			break;

			case 20:
				$response = $this->getHeaderList($args);
			break;

			case 21:
				$response = $this->setHeader($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		public function currentUser($args)
		{
			$uid = Session::get('uid');
			if($uid == 0 || $uid === null)
				return 104;
			$rq = "Profile()<User('$uid');";

			$res = $this->resManager->queryAssoc($rq);
			if(!$res)
				return 104;

			$res = $res[0][0];
			$ref = $this->resManager->getHandlerRef($res);
			include_once(LibLoad::shared('kura', 'profile'));
			include_once(LibLoad::shared('vpatch', 'users'));

			$ulib = new usersLibrary($this->db);
			$plib = new profileLibrary($this->db);

			$details = $plib->getProfile($ref);
			if($details == null)
				return 104;

			$res = $ulib->getAccount($uid);
			$details[0][] = $res[0][1];
			$details[0][] = 1;
			$this->getExtraItems($ref, $details[0]);

			if($args == null)
				return $this->arrayToString($details);

			return $details;
		}

		public function getUserProfile($args)
		{
			$vars = $this->argVar(array('profile' => 'user'), $args);

			if($vars->user == null)
				return 104;

			if(!is_numeric($vars->user)) {
				$res = $this->resManager->queryAssoc("User('{$vars->user}');");
				if(!$res)
					return 104;

				$vars->user = $this->resManager->getHandlerRef($res[0][0]);
			}

			$ridp = $this->resManager->queryAssoc("Profile()<User('{$vars->user}');");
			if(!$ridp)
				return 104;

			$ref = $this->resManager->getHandlerRef($ridp[0][0]);

			include_once(LibLoad::shared('kura', 'profile'));
			include_once(LibLoad::shared('vpatch', 'users'));

			$ulib = new usersLibrary($this->db);
			$plib = new profileLibrary($this->db);

			$details = $plib->getProfile($ref);
			if($details == null)
				return 104;

			$res = $ulib->getAccount($vars->user);
			$details[0][] = $res[0][1];

			$uid = Session::get('uid');

			if($uid == $vars->user)
				$details[0][] = 1;
			else
				$details[0][] = 0;

			$this->getExtraItems($ref, $details[0]);

			if($args == null)
				return $this->arrayToString($details);
			return $details;
		}

		private function getExtraItems($id, &$details)
		{
			$res = $this->db->sendQuery("SELECT * FROM kura_profile_item WHERE profile='$id' ORDER BY seq;", false, false);
			if(!$res)
				return;

			foreach($res as $item) {
				$details[] = array($item[2], $item[4]);
			}
		}

		private function addProfileItem($args)
		{
			$vars = $this->argVar(array('pr-item' => 'type',
						'pr-profile' => 'profile'), $_POST);

			$uid = Session::get('uid');
			$chk = $this->resManager->queryAssoc("User('$uid')>Profile('{$vars->profile}');");
			if(!$chk)
				return 104;

			include_once(LibLoad::shared('kura', 'profile'));

			$plib = new profileLibrary($this->db);

			$plib->addProfileItem($vars->profile, $vars->type);
			return 102;
		}

		private function removeProfileItem($args)
		{
			$vars = $this->argVar(array('pr-item' => 'type',
						'pr-profile' => 'profile'), $_GET);

			$uid = Session::get('uid');
			$chk = $this->resManager->queryAssoc("User('$uid')>Profile('{$vars->profile}');");
			if(!$chk)
				return 104;

			include_once(LibLoad::shared('kura', 'profile'));

			$plib = new profileLibrary($this->db);

			$plib->removeProfileItem($vars->profile, $vars->type);
			return 102;
		}

		private function fullUpdate($args)
		{
			$pid = null;
			if(isset($_POST['pr-profile']))
				$pid = $_POST['pr-profile'];
			else
				return 104;

			$uid = Session::get('uid');
			$chk = $this->resManager->queryAssoc("User('$uid')>Profile('{$pid}');");
			if(!$chk)
				return 104;

			include_once(LibLoad::shared('kura', 'profile'));

			$plib = new profileLibrary($this->db);

			$profile =  array();
			$items = array();
			foreach($_POST as $k => $v) {
				if(strlen($k) < 3)
					continue;

				$parts = explode("-", $k);
				if($parts[0] != "pr")
					continue;

				switch($parts[1]) {
				case 'first':
					$profile['first'] = $v;
				break;

				case 'last':
					$profile['last'] = $v;
				break;

				case 'profile':
				break;

				default:
					$items[$parts[1]] = $v;
				break;
				}
			}

			$plib->updateProfile($pid, $profile['first'], $profile['last']);
			foreach($items as $k => $v)
				$plib->updateProfileItem($pid, $k, $v);

			return 102;
		}

		private function getHeaderList($args)
		{
			$fm = Koda::getFileManager();
			$path = SystemConfig::relativeLibPath("/media/kura/profile/headers");
			$list = $fm->listFiles($path);
			$items = array();
			foreach($list as $f) {
				if(strlen($f) < 5)
					continue;

				$parts = explode("_", $f);
				if($parts[0] != "prev")
					continue;

				$items[] = array($parts[1], $f);
			}
			return $items;
		}

		private function setHeader($args)
		{
			$vars = $this->argVar(array('pr-header' => 'header',
						'pr-profile' => 'profile'), $_GET);

			$uid = Session::get('uid');
			$chk = $this->resManager->queryAssoc("User('$uid')>Profile('{$vars->profile}');");
			if(!$chk)
				return 104;

			include_once(LibLoad::shared('kura', 'profile'));

			$plib = new profileLibrary($this->db);
			$plib->updateHeader($vars->profile, $vars->header);
		}
	}
?>
