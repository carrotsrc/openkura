<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_coursemanComponent extends Component
	{
		private $resManager;
		private $mlib;

		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			$this->mlib = null;
		}

		public function createInstance($params = null)
		{
			return 1;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;
			
			switch($channel) {
			case 1:
				$response = $this->addActivity($args);
			break;

			case 2:
				$response = $this->getActivityComponents($args);
			break;

			case 3:
				$response = $this->getActivityManager($args);
			break;

			case 4:
				$response  = $this->getActivities($args);
			break;

			case 5:
				$response = $this->getActivityDetails($args);
			break;

			case 6:
				$response = $this->getTypeManager($args);
			break;

			case 7:
				$response = $this->getUsers($args);
			break;

			case 8:
				$response = $this->getUserDetails($args);
			break;

			case 9:
				$response = $this->addGroup($args);
			break;

			case 10:
				$response = $this->getGroups($args);
			break;

			case 11:
				$response = $this->getGroupDetails($args);
			break;

			case 12:
				$response = $this->addGroupUser($args);
			break;

			case 13:
				$response = $this->remGroupUser($args);
			break;

			case 14:
				$response = $this->getAssocs($args);
			break;

			case 15:
				$response = $this->disassocResource($args);
			break;

			case 20:
				$response = $this->updateState($args);
			break;


			case 100:
				$response = $this->addResource($args);
			break;

			case 101:
				$response = $this->dropResources($args);
			break;
			}

			if($args == null)
				echo $response;

			return $response;
		}

		private function addActivity($args)
		{
			$mlib = null;
			$vars = $this->argVar(array('acname' => 'name',
							'acdesc' => 'desc',
							'actype' => 'type',
							'acstart' => 'start',
							'acend' => 'end',
							'acinc' => 'inclusion',
							'aclimited' => 'limited',
							'kmodule' => 'module'), $_POST);
			if(isset($_GET['kmodule']))
				$vars->module=$_GET['kmodule'];

			if($vars->start != "0") {
				$dt = explode(" ",$vars->start);
				$date = explode("/", $dt[0]);
				$time = null;
				if(isset($dt[1]))
					$time = explode(":", $dt[1]);
				else
					$time = array(0,0,0);
				$vars->start = "{$date[2]}-{$date[1]}-{$date[0]} {$time[0]}:{$time[1]}:00";
			}

			if($vars->end != "") {
				$dt = explode(" ",$vars->end);
				$date = explode("/", $dt[0]);
				$time = null;
				if(isset($dt[1]))
					$time = explode(":", $dt[1]);
				else
					$time = array(0,0,0);
				$vars->end = "{$date[2]}-{$date[1]}-{$date[0]} {$time[0]}:{$time[1]}:00";
			}
			include_once(LibLoad::shared('stdint', 'areas'));
			if($this->mlib == null) {
				include_once(LibLoad::shared('kura', 'module'));
				$this->mlib = $modlib = new ModuleLibrary($this->db);
			}
			else
				$modlib = $this->mlib;

			// first we find the student area for the module
			// check to see if we know where we are
			if($vars->module == null)
				if($args == null)
					return $this->jsonError("Module unspecified");
				else
					return 104; // we're lost

			$mod = $this->resManager->queryAssoc("KModule('{$vars->module}');");
			if(!$mod)
				if($args == null)
					return $this->jsonError("Invalid module");
				else
					return 104;

			$mod = $mod[0][0];

			$modarea = $this->resManager->queryAssoc("Area()<KModule('{$vars->module}'):cstudent;");
			if(!$modarea)
				if($args == null)
					return $this->jsonError("No student area");
				else
					return 104;

			$modarea = $modarea[0][0];

			$id = $rid = null;
			if($vars->limited == null)
				$vars->end = 0;

			if(($id = $modlib->addActivity($vars->name, $vars->desc,
							$vars->start, $vars->end, $vars->inclusion)) == null)
								if($args == null)
									return $this->jsonError("Failed to create activity");
								else
									return 104;

			$vars->name = str_replace(' ', '_', $vars->name);
			$vars->name = strtolower($vars->name);

			if(!($rid = $this->resManager->addResource('Activity', $id, $vars->name))) {
				$modlib->removeActivity($id);
				if($args == null)
					return $this->jsonError("Failed to add activity (Resource)");
				else
					return 104;
			}

			//set the current user as a coordinator of activity
			$uid = Session::get('uid');
			$ridu = $this->resManager->queryAssoc("User('$uid');");
			if($ridu)
				$this->resManager->createRelationship($rid, $ridu[0][0], $this->resManager->getEdge("coord"));

			$this->resManager->createRelationship($mod, $rid);

			// we have now created an activity and added it to the module
			// now we create an area for the activity
			$acname = "{$vars->name}_actarea";
			$arid = null;
			if(!$arid = $this->createArea($acname, $modarea))
				if($args == null)
					return $this->jsonError("Failed to create area");
				else
					return 104;

			switch($vars->inclusion) {
			case 2:
				$crid = $this->resManager->queryAssoc("Channel('cact_grpaccess');");
				if(!$crid)
					if($args == null)
						return $this->jsonError("Failed to locate channel");
					else
						return 104;

				$this->resManager->createRelationship($arid, $crid[0][0]);
			break;

			default:
				$crid = $this->resManager->queryAssoc("Channel('cact_moduleaccess');");
				if(!$crid)
					if($args == null)
						return $this->jsonError("Failed to locate channel");
					else
						return 104;

				$this->resManager->createRelationship($arid, $crid[0][0]);
			break;
			}

			// now we assoc the area with the activity (Area is space so parent)
			$this->resManager->createRelationship($arid, $rid);

			// now we create an activity manager (for the component)
			// if there isn't one already
			$route = $this->createActivityManager($vars->module, $vars->type);
			if(!$route)
				if($args == null)
					return $this->jsonError("Failed to create activity manager");
				else
					return 104;

			// set the type of working component for the activity
			$ridc = $this->resManager->queryAssoc("Component('{$vars->type}');");
			if(!$ridc) // TODO: still got crap left over if this happens
				if($args == null)
					return $this->jsonError("No working component");
				else
					return 104;

			$this->resManager->createRelationship($rid, $ridc[0][0]);

			$this->setRio(RIO_INS, $rid);
			$this->addTrackerParam('acret', 102);
			$this->addTrackerParam('aca', $this->resManager->getHandlerRef($route[0]));
			$this->addTrackerParam('acl', $this->resManager->getHandlerRef($route[1]));
			$this->addTrackerParam('aci', $id);
			$this->addTrackerParam('acp', $route[2]);

			if($args == null)
				return $this->jsonAddActivity();

			return 102;
		}

		private function jsonAddActivity()
		{
			$str = "{\"code\":102}";
			return $str;
		}

		private function jsonError($message)
		{
			$str = "{\"code\":104,\n";
			$str .="\"data\": \"$message\"}";

			return $str;
		}

		private function createArea($name, $pid)
		{
			$arealib = new AreasLibrary($this->db);
			$rid = $id = null;

			$rid = $this->resManager->getResourceFromId($pid);
			$ref = $rid['handler'];
			$ref = $arealib->getAreaDetails($ref);

			// we use the parent area's surround and template by default
			if(($id = $arealib->addArea($name, $ref[1], $ref[2])) == null)
				return false;

			if(!($rid = $this->resManager->addResource('Area', $id, $name))) {
				$arealib->removeArea($id);
				return false;
			}

			if(!$this->resManager->createRelationship($pid, $rid))
				return false;

			return $rid;

		}

		private function createActivityManager($module, $ridt)
		{
			$res = $this->resManager->queryAssoc("Area()<KModule('$module'):cadmin;");
			if(!$res)
				return false;

			$res = $res[0][0];

			// find the component for management of this activity
			$ridc = $this->resManager->queryAssoc("Component()>Component('$ridt');");
			if(!$ridc)
				return false;

			// get the panel
			$ridc = $ridc[0][0];

			$crud = $this->resManager->queryAssoc("Panel()>(Crud()<Component($ridc));");
			if(!$crud)
				return false;

			$crud = $crud[0][0];

			// check to see if there is an instance of the manager
			// for this activity in the module area
			// TODO:
			// This needs to be a single query once the query system is updated again
			// to handle combining two branches of queries
			$inst = $this->resManager->queryAssoc("Instance()<Area($res);");
			$man = false;
			if($inst) {
				foreach($inst as $i) {
					if(($man = $this->resManager->queryAssoc("Instance({$i[0]})<Component($ridc);")))
						break;
				}
			}

			
			$lid = null;
			$name = "{$module}_{$ridt}_manager";

			if(!$man) {
				// if there is no instance then we need to create one
				if(!($man = $this->createCmptInst($ridc, $res, $module)))
				return false;
				
				$crudop = $this->resManager->queryAssoc("CrudOps('crudop_activity');");
				$this->resManager->createRelationship($man, $crudop[0][0]);
				// presumably if there is no instance then there is no layout
				$mref = $this->resManager->getHandlerRef($man);
				$lid = $this->createManagementLayout($name, $ridc, $res, $mref, $crud);
			}
			else {
				$man = $man[0][0];
				$mref = $this->resManager->getHandlerRef($man);
				// TODO:
				// This needs to be a single query once the query system is updated again
				// to handle combining two branches of queries
				$lid = $this->resManager->queryAssoc("Layout()<Area($res);");
				if($lid) {
					foreach($lid as $l) {
						if(($r = $this->resManager->queryAssoc("Layout({$l[0]})<Component($ridc);"))) {
							$lid = $r;
							break;
						}
					}
				}

				if(!$lid) { // TODO: $ridc points to activity component not manager
					$lid = $this->createmanagementLayout($name, $ridc, $res, $mref, $crud);
				}
				else {
					$lid = $lid[0][0];
				}
			}

			// associate the layout with the manager instance
			$this->resManager->createRelationship($man, $lid);
			return array($res, $lid, $this->resManager->getHandlerRef($crud));
		}

		private function createCmptInst($ridc, $rida, $module)
		{
			$c = $this->resManager->getResourceFromId($ridc);
			$cid = $c['handler'];
			$cmpt = ModMan::getComponent($cid, 0, $this->db);
			if(!$cmpt)
				return false;

			$ref = $cmpt->createInstance();
			$ref = $this->resManager->addResource('Instance', $ref, "{$module}_{$c['label']}_$rida");

			$this->resManager->createRelationship($ridc, $ref);
			$this->resManager->createRelationship($rida, $ref);

			return $ref;
		}

		private function createManagementLayout($name, $ridc, $rida, $ref, $crud = null)
		{
			// presumeably if there is no instance then there is no
			// management layout
			$pid = $this->resManager->getHandlerRef($crud);
			$cid = $this->resManager->getHandlerRef($ridc);

			$cid = $this->resManager->getHandlerRef($ridc);

			$markup = "<node type=\"0\" style=\"display: block; float: none; clear: none;\"><leaf type=\"0\" pid=\"$pid\" cid=\"$cid\" ref=\"$ref\" grp=\"0\" style=\"display: block; float: none; clear: none;\"/></node>";

			include_once(LibLoad::shared('stdint', 'layout'));
			$layoutlib = new LayoutLibrary($this->db);
			$lid = null;
			if(!($lid = $layoutlib->addLayout($name, $markup)))
				return false;

			$ridl = $this->resManager->addResource("Layout", $lid, $name);
			$this->resManager->createRelationship($rida, $ridl);
			$this->resManager->createRelationship($ridc, $ridl);

			return $ridl;
		}
/*
*  JACK 2: Get actitivity Components
*/
		private function getActivityComponents($args)
		{
			$result = $this->resManager->queryAssoc("Component()<Type('Activity');");
			if(!$result)
				return 104;
			$cmpt = array();
			foreach($result as $row)
				$cmpt[] = $this->resManager->getResourceFromId($row[0]);

			if($args == null)
				return $this->jsonActivityComponents($cmpt);

			return $cmpt;
		}

		private function jsonActivityComponents($cmpt)
		{
			if(!$cmpt)
				return "{\"code\":104}";

			$uid = Session::get('uid');

			$sz = sizeof($cmpt)-1;
			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";
			
			foreach($cmpt as $c) {
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$c['handler']},\n";
				$str .= "\t\t\"label\":\"{$c['label']}\"\n";
				$str .= "\t}";
				if($sz-- > 0)
					$str.=",\n";
				$str .="\n";

			}
			$str .= "]\n";
			
			$str .="}";
			return $str;
		}

/*
*  JACK 3: Get activity management area
*/
		private function getActivityManager($args)
		{
			$vars = $this->argVar(array('activity' => 'activity',
							'kmodule' => 'module'), $args);
			$rq = "Activity('{$vars->activity}')<KModule('{$vars->module}');";

			if(!$this->resManager->queryAssoc($rq))
				return 104;

			$route = $this->getManager($vars->module, $vars->activity);
			if($args == null)
				return $this->arrayToString($route);

			return $route;
		}

		private function getManager($module, $activity, $cmpt = null)
		{
			$ridca = null;
			
			// get the component of the activity
			if($activity != null && $cmpt == null) {
				// TODO: Include <Type('Activity') to deal with only activity components
				$rq = "Component()<Activity('{$activity}');";
				$ridca = $this->resManager->queryAssoc($rq);
				if(!$ridca)
					return null;

				$ridca = $ridca[0][0];
			}
			else
			if($activity == null && $cmpt != null) {
				$rq = "Component('$cmpt');";
				$ridca = $this->resManager->queryAssoc($rq);
				if(!$ridca)
					return null;

				$ridca = $ridca[0][0];
			}

			$refca = $this->resManager->getHandlerRef($ridca);

			// now we know the component we can get the manager
			$rq = "Instance()<(Component()>Component($ridca));";
			$ridma = $this->resManager->queryAssoc($rq);
			if(!$ridma)
				return null;
			$ridma = $ridma[0][0];

			$refma = $this->resManager->getHandlerRef($ridma);
			// now we can get the layout for the manager
			$rq = "Layout()<Instance($ridma);";
			$ridl = $this->resManager->queryAssoc($rq);

			if(!$ridl)
				return null;

			$refl = $this->resManager->getHandlerRef($ridl[0][0]);

			// and get the admin area
			$rq = "Area()<KModule('{$module}'):cadmin;";
			$ridaa = $this->resManager->queryAssoc($rq);
			if(!$ridaa)
				return null;

			$refaa = $this->resManager->getHandlerRef($ridaa[0][0]);

			// get the crud panel
			$rq = "Panel()>(Crud()<(Component()>Component($ridca)));";
			$ridp = $this->resManager->queryAssoc($rq);
			if(!$ridp)
				return null;

			$refp = $this->resManager->getHandlerRef($ridp[0][0]);
			return array($refaa,$refl, $refp);
		}

/*
*  JACK 4: Get Activites
*/
		public function getActivities($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module'), $args);
			if($vars->module == null)
				return 104;

			$actls = $this->resManager->queryAssoc("Activity()<KModule('{$vars->module}');");
			if(!$actls)
				return 104;
			include_once(LibLoad::shared('kura', 'module'));
			$modlib = new ModuleLibrary($this->db);
			$details = array();

			foreach($actls as $act) {
				$c = array();
				$id = $this->resManager->getHandlerRef($act[0]);
				$c = $modlib->getActivityTitle($id);
				$c[] = $id;
				$details[] = $c;
			}

			if($args == null)
				return $this->jsonGetActivities($details);

			return $details;
		}

		private function jsonGetActivities($list)
		{
			if(!$list)
				return "{\"code\":104}";

			$uid = Session::get('uid');

			$sz = sizeof($list)-1;
			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";
			
			foreach($list as $c) {
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$c[1]},\n";
				$str .= "\t\t\"title\":\"{$c[0]}\"\n";
				$str .= "\t}";
				if($sz-- > 0)
					$str.=",\n";
				$str .="\n";

			}
			$str .= "]\n";
			
			$str .="}";
			return $str;
		}

/*
*  JACK 5: Get Activity details
*/
		public function getActivityDetails($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'activity' => 'activity'), $args);
			if($vars->module == null || $vars->activity == null)
				if($args == null)
					return $this->jsonError("No module or activity specified");
				else
					return 104;

			$actls = $this->resManager->queryAssoc("Activity('{$vars->activity}')<KModule('{$vars->module}');");
			if(!$actls)
				if($args == null)
					return $this->jsonError("Invalid activity");
				else
					return 104;

			include_once(LibLoad::shared('kura', 'module'));
			$modlib = new ModuleLibrary($this->db);
			$details = $modlib->getActivityDetails($vars->activity);
			$rids = $this->resManager->queryAssoc("Instance()<Activity('{$vars->activity}');");
			$details[] = $vars->activity;
			// set the working component type
			$type = $this->resManager->queryAssoc("Component()<Activity('{$vars->activity}');");
			$type = $type[0][0];
			$typel = $this->resManager->getResourceFromId($type);
			$typel = $typel['label'];
			$details[] = $typel;

			$ridaa = $this->resManager->queryAssoc("Area()>Activity('{$vars->activity}');");
			$actarea = $this->resManager->getHandlerRef($ridaa[0][0]);

			$r = $this->getManager($vars->module, $vars->activity);
			$details[] = $r;

			/*$ridcoord = $this->resManager->queryAssoc("User()<Activity('{$vars->activity}'):coord");
			$coords = array();
			if($ridcoord) {
				foreach($ridcoord as $c) {
					$res = $this->resManager->getResourceFromId($c[0]);
					$coords[] = array($res['label'], $res['id']);
				}

			}
			$details[] = $coords;
			*/
			// get all the instance of the component associated with the activity

			$details[] = $actarea;

			$ridi = $this->resManager->queryAssoc("Instance()<Activity('{$vars->activity}');");
			if($ridi != false) {
				foreach($ridi as &$rid) {
					$res = $this->resManager->getResourceFromId($rid[0]);
					$ridal = $this->resManager->queryAssoc("Layout()<Instance({$rid[0]});");
					$layout = $this->resManager->getHandlerRef($ridal[0][0]);
					$rid = array($res['handler'], $res['label'], $res['id'], $layout);
				}

				$details = array_merge($details, $ridi);
			}
			if($args == null)
				return $this->jsonActivityDetails($details);
			return $details;
		}

		private function jsonActivityDetails($details)
		{
			if(!$details)
				return "{\"code\":104}";

			$sz = sizeof($details);
			$str = "{\"code\":102,\n";
			$str .="\"data\":\n";
			$c = $details;
			$str .= "\t{\n";
			$str .= "\t\t\"id\":{$c[5]},\n";
			$str .= "\t\t\"title\":\"{$c[0]}\",\n";
			$str .= "\t\t\"description\":\"{$c[1]}\",\n";
			$str .= "\t\t\"type\":\"{$c[6]}\",\n";
			$str .= "\t\t\"inclusion\":{$c[2]},\n";
			$str .= "\t\t\"active\":{$c[3]},\n";
			$str .= "\t\t\"route\": \n\t\t{\n";
				if(is_array($c[7])) {
					$str .= "\t\t\t\"area\":{$c[7][0]},\n";
					$str .= "\t\t\t\"layout\":{$c[7][1]},\n";
					$str .= "\t\t\t\"panel\":{$c[7][2]}\n";
				}
			$str .= "\t\t},\n";
			$str .= "\t\t\"area\":{$c[8]}";
			
			if($sz > 8) {
				$str .= ",\n";
				$str .= "\t\t\"instances\": [\n";
				for($i = 9; $i < $sz; $i++) {
						$str .= "\t\t\t\t{\n";
						$str .= "\t\t\t\t\"instance\":{$c[$i][0]},\n";
						$str .= "\t\t\t\t\"label\":\"{$c[$i][1]}\",\n";
						if(isset($c[$i][2]))
							$str .= "\t\t\t\t\"rid\":{$c[$i][2]},\n";
						if(isset($c[$i][3]))
							$str .= "\t\t\t\t\"layout\":{$c[$i][3]}\n";
						$str .= "\t\t\t\t}";
					if($i < $sz-1)
						$str .= ",\n";
				}

				$str .= "\n";
				$str .= "\t\t]";
			}

			$str .= "\n";


			$str .= "\t}";
			$str .="\n";
			
			$str .="}";
			return $str;
		}

/*
*  JACK 14: Get the assocs with the working instance
*/
		public function getAssocs($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'activity' => 'activity',
							'kinst' => 'instance'), $args);

			$mlib = null;
			if($this->mlib == null) {
				include_once(LibLoad::shared('kura', 'module'));
				$this->mlib = $mlib = new ModuleLibrary($this->db);
			}
			else
				$mlib = $this->mlib;


			$inc = $mlib->getActivityInc($vars->activity);
			$type = "";
			switch($inc) {
			case 1:
				$type = "User";
			break;
			case 2:
				$type = "Group";
			break;
			}
			$rids = $this->resManager->queryAssoc("$type()<(Instance('{$vars->instance}')<Activity('{$vars->activity}'));");
			if(!$rids)
				if($args == null)
					return $this->jsonAssocs($vars->activity, $vars->instance, false);
				else
					return 104;

			foreach($rids as &$rid) {
				$tmp = $this->resManager->getResourceFromId($rid[0]);
				$rid[] = $tmp['label'];
			}

			if($args == null)
				return $this->jsonAssocs($vars->activity, $vars->instance, $rids);

			return $rids;
		}

		private function jsonAssocs($aid, $instance, $assocs)
		{
			if(!$assocs) {
				$str = "{\"code\":104,\n";
				$str .="\"data\":\n";
				$str .= "\t{\n";
				$str .= "\t\t\"aid\":{$aid},\n";
				$str .= "\t\t\"instance\":{$instance}\n";
				$str .= "\t}\n}";
				return $str;
			}

			$sz = sizeof($assocs);
			$str = "{\"code\":102,\n";
			$str .="\"data\":\n";
			$str .= "\t{\n";
			$str .= "\t\t\"aid\":{$aid},\n";
			$str .= "\t\t\"instance\":{$instance},\n";
			$str .= "\t\t\"assocs\":\n\t\t[\n";
				for($i = 0; $i < $sz; $i++) {
					$str .= "\t\t\t{\n";
						$str .= "\t\t\t\t\"rid\":{$assocs[$i][0]},\n";
						$str .= "\t\t\t\t\"label\":\"{$assocs[$i][1]}\"\n";
					$str .= "\t\t\t}";

					if($i < $sz-1)
						$str .= ",";
					$str .= "\n";
				}
				$str .= "\t\t]\n";
			
			$str .= "\t}\n}";
			return $str;
		}
/*
*  JACK 6: Get Type Manager of activity
*/
		public function getTypeManager($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'actype' => 'type'), $_POST);
			$route = $this->getManager($vars->module, null, $vars->type);
			$this->addTrackerParam('aca', $route[0]);
			$this->addTrackerParam('acl', $route[1]);
			$this->addTrackerParam('acp', $route[2]);
			if($args == null)
				return $this->arrayToString($route);

			return $route;
		}

/*
*  JACK 7: Get Users in Module
*/
		public function getUsers($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module'), $_POST);
			$rids = $this->resManager->queryAssoc("User()<KModule('{$vars->module}');");
			if(!$rids)
				return 104;

			foreach($rids as &$rid) {
				$det = $this->resManager->getResourceFromId($rid[0]);
				$rid[] = $det['label'];
				$rid[] = $det['handler'];
			}

			if($args == null)
				return $this->arrayToString($rids);

			return $rids;
		}

/*
*  JACK 8: Get User details
*/
		public function getUserDetails($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
						'kuser' => 'user'), $args);

			// check if user is in module
			if(!$this->resManager->queryAssoc("User('{$vars->user}')<KModule('{$vars->module}');"))
				return 104;
			$ridp = $this->resManager->queryAssoc("Profile()<User('{$vars->user}');");
			if(!$ridp)
				return 104;

			$pid = $this->resManager->getHandlerRef($ridp[0][0]);
			include_once(LibLoad::shared('kura', 'profile'));
			$plib = new profileLibrary($this->db);
			$details = $plib->getProfile($pid);
			$details = $details[0];
			$details[] = $vars->user;

			if(!$this->resManager->queryAssoc("User('{$vars->user}')<KModule('{$vars->module}'):cadmin;"))
				$details[] = 0;
			else
				$details[] = 1;

			if($args == null)
				return $this->arrayToString($details);

			return $details;
		}

/*
*  JACK 9: Get add a group 
*/
		public function addGroup($args)
		{
			$args = $_POST;
			$vars = $this->argVar(array('kmodule' => 'module',
						'kgname' => 'name'), $args);
			$vars->module = $_GET['kmodule'];
			$ridm = $this->resManager->queryAssoc("KModule('{$vars->module}');");
			if(!$ridm)
				return 104;

			$ridm = $ridm[0][0];
			$label = $this->resManager->getResourceFromId($ridm);
			if(!$label)
				return 104;
			$label = $label['label'] ."g_".str_replace(" ", "_", strtolower($vars->name));
			$ridg = $this->resManager->addResource('Group', 0, $label);
			$this->resManager->createRelationship($ridm, $ridg);
			return 102;
		}

/*
*  JACK 10: get groups
*/
		public function getGroups($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module'), $args);

			$rids = $this->resManager->queryAssoc("Group()<KModule('{$vars->module}');");
			if(!$rids)
				return 104;

			foreach($rids as &$rid) {
				$tmp = $this->resManager->getResourceFromId($rid[0]);
				$rid[] = $tmp['label'];
			}

			if($args == null)
				return $this->arrayToString($rids);

			return $rids;
		}
/*
*  JACK 11: Get group details
*/
		public function getGroupDetails($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'kgroup' => 'group'), $args);

			$rids = $this->resManager->queryAssoc("User()<Group({$vars->group});");
			if(!$rids)
				return 104;

			foreach($rids as &$rid) {
				$res = $this->resManager->getResourceFromId($rid[0]);
				$rid = array($res['handler'], $res['label']);
			}

			if($args == null)
				return $this->arrayToString($rids);

			return $rids;
		}

/*
*  JACK 12: Add user to group
*/
		public function addGroupUser($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'kgroup' => 'group',
							'kuser' => 'user'), $args);
			// is user in module
			$rids = $this->resManager->queryAssoc("User('{$vars->user}')<KModule('{$vars->module}');");
			if(!$rids)
				return 104;

			$ridu = $rids[0][0];

			// is group in module
			$rids = $this->resManager->queryAssoc("Group({$vars->group})<KModule('{$vars->module}');");
			if(!$rids)
				return 104;
			
			// is user already in group
			$rids = $this->resManager->queryAssoc("User('{$vars->user}')<Group({$vars->group});");
			if($rids)
				return 102;

			$this->resManager->createRelationship($vars->group, $ridu);
			return 102;
		}

/*
*  JACK 13: Rem user from group
*/
		public function remGroupUser($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'kgroup' => 'group',
							'kuser' => 'user'), $args);
			// is user in module
			$rids = $this->resManager->queryAssoc("User('{$vars->user}')<KModule('{$vars->module}');");
			if(!$rids)
				return 104;

			$ridu = $rids[0][0];

			// is group in module
			$rids = $this->resManager->queryAssoc("Group({$vars->group})<KModule('{$vars->module}');");
			if(!$rids)
				return 104;
			
			// is in group
			$rids = $this->resManager->queryAssoc("User('{$vars->user}')<Group({$vars->group});");
			if(!$rids)
				return 104;

			$this->resManager->removeRelationship($vars->group, $ridu);
			return 102;
		}

/*
*  JACK 15: Disassociate a resource
*/
		private function disassocResource($args)
		{
			$vars = $this->argVar(array('kmodule' => 'module',
							'aid' => 'aid',
							'bid' => 'bid'), $args);
			$atype = $this->resManager->getTypeFromId($vars->aid);
			$btype = $this->resManager->getTypeFromId($vars->bid);
			$rel = ResRel::getRelationship($atype['base'], $btype['base']);
			if($atype['base'] == $rel['parent'])
				$this->resManager->removeRelationship($vars->aid, $vars->bid);
			else
				$this->resManager->removeRelationship($vars->bid, $vars->aid);
			return 102;
		}

		private function updateState($args)
		{
			$vars = $this->argVar(array('aid' => 'aid',
						'amacti' => 'state'), $_POST);
			$outcome = $this->arrayUpdate('kura_activities', array(
								'active' => $vars->state
								), "id='{$vars->aid}'");

			if($args == null)
				return $this->jsonState($outcome);
		}

		private function jsonState($outcome) 
		{
			if(!$outcome)
				return "{\"code\":104}";

			return "{\"code\":102}";
		}

/*
*  JACK 100: Load resource into rbin
*/
		public function addResource($args)
		{
			include_once(LibLoad::shared('vpatch', 'rbin'));
			$rlib = new rbinLibrary($this->db);
			if(!isset($_GET['krid']))
				return 104;

			$rlib->addResource($_GET['krid']);
			return 102;
		}

/*
*  JACK 101: Pull resources from rbin and assoc
*/
		public function dropResources($args)
		{
			include_once(LibLoad::shared('vpatch', 'rbin'));
			$vars = $this->argVar(array('kmodule' => 'module',
							'kid' => 'id'), $args);

			$rlib = new rbinLibrary($this->db);

			$bin = $rlib->getBin();
			if($bin == null)
				return 104;

			$res = $this->resManager->getResourceFromId($vars->id);
			foreach($bin as $k => $r) {
				if($r[2] == 0)
					continue;

				$this->resManager->createRelationship($vars->id, $k);
				$rlib->removeResource($k);
			}

			return 102;
		}
	}



?>
