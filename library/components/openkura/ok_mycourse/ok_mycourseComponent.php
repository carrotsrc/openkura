<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_mycourseComponent extends Component
	{
		private $resManager;
		private $modLib;

		public function initialize()
		{
			include(LibLoad::shared('kura','module'));
			$this->resManager = Managers::ResourceManager();
			$this->modLib = new ModuleLibrary($this->db);
		}

		public function createInstance($params = null)
		{
			$this->resManager = Managers::ResourceManager();
			$rids = $this->resManager->queryAssoc("Instance()<Component('ok_mycourse');");
			if(!$rids)
				return 1;

			return sizeof($rids)+1;			
		}

		public function run($channel = null, $args = null)
		{
			$response = null;
			
			switch($channel) {
			case 1: // request course details
				$response = $this->requestCourses($args);
				
			break;

			case 2:
				$response = $this->requestActivities($args);
			break;
			}

			if($args == null)
				echo $response;

			return $response;
		}

		private function requestCourses($args = null)
		{
			// user is not a guest
			// test checking against an rid
			$uid = Session::get('uid');
			$rids = $this->resManager->queryAssoc("KModule()>User('$uid');");
			if(!$rids)
				return 201;

			$this->modLib->getRidModuleDetails($rids);
			$this->getStudentArea($rids);
			if($args == null)
				return $this->arrayToString($rids);
			return $rids;
			// find out if they are admin to any of these courses?
		}

		private function getStudentArea(array &$rids)
		{
			foreach($rids as &$rid) {
				$rq = "Area()<KModule({$rid[0]}):cstudent;";
				$area = $this->resManager->queryAssoc($rq);
				$res = $this->resManager->getResourceFromId($area[0][0]);
				$rid[] = $res['label'];
			}
		}

		private function checkAdmin(array &$rids)
		{
			$uid = Session::get('uid');
			foreach($rids as &$rid) {
				if(!$this->resManager->queryAssoc("User('{$uid}')<(Instance()<Role('admin')<KModule({$rid[0]})'"))
					continue;

				$rid[1] = true;
			}
		}
/*
*  JACK 2: Get activities
*/
		private function requestActivities(&$args)
		{
			$uid = Session::get('uid');
			if($uid == null)
				$uid = 0;

			$vars = $this->argVar( array("kmodule" => 'module'), $args);
			$res = null;
			if($vars->module == null)
				$res = $this->resManager->queryAssoc("Activity()<(KModule()>User('$uid'));");
			else
				$res = $this->resManager->queryAssoc("Activity()<(KModule('{$vars->module}')>User('$uid'));");

			if(!$res)
				return 104;


			foreach($res as $k => &$rid) {
				$ref = $this->resManager->getResourceFromId($rid[0]);
				if(!$ref)
					continue;

				$result = $this->modLib->getActivityDetails($ref['handler'], false);
				if(!$result) {

					unset($res[$k]);
					continue;
				}

				$result[] = $this->getActivityAreas($rid[0]);
				$result[] = $this->modLib->getActivityCountdown($result[3]);
				if(!$result)
					continue;

				$rid = $result;
			}



			if($args == null)
				return $this->arrayToString($res);

			return $res;
		}

		private function getActivityAreas($rid)
		{
				$res = $this->resManager->queryAssoc("Area()>Activity($rid);");


				if(!$res)
					return 0;
					$res = $this->resManager->getHandlerRef($res[0][0]);
				return intval($res);
		}
	}
?>
