<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_articlemanComponent extends Component
	{
		private $resManager;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
		}

		public function createInstance($params = null)
		{
			$r = Managers::ResourceManager();
			$rids = $r->queryAssoc("Instance()<Component('ok_articleman');");
			if(!$rids)
				return 1;

			return sizeof($rids)+1;

		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getArticleStyles($args);
			break;

			case 2:
				$response = $this->saveArticle($args);
			break;

			case 3:
				$response = $this->updateArticle($args);
			break;

			case 10:
				$response = $this->getArticle($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getArticleStyles($args)
		{
			$fm = Koda::getFileManager();
			$path = SystemConfig::relativeAppPath("library/panel/openkura/ok_articleview_ui/template");
			$styles = $fm->listDirectories($path);

			return $styles;
		}

		private function saveArticle($args)
		{
			$vars = $this->argVar(array(
						'artitle' => 'title',
						'arcontent' => 'content',
						'arstyle' => 'style'), $_POST);

			$ridc = $this->resManager->queryAssoc("Component('ok_articleview');");
			if(!$ridc)
				return false;

			$ridc = $ridc[0][0];

			include_once(LibLoad::shared('kura', 'act_article'));
			$arlib = new act_articleLibrary($this->db);
			$ref = $arlib->addArticle($vars->title, $vars->content, $vars->style);
			if(!$ref)
				return 104;

			$this->addTrackerParam('ref', $ref);


			$ridi = $this->resManager->addResource("Instance", $ref, "act_arview_$ref");
			$this->resManager->createRelationship($ridc, $ridi);

			$this->setRio(RIO_INS, $ridi);
			return 102;
		}

		private function updateArticle($args)
		{
			$vars = $this->argVar(array(
						'arref' => 'id',
						'artitle' => 'title',
						'arcontent' => 'content',
						'arstyle' => 'style'), $_POST);

			include_once(LibLoad::shared('kura', 'act_article'));
			$arlib = new act_articleLibrary($this->db);
			$ref = $arlib->updateArticle($vars->id, $vars->title, $vars->content, $vars->style);
		}

		private function getArticle($args)
		{
			$vars = $this->argVar(array(
						'arref' => 'id',), $args);


			include_once(LibLoad::shared('kura', 'act_article'));
			$arlib = new act_articleLibrary($this->db);

			$article = $arlib->getArticle($vars->id);
			if(!$article)
				return false;

			return $article[0];
		}

	}
?>
