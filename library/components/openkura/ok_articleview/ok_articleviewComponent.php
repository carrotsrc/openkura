<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_articleviewComponent extends Component
	{	
		private $clib;
		private $plib;
		private $resManager;
		public function initialize()
		{
			$this->clib = null;
			$this->plib = null;
			$this->resManager = Managers::ResourceManager();
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {

			case 1:
				$response = $this->getArticle($args);
			break;

			case 10:
				$response = $this->getParentComments($args);
			break;

			case 11:
				$response = $this->addComment($args);
			break;

			case 12:
				$response = $this->getChildren($args);
			break;

			}

			if($args == null)
				echo $response;

			 return $response;
		}

		public function getArticle($args)
		{
			include_once(LibLoad::shared('kura', 'act_article'));
			$arlib = new act_articleLibrary($this->db);
			$article = $arlib->getArticle($this->instanceId);
			if(!$article)
				return false;
			return $article[0];
		}

		private function loadCommentsLibrary()
		{
			if($this->clib != null)
				return;

			include_once(LibLoad::shared('kura', 'comments'));

			$this->clib = new commentsLibrary($this->db);

		}

		public function getParentComments($args)
		{
			$this->loadCommentsLibrary();
			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('ok_articleview');");
			$ridi = $ridi[0][0];
			$rq = "Comment()<Instance($ridi);";
			$rids = $this->resManager->queryAssoc($rq);
			if(!$rids) {
				if($args == null)
					return $this->errorToJson("");
				return 104;

			}
			foreach($rids as &$rid) {
				$rid[] = $this->resManager->getHandlerRef($rid[0]);
			}


			$parents = $this->clib->getlsParents($rids, 1);
			$this->addProfileDetails($parents, 4);
	
			if($args == null) {
				return $this->parentToJson($parents);
			}

			return $parents;
		}

		private function errorToJson($content)
		{
			$str = "{\"code\":104,\n";
			$str .= "\"data\":\"$content\"}";

			return $str;
		}

		private function parentToJson($children)
		{
			if(!$children)
				return "{\"code\":104}";

			$sz = sizeof($children)-1;
			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";
			
			foreach($children as $child) {
				$contents = $child[2];
				$contents = $this->clib->outboundJsString($contents);
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$child[0]},\n";
				$str .= "\t\t\"root\":0,\n";
				$str .= "\t\t\"parentid\":0,\n";
				$str .= "\t\t\"contents\":\"{$contents}\",\n";
				$str .= "\t\t\"posted\":\"{$child[3]}\",\n";
				$str .= "\t\t\"username\":\"{$child[5]}\",\n";
				$str .= "\t\t\"avatar\":\"{$child[6]}\",\n";
				$str .= "\t\t\"uid\":{$child[4]}\n";
				$str .= "\t}";
				if($sz-- > 0)
					$str.=",\n";

				$str .="\n";

			}
			$str .= "]";

			if($this->plib != null) {
				$str .= ",\n\"plink\":\"".$this->plib->profileArea(null)."\"";
			}
			
			$str .="\n}";
			return $str;
		}

		private function loadProfileLibrary()
		{
			if($this->plib != null)
				return;

			include_once(LibLoad::shared('kura', 'profile'));
			$this->plib = new profileLibrary($this->db);
		}

		public function addProfileDetails(&$list, $col)
		{
			$this->loadProfileLibrary();
			$mpath = $this->plib->mediaPath();
			// this is horrible
			$ids = array();
			foreach($list as &$p) {
				if($p[$col] == 0) {
					$p[] = "Anon";
					$p[] = "$mpath/anon.png";
					continue;
				}

				if(isset($ids[$p[$col]])) {
					$p[] = $ids[$p[$col]][0];
					$p[] = $ids[$p[$col]][1];
					continue;
				}

				$profile = $this->resManager->queryAssoc("Profile()<User('{$p[$col]}');");
				if(!$profile)
					continue;
				$ref = $this->resManager->getHandlerRef($profile[0][0]);
				$details = $this->plib->getProfile($ref);
				$details=$details[0];
				$p[] = $details[1];
				$p[] = "$mpath/{$details[4]}";
				$ids[$p[$col]] = array($details[1], "$mpath/{$details[4]}");
			}
		}

		public function addComment($args)
		{
			$vars = $this->argVar(array(
						'artc' => 'content',
						'artr' => 'root',
						'artp' => 'parent'), $_POST);
			$this->loadCommentsLibrary();

			$sz = strlen($vars->content);
			$title = date('y\/m\/d') . " ";
			if($sz > 119)
				$title .= substr($vars->content, 0, 119);
			else
				$title .= $vars->content;
			$owner = Session::get('uid');
			if($owner == null)
				$owner = 0; // Anonymous owner

			// add comment
			$ref = $this->clib->addComment($title, $vars->content,
						$vars->parent, $vars->root, $owner);

			$title = strtolower(str_replace(" ", "_", $title));
			$rid = $this->resManager->addResource('Comment', $ref, substr($title, 0, 64)); 

			// assoc with instance
			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('ok_articleview');");
			$this->resManager->createRelationship($ridi[0][0], $rid, $this->resManager->getEdge('art_comment'));

			$ridu = $this->resManager->queryAssoc("User('$owner');");

			if($ridu != false)
				$this->resManager->createRelationship($ridu[0][0], $rid, $this->resManager->getEdge('speaker'));

			$this->setRio(RIO_INS, $rid[0][0]);

			if($args == null)
				return $this->commentIdtoJson($vars->root);
		}

		private function commentIdToJson($commentId)
		{
			$str = "{\"code\":102,\n";
			$str .= "\"data\":\"$commentId\"}";

			return $str;
		}

		public function getChildren($args)
		{
			$vars = $this->argVar(array(
						'artr' => 'roots',
						), $args);

			$this->loadCommentsLibrary();
			if($vars->roots == 0) {
				$comments = $this->getParentComments(101);
				if($args == null)
					return $this->parentToJson($comments);
			}

	
			$comments = $this->clib->getChildren(explode(";",$vars->roots));
			if(!$comments) {
				if($args == null)
					return $this->errorToJson($vars->roots);

				return 104;
			}

			$this->addProfileDetails($comments, 6);

			if($args == null)
				return $this->childrenToJson($comments);

			return $comments;
		}

		private function childrenToJson($children)
		{
			if(!$children)
				return $this->errorToJson("No Children");

			$sz = sizeof($children)-1;
			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";
			
			foreach($children as $child) {
				$contents = $child[4];
				$contents = $this->clib->outboundJsString($contents);
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$child[0]},\n";
				$str .= "\t\t\"root\":{$child[1]},\n";
				$str .= "\t\t\"parentid\":{$child[2]},\n";
				$str .= "\t\t\"contents\":\"{$contents}\",\n";
				$str .= "\t\t\"posted\":\"{$child[5]}\",\n";
				$str .= "\t\t\"username\":\"{$child[7]}\",\n";
				$str .= "\t\t\"avatar\":\"{$child[8]}\",\n";
				$str .= "\t\t\"uid\":{$child[6]}\n";
				$str .= "\t}";
				if($sz-- > 0)
					$str.=",\n";

				$str .="\n";

			}
			$str .= "]";

			if($this->plib != null) {
				$str .= ",\n\"plink\":\"".$this->plib->profileArea(null)."\"";
			}
			
			$str .="\n}";
			return $str;
		}
	}
?>
