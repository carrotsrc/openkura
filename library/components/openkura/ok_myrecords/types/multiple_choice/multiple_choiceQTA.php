<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class multiple_choiceQTA extends QTypeActivity
	{
		protected $session = array();
		protected $tagStack = array();

		public function getSession()
		{
			return $this->session;
		}

		public function printSession()
		{
		print_r($this->session);
		}

		public function loadSection($tag, $properties, $section)
		{
			switch($tag) {
			case 'question':
				array_push($this->tagStack, 1);
				$this->session[$section] = array();
			break;

			case '/question':
				if(end($this->tagStack) == 1)
					array_pop($this->tagStack);
				else
					return false;
			break;

			case 'choice':
				array_push($this->tagStack, 2);
				if(sizeof($this->session[$section]) == 0)
					$this->session[$section][0] = "";

				$this->session[$section][]= array();
				$cor = 0;
				foreach($properties as $p => $v)
					if($p == "correct")
						$cor = $v;

				$sz = sizeof($this->session[$section]) - 1;
				$this->session[$section][$sz][1]= $cor;
			break;

			case '/choice':
				if(end($this->tagStack) == 2)
					array_pop($this->tagStack);
				else
					return false;
			break;

			case '_text_':
				$cur = end($this->tagStack);
				switch($cur) {
				case 1:
					$this->session[$section][0] = $properties['contents'];
				break;
				case 2:
					$sz = sizeof($this->session[$section]) - 1;
					$this->session[$section][$sz][0]= $properties['contents'];

				break;
				}
			break;
			}
		}

		public function generateRecord($id)
		{
			if(!isset($_POST["qmc$id"])) {
				echo "\t<mcsel id=\"0\" />\n";
				return;
			}
			$val = $_POST["qmc$id"];

			echo "\t<mcsel id=\"$val\" />\n";
		}
	}
?>
