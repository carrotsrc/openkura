<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	/*include("qta_prop.php");
	include("qtypeactivity.php");*/
	include_once(SystemConfig::relativeAppPath("system/helpers/vpxml.php"));
	include_once(LibLoad::shared('kura/records', 'rectemplate'));
	class ok_myrecordsComponent extends Component
	{
		private $resManager;
		private $rtlib;

		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			$this->rtlib = null;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getRecords();
			break;

			case 2:
				$response = $this->viewRecord($args);
			break;

			case 3:
				$response = $this->getScheme();
			break;

			case 4:
				$response = $this->getTemplateSession();
			break;

			case 5:
				$response = $this->getHeader();
			break;

			}

			if($args == null)
				echo $response;

			return $response;
		}

		private function loadActObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTA.php");
			$str = "{$type}QTA";
			$obj = new $str();
			$this->types[$type] = $obj;
			return $obj;
		}

		private function loadRecObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTR.php");
			$str = "{$type}QTR";
			$obj = new $str();
			$this->types[$type] = $obj;
			return $obj;
		}
		
		public function getRecords()
		{
			$uid = Session::get('uid');
			if($uid == null)
				return 104;

			$ridr = $this->resManager->queryAssoc("Record()<User('$uid');");
			if(!$ridr)
				return 104;
			$refs = array();
			$det = array();
			foreach($ridr as $r) {
				$ref = $this->resManager->getHandlerRef($r[0]);
				$rida = $this->resManager->queryAssoc("Activity()>Record('$ref');");
				$rida = $rida[0][0];
				$ares = $this->resManager->getResourceFromId($rida);
				$ares = $ares['label'];

				$ridm = $this->resManager->queryAssoc("KModule()>Activity($rida);");
				$ridm = $ridm[0][0];

				$mres = $this->resManager->getResourceFromId($ridm);
				$mres = $mres['label'];
				$refs[] = array($ref, $ares, $mres);
			}

			include_once(LibLoad::shared('kura/records', 'record'));
			$rlib = new recordLibrary($this->db);
			$recs = $rlib->getRecords($refs, 0);
			foreach($recs as $rec) {
				foreach($refs as $k => &$ref) {
					if($ref[0] == $rec[0]) {
						$ref[] = $rec[0];
						$ref[] = $rec[1];
						$ref[] = $rec[2];
						$ref[] = $rec[3];
						$id = $k;
						break;
					}
				}
			}
			return $refs;
		}

		public function viewRecord($args)
		{
			$vars = $this->argVar(array(
						'recid' => 'id'
						));

			include_once(LibLoad::shared('kura/records', 'record'));
			$rt = $this->resManager->queryAssoc("RecTemplate()>Record('{$vars->id}');");
			if(!$rt)
				return 104;
			$rt = $this->resManager->getHandlerRef($rt[0][0]);
			$this->loadRtlib($rt);

			$rlib = new recordLibrary($this->db);
			$rlib->setScheme($this->rtlib->getScheme());
			$rlib->loadRecord($vars->id);
			return $this->cycleRecords($this->rtlib, $rlib);
		}

		private function cycleRecords($rtlib, $rlib)
		{
			$scheme = $rtlib->getScheme();
			$sessions = $rtlib->getTypeSessions(true, 1);
			$ses = array();
			foreach($scheme as $k => $s)
				$rlib->processResult($k, $sessions[$s]);

			return $rlib->getTypeSessions();
		}

		private function getScheme()
		{
			$vars = $this->argVar(array(
						'recid' => 'id'
						));

			$rt = $this->resManager->queryAssoc("RecTemplate()>Record('{$vars->id}');");
			if(!$rt)
				return 104;
			$rt = $this->resManager->getHandlerRef($rt[0][0]);
			$this->loadRtlib($rt);

			return $this->rtlib->getScheme();
		}

		private function getTemplateSession()
		{
			$vars = $this->argVar(array(
						'recid' => 'id'
						));

			$rt = $this->resManager->queryAssoc("RecTemplate()>Record('{$vars->id}');");
			if(!$rt)
				return 104;
			$rt = $this->resManager->getHandlerRef($rt[0][0]);
			$this->loadRtlib($rt);
			$this->loadRtlib($vars->id);

			return $this->rtlib->getTypeSessions(true, 1);
		}

		private function getHeader()
		{	
			$vars = $this->argVar(array(
						'recid' => 'id'
						));

			$rt = $this->resManager->queryAssoc("RecTemplate()>Record('{$vars->id}');");
			if(!$rt)
				return 104;
			$rt = $this->resManager->getHandlerRef($rt[0][0]);
			$this->loadRtlib($rt);
			return $this->rtlib->getTemplateHeader();
		}

		private function loadRtlib($id)
		{
			if($this->rtlib == null) {
				$this->rtlib = new rectemplateLibrary($this->db, 'qm');
				$this->rtlib->loadTemplate($id, 1);
			}
		}
	}
?>
