<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 
	class ok_bulletinComponent extends Component
	{
		private $title;
		private $alib;
		private $resman;
		public function initialize()
		{
			$r = $this->db->sendQuery("SELECT * FROM bulletin WHERE id='{$this->instanceId}';", false, false);
			if(!$r)
				return;

			$this->resman = Managers::ResourceManager();
			$this->title = $r[0][1];
			include_once(LibLoad::shared('kura', 'attachment'));
			$this->alib = new attachmentLibrary($this->db);


		}

		public function createInstance($params = null)
		{
			if(!$this->maintainReady())
				return null;

			if(!isset($params['title']))
				return null;

			if(!$this->arrayInsert('bulletin', array('title' => $params['title'])))
				return null;
			
			return $this->db->getLastId();
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel)
			{
			case 1:
				$response = $this->getHeader();
			break;

			case 2:
				$response = $this->getPosts($args);
			break;

			case 3:
				$response = $this->addBulletin($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getHeader()
		{
			return $this->title;
		}

		// generate an attachment from a URL
		private function genAttachment($url)
		{
			include_once(LibLoad::shared('vpatch', 'htmlparse'));
			$hlib = new htmlparseLibrary();
			if(substr($url, 0, 7) != "http://")
				$url = "http://$url";
			$xml = file_get_contents($url, false, null, -1, 3072);
			$hlib->init($xml);
			$title = "";
			if($hlib->findElement('title') != null)
				if(($tag = $hlib->findElement('_text_')) != null)
					$title = $tag->attributes['content'];

			$attachment = $this->alib->generateAttachment($url);
			if($title == "")
				$title = $url;

			$attachment['title'] = $title;
			return $attachment;
		}

		private function addBulletin($args = null)
		{
			include_once(LibLoad::shared('kura', 'bulletin'));
			$blib = new bulletinLibrary($this->db, $this->instanceId);

			$vars = $this->argVar(array( 'title' => 'title', 'anno' => 'anno'), $_POST);


			$urls = array();
			$cword = "";
			$anno = "";
			$inurl = false;

			/*
			*  extract the URLs from the body of text
			*  so we can generate attachments
			*/
			while(isset($vars->anno[$i])) {
				if($vars->anno[$i] == " " || $vars->anno[$i] == "\n") {
					if($inurl) {
						$inurl = false;
						$urls[] = $cword;
						$cword = "";
						continue;
					}
					else {
						// not in a URL add to message body
						$anno .=  "$cword".$vars->anno[$i];
						$cword = "";
					}
				}
				else
					$cword .= $vars->anno[$i];

				
				if(!$inurl) {
					$chk = strtolower($cword);
					if(preg_match("/(http\:)|(www\.)|(https\:)/", $chk)) {
						// entered a URL
						trim($cword);
						$inurl = true;
					}
				}
				$i++;
			}

			if($cword != "") {
				if($inurl)
					$urls[] = $cword;
				else
					$anno .= $cword;
			}

			// now we have the annotation. We need to create a bunch of attachments
			foreach($urls as &$url)
				$url = $this->genAttachment($url);


			$uid = Session::get('uid');
			if($uid == null)
				$uid = 0;

			$ref = $blib->addBulletin($vars->title, $anno, $uid);
			$rtitle = "blt_$ref".str_replace(" ", "_", $vars->title);
			$prid = $this->resman->addResource('Bulletin', $ref, $rtitle);

			foreach($urls as &$url) {
				$ref = $this->alib->addAttachment($url['type'], $url['title'], $url['url']);
				if(!$ref)
					continue;

				$rtitle = "att_$ref";
				$rid = $this->resman->addResource($url['name'], $ref, $rtitle);
				$this->resman->createRelationship($prid, $rid);
			}

			if(isset($_FILES['bbul']) && $_FILES['bbul']['size'] > 0) {
				$this->uploadAttachment($prid);
			}

			$this->addTrackerParam('bret', '102');
			$this->setRio(RIO_INS, $prid);

			return 102;
		}

		private function uploadAttachment($prid)
		{
			$path = $this->alib->handleUpload($_FILES['bbul']);
			$bid = null;

			if($path == null)
				return 104;

			$t = $this->alib->getType($path);
			$f = explode("/", $path);
			$f = $f[sizeof($f)-1];
			$path = explode("library", $path);
			$path = "library" . $path[sizeof($path)-1];

			$ref = $this->alib->addAttachment($t[0], $f, $path);
			$rid = $this->resman->addResource($t[2], $ref, $f);
			$this->resman->createRelationship($prid, $rid);

			return $rid;
		}

		private function getPosts($args)
		{
			$posts = $this->db->sendQuery("SELECT * FROM bulletin_post WHERE instance='{$this->instanceId}' ORDER BY posted DESC;", false, false);
			if(!$posts)
				return 104;

			include_once(LibLoad::shared('kura', 'profile'));
			$plib = new profileLibrary($this->db, $this->instanceId);
			$users = array();
			$link = $plib->profileArea(null);


			foreach($posts as &$post) {
				$fname = "";

				if($post[5] == 0)
					$fname = "Anonymous";
				else {
					if(!isset($users[$post[5]])) {
						$ridp = $this->resman->queryAssoc("Profile()<User('{$post[5]}');");
						$ref = $this->resman->getHandlerRef($ridp[0][0]);
						$t = $plib->getProfile($ref);
						$users[$post[5]] = $t[0][1];
						$fname = $t[0][1];
					}
					else
						$fname = $users[$post[5]];
				}
				$post[] = $fname;
				$post[] = $link.$post[5];


				// get any attachments
				$attachments = array();
				$rids = $this->resman->queryAssoc("[Attachment]<Bulletin('{$post[0]}');");
				if(!$rids)
					continue;

				foreach($rids as $rid) {
					$att = $this->alib->getAttachment($this->resman->getHandlerRef($rid[0]));
					if($att == false)
						continue;
					if($att[0][1] == 0)
						$att[0][4] = "External";
					$attachments[] = $att[0];
				}

				$post[] = $attachments;
				$rids = null;
			}

			$users = null;
			if($args == null)
				return $this->postsToJson($posts);

			return $posts;
		}

		private function postsToJson($posts)
		{
			if(!$posts)
				return "{\"code\":104}";

			$sz = sizeof($posts)-1;
			$str = "{\"code\":102,\n";
			$str .= "\"data\":\n{\n"; 
			$str .= "\"posts\": "; 
			$str .= "[\n";
			$path = SystemConfig::relativeLibPath("/media/kura/bulletin/embed");

			foreach($posts as $post) {
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$post[0]},\n";
				$str .= "\t\t\"inst\":{$post[1]},\n";
				
				$contents = $post[2];
				$contents = $this->outboundJsString($contents);
				$str .= "\t\t\"title\":\"{$contents}\",\n";

				$contents = $post[3];
				$contents = $this->outboundJsString($contents);
				$str .= "\t\t\"contents\":\"{$contents}\",\n";
				$str .= "\t\t\"posted\":\"{$post[4]}\",\n";
				$str .= "\t\t\"uid\":{$post[5]},\n";
				$str .= "\t\t\"username\":\"{$post[6]}\",\n";
				$str .= "\t\t\"profile\":\"{$post[7]}\"";


				if(isset($post[8])) {
					
					$str .= ",\n\t\t\"attachments\": [\n";
					$sy = sizeof($post[8])-1;
					foreach($post[8] as $att) {
						$str .= "\t\t\t{\n";
						$str .= "\t\t\t\t\"id\":{$att[0]},\n";
						$str .= "\t\t\t\t\"type\":{$att[1]},\n";

						$contents = $att[2];
						$contents = $this->outboundJsString($contents);
						$str .= "\t\t\t\t\"title\":\"{$contents}\",\n";

						$contents = $att[3];
						$contents = $this->outboundJsString($contents);
						$str .= "\t\t\t\t\"url\":\"{$contents}\",\n";

						$str .= "\t\t\t\t\"rtype\":\"{$att[4]}\",\n\t\t\t\t";
						if(file_exists("$path/{$att[4]}.js"))
							$str .= "\"embed\":1\n";
						else
							$str .= "\"embed\":0\n";

						$str .= "\t\t\t}";

						if($sy-- > 0)
							$str.=",\n";
					}
				
					$str .= "\n\t\t]";
					
				}

				$str .= "\n\t}";
				if($sz-- > 0)
					$str.=",\n";

				$str .="\n";

			}
			$str .= "],\n";
			$path = $this->outboundJsString(SystemConfig::relativeLibPath("/media/kura/bulletin"));
			$str .= "\"media\":\"".$path."\"";
			$str .="}\n}";
			return $str;
		}

		private function outboundJsString($str)
		{
			$problems = array("\n", "&lt;", "&gt;", "/", "\b", "\f", "\r", "\t", "\u", "\\'");
			$solutions = array("\\n", "<",">", "\\/", "\\b", "\\f", "\\r", "\\t", "\\u", "'");


/*
			This code is dodgy when using database entries that were migrated
			from a server with magic_quotes turned on to a server with them off

			switched code off for now
			if(!get_magic_quotes_gpc()) {
				$problems[] = "\"";
				$solutions[] = "\\\"";

			}
*/				
			return str_replace($problems, $solutions, $str);

		}
	}

?>
