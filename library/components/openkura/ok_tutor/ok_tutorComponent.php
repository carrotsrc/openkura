<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_tutorComponent extends Component
	{
		private $resManager;
		private $plib;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			$this->plib = null;
		}

		public function createInstance($params = null)
		{
			$this->resManager = Managers::ResourceManager();
			$rids = $this->resManager->queryAssoc("Instance()<Component('ok_tutor');");
			if(!$rids)
				return 1;

			return sizeof($rids)+1;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getTickets($args);
			break;

			case 2:
				$response = $this->getUserGroups($args);
			break;

			case 3:
				$response = $this->getDialogue($args);
			break;

			case 10:
				$response = $this->postMessage($args);
			break;

			case 11:
				$response = $this->postDialogue($args);
			break;

			case 20:
				$response = $this->changeState($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getTickets($args)
		{
			$vars = $this->argVar(array(
						'fstatus' => 'status',
						'fpriority' => 'priority',
						'fgroups' => 'groups',
						'fpublic' => 'public'));

			$uid = Session::get('uid');
			$xs = $xg = $xp = null;

			$groups = $this->organiseUserGroups($uid, $vars->groups);
			if($vars->status != null)
				$xs = explode(";", $vars->status);

			if($vars->priority != null)
				$xp = explode(";", $vars->priority);

			if($vars->public == null)
				$vars->public = 0;

			if(!$groups)
				return 104;
			include(LibLoad::shared('kura', 'tutortickets'));
			$tlib = new tutorticketsLibrary($this->db);
			if($tlib == null)
				return 104;

			$posts = $tlib->getTickets($uid, $vars->public, $groups, $xp, $xs);

			if($args == null)
				return $this->jsonPosts($posts);

			return $posts;
		}

		private function getDialogue($args)
		{
			$vars = $this->argVar(array(
						'tdti' => 'tid'
						));

			include(LibLoad::shared('kura', 'tutortickets'));
			$tlib = new tutorticketsLibrary($this->db);
			if($tlib == null)
				return 104;

			$uid = Session::get('uid');
			$ticket = $tlib->getTicket($vars->tid);
			if($ticket == null) {
				if($args == null)
					return $this->jsonError("Could not find ticket");

				return 104;
			}

			$this->addProfileDetails($ticket, 2);

			// the ticket owned by the user?
			if($uid == $ticket[0][2]) {
				// no need to check anything else
				$dialogue = $tlib->getDialogue($vars->tid);
				$this->addProfileDetails($dialogue, 2);
				if($args == null)
					return $this->jsonDialogue($ticket, $dialogue, true);

				return $dialogue;
			}

			// is user in the ticket's group?
			$inGroup = true;
			$tutor = false;
			if(!$this->resManager->queryAssoc("User('$uid')<Group({$ticket[0][1]}):tutee;")) {
				if(!$this->resManager->queryAssoc("User('$uid')<Group({$ticket[0][1]}):tutor;"))
					$inGroup = false;
				else
					$tutor = true;
			}

			if(!$inGroup) {
				if($args == null)
					return $this->jsonError("Not in group ". $ticket[0][1]);

				return 104;
			}

			// check to see if the ticket is public
			if($ticket[0][3] == 1) {
				// no need to check anything else
				$dialogue = $tlib->getDialogue($vars->tid);
				$this->addProfileDetails($dialogue, 2);
				if($args == null)
					return $this->jsonDialogue($ticket, $dialogue);

				return $dialogue;
			}
			else
			if(!$tutor) {
				// it's not public and the user isn't a tutor of the group
				if($args == null)
					return $this->jsonError("Resource not accessible");

				return 104;
			}

			// we are good to dispatch
			$dialogue = $tlib->getDialogue($vars->tid);
			$this->addProfileDetails($dialogue, 2);
			if($args == null)
				return $this->jsonDialogue($ticket, $dialogue);

			return $dialogue;
		}

		private function organiseUserGroups($uid, $fgroups = null)
		{
			$tutor = true;
			$groups = null;
			if($fgroups != null) {
				$xg = explode(";", $fgroups);
				foreach($xg as $k => $g) {
					$tutor = true;
					$gid = $this->resManager->queryAssoc("Group()>User('$uid'):tutor;");
					if(!$gid) {
						$tutor = false;
						$gid = $this->resManager->queryAssoc("Group()>User('$uid'):tutee;");
					}

					if($gid) {
						$groups = array();
						foreach($gid as $g)
							$groups[] = array($g, $tutor);
					}
				}
			}
			else {
				$gid = $this->resManager->queryAssoc("Group()>User('$uid'):tutor;");
				if(!$gid) {
					$tutor = false;
					$gid = $this->resManager->queryAssoc("Group()>User('$uid'):tutee;");
				}
				if($gid) {
					$groups = array();
					foreach($gid as $g)
						$groups[] = array($g, $tutor);
				}
			}

			return $groups;
		}

		private function getUserGroups($args)
		{
			$uid = Session::get('uid');
			$groups = $this->organiseUserGroups($uid);

			if($args == null)
				return $this->jsonUserGroups($groups);

			return $groups;
		}

		private function jsonUserGroups($groups)
		{
			if(!$groups)
				return $this->jsonError("Not part of any tutor groups");

			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";

			$sz = sizeof($groups);
			foreach($groups as $g) {
				$str .= "\t{\n\t\t\"group\":\"{$g[0][0]}\",\n";
				$gname = $this->resManager->queryAssoc("Group({$g[0][0]}){l};");
				$str .= "\t\t\"name\":\"{$gname[0][1]}\",\n";
				if($g[1])
					$str .= "\t\t\"tutor\":1\n\t}";
				else
					$str .= "\t\t\"tutor\":0\n\t}";

				if($sz-- > 1)
					$str .= ",\n";
			}

			$str .= "\n]\n}";
			return $str;
		}

		private function jsonPosts($posts)
		{
			if(!$posts)
				return $this->jsonError("No posts");

			$str = "{\"code\":102,\n";
			$str .="\"data\": [";

			$sz = sizeof($posts);
			foreach($posts as $g) {
				$str .= "\n";
				$str .= "\t{\n";
				$str .= "\t\t\"id\":\"{$g[0]}\",\n";
				$str .= "\t\t\"gid\":\"{$g[1]}\",\n";
				$str .= "\t\t\"group\":\"{$g[9]}\",\n";
				$str .= "\t\t\"owner\":\"{$g[2]}\",\n";
				$str .= "\t\t\"status\":\"{$g[4]}\",\n";
				$str .= "\t\t\"priority\":\"{$g[5]}\",\n";
				$str .= "\t\t\"subject\":\"{$g[6]}\",\n";
//				$str .= "\t\t\"body\":\"{$g[7]}\",\n";
				$str .= "\t\t\"posted\":\"{$g[8]}\"\n";
				$str .= "\t}";
				if($sz-- > 1)
					$str .= ",\n";
			}

			$str .= "\n]\n}";

			return $str;
		}

		private function jsonDialogue($ticket, $dialogue, $isOwner = false)
		{

			$str = "{\"code\":102,\n";
			$str .= "\"ticket\": { \n";
				$str .= "\t\t\"id\":\"{$ticket[0][0]}\",\n";
				$str .= "\t\t\"gid\":\"{$ticket[0][1]}\",\n";
				$str .= "\t\t\"owner\":\"{$ticket[0][2]}\",\n";
				$str .= "\t\t\"status\":\"{$ticket[0][4]}\",\n";
				$str .= "\t\t\"priority\":\"{$ticket[0][5]}\",\n";
				$str .= "\t\t\"subject\":\"{$ticket[0][6]}\",\n";
				$text = $this->outboundJsString($ticket[0][7]);
				$str .= "\t\t\"body\":\"{$text}\",\n";
				$str .= "\t\t\"posted\":\"{$ticket[0][8]}\",\n";
				if($isOwner)
					$str .= "\t\t\"isOwner\":1,\n";
				else
					$str .= "\t\t\"isOwner\":0,\n";
				$str .= "\t\t\"name\":\"{$ticket[0][9]}\",\n";
				$str .= "\t\t\"avatar\":\"{$ticket[0][10]}\"\n";
			$str .= "},\n";

			if(!$dialogue) {
				$str .= "\"data\": 0\n}";
				return $str;
			}

			$str .="\"data\": [";

			$sz = sizeof($dialogue);
			foreach($dialogue as $g) {
				$str .= "\n";
				$str .= "\t{\n";
				$str .= "\t\t\"id\":\"{$g[0]}\",\n";
				$str .= "\t\t\"tid\":\"{$g[1]}\",\n";
				$str .= "\t\t\"owner\":\"{$g[2]}\",\n";
				$text = $this->outboundJsString($g[3]);
				$str .= "\t\t\"body\":\"{$text}\",\n";
				$str .= "\t\t\"posted\":\"{$g[4]}\",\n";
				$str .= "\t\t\"name\":\"{$g[5]}\",\n";
				$str .= "\t\t\"avatar\":\"{$g[6]}\"\n";
				$str .= "\t}";
				if($sz-- > 1)
					$str .= ",\n";
			}

			$str .= "\n]\n}";

			return $str;
		}

		private function jsonError($message)
		{
			$str = "{\"code\":104,\n";
			$str .="\"data\": \"$message\"}";

			return $str;
		}

		private function jsonSuccess($message)
		{
			$str = "{\"code\":102,\n";
			$str .="\"data\": \"$message\"}";

			return $str;
		}

		private function postMessage($args)
		{
			$vars = $this->argVar(array(
						'tdp' => 'priority',
						'tdt' => 'type',
						'tds' => 'subject',
						'tdb' => 'body',
						'tdg' => 'group'
						), $_POST);

			$uid = Session::get('uid');
			if($vars->group == null) {
				/* no group is specified so it should mean that the user is only
				*  in one relevant group
				*/
				$rids = $this->resManager->queryAssoc("Group()>User('$uid'):tutor;");
				if(!$rids)
					$rids = $this->resManager->queryAssoc("Group()>User('$uid'):tutee;");

				if(!$rids) {
					if($args == null)
						return $this->jsonError("User not in any groups");

					return 104;
				}

				$vars->group = $rids[0][0];
			}

			include_once(LibLoad::shared('kura', 'tutortickets'));
			$tlib = new tutorticketsLibrary($this->db);
			$tlib->addTicket($vars->group, $uid, $vars->type, $vars->subject, $vars->body, $vars->priority);

			if($args == null)
				return $this->jsonSuccess("Posted message");

			return 102;
		}

		public function addProfileDetails(&$list, $col)
		{
			if(!$list)
				return;

			$this->loadProfileLibrary();
			$mpath = $this->plib->mediaPath();
			// this is horrible
			$ids = array();
			foreach($list as &$p) {
				if($p[$col] == 0) {
					$p[] = "Anon";
					$p[] = "$mpath/anon.png";
					continue;
				}

				if(isset($ids[$p[$col]])) {
					$p[] = $ids[$p[$col]][0];
					$p[] = $ids[$p[$col]][1];
					continue;
				}

				$profile = $this->resManager->queryAssoc("Profile()<User('{$p[$col]}');");
				if(!$profile)
					continue;
				$ref = $this->resManager->getHandlerRef($profile[0][0]);
				$details = $this->plib->getProfile($ref);
				$details=$details[0];
				$p[] = $details[1];
				$p[] = "$mpath/{$details[4]}";
				$ids[$p[$col]] = array($details[1], "$mpath/{$details[4]}");
			}
		}

		private function loadProfileLibrary()
		{
			if($this->plib != null)
				return;

			include_once(LibLoad::shared('kura', 'profile'));
			$this->plib = new profileLibrary($this->db);
		}

		private function postDialogue($args)
		{
			$vars = $this->argVar(array(
						'tdi' => 'tid',
						'tdb' => 'body',
						), $_POST);
			include(LibLoad::shared('kura', 'tutortickets'));
			$tlib = new tutorticketsLibrary($this->db);
			if($tlib == null)
				return 104;

			$uid = Session::get('uid');
			$ticket = $tlib->getTicket($vars->tid);
			if($ticket == null) {
				if($args == null)
					return $this->jsonError("Could not find ticket");

				return 104;
			}

			$this->addProfileDetails($ticket, 2);

			// the ticket owned by the user?
			if($uid == $ticket[0][2]) {
				// no need to check anything else
				$tlib->addMessageToDialogue($vars->tid, $uid, $vars->body);
				if($args == null)
					return $this->jsonSuccess($vars->tid);

				return 102;
			}

			// is user in the ticket's group?
			$inGroup = true;
			$tutor = false;
			if(!$this->resManager->queryAssoc("User('$uid')<Group({$ticket[0][1]}):tutee;")) {
				if(!$this->resManager->queryAssoc("User('$uid')<Group({$ticket[0][1]}):tutor;"))
					$inGroup = false;
				else
					$tutor = true;
			}

			if(!$inGroup) {
				if($args == null)
					return $this->jsonError("Not in group ". $ticket[0][1]);

				return 104;
			}

			// check to see if the ticket is public
			if($ticket[0][3] == 1) {
				// no need to check anything else
				$tlib->addMessageToDialogue($vars->tid, $uid, $vars->body);
				if($args == null)
					return $this->jsonSuccess($vars->tid);

				return 102;
			}
			else
			if(!$tutor) {
				// it's not public and the user isn't a tutor of the group
				if($args == null)
					return $this->jsonError("Resource not accessible");

				return 104;
			}

			// we are good to add message
			$tlib->addMessageToDialogue($vars->tid, $uid, $vars->body);
			if($args == null)
				return $this->jsonSuccess($vars->tid);

			return 102;
		}

		private function outboundJsString($str)
		{
			$problems = array("\n", "&lt;", "&gt;", "/", "\b", "\f", "\r", "\t", "\u", "\\'");
			$solutions = array("\\n", "<",">", "\\/", "\\b", "\\f", "\\r", "\\t", "\\u", "'");


/*
			This code is dodgy when using database entries that were migrated
			from a server with magic_quotes turned on to a server with them off

			switched code off for now
			if(!get_magic_quotes_gpc()) {
				$problems[] = "\"";
				$solutions[] = "\\\"";

			}
*/				
			return str_replace($problems, $solutions, $str);

		}

		private function changeState($args)
		{
			$vars = $this->argVar(array(
						'tdi' => 'tid',
						'tds' => 'status',
						), $_POST);
			include(LibLoad::shared('kura', 'tutortickets'));
			$tlib = new tutorticketsLibrary($this->db);
			if($tlib == null)
				return 104;

			$uid = Session::get('uid');
			$ticket = $tlib->changeTicketStatus($vars->tid, $uid, $vars->status);
			if($args == null)
				return $this->jsonSuccess($vars->tid);
		}
	}
?>
