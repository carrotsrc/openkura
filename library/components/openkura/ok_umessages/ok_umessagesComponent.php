<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_umessagesComponent extends Component
	{
		private $resManager;
		private $plib;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getInbox($args);
			break;

			case 2:
				$response = $this->postMessage($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function postMessage($args)
		{
			$vars = $this->argVar(array(
						'umprofile' => 'user',
						'umprivate' => 'private',
						'umcontent' => 'content'
						), $_POST);
			$uid = Session::get('uid');
			
			if(!is_numeric($vars->user)) {
				$rid = $this->resManager->queryAssoc("User('{$vars->user}');");
				$ref = $this->resManager->getHandlerRef($rid[0][0]);
				$vars->user = $ref;
			}
			else
			if($vars->user == 0) {
				$vars->user = $uid;
			}


			include_once(LibLoad::shared('kura', 'umessage'));

			$umlib = new umessageLibrary($this->db);
			if(!$umlib->postMessage($uid, $vars->user, $vars->private, $vars->content))
				return $this->addpostToJson(false);
			
			return $this->addpostToJson(true);
		}

		private function getInbox($args)
		{
			$vars = $this->argVar(array(
						'umprofile' => 'user',
						'umprivate' => 'private'), $args);
			$uid = Session::get('uid');
			if(!is_numeric($vars->user)) {
				$rid = $this->resManager->queryAssoc("User('{$vars->user}');");
				$ref = $this->resManager->getHandlerRef($rid[0][0]);
				$vars->user = $ref;
			}
			else
			if($vars->user == 0) {
				$vars->user = $uid;
			}

			if($vars->private == 1) {
				if($vars->user != $uid)
					if($args == null)
						return $this->inboxToJson(false);
					else
						return 104;
			}
			include_once(LibLoad::shared('kura', 'umessage'));

			$umlib = new umessageLibrary($this->db);
			$inbox = $umlib->getInbox($vars->user, $vars->private);

			if($inbox)
				$this->addProfileDetails($inbox, 1);

			if($args == null)
				return $this->inboxToJson($inbox);

			return $inbox;
		}

		private function inboxToJson($inbox)
		{
			if(!$inbox)
				return "{\"code\":104}";

			$sz = sizeof($inbox)-1;
			$str = "{\"code\":102,\n";
			$str .="\"data\": [\n";
			
			foreach($inbox as $msg) {
				$contents = $msg[5];
				$contents = $this->outboundJsString(StrSan::mysqlDesanatize($contents, true));
				$str .= "\t{\n";
				$str .= "\t\t\"id\":{$msg[0]},\n";
				$str .= "\t\t\"from\":\"{$msg[7]}\",\n";
				$str .= "\t\t\"profile\":\"{$msg[1]}\",\n";
				$str .= "\t\t\"parent\":{$msg[3]},\n";
				$str .= "\t\t\"contents\":\"{$contents}\",\n";
				$str .= "\t\t\"posted\":\"{$msg[6]}\",\n";
				$str .= "\t\t\"avatar\":\"{$msg[8]}\"\n";
				$str .= "\t}";
				if($sz-- > 0)
					$str.=",\n";

				$str .="\n";

			}
			$str .= "]\n";
			
			$str .="}";
			return $str;
		}

		private function loadProfileLibrary()
		{
			if($this->plib != null)
				return;

			include_once(LibLoad::shared('kura', 'profile'));
			$this->plib = new profileLibrary($this->db);
		}

		public function addProfileDetails(&$list, $col)
		{
			$this->loadProfileLibrary();
			$mpath = $this->plib->mediaPath();
			// this is horrible
			$ids = array();
			foreach($list as &$p) {
				if($p[$col] == 0) {
					$p[] = "Anon";
					$p[] = "$mpath/anon.png";
					continue;
				}

				if(isset($ids[$p[$col]])) {
					$p[] = $ids[$p[$col]][0];
					$p[] = $ids[$p[$col]][1];
					continue;
				}

				$profile = $this->resManager->queryAssoc("Profile()<User('{$p[$col]}');");
				if(!$profile)
					continue;
				$ref = $this->resManager->getHandlerRef($profile[0][0]);
				$details = $this->plib->getProfile($ref);
				$details=$details[0];
				$p[] = $details[1];
				$p[] = "$mpath/{$details[4]}";
				$ids[$p[$col]] = array($details[1], "$mpath/{$details[4]}");
			}
		}

		private function addpostToJson($posted)
		{
			if(!$posted)
				return "{\"code\":104}";

			return "{\"code\":102}";
		}

		private function outboundJsString($str)
		{
			$problems = array("\n", "&lt;", "&gt;", "/", "\b", "\f", "\r", "\t", "\u", "\\'");
			$solutions = array("\\n", "<",">", "\\/", "\\b", "\\f", "\\r", "\\t", "\\u", "'");

			if(!get_magic_quotes_gpc()) {
				$problems[] = "\"";
				$solutions[] = "\\\"";
			}
				
			return str_replace($problems, $solutions, $str);

		}
	}
?>
