<?php
	class ok_mntinboxComponent extends Component
	{
		private $resManager;
		private $uid;
		private $plib;
		private $mlib;
		private $ids;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			$this->uid = Session::get('uid');
		}

		public function createInstance($params = null)
		{
			$this->resManager = Managers::ResourceManager();
			$rids = $this->resManager->queryAssoc("Instance()<Component('ok_mntinbox');");
			if(!$rids)
				return 1;

			return sizeof($rids)+1;
		}

		private function loadProfileLibrary()
		{
			if($this->plib != null)
				return;

			include_once(LibLoad::shared('kura', 'profile'));
			$this->plib = new profileLibrary($this->db);
		}

		private function loadMentorLibrary()
		{
			if($this->mlib != null)
				return;

			include_once(LibLoad::shared('kura', 'mentorinbox'));
			$this->mlib = new mentorinboxLibrary($this->db);
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getInbox($args);
			break;

			case 2:
				$response = $this->getContacts($args);
			break;

			case 3:
				$response = $this->getReplies($args);
			break;

			case 4:
				$response = $this->getQuestion($args);
			break;

			case 5:
				$response = $this->getGroupMentors($args);
			break;

			case 10:
				$response = $this->postMessage($args);
			break;

			case 11:
				$response = $this->postReply($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getInbox($args)
		{
			$this->ids = array();
			$inbox = null;

			if($this->isSuperMentor())
				return $this->getSuperInbox();

			if($r = $this->getMentorGroup("mentor"))
				return $this->getMentorInbox($r);

			if($r = $this->getMentorGroup("mentee"))
				return $this->getMenteeInbox($r);

			return 104;
		}

		private function getQuestion($args)
		{
			$vars = $this->argvar(array('qid' => 'id'), $args);
			$this->loadMentorLibrary();
			$inbox = $this->mlib->getQuestion($vars->id);

			if($inbox[0][2] != $this->uid) { // it is not this user's message
				if($inbox[0][3] == 1) { // public message
					// check if user is part of group
					if(!$this->resManager->queryAssoc("User('{$this->uid}')<Group({$inbox[0][1]});"))
						return 104;
				}
				else { // private message
					if(!$this->resManager->queryAssoc("User('{$this->uid}')<Group({$inbox[0][1]}):mentor;"))
						return 104;
				}
			}
			$this->loadProfileLibrary();
			$this->addProfileDetails($inbox, 2);
			$this->addProfileDetails($inbox, 4);
			return $inbox;
		}

		private function getMentorGroup($edge)
		{
			$rid = $this->resManager->queryAssoc("Group(){r}>User('{$this->uid}'):$edge;");
			if(!$rid)
				return false;

			return $rid;
		}

		private function getGroupMentors($args)
		{
			$rql = "User(){r}<Group";
			if($this->isSuperMentor())
				$rql .= "()";
			else
			if($r = $this->getMentorGroup("mentor")) {
				$rql .= "({$r[0][0]})";
			}
			else
			if($r = $this->getMentorGroup("mentee"))
				$rql .= "({$r[0][0]})";
			else
				return 104;
	
			$rql .=":mentor;";
			$ref = $this->resManager->queryAssoc($rql);
			$this->loadProfileLibrary();
			$this->addProfileDetails($ref, 1);
			return $ref;
		}
		private function isSuperMentor()
		{
			if(!$this->resManager->queryAssoc("User('{$this->uid}')<(Instance('{$this->instanceId}')<Component('ok_mntinbox')):smentor;"))
				return false;

			return true;
		}

		private function getSuperInbox()
		{
			$this->loadMentorLibrary();
			$inbox = $this->mlib->getSuperInbox($this->uid);
			$this->addProfileDetails($inbox, 2);
			$this->addProfileDetails($inbox, 4);
			return $inbox;
			
		}

		private function getMentorInbox($g)
		{
			$this->loadMentorLibrary();
			$inbox = $this->mlib->getMentorInbox($g, $this->uid);
			$this->addProfileDetails($inbox, 2);
			$this->addProfileDetails($inbox, 4);
			return $inbox;
		}

		private function getMenteeInbox($g)
		{
			$this->loadMentorLibrary();
			$inbox = $this->mlib->getMenteeInbox($g, $this->uid);
			$this->addProfileDetails($inbox, 2);
			$this->addProfileDetails($inbox, 4);
			return $inbox;
		}

		private function getContacts()
		{
			$this->ids = array();
			if($this->isSuperMentor())
				return $this->getSuperContacts();

			if($r = $this->getMentorGroup("mentor"))
				return $this->getMentorContacts($r);

			if($r = $this->getMentorGroup("mentee"))
				return $this->getMenteeContacts($r);
		}

		private function getSuperContacts()
		{
			$list = array();
			$rids = $this->resManager->queryAssoc("User(){r}<Group():mentee;");
			$this->addProfileDetails($rids, 1);
			$list['mentee'] = $rids;

			$rids = $this->resManager->queryAssoc("User(){r}<Group():mentor;");
			$this->addProfileDetails($rids, 1);
			$list['mentor'] = $rids;


			return $list;
		}

		private function getMentorContacts($gid)
		{
			$list = array();

			$rql = "User(){r}<Group(";
			$sz = sizeof($gid);
			foreach($gid as $g) {
				$rql .= $g[0];
				if($sz-- > 1)
					$rql .= ",";
			}
			$rql .= "):mentee;";

			$rids = $this->resManager->queryAssoc($rql);
			$this->addProfileDetails($rids, 1);
			$list['mentee'] = $rids;

			$rql = "User(){r}<Group(";
			$sz = sizeof($gid);
			foreach($gid as $g) {
				$rql .= $g[0];
				if($sz-- > 1)
					$rql .= ",";
			}
			$rql .= "):mentor;";

			$rids = $this->resManager->queryAssoc($rql);
			$this->addProfileDetails($rids, 1);
			$list['mentor'] = $rids;

			return $list;
		}

		private function getMenteeContacts($gid)
		{
			$list = array();
			$rids = $this->resManager->queryAssoc("User(){r}<Group({$gid[0][0]}):mentor;");
			$this->addProfileDetails($rids, 1);
			$list['mentor'] = $rids;

			return $list;
		}


		private function addProfileDetails(&$list, $col)
		{
			if(!$list)
				return;

			$this->loadProfileLibrary();
			$mpath = $this->plib->mediaPath();
			// this is horrible
			foreach($list as &$p) {
				if($p[$col] == 0) {
					$p[] = "Anon";
					$p[] = "$mpath/anon.png";
					continue;
				}

				if(isset($this->ids[$p[$col]])) {
					$p[] = $this->ids[$p[$col]][0];
					$p[] = $this->ids[$p[$col]][1];
					continue;
				}

				$profile = $this->resManager->queryAssoc("Profile()<User('{$p[$col]}');");
				if(!$profile)
					continue;
				$ref = $this->resManager->getHandlerRef($profile[0][0]);
				$details = $this->plib->getProfile($ref);
				$details=$details[0];
				if($this->uid == $p[$col]) {
					$details[1] = "Me";
					$p[] = "Me";
				}
				else
					$p[] = $details[1];

				$p[] = "$mpath/{$details[4]}";
				$this->ids[$p[$col]] = array($details[1], "$mpath/{$details[4]}");
			}
		}

		private function getUserGroup($uid)
		{
			$rids = $this->resManager->queryAssoc("Group()>User('$uid'):mentor;");
			if(!$rids)
				$rids = $this->resManager->queryAssoc("Group()>User('$uid'):mentee;");

			return $rids;
		}

		private function postMessage($args)
		{
			$gid = 0;
			if($r = $this->getMentorGroup("mentor"))
				$gid = $r;
			else
			if($r = $this->getMentorGroup("mentee"))
				$gid = $r;
			else
				return 104;

			if($_POST['uid'] > 0) {
				$ug = $this->getUserGroup($_POST['uid']);
				if(sizeof($ug) > 1) {
					foreach($gid as $g) {
						foreach($ug as $u) {
							if($g[0] == $u[0]) {
								$gid = $u[0];
							}
						}
					}
				} else
					$gid = $ug[0][0];
			}

			$this->loadMentorLibrary();

			if($_POST['uid'] > 0)
				$r = $this->mlib->postMessage($this->uid, $_POST['uid'], $gid, StrSan::mysqlSanatize($_POST['subject']), StrSan::mysqlSanatize($_POST['content']));
			else {
				foreach($gid as $g) {
					$r = $this->mlib->postMessage($this->uid, $_POST['uid'], $g[0], StrSan::mysqlSanatize($_POST['subject']), StrSan::mysqlSanatize($_POST['content']));
				}
			}
			$this->addTrackerParam('mntmode', null);
		}

		private function getReplies($args)
		{
			$vars = $this->argvar(array('qid' => 'id'), $args);
			$gid = 0;
			if($r = $this->getMentorGroup("mentor"))
				$gid = $r[0];
			else
			if($r = $this->getMentorGroup("mentee"))
				$gid = $r[0];
			else
				return 104;


			$this->loadMentorLibrary();
			$r = $this->mlib->getReplies($vars->id);
			if(!$r)
				return 104;

			$this->loadProfileLibrary();
			$this->addProfileDetails($r, 3);

			return $r;
		}

		private function postReply($args)
		{
			$gid = 0;
			if($r = $this->getMentorGroup("mentor"))
				$gid = $r[0];

			if($r = $this->getMentorGroup("mentee"))
				$gid = $r[0];
			else
				return 104;

			$this->loadMentorLibrary();
			$r = $this->mlib->postReply($_POST['qid'], $this->uid, $_POST['pid'], $_POST['content']);
			$this->addTrackerParam("mntcrp", null);
		}
	}
?>
