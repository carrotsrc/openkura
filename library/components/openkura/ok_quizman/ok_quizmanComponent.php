<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once(LibLoad::shared('kura/records', 'rectemplate'));
	class ok_quizmanComponent extends Component
	{
		private $resManager;
		private $rlib;
		public function initialize()
		{
			//if(($r = $this->initReady())) {
				$this->rlib = new rectemplateLibrary($this->db, 'qm');
				$this->rlib->initLoad();
			//}
			/*
			*  TODO:
			*  move the loading types out so
			*  when there is a type comms, it
			*  only loads the single type
			*/
			$this->resManager = Managers::ResourceManager();
		}

		public function createInstance($params = null)
		{
			$this->resManager = Managers::ResourceManager();
			$rids = $this->resManager->queryAssoc("Instance()<Component('ok_quizman');");
			if(!$rids)
				return 1;

			return sizeof($rids)+1;
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getTypes($args);
			break;

			case 2:
				$response = $this->getTypeSession($args);
			break;

			case 3:
				$response = $this->getQuestionScheme($args);
			break;

			case 4:
				$response = $this->clearSession($args);
			break;

			case 5:
				$response = $this->loadQuiz($args);
			break;

			case 6:
				$response = $this->fixSession($args);
			break;

			case 7:
				$response = $this->registerQuizId($args);
			break;


			case 10:
				$response = $this->addQuestion($args);
			break;

			case 11:
				$response = $this->typeComms($args);
			break;

			case 12:
				$response = $this->remQuestion($args);
			break;


			case 20:
				$response = $this->addHeaderProperty($args);
			break;

			case 21:
				$response = $this->modHeaderProperty($args);
			break;

			case 22:
				$response = $this->getQuizHeader($args);
			break;

			case 23:
				$response = $this->addSectionProperty($args);
			break;

			case 24:
				$response = $this->getSectionProperties($args);
			break;

			case 25:
				$response = $this->modSectionProperties($args);
			break;

			case 26:
				$response = $this->remSectionProperty($args);
			break;

			case 27:
				$response = $this->remHeaderProperty($args);
			break;

			case 30:
				$response = $this->generateRecord($args);
			break;

			case 31:
				$response = $this->rewriteRecord($args);
			break;

			case 32:
				$response = $this->SaveTemplate($args);
			break;

			case 33:
				$response = $this->dispatchQuiz($args);
			break;

			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function typeComms($args)
		{
			$vars = $this->argVar(array (
						'qmid' => 'id'
						), $args);
			$this->rlib->typeComms($vars->id);
		}

		private function addQuestion($args)
		{
			$vars = $this->argVar(array (
						'qmtype' => 'type'
						), $_POST);
			$sid = $this->rlib->addSection($vars->type);
			$this->addTrackerParam('qmedit', $sid); 
		}

		private function getTypes($args)
		{
			$list = $this->rlib->getTypes();
			if($args == null)
				return $this->jsonGetTypes($list);

			return $list;
		}

		private function jsonGetTypes($list)
		{
			if(!$list)
				return "{\"code\":104}";

			$sz = sizeof($list)-1;
			$str = "{\"code\":102,\n";
			$str .= "\"data\":[\n"; 

			foreach($list as $l) {
				$str .= "\t{\n";
				$str .= "\t\t\"value\":\"{$l[0]}\",\n";
				$str .= "\t\t\"id\":\"{$l[1]}\"";

				$str .= "\n\t}";
				
				if($sz-- > 0)
					$str.=",\n";
					
				$str .="\n";

			}
			$str .= "]\n";
			$str .="}";

			return $str;
		}

		private function remQuestion($args)
		{
			/*
			* TODO:
			* fix problem when delete last question
			* from the cache
			*/
			$vars = $this->argVar(array (
						'qmid' => 'id'
						), $args);
			$this->rlib->removeSection($vars->id);
			return 102;
		}

		private function getTypeSession($args)
		{
			$ses = $this->rlib->getTypeSessions(true, 0);
			if($args == null)
				return $this->arrayToString($ses);

			return $ses;
		}

		private function getQuestionScheme($args)
		{
			$qar = $this->rlib->getScheme();
			$qs = array();
			foreach($qar as $n => $q)
				$qs[] = array($n, $q);
			if($args == null)
				return $this->arrayToString($qs);

			return $qs;
		}

		private function addHeaderProperty($args)
		{
			if(!isset($_POST['qmr-nprop']))
				return 104;

			$this->rlib->addHeaderProperty($_POST['qmr-nprop']);
			return 102;
		}


		private function modHeaderProperty($args)
		{
			$this->rlib->modHeaderProperty();
			return 102;
		}

		private function remHeaderProperty($args)
		{
			$vars = $this->argVar(array(
						'qmpid' => 'property'), $args);

			if($vars->property == null)
				return 104;

			$this->rlib->remHeaderProperty($vars->property);
			return 102;
		}

		private function getQuizHeader($args)
		{
			$header = $this->rlib->getTemplateHeader();
			if($args == null) {
				$qs = array();
				foreach($header as $n => $q)
					$qs[] = array($n, $q);
				return $this->arrayToString($qs);
			}

			return $header;
		}

		private function addSectionProperty($args)
		{
			if(!isset($_POST['qmr-nprop']))
				return 104;

			$id = 0;
			if(!isset($_GET['qmid']))
				return 104;

			$id = $_GET['qmid'];
			$this->rlib->addSectionProperty($id, $_POST['qmr-nprop']);
			return 102;
		}


		private function modSectionProperties($args)
		{
			$id = 0;
			if(!isset($_GET['qmid']))
				return 104;

			$id = $_GET['qmid'];
			$this->rlib->modSectionProperties($id);
			return 102;
		}

		private function getSectionProperties($args)
		{
			$vars = $this->argVar(array('qmid' => 'id'), $args);

			if($vars->id == null)
				return 104;
			return $this->rlib->getSectionProperties($vars->id);
		}

		private function remSectionProperty($args)
		{
			$vars = $this->argVar(array(
						'qmid' => 'id',
						'qmpid' => 'property'), $args);

			if($vars->id == null || $vars->property == null)
				return 104;

			if(!$this->rlib->remSectionProperty($vars->id, $vars->property))
				return 104;

			return 102;
		}

		private function generateRecord()
		{
			$recid = $this->rlib->generateRecordTemplate();
			if(!$recid)
				return 104;

			$rid = $this->resManager->addResource('RecTemplate', $recid, "RT_$recid");
			$qid = $this->resManager->addResource("Instance", $recid,"Quiz_RT$recid");
			$ridc = $this->resManager->queryAssoc("Component('ok_quizact');");
			$ridc = $ridc[0][0];

			$this->resManager->createRelationship($ridc, $qid);
			$this->resManager->createRelationship($qid, $rid);
			$this->setRio(RIO_INS, $qid);
			return 102;
		}

		private function rewriteRecord()
		{
			$id = $_GET['qmref'];
			if(!$this->rlib->regenerateRecordTemplate($id))
				return 104;

			$this->setRio(RIO_UPD, $id);
			return 102;
		}

		private function clearSession($args)
		{
			$this->rlib->clearSession();
		}

		private function loadQuiz($args)
		{
			/*
			*  TODO:
			*  need to do some checks to see that this
			*  ref is actually a child of this manager
			*/
			$vars = $this->argVar(array(
						'qmref' => 'id'), $args);

			if($vars->id == null)
				return 104;

				$this->rlib->loadTemplate($vars->id, 0);
		}

		private function registerQuizId($args)
		{

			$vars = $this->argVar(array(
						'qmref' => 'id'), $args);
			if($vars->id == null)
				return 104;
			Session::set('qmreg', $vars->id);
		}

		private function dispatchQuiz($args)
		{
			$id = Session::get('qmreg');
			if($id == null)
				return $this->jsonError("No quiz template");
			$xml =  $this->rlib->loadTemplate($id, 4);
			return $this->jsonDispatchTemplate($id, $xml[0]);
		}
		/*
		*  take a full template and save it
		*/
		private function saveTemplate($args)
		{
			$vars = $this->argVar(array(
						'qmid' => 'id'), $_POST);
			$id = $vars->id;

			$recid = $this->rlib->saveRecordTemplate($_POST['qmtemplate'], $id);
			if($id == null) {
				$rid = $this->resManager->addResource('RecTemplate', $recid, "RT_$recid");
				$qid = $this->resManager->addResource("Instance", $recid,"Quiz_RT$recid");
				$ridc = $this->resManager->queryAssoc("Component('ok_quizact');");
				$ridc = $ridc[0][0];

				$this->resManager->createRelationship($ridc, $qid);
				$this->resManager->createRelationship($qid, $rid);
				$this->setRio(RIO_INS, $qid);
			}
			return $this->jsonTemplateId($recid);
		}

		private function jsonTemplateId($id)
		{
			$str = "{\"code\":102,\n";
			$str .= "\"data\":$id\n"; 
			$str .="}";

			return $str;
		}

		private function jsonError($msg)
		{
			$str = "{\"code\":104,\n";
			$str .= "\"data\":\"$msg\"\n"; 
			$str .="}";

			return $str;
		}

		private function jsonDispatchTemplate($id, $xml)
		{
			$str = "{\"code\":102,\n";
			$str .= "\"data\": {\n";
				$str .= "\"id\":$id,\n";
				$str .= "\"template\":\"";
				$str .= $this->outboundJsString($xml);
				$str .= "\"\n";
			$str .= "}\n";
			$str .="}";

			return $str;
		}

		private function outboundJsString($str)
		{
			$str = StrSan::mysqlDesanatize($str, true);
			$problems = array("\n", "&lt;", "&gt;", "/", "\b", "\f", "\r", "\t", "\u", "\\'");
			$solutions = array("\\n", "<",">", "\\/", "\\b", "\\f", "\\r", "\\t", "\\u", "'");

			$problems[] = "\"";
			$solutions[] = "\\\"";
				
			return str_replace($problems, $solutions, $str);
		}
	}
?>
