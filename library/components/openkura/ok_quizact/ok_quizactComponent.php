<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once(SystemConfig::relativeAppPath("system/helpers/vpxml.php"));
	include_once(LibLoad::shared('kura/records', 'rectemplate'));
	class ok_quizactComponent extends Component
	{
		private $rlib;
		private $loaded;
		private $resManager;
		public function initialize()
		{
			$this->rlib = new rectemplateLibrary($this->db, 'qm');
			$this->loaded = false;
			$this->resManager = Managers::ResourceManager();
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getHeader($args);
			break;

			case 2:
				$response = $this->getQuestionScheme($args);
			break;
			
			case 3:
				$response = $this->getTypeSessions($args);
			break;

			case 4:
				$response = $this->getSectionHeaders($args);
			break;

			case 5:
				$response = $this->generateRecord($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}


		private function getHeader($args)
		{
			if(!$this->loaded) {
				$this->rlib->loadTemplate($this->instanceId);
				$this->loaded = true;
			}

			$header = $this->rlib->getTemplateHeader();
			if(!isset($header['repeat']) || $header['repeat'] == 0) {
				// check to see if there is a record already
				$uid = Session::get('uid');
				if($this->resManager->queryAssoc("Record()<User('$uid')<RecTemplate('{$this->instanceId}');")) {
					return 104;
				}
			}
			return $this->rlib->getTemplateHeader();
		}

		private function getQuestionScheme($args)
		{
			if(!$this->loaded) {
				$this->rlib->loadTemplate($this->instanceId);
				$this->loaded = true;
			}
			return $this->rlib->getScheme();
		}

		private function getTypeSessions($args)
		{
			if(!$this->loaded) {
				$this->rlib->loadTemplate($this->instanceId);
				$this->loaded = true;
			}
			return $this->rlib->getTypeSessions(true, 2);
		}

		private function getSectionHeaders($args)
		{
			if(!$this->loaded) {
				$this->rlib->loadTemplate($this->instanceId);
				$this->loaded = true;
			}
			return $this->rlib->getSectionHeaders();
		}


		public function generateRecord($args)
		{
			$this->rlib->loadTemplate($this->instanceId, 1);
			$resManager = Managers::ResourceManager();

			// see if there is a record already
			$uid = Session::get('uid');
			$recid = $this->resManager->queryAssoc("Record()<RecTemplate('{$this->instanceId}')<User('$uid');");
			if($recid != false) {

				// check to see if it is allowed to be repeated
				if($this->rlib->getHeaderProperty('repeat') == null)
					return;

				// they can repeat so clean up
				$this->resManager->cleanEdges($recid[0][0]);
				$this->resManager->removeResource($recid[0][0]);
			}
			$recid = $this->rlib->generateRecord($this->instanceId);
			$htitle = $this->rlib->getHeaderProperty("title");
			$rtitle = str_replace(" ", "_", $htitle . "_$recid");
			$rid = $resManager->addResource('Record', $recid, $rtitle);
			$qid = $resManager->queryAssoc("RecTemplate('{$this->instanceId}');");
			$resManager->createRelationship($qid[0][0], $rid);
			$this->setRio(RIO_INS, $rid);
			$this->addTrackerParam('qtamcc', 1);
			return 102;
		}
	}
?>
