<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_recqueueComponent extends Component
	{
		private $resManager;
		private $rtlib = null;
		private $rlib = null;
		private $recid;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getQueuedRecords($args);
			break;

			case 2:
				$response = $this->getRoute($args);
			break;

			case 10:
				$response = $this->loadRecord($args);
			break;

			case 11:
				$response = $this->pullSession($args);
			break;

			case 12:
				$response = $this->clearSession($args);
			break;

			case 20:
				$response = $this->getTemplateHeader($args);
			break;

			case 21:
				$response = $this->getScheme();
			break;

			case 22:
				$response = $this->getTypeSession();
			break;

			case 23:
				$response = $this->getSectionHeaders();
			break;

			case 24:
				$response = $this->getRecord();
			break;

			case 25:
				$response = $this->getRecordSectionHeaders($args);
			break;

			case 30:
				$response = $this->addSectionProperty($args);
			break;

			case 31:
				$response = $this->removeSectionProperty($args);
			break;

			case 32:
				$response = $this->modifyProperty($args);
			break;

			case 33:
				$response = $this->saveRecord($args);
			break;

			case 40:
				$response = $this->bindRecord($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function loadActObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTA.php");
			$str = "{$type}QTA";
			$obj = new $str();
			$this->types[$type] = $obj;
			return $obj;
		}

		private function loadRecObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTR.php");
			$str = "{$type}QTR";
			$obj = new $str();
			$this->types[$type] = $obj;
			return $obj;
		}

		private function loadRtlib($id)
		{
			$ridrt = $this->resManager->queryAssoc("RecTemplate()>Record('{$id}');");
			if(!$ridrt)
				return 104;

			$refrt = $this->resManager->getHandlerRef($ridrt[0][0]);
			include_once(LibLoad::shared('kura/records', 'rectemplate'));
			$this->rtlib = new rectemplateLibrary($this->db, 'rf');
			$this->rtlib->loadTemplate($refrt, 0);
			$this->rtlib->loadSecHeader();
		}

		private function pullRtlib($id)
		{
			$ridrt = $this->resManager->queryAssoc("RecTemplate()>Record('{$id}');");
			if(!$ridrt)
				return 104;

			$refrt = $this->resManager->getHandlerRef($ridrt[0][0]);
			include_once(LibLoad::shared('kura/records', 'rectemplate'));
			$this->rtlib = new rectemplateLibrary($this->db, 'rf');
			$this->rtlib->initLoad();
			$this->rtlib->loadSecHeader();
		}

		private function getQueuedRecords($args)
		{
			
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			
			include_once(LibLoad::shared('kura', 'module'));
			$mlib = new ModuleLibrary($this->db);

			// we want to find the records that are in the 
			// search graph of the course that owns the
			// current instance of recqueue
			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('ok_recqueue');");
			$ridi = $ridi[0][0];
			$ridg = $this->resManager->queryAssoc("Graph()<(KModule()>Instance($ridi));");
			$ridg = $ridg[0][0];
			$res = $this->resManager->QueryAssoc("Record()<Graph($ridg);");
			foreach($res as &$r) {
				$t = $this->resManager->getHandlerRef($r[0]);
				$r = array($r[0], $t);
			}
			$records = $rlib->getUnboundRecords($res, 1);
			if(!$records)
				return 104;

			$final = array();
			foreach($records as &$r) {
				foreach($res as &$i) {
					if($r[0] == $i[1])
						$final[] = array_merge($i, array_splice($r, 1));
				}
			}

			foreach($final as &$f) {
				$rid = $this->resManager->queryAssoc("User()>Record({$f[0]}):speaker;");
				$res = $this->resManager->getResourceFromId($rid[0][0]);
				$f[] = $res['handler'];
				$f[] = $res['label'];

				$rid = $this->resManager->queryAssoc("Activity()>Record({$f[0]});");
				$det = $mlib->getActivityDetails($this->resManager->getHandlerRef($rid[0][0]));
				$f[] = $det[0];
			}

			return $final;
		}

		private function getRoute($args)
		{
			$ridl = $this->resManager->queryAssoc("Layout()<(Instance('{$this->instanceId}')<Component('ok_recqueue'));");
			if(!$ridl)
				$ridl = $this->generateLayout();
			else
				$ridl = $ridl[0][0];
			$refl = $this->resManager->getHandlerRef($ridl);
			$rida = explode("/", $_GET['loc']);
			$rida = $rida[0];

			if($args == null)
				return $this->arrayToString(array($rida, $refl));

			return array($rida, $refl);
		}

		private function generateLayout()
		{
			$pid = "recfeedback_ui";
			$ref = $this->instanceId;
			$ridc = $this->resManager->queryAssoc("Component('recqueue');");
			if(!$ridc)
				return false;

			$cid = $this->resManager->getHandlerRef($ridc[0][0]);
			$markup = "<node type=\"0\"><leaf type=\"0\" pid=\"$pid\" cid=\"$cid\" ref=\"$ref\" grp=\"0\" /></node>";
			include_once(LibLoad::shared('stdint', 'layout'));
			$llib = new LayoutLibrary($this->db);
			$lid = null;

			$atm = explode("/", $_GET['loc']);
			$alab = $atm[0];
			$rida = $this->resManager->queryAssoc("Area('$alab');");
			if(!$rida)
				return false;

			$rida = $rida[0][0];

			$ridi = $this->resManager->queryAssoc("Instance('$ref')<Component('ok_recqueue');");
			if(!$ridi)
				return false;
			$ridi = $ridi[0][0];

			if(!($lid = $llib->addLayout("rfeedback_$ref", $markup)))
				return false;

			$ridl = $this->resManager->addResource("Layout", $lid, "rfeedback_$ref");


			$this->resManager->createRelationship($rida, $ridl);
			$this->resManager->createRelationship($ridi, $ridl);

			return $ridl;
		}


// JACK 10 
		private function loadRecord($args)
		{
			$vars = $this->argVar(array('recid' => 'id'), $args);
			$this->loadRtlib($vars->id);

			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->setScheme($this->rtlib->getScheme());
			$this->rlib->loadRecord($vars->id);
			$this->cycleRecords();
			$this->rlib->storeSession();
			Session::set('rfrrid', $vars->id);
		}

// JACK 11
		private function pullSession($args)
		{
			$vars = $this->argVar(array('recid' => 'id'), $args);
			$this->pullRtlib($vars->id);
			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->setScheme($this->rtlib->getScheme());
			$this->rlib->pullSession();
		}

// JACK 12
		private function clearSession($args)
		{
			$vars = $this->argVar(array('recid' => 'id'), $args);
			$this->pullRtlib($vars->id);
			$this->rtlib->clearSession();
			Session::uset('rfrrid');
		}

// JACK 20
		private function getTemplateHeader()
		{
			return $this->rtlib->getTemplateHeader();
		}
// JACK 21

		private function getScheme()
		{
			return $this->rtlib->getScheme();
		}

// Jack 22
		private function getTypeSession()
		{
			return $this->rtlib->getTypeSessions(true);
		}

// JACK 23
		private function getSectionHeaders()
		{
			return $this->rtlib->getSectionHeaders();
		}

// JACK 24
		private function getRecord()
		{
			return $this->rlib->getTypeSessions(true);
		}

		private function cycleRecords()
		{
			$ses = array();
			$scheme = $this->rtlib->getScheme();
			$sessions = $this->rtlib->getTypeSessions(true, 1);
			foreach($scheme as $k => $s)
			$this->rlib->processResult($k, $sessions[$s]);
		}

// JACK 25
		private function getRecordSectionHeaders()
		{
			return $this->rlib->getSectionHeaders();
		}

// JACK 30
		private function addSectionProperty($args)
		{
			$vars = $this->argVar(array(
					'qrftype' => 'type',
					'qrfsid' => 'sid'
					), $_POST);

			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->addSectionProperty($vars->sid, $vars->type);
			$this->addTrackerParam('_ref', null);
			$this->addTrackerParam('rfref', 2);
		}
// JACK 31
		private function removeSectionProperty($args)
		{
			$vars = $this->argVar(array(
					'qrftype' => 'type',
					'qrfsid' => 'sid'
					), $args);
			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->removeSectionProperty($vars->sid, $vars->type);
			$this->addTrackerParam('_ref', null);
			$this->addTrackerParam('rfref', 2);
		}

// JACK 32
		private function modifyProperty($args)
		{
			$vars = $this->argVar(array(
					'qrfsid' => 'sid'
					), $_POST);
			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->modifySectionProperties($vars->sid);
			$this->addTrackerParam('_ref', null);
			$this->addTrackerParam('rfref', 2);
		}

// JACK 33
		private function saveRecord($args)
		{
			include_once(SystemConfig::appRootPath("system/helpers/vpxml.php"));
			$recid = Session::get('rfrrid');
			$this->loadRtlib($recid);

			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$this->rlib->setScheme($this->rtlib->getScheme());
			$this->rlib->regenerateRecord($recid);
			$this->addTrackerParam('_ref', null);
			$this->addTrackerParam('rfref', 2);
		}

// JACK 40
		private function bindRecord($args)
		{
			include_once(LibLoad::shared('kura/records', 'record'));
			$this->rlib = new recordLibrary($this->db, 'rf');
			$recid = Session::get('rfrrid');
			$this->rlib->bindRecord($recid);
			$this->addTrackerParam("rfbd", "1");
		}
	}
?>
