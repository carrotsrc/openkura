<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class vp_blogmanComponent extends Component
	{
		private $resManager;
		private $blib;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			include_once(LibLoad::shared('vpatch', 'vpblog'));
			$this->blib = new vpblogLibrary($this->db);
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getPostListing($args);
			break;

			case 2:
				$response = $this->getPostDetails($args);
			break;

			case 3:
				$response = $this->addPost($args);
			break;

			case 4:
				$response = $this->updatePost($args);
			break;

			case 10:
				$response = $this->createBlog($args);
			break;

			case 11:
				$response = $this->getBlogs($args);
			break;

			case 20:
				$response = $this->addViewer($args);
			break;

			case 21:
				$response = $this->getViewers($args);
			break;

			case 30:
				$response = $this->uploadAttachment($args);
			break;

			case 31:
				$response = $this->getBlogAttachments($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getPostListing($args)
		{
			$vars = $this->argVar(array(
						'vpbb' => 'id',
						'vpbl' => 'limit',
						'vpbp' => 'page'), $args);
			$chk = $this->resManager->queryAssoc("VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman'));");
			if(!$chk)
				return 104;

			return $this->blib->getPostList($vars->id, $vars->limit, $vars->page);
		}

		private function getPostDetails($args)
		{
			$vars = $this->argVar(array(
						'vpbb' => 'id',
						'vpbi' => 'pid'
						), $args);
			$chk = $this->resManager->queryAssoc("VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman'));");
			if(!$chk)
				return 104;
			$post = $this->blib->getPost($vars->id, $vars->pid);

			/*
			*  OpenKura modification for including attachments in a post
			*  Updated: 2013-02-06
			*/
			include_once(LibLoad::shared('kura', 'attachment'));
			$alib = new attachmentLibrary($this->db);
			
			$catt = $this->resManager->queryAssoc("[attachment]{r}<(VPBlogPost('{$vars->pid}')<(VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman'))));");
			$cls = array();
			if($catt) {
				// filter out the currently attached resources
				foreach($catt as $rid)
					$cls[] = $rid[1];
				$cls = $alib->getlsAttachment($cls);
			}

			$post[] = $cls;

			return $post;
		}

		private function addPost($args)
		{
			// the form is combined so if there are files
			// then handle files instead of adding a post
			if($_FILES['vpbu']['size'] > 0) {
				return $this->uploadAttachment($args);
			}
			$attachments = array();

			foreach($_POST as $p => $v) {
				if(strlen($p) > 4 && substr($p, 0, 4) == "ainc")
					$attachments[] = $v;
			}
			if(sizeof($attachments) > 0) {
				// get the attachment rids if there are any attached
				include_once(LibLoad::shared('kura', 'attachment'));
				$alib = new attachmentLibrary($this->db);
				if(($ls = $alib->getlsAttachment($attachments))) {
					$attachments = array();
					foreach($ls as $l) {
						$r = $this->resManager->queryAssoc("{$l[4]}('{$l[0]}');");

						if($r)
							$attachments[] = $r[0][0];
					}
				}
			}

			$vars = $this->argVar(array(
						'vpbb' => 'id',
						'vpbt' => 'title',
						'vpbc' => 'contents',
						'vpbs' => 'state'), $_POST);
			$ridb = $this->resManager->queryAssoc("VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman'));");
			if(!$ridb)
				return 104;
			$ridb = $ridb[0][0];

			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('vp_blogman');");
			$ridi = $ridi[0][0];
			$pid = $this->blib->addPost($vars->id, $vars->title, $vars->contents, $vars->state);

			$ridp = $this->resManager->addResource("VPBlogPost", $pid, "VPB_$pid");

			$uid = Session::get('uid');
			$ridu = $this->resManager->queryAssoc("User('$uid');");
			if($ridu != false)
				$ridu = $ridu[0][0];

			$this->resManager->createRelationship($ridu, $ridp);
			$this->resManager->createRelationship($ridb, $ridp);

			foreach($attachments as $a)
				$this->resManager->createRelationship($ridp, $a);
		}

		private function updatePost($args)
		{
			if($_FILES['vpbu']['size'] > 0)
				return $this->uploadAttachment($args);

			$vars = $this->argVar(array(
						'vpbb' => 'id',
						'vpbi' => 'pid',
						'vpbt' => 'title',
						'vpbc' => 'contents',
						'vpbs' => 'state'), $_POST);
			
			$ridp = null;

			/*
			*  OpenKura modification for including attachments in a post
			*  Updated: 2013-02-06
			*/
			// attachments for removal and inclusion
			$arem = array();
			$ainc = array();
			include_once(LibLoad::shared('kura', 'attachment'));

			foreach($_POST as $p => $v) {
				if(strlen($p) > 4 && substr($p, 0, 4) == "arem")
					$arem[] = $v;
				else
				if(strlen($p) > 4 && substr($p, 0, 4) == "ainc")
					$ainc[] = $v;
			}

			// remove any attachment queued for removal
			if(sizeof($arem) > 0) {
				$ridp = $this->resManager->queryAssoc("VPBlogPost('$vars->pid')<(VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman')));");
				$alib = new attachmentLibrary($this->db);
				$arem = $alib->getlsAttachment($arem);

				foreach($arem as $l) {
					$rida = $this->resManager->queryAssoc("{$l[4]}('{$l[0]}');");
					if($rida)
						$this->resManager->removeRelationship($ridp[0][0], $rida[0][0]);
				}
			}

			// include any attachments queued
			if(sizeof($ainc) > 0) {
				if($ridp == null)
					$ridp = $this->resManager->queryAssoc("VPBlogPost('$vars->pid')<(VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman')));");

				$alib = new attachmentLibrary($this->db);
				$ainc = $alib->getlsAttachment($ainc);

				foreach($ainc as $l) {
					$rida = $this->resManager->queryAssoc("{$l[4]}('{$l[0]}');");
					if($rida)
						$this->resManager->createRelationship($ridp[0][0], $rida[0][0]);
				}
			}

			$ridb = $this->resManager->queryAssoc("VPBlog('{$vars->id}')<(Instance('$this->instanceId')<Component('vp_blogman'));");
			if(!$ridb)
				return 104;
			$this->blib->editPost($vars->id, $vars->pid, $vars->title, $vars->contents, $vars->state);

			return 102;
		}

		private function createBlog($args)
		{
			$vars = $this->argVar(array(
						'vpbt' => 'title',
						), $_POST);

			$id = $this->blib->createBlog($vars->title);
			if(!$id)
				return 104;

			$ridb = $this->resManager->addResource("VPBlog", $id, "VPBlog_$id");

			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('vp_blogman');");
			$ridi = $ridi[0][0];
			$this->resManager->createRelationship($ridi, $ridb);
			$this->addTrackerParam('vpbbid', null);
		}

		private function getBlogs()
		{
			$rids = $this->resManager->queryAssoc("VPBlog()<(Instance('{$this->instanceId}')<Component('vp_blogman'));");
			$blogs = array();
			if(!$rids)
				return $blogs;

			foreach($rids as $rid) {
				$ref = $this->resManager->getHandlerRef($rid[0]);

				if(!$ref)
					continue;

				$details = $this->blib->getBlogDetails($ref);
				if(!$details)
					continue;

				$blogs[] = $details;
			}

			return $blogs;
		}

		private function addViewer($args)
		{
			$vars = $this->argVar(array(
						'vpbb' => 'id',
						'vpbin' => 'title'
						), $_POST);

			$id = 1;
			$hi = 0;
			$rids = $this->resManager->queryAssoc("Instance()<Component('vp_blogview');");
			if($rids) {
				foreach($rids as $r) {
					$ref = $this->resManager->getHandlerRef($r[0]);
					if($ref > $hi)
						$hi = $ref;
				}
			}

			$id = $hi+1;
			$ridc = $this->resManager->queryAssoc("Component('vp_blogview');");
			$ridc = $ridc[0][0];
			$ridmi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('vp_blogman');");
			$ridmi = $ridmi[0][0];
			$ridb = $this->resManager->queryAssoc("VPBlog('{$vars->id}');");
			$ridb = $ridb[0][0];

			$ridi = $this->resManager->addResource("Instance", $id, $vars->title);
			$this->resManager->createRelationship($ridc, $ridi);
			$this->resManager->createRelationship($ridi, $ridb);
			$this->resManager->createRelationship($ridmi, $ridi);
		}

		private function getViewers($args)
		{
			$vars = $this->argVar(array(
						'vpbb' => 'id',
						), $args);
			$rids = $this->resManager->queryAssoc("Instance()<Component('vp_blogview')<(Instance('{$this->instanceId}')<Component('vp_blogman'));");
			$instances = array();
			if(!$rids)
				return $instances;

			foreach($rids as $r) {
				$res = $this->resManager->getResourceFromId($r[0]);
				$instances[] = $res;
			}

			return $instances;
		}

		/*
		*  OpenKura modification for including attachments in a post
		*  this method will generate an attachment and associate it with the blog
		*  Updated: 2013-01
		*/
		private function uploadAttachment($args)
		{
			include_once(LibLoad::shared('kura', 'attachment'));
			$alib = new attachmentLibrary($this->db);
			$path = $alib->handleUpload($_FILES['vpbu']);
			$bid = null;

			if(isset($_POST['vpbb']))
				$bid = $_POST['vpbb'];

			if($path == null)
				return 104;

			$t = $alib->getType($path);
			$f = explode("/", $path);
			$f = $f[sizeof($f)-1];
			$path = explode("library", $path);
			$path = "library" . $path[sizeof($path)-1];

			$ref = $alib->addAttachment($t[0], $f, $path);
			$rid = $this->resManager->addResource($t[2], $ref, $f);

			$ridi = $this->resManager->queryAssoc("Instance('{$this->instanceId}')<Component('vp_blogman');");
			$ridi = $ridi[0][0];
			$this->resManager->createRelationship($ridi, $rid);

			if($bid == null)
				return 102;

			$ridb = $this->resManager->queryAssoc("VPBlog('{$bid}')<(Instance('{$this->instanceId}')<Component('vp_blogman'));");
			if(!$ridb)
				return 104;
			$ridb = $ridb[0][0];
			$this->resManager->createRelationship($ridb, $rid);
			return 102;
		}

		/*
		*  OpenKura modification for including attachments in a post
		*  this method will get all attachments associated with a blog
		*  Updated: 2013-01
		*/
		private function getBlogAttachments($args)
		{
			$vars = $this->argVar(array(
						'vpbb' => 'id'
						), $args);

			$rids = $this->resManager->queryAssoc("[attachment]{r}<VPBlog('{$vars->id}');");
			if(!$rids)
				return 104;

			include_once(LibLoad::shared('kura', 'attachment'));
			$alib = new attachmentLibrary($this->db);
			$ls = array();
			foreach($rids as $rid)
				$ls[] = $rid[1];
			$ls = $alib->getlsAttachment($ls);
			if($args == null)
				return;

			return $ls;
		}
	}
?>
