<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class vp_blogviewComponent extends Component
	{
		private $resManager;
		private $blogId;
		private $blib;
		public function initialize()
		{
			$this->resManager = Managers::ResourceManager();
			$rid = $this->resManager->queryAssoc("VPBlog()<(Instance('{$this->instanceId}')<Component('vp_blogview'));");
			if(!$rid) {
				$this->blogId = null;
				return;
			}

			include_once(LibLoad::shared('vpatch', 'vpblog'));
			$this->blib = new vpblogLibrary($this->db);
			$this->blogId = $this->resManager->getHandlerRef($rid[0][0]);
		}

		public function run($channel = null, $args = null)
		{
			$response = null;

			switch($channel) {
			case 1:
				$response = $this->getPosts($args);
			break;

			case 2:
				$response = $this->getBlogDetails($args);
			break;

			case 3:
				$response = $this->getPost($args);
			break;
			}

			if($args == null)
				echo $response;

			 return $response;
		}

		private function getPosts($args)
		{
			if($this->blogId == null)
				return 104;
			$posts = $this->blib->getPosts($this->blogId, 10, 1);
			/*
			*  OpenKura modification for including attachments in a post
			*  Updated: 2013-02-06
			*/
			include_once(LibLoad::shared('kura', 'attachment'));
			$alib = new attachmentLibrary($this->db);


			foreach($posts as &$p) {
				$cls = array();
				$att = $this->resManager->queryAssoc("[attachment]{r}<(VPBlogPost('{$p[2]}')<VPBlog('{$this->blogId}'));");
				
				// this is horrible and will query the attachment every time
				// even if it is duplicated in another post
				// TODO: fix this when time is less pressing
				if($att) {
				foreach($att as $a) 
					$cls[] = $a[1];
				}
	

				if(sizeof($cls) > 0) {
					$cls = $alib->getlsAttachment($cls);
				}
				else
					$cls = null;
				
				$p[] = $cls;
			}


			return $posts;
		}

		private function getBlogDetails($args)
		{
			if($this->blogId == null)
				return 104;

			$details = $this->blib->getBlogDetails($this->blogId);
			return $details;
		}

		private function getPost($args)
		{
			$vars = $this->argVar(array(
						'vbpost' => 'id',
						), $args);
			$post = $this->blib->getPost($this->blogId, $vars->id);
			return $post[0];
		}
	}
?>
