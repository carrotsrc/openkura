<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class commentsLibrary extends DBAcc
	{
		private $plib;
		public function __construct($database)
		{
			$this->db = $database;
			$plib = null;
		}

		public function addComment($title, $content, $parent = 0, $root = 0, $owner = 0)
		{
			$problems = array("<",">");
			$conversionse = array("&lt;", "&gt;");
			$content = str_replace($problems, $conversionse, $content);
			if(!$this->arrayInsert('kura_comments', array(
						'title' => $title,
						'content' => $content,
						'parent' => $parent,
						'owner' => $owner,
						'root' => $root)))
				return false;

			return $this->db->getLastId();
		}

		public function getChildren($roots)
		{
			$sql = "SELECT id, root, parent, title, content, posted, owner FROM kura_comments WHERE ";
			$sz = sizeof($roots)-1;
			foreach($roots as $id) {
				$sql .= "root='$id'";

				if($sz-- > 0)
					$sql .= " OR ";
			}

			$sql .= " ORDER BY posted;";

			return $this->db->sendQuery($sql, false, false);
		}

		public function hasChildren($id)
		{
			$sql = "SELECT id FROM kura_comments WHERE root='$id' OR parent='$id' LIMIT 1;";
			if(!$this->db->sendQuery($sql, false, false))
				return false;

			return true;
		}

		public function updateComment($id, $content, $owner = null)
		{
			$alteration['content'] = $content;
			if($owner !== null)
				$alteration['owner'] = $owner;

			return $this->arrayUpdate('kura_comments', $alteration, "id='$id'");
		}

		public function getOwner($id)
		{
			$res = $this->db->sendQuery("SELECT owner FROM kura_comments WHERE id='$id';", false, false);
			if(!$res)
				return null;

			return $res[0][0];
		}

		public function getlsParents(&$ids, $col=0)
		{
			$sql = "SELECT id, title, content, posted, owner FROM kura_comments WHERE parent='0' AND ";
			$sql .= "id IN ( ";
			$sql .= "SELECT id FROM kura_comments WHERE ";
				$sz = sizeof($ids)-1;
				foreach($ids as $k => &$id) {
					$sql .= "id='{$id[$col]}'";
					if($k < $sz)
						$sql .= "OR ";
				}
			$sql .= " );";
			
			return $this->db->sendQuery($sql, false, false);
		}

		public function outboundJsString($str)
		{
			$problems = array("\n", "&lt;", "&gt;", "/", "\b", "\f", "\r", "\t", "\u", "\\'");
			$solutions = array("\\n", "<",">", "\\/", "\\b", "\\f", "\\r", "\\t", "\\u", "'");

			if(!get_magic_quotes_gpc()) {
				$problems[] = "\"";
				$solutions[] = "\\\"";
			}
				
			return str_replace($problems, $solutions, $str);

		}

	}
?>
