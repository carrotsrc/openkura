<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class umessageLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function postMessage($from, $to, $private, $content)
		{
			$content = StrSan::mysqlSanatize($content);
			if(!$this->arrayInsert('kura_umsg', array(
							'recp' => $to,
							'from' => $from,
							'private' => $private,
							'content' => $content)))
				return false;

			return $this->db->getLastId();
		}

		public function getInbox($user, $private = 0)
		{
			return $this->db->sendQuery("SELECT * FROM kura_umsg WHERE `recp`='$user' AND `private`='$private' ORDER BY posted DESC;", false, false);
		}
	}
?>
