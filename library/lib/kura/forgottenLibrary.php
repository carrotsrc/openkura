<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class forgottenLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function addRequest($username)
		{
			include_once(LibLoad::shared('vpatch', 'users'));
			$ulib = new usersLibrary($this->db);
			$details = $ulib->getAccountFromUName($username);
			if(!$details)
				return false;

			$email = $details[0][4];
			$hash = md5($username.$email.time());
			if(!$this->arrayInsert('ok_forgottenrq', array(
							'username'=>$username,
							'hash' => $hash,
							'email' => $email)))
				return false;

			$this->mailMessage($username, $email, $hash);

			return true;
		}

		public function regeneratePassword($hash, $password)
		{
			$r = $this->db->sendQuery("SELECT * FROM `ok_forgottenrq` WHERE `hash`='$hash';", false, false);
			if(!$r)
				return false;

			include_once(LibLoad::shared('vpatch', 'users'));
			$ulib = new usersLibrary($this->db);

			$newHash = $ulib->generateHash($password);
			$newSalt = $ulib->getSalt();

			$details = $ulib->getAccountFromUName($r[0][2]);
			if(!$details)
				return false;

			return $ulib->updateAccount($details[0][0], null, $newHash, $newSalt);
		}

		private function mailMessage($username, $mail, $hash)
		{
			$to      = $mail;
			$subject = 'OpenKura password reset';
			$link = "http://".SystemConfig::appServerRoot("index.php?loc=web/forgotten");
			$message = "
Hello $username,<br />
<br />
This email contains the link for resetting your password.<br />
Please click the link (or if it's not clickable then copy the url beneath into the address bar).<br />
<br />
Link:<br />
<a href=\"$link&fpkey=$hash\">Reset Password</a><br />
<br />
URL:<br />
$link&fpkey=$hash<br />
<br />
The benevolent admin<br />
---<br />
admin@openkura.org<br />
";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: noreply@carrotsrc.org' . "\r\n" .
			    'Reply-To: noreply@openkura.org' . "\r\n" .
			    'X-Mailer: PHP/' . phpversion();

			$result = mail($to, $subject, $message, $headers);
		}

		public function removeHash($hash)
		{
			$this->db->sendQuery("DELETE FROM `ok_forgottenrq` WHERE `hash`='$hash';", false, false);
		}
	}
?>
