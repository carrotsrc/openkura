<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("qtm_prop.php");
	include_once("types/iqtype.php");
	class rectemplateLibrary extends DBAcc
	{
		protected $types;
		protected $qar;
		protected $sechdr;
		protected $prefix;
		protected $header;
		public function __construct($db, $prefix)
		{
			$this->db = $db;
			$this->prefix = $prefix;
			$this->types = array();
			$this->qar = array();
			$this->sechdr = array();
			$this->header = array ('title' => "New Quiz");
		}

		public function initLoad()
		{
			$this->loadStoredTypes();
			$this->loadQar();
			$this->loadHeader();
		}

		protected function loadManObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTM.php");
			$str = "{$type}QTM";
			$obj = new $str();
			$this->types[$type] = $obj;
				return $obj;
		}

		protected function loadActObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTA.php");
			$str = "{$type}QTA";
			$obj = new $str();
			$this->types[$type] = $obj;
				return $obj;
		}

		protected function loadRecObj($type)
		{
			
		}

		public function loadStoredTypes()
		{
			$list = Session::get("{$this->prefix}ls");
			if($list == null)
				return;

			foreach($list as $l)
				$this->loadManObj($l);
		}

		public function loadQar()
		{
			if(($sq = Session::get("{$this->prefix}qa")) == null)
				return;
			else
				$this->qar = $sq;
		}

		public function loadHeader()
		{
			if(($sq = Session::get("{$this->prefix}hdr")) == null)
				return;
			else
				$this->header = $sq;
		}

		public function storeHeader()
		{
			Session::set("{$this->prefix}hdr", $this->header);
		}


		public function loadSecHeader()
		{
			if(($sq = Session::get("{$this->prefix}sh")) == null)
				return;
			else
				$this->sechdr = $sq;
		}

		public function storeSecHeader()
		{
			Session::set("{$this->prefix}sh", $this->sechdr);
		}

		public function storeQar()
		{
			if(sizeof($this->qar) == 0)
				return;

			Session::set("{$this->prefix}qa", $this->qar);
		}

		public function storeTypes()
		{
			if(sizeof($this->types) == 0)
				return;

			$list = array();
			foreach($this->types as $k => $t)
				$list[] = $k;

			Session::set("{$this->prefix}ls", $list);
		}

		public function pushTypeSessions()
		{
			foreach($this->types as $t)
				$t->pushSession();
		}

		public function pullTypeSessions()
		{
			foreach($this->types as $t)
				$t->pullSession();
		}

		public function storeSession()
		{
			$this->storeTypes();
			$this->storeQar();
			$this->pushTypeSessions();
		}
		public function clearTypes()
		{
			Session::uset("{$this->prefix}ls");
		}

		public function typeComms($id)
		{
			$this->pullTypeSessions();
			return $this->types[$this->qar[$id]]->handleComms($id);
		}

		public function getTypes()
		{
			$fm = Koda::getFileManager();
			$path = SystemConfig::appRootPath("library/lib/kura/records/types");
			$list = $fm->listDirectories($path);
			foreach($list as &$l) {
				$l = array(str_replace("_", " ", $l), $l);
			}

			return $list;
		}

		public function removeSection($id)
		{
			$this->pullTypeSessions();
			$this->types[$this->qar[$id]]->remQuestion($id);
			unset($this->qar[$id]);
			$this->loadSecHeader();
			unset($this->sechdr[$id]);
			$this->storeSecHeader();
			$this->storeSession();
		}

		public function addSection($type)
		{
			$this->pullTypeSessions();
			$sid = null;
			foreach($this->qar as $k => $v)
				$sid = $k;

			if($sid == null)
				$qid = 0;

			$sid++;

			if(!isset($this->types[$type]))
				$this->loadManObj($type);

			$this->qar[$sid] = $type;
			$this->types[$type]->addQuestion($sid);
			$this->storeSession();
			return $sid;
		}

		public function getTypeSessions($loc = false, $mode = 0)
		{
			if($mode == 0)
				$this->pullTypeSessions();

			$ses = array();
			if(!$loc)
				foreach($this->types as $k => $o)
					$ses[] = array($k, $o->getSession());
			else
				foreach($this->types as $k => $o)
					$ses[$k] =  $o->getSession();
				
			return $ses;
		}

		public function getScheme()
		{
			return $this->qar;
		}

		public function getSectionHeaders()
		{
			return $this->sechdr;
		}

		public function getSectionProperties($id)
		{
			
			$this->loadSecHeader();
			if(!isset($this->sechdr[$id]))
				return 104;

			return $this->sechdr[$id];
		}

		public function getTemplateHeader()
		{
			return $this->header;
		}

		public function addSectionProperty($id, $type)
		{
			$this->loadSecHeader();
			$qtmProp = new QTM_Prop();

			if(!isset($this->sechdr[$id]))
				$this->sechdr[$id] = array();

			switch($type) {
			case 'text':
				$qtmProp->sesTextProperty("", $this->sechdr[$id]);
			break;
			case 'mark':
				$qtmProp->sesMarkProperty("", $this->sechdr[$id]);
			break;
			}
			$this->storeSecHeader();
		}

		public function addHeaderProperty($type, $value = null)
		{
			$qtmProp = new QTM_Prop();

			switch($type) {
			case 'text':
				$qtmProp->sesTextProperty($value, $this->header);
			break;

			case 'repeat':
				$qtmProp->sesRepeatProperty("1", $this->header);
			break;
			}
			$this->storeHeader();
		}

		public function modHeaderProperty()
		{
			$pfx = "{$this->prefix}hp_";
			$sz = strlen($pfx);
			foreach($_POST as $p => $v) {
				if(strlen($p) < $sz)
					continue;
				if(substr($p, 0, $sz) != $pfx)
					continue;

				$val = substr($p, $sz);

				if(!isset($this->header[$val]))
					continue;
				$this->header[$val] = $v;
			}
			$this->storeHeader();
		}

		public function modSectionProperties($id)
		{
			$this->loadSecHeader();

			if(!isset($this->sechdr[$id]))
				return 104;

			foreach($_POST as $p => $v) {
				if(strlen($p) < 5)
					continue;

				if(substr($p, 0, 5) != 'qmsp_')
					continue;
				$val = substr($p, 5);
				if(!isset($this->sechdr[$id][$val]))
					continue;

				$this->sechdr[$id][$val] = $v;
			}

			$this->storeSecHeader();
			return 102;
		}

		public function remHeaderProperty($property)
		{
			unset($this->header[$property]);
			$this->storeHeader();
		}

		public function remSectionProperty($id, $property)
		{
			$this->loadSecHeader();

			if(!isset($this->sechdr[$id]))
				return false;

			unset($this->sechdr[$id][$property]);
			$this->storeSecHeader();
			return true;
		}

		public function generateRecordTemplate()
		{
			$this->loadSecHeader();
			$this->pullTypeSessions();
			$qtm_prop = new QTM_Prop();
			ob_start();
			$this->generateRml();
			$rml = ob_get_clean();

			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			return $rlib->addRecordTemplate($rml);
		}

		public function saveRecordTemplate(&$rml, $id = null)
		{
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			if($id == null)
				return $rlib->addRecordTemplate($rml);
			else {
				if($rlib->updateTemplate($id, $rml))
					return $id;

				return 0;
			}
		}

		public function regenerateRecordTemplate($id)
		{
			$this->loadSecHeader();
			$this->pullTypeSessions();
			ob_start();
			$this->generateRml();
			$rml = ob_get_clean();

			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			$recid = $rlib->updateTemplate($id, $rml);
			if(!$recid)
				return false;
			return true;
		}

		private function generateRml()
		{
			$qtm_prop = new QTM_Prop();
			echo "<rtemplate>\n\n";
			
			echo "<header>\n";
			// set the title
			echo "<title>{$this->header['title']}</title>\n";
			// generate the header
			foreach($this->header as $k => $h) {
				if($k == "title")
					continue;

				echo $qtm_prop->genMarkup($k, $h);

			}
			echo "</header>\n";
			$i = 1;
			foreach($this->qar as $k => $q) {
				echo "<section id=\"$i\">\n";
				if(isset($this->sechdr[$k]))
					foreach($this->sechdr[$k] as $p => $s)
						echo $qtm_prop->genMarkup($p, $s);
				echo "<obj type=\"$q\">\n";
				$this->types[$q]->generateMarkup($k);
				echo "</obj>\n";
				echo "</section>\n\n";
				$i++;
			}
			echo "</rtemplate>";
		}

		public function clearSession()
		{
			foreach($this->types as $t)
				$t->clearSession();

			Session::uset("{$this->prefix}hdr");
			Session::uset("{$this->prefix}sh");
			Session::uset("{$this->prefix}qa");
			Session::uset("{$this->prefix}ls");
			$this->header = array('title'=>"New Quiz");
			$this->sechdr = array();
			$this->qar = array();
			$this->types = array();
		}

		public function loadTemplate($id, $mode = 0)
		{
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			$rlib->loadTemplate($id);
			if($mode < 4) {
				$this->parseXML($rlib, $mode);
				if($mode == 0 || $mode == 2) {
					$this->storeSession();
					$this->storeSecHeader();
				}
			}
			else {
				return $rlib->getTemplate($id);

			}
		}

		public function setTemplate($template, $mode = 0)
		{
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			$rlib->setTemplate($template);
			$this->parseXML($rlib, $mode);
			if($mode == 0) {
				$this->storeSession();
				$this->storeSecHeader();
			}
		}

		private function parseXML($rlib, $mode)
		{
			$qtm_prop = new QTM_Prop();
			$state = array();
			$carr = null;
			$sid = 0;
			while(($tag = $rlib->getNextTag()) != null) {
				switch($tag->element) {
				case 'rtemplate':
					array_push($state, $tag);
				break;

				case '/rtemplate':
					if(end($state)->element == "rtemplate")
						array_pop($state);
					else
						return false;
				break;

				case 'header':
					if(end($state)->element != "rtemplate")
						return false;

					array_push($state, $tag);
					$carr = $this->header;
				break;

				case '/header':
					if(end($state)->element == "header")
						array_pop($state);
					else
						return false;

					$this->header = $carr;
					$this->storeHeader();
				break;


				case 'title':
					array_push($state, $tag);
				break;

				case '/title':
					if(end($state)->element == "title")
						array_pop($state);
					else
						return false;
				break;

				case 'text':
					array_push($state, $tag);
				break;

				case '/text':
					if(end($state)->element == "text")
						array_pop($state);
					else
						return false;
				break;

				case 'section':
					array_push($state, $tag);
					$id = 0;
					$type = null;
					foreach($tag->attributes as $p => $v)
						if($p == "id")
							$id = $v;
					$this->sechdr[$id] = array();
					$carr = $this->sechdr[$id];
					$sid = $id;

				break;

				case '/section':
					if(end($state)->element == "section") {
						array_pop($state);
						$this->sechdr[$sid] = $carr;
						$carr = null;
						$sid = 0;
					}
					else
						return false;
				break;

				case 'mark':
					$id = 0;
					$mark = 1;
					foreach($tag->attributes as $p => $v)
						if($p == "id")
							$id = $v;
						else
						if($p == "value")
							$mark = $v;
					$qtm_prop->sesMarkProperty($mark, $carr, $id);
				break;

				case 'repeat':
					$value = 1;
					foreach($tag->attributes as $p => $v)
						if($p == "value")
							$value = $v;
					$qtm_prop->sesRepeatProperty($value, $carr);
				break;

				case 'obj':
					$type = null;
					foreach($tag->attributes as $p => $v)
						if($p == "type")
							$type = $v;

					$this->qar[$sid] = $type;
					if(!isset($this->types[$type])) {
						if($mode == 0)
							$this->loadManObj($type);
						else
						if($mode == 1)
							$this->loadActObj($type);
						else
						if($mode == 2)
							$this->loadRecObj($type);
					}

					while(($tag = $rlib->getNextTag()) != null) {
						if($tag->element == "/obj")
							break;

						$prop = $tag->attributes;

						if($tag->element == "_text_")
							$prop = trim($tag->attributes['content']);

						$this->types[$this->qar[$sid]]->processXml($tag->element, $prop, $sid);
					}
				break;



				case '_text_':
					$cur = end($state);
					$text = trim($tag->attributes['content']);
					$text = StrSan::mysqlDesanatize($text);
					switch($cur->element) {
					case "title":
						$qtm_prop->sesTitleProperty($text,$carr);
					break;
					
					case "text":
						$id = 0;
						foreach($cur->attributes as $p)
							if($p[0] == "id")
								$id = $p[1];

						$qtm_prop->sesTextProperty($text, $carr, $id);
					break;
					}
				break;
				}
			}
		}

		public function generateRecord($id)
		{
			ob_start();
			echo "<record>\n\n";
			foreach($this->qar as $k => $q) {
				echo "<section id=\"$k\">\n";
					echo "<obj>\n";
						$this->types[$q]->generateMarkup($k);
					echo "</obj>\n";
				echo "</section>\n\n";
			}
			echo "</record>\n";
			$rml = ob_get_clean();
			$feedback = $this->getHeaderProperty('feedback');
			$bound = 1;
			if($feedback != null)
				$bound = 0;
			$recid = $this->arrayInsert('kura_records', array(
									'template_id' => $id,
									'result' => $rml,
									'bound' => $bound,
									'title' => $this->header['title']
									));

			if($recid)
				$recid = $this->db->getLastId();
			return $recid;
		}

		public function getHeaderProperty($property)
		{
			if(!isset($this->header[$property]))
				return null;

			return $this->header[$property];
		}

		public function numSections()
		{
			return sizeof($this->qar);
		}

	}
?>
