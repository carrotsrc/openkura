<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class QTR_Prop
	{
		public function genMarkup($prop, $val)
		{
			$pos = strpos($prop, "-");
			$p = $prop;
			$id = null;
			if($pos != false) {
				$p = substr($prop, 0, $pos);
				$id = substr($prop, $pos+1);
			}
			ob_start();
			switch($p) {
			case 'feedback':
				$this->rmlFeedbackProperty($id, $val);
			break;

			default:

			break;
			}

			$str = ob_get_contents();
			ob_end_clean();

			return $str;
		}

		public function generateSessionProperty($type, $val, &$arr)
		{
			switch($type) {
			case 'title':
				$this->sesTitleProperty($val, $arr);
			break;
			}
		}

		public function sesFeedbackProperty($val, &$arr)
		{
			$arr['feedback'] = $val;
		}


		private function getNextId($prefix, &$arr)
		{
			$id = 1;
			$str = "$prefix-$id";
			while(isset($arr[$str])) {
				$id++;
				$str = "$prefix-$i";
			}

			return $id;
		}

		public function rmlFeedbackProperty($id, $val)
		{
			echo "<feedback>\n";
			echo "\t$val\n";
			echo "</feedback>\n";
		}

		public function _rmlUnknown($prop, $val)
		{
			echo "<rmlunk property=\"$prop\">\n";
			echo "\t$val\n";
			echo "</rmlunk>";
		}
		
	}
?>

