<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("qtr_prop.php");
	class recordLibrary extends DBAcc
	{
		private $scheme;
		private $types;
		private $headers;
		private $prefix;

		public function __construct($database, $prefix = null)
		{
			$this->db = $database;
			$this->prefix = $prefix;
			$this->headers = array();
		}

		public function setScheme($scheme)
		{
			$this->scheme = $scheme;
		}

		public function loadRecObj($type)
		{
			if(isset($this->types[$type]))
				return;

			include_once("types/{$type}/{$type}QTR.php");
			$str = "{$type}QTR";
			$obj = new $str();
			$this->types[$type] = $obj;
				return $obj;
		}

		public function getRecords(&$ids, $col = null)
		{
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			return $rlib->getRecords($ids, $col);
		}

		public function loadRecord($id)
		{
			include_once(LibLoad::shared('kura', 'reckeep'));
			$rlib = new reckeepLibrary($this->db);
			$rlib->loadRecord($id);
			$this->parseXml($rlib);
		}

		public function pullSession()
		{
			foreach($this->scheme as $s)
				$this->loadRecObj($s);

			foreach($this->types as $t)
				$t->loadSession();

			$this->pullSectionHeaders();
		}

		public function storeSectionHeaders()
		{
			Session::set("{$this->prefix}rsh", $this->headers);
		}

		public function pullSectionHeaders()
		{
			if(($hdr = Session::get("{$this->prefix}rsh")) != null)
				$this->headers = $hdr;
		}

		public function storeSession()
		{
			foreach($this->types as $t)
				$t->storeSession();

			Session::set("{$this->prefix}rsh", $this->headers);
		}

		public function parseXml($rlib)
		{
			$qtrProp = new QTR_Prop();
			$state = array();
			$carr = null;
			$sid = 0;
			while(($tag = $rlib->getNextTag()) != null) {
				switch($tag->element) {
				case 'record':
					array_push($state, $tag);
				break;

				case '/record':
					if(end($state)->element == "record")
						array_pop($state);
					else
						return false;
				break;

				case 'section':
					array_push($state, $tag);
					$id = 0;
					foreach($tag->attributes as $p => $v)
						if($p == "id")
							$id = $v;
					$sid = $id;
					$this->headers[$sid] = array();

				break;

				case '/section':
					if(end($state)->element == "section") {
						array_pop($state);
						$sid = 0;
					}
					else
						return false;
				break;

				case 'feedback':
					array_push($state, $tag);
				break;

				case '/feedback':
					if(end($state)->element == "feedback")
						array_pop($state);
					else
						return false;

				case 'obj':
					$type = $this->scheme[$sid];
					if(!isset($this->types[$type]))
						$this->loadRecObj($type);

					while(($tag = $rlib->getNextTag()) != null) {
						if($tag->element == "/obj")
							break;

						$prop = $tag->attributes;
						if($tag->element == "_text_")
							$prop = trim($tag->attributes['content']);

						$this->types[$type]->processXml($tag->element, $prop, $sid);
					}
				break;

				case '_text_':
					$cur = end($state);
					if(!$cur)
						break;

					switch($cur->element) {
					case "feedback":
						$qtrProp->sesFeedbackProperty(trim($tag->attributes['content']), $this->headers[$sid]);
					break;
					}
				break;
				}
			}
		}

		public function processResult($section, $session)
		{
			$this->types[$this->scheme[$section]]->processResult($section, $session);
		}

		public function regenerateRecord($id)
		{
			$this->pullSession();
			$this->pullSectionHeaders();
			$qtrProp = new QTR_Prop();
			ob_start();
			echo "<record>\n";

			foreach($this->scheme as $sid => $s) {
				echo "<section id=\"$sid\">\n";
				if(isset($this->headers[$sid]))
					foreach($this->headers[$sid] as $p => $v)
						echo $qtrProp->genMarkup($p, $v);
				echo "<obj>\n";
					echo $this->types[$s]->generateMarkup($sid);
				echo "</obj>\n";
				echo "</section>\n";
			}

			echo "</record>";
			$rml = ob_get_contents();
			ob_end_clean();

			include_once(LibLoad::shared('kura', 'reckeep'));
			$rklib = new reckeepLibrary($this->db);
			return $rklib->updateRecord($id, $rml);
		}

		public function getTypeSessions()
		{
			$ses = array();
			foreach($this->types as $k => $v) {
				$ses[$k] = $v->getSession();
			}

			return $ses;
		}

		public function storeTypeSessions()
		{

		}

		public function addSectionProperty($id, $type)
		{
			$this->pullSectionHeaders();
			$qtrProp = new QTR_Prop();

			if(!isset($this->headers[$id]))
				$this->headers[$id] = array();

			switch($type) {
			case 'feedback':
				$qtrProp->sesFeedbackProperty("", $this->headers[$id]);
			break;
			}
			$this->storeSectionHeaders();
		}

		public function removeSectionProperty($id, $property)
		{
			$this->pullSectionHeaders();
			if(!isset($this->headers[$id]))
				return;
			
			unset($this->headers[$id][$property]);
			if(sizeof($this->headers[$id]) == 0)
				unset($this->headers[$id]);
			$this->storeSectionHeaders();
		}

		public function getSectionHeaders($loc = true)
		{
			if($loc)
				return $this->headers;
		}

		public function modifySectionProperties($id)
		{
			$this->pullSectionHeaders();

			if(!isset($this->headers[$id]))
				return 104;

			foreach($_POST as $p => $v) {
				if(strlen($p) < 6)
					continue;

				if(substr($p, 0, 6) != 'qrfsp_')
					continue;

				$val = substr($p, 6);
				if(!isset($this->headers[$id][$val]))
					continue;

				$this->headers[$id][$val] = $v;
			}
			$this->storeSectionHeaders();
		}

		public function bindRecord($id)
		{
			return $this->arrayUpdate('kura_records', array(
								'bound'=>'1'
								), "id='$id'");
		}
	}
?>
