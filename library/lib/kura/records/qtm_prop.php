<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class QTM_Prop
	{
		public function genMarkup($prop, $val)
		{
			$pos = strpos($prop, "-");
			$p = $prop;
			$id = null;
			if($pos != false) {
				$p = substr($prop, 0, $pos);
				$id = substr($prop, $pos+1);
			}

			ob_start();
			switch($p) {
			case 'text':
				$this->rmlTextProperty($id, $val);
			break;

			case 'title':
				$this->rmlTitleProperty($val);
			break;

			case 'mark':
				$this->rmlMark($id, $val);
			break;

			case 'repeat':
				$this->rmlRepeat($id, $val);
			break;

			default:

			break;
			}

			$str = ob_get_contents();
			ob_end_clean();

			return $str;
		}

		public function generateSessionProperty($type, $val, &$arr)
		{
			switch($type) {
			case 'title':
				$this->sesTitleProperty($val, $arr);
			break;
			}
		}

		public function sesTitleProperty($val, &$arr)
		{
			$arr['title'] = $val;
		}

		public function sesTextProperty($val, &$arr, $id = null)
		{
			if($id == null)
				$id = $this->getNextId('text', $arr);
			
			$arr["text-$id"] = $val;
		}

		public function sesMarkProperty($val, &$arr, $id = null)
		{
			if($id == null)
				$id = $this->getNextId('mark', $arr);

			$arr["mark-$id"] = $val;
		}

		public function sesRepeatProperty($val, &$arr)
		{
			$arr["repeat"] = $val;
		}

		private function getNextId($prefix, &$arr)
		{
			$id = 1;
			$str = "$prefix-$id";
			while(isset($arr[$str])) {
				$id++;
				$str = "$prefix-$i";
			}

			return $id;
		}

		public function rmlTextProperty($id, $val)
		{
			echo "<text id=\"$id\">\n";
			echo "\t$val\n";
			echo "</text>\n";
		}

		public function rmlTitleProperty($val)
		{
			echo "<title>$val</title>\n";
		}

		public function rmlMark($id, $val)
		{
			echo "<mark id=\"$id\" value=\"$val\" />\n";
		}

		public function rmlRepeat($id, $val)
		{
			echo "<repeat value=\"$val\" />\n";
		}

		public function _rmlUnknown($prop, $val)
		{
			echo "<rmlunk property=\"$prop\">\n";
			echo "\t$val\n";
			echo "</rmlunk>";
		}
		
	}
?>
