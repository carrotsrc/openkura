<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	interface iQuizType
	{
		public function processXml($tag, $properties, $section);
		public function generateMarkup($id);

		public function getSession();
	}

	interface iQuizTypeManager
	{
		public function handleComms($id);

		public function pushSession();
		public function pullSession();
		public function clearSession();
	}

	interface iQuizTypeRecord
	{
		public function processResult($section, $session);
	}
?>
