<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	abstract class MC_QTmpHandler implements iQuizType
	{
		protected $session;
		protected $tagStack;


		public function processXml($tag, $properties, $section)
		{
			switch($tag) {
			case 'question':
				array_push($this->tagStack, 1);
				$this->session[$section] = array();
			break;

			case '/question':
				if(end($this->tagStack) == 1)
					array_pop($this->tagStack);
				else
					return false;
			break;

			case 'choice':
				array_push($this->tagStack, 2);
				if(sizeof($this->session[$section]) == 0)
					$this->session[$section][0] = "";

				$this->session[$section][]= array();
				$cor = 0;
				foreach($properties as $p => $v)
					if($p == "correct")
						$cor = $v;

				$sz = sizeof($this->session[$section]) - 1;
				$this->session[$section][$sz][1]= $cor;
			break;

			case '/choice':
				if(end($this->tagStack) == 2)
					array_pop($this->tagStack);
				else
					return false;
			break;

			case 'desc':
				array_push($this->tagStack, 3);
			break;


			case '/desc':
				if(end($this->tagStack) == 3)
					array_pop($this->tagStack);
				else
					return false;
			break;

			case '_text_':
				$cur = end($this->tagStack);

				$text = trim($properties);
				$text = StrSan::mysqlDesanatize($text);

				switch($cur) {
				case 1:
					$this->session[$section][0] = $text;
				break;
				case 2:
					$sz = sizeof($this->session[$section]) - 1;
					$this->session[$section][$sz][0]= $text;

				break;
				case 3:
					$sz = sizeof($this->session[$section]) - 1;
					$this->session[$section][$sz]['d'] = $text;
				break;
				}
			break;
			}
		}

		public function getSession()
		{
			return $this->session;
		}
	}
?>
