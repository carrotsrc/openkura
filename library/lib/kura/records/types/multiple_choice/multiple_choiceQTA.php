<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("mc_qtmphandler.php");
	class multiple_choiceQTA extends MC_QTmpHandler
	{
		protected $session = array();
		protected $tagStack = array();

		public function generateMarkup($id)
		{
			if(!isset($_POST["qmc$id"])) {
				echo "\t<mcsel id=\"0\" />\n";
				return;
			}
			$val = $_POST["qmc$id"];

			echo "\t<mcsel id=\"$val\" />\n";
		}
	}
?>
