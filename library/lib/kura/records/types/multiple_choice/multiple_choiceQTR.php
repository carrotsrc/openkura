<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("mc_qtmphandler.php");
	class multiple_choiceQTR implements iQuizType
	{
		protected $session = array();
		protected $tagStack = array();

		public function processXml($tag, $properties, $section)
		{
			switch($tag) {
			case 'mcsel':
				$selection = 0;
				foreach($properties as $p => $v)
					if($p == "id")
						$selection = $v;

				$this->session[$section] = array($selection);
			break;
			}
		}

		public function getTags()
		{
			return array('mcsel');
		}

		public function getProperties()
		{
			return array();
		}

		public function getSession()
		{
			return $this->session;
		}

		public function processResult($section, &$session)
		{	
			$correct = false;
			$ans = $this->session[$section][0];
			foreach($session[$section] as $k => $s) {
				if($k == 0)
					continue;

				if($k == $ans && $s[1] == 1)
					$correct = true;
			}

			$this->session[$section][] = $correct;

			return $correct;
		}

		public function storeSession()
		{
			Session::set('mc_qtr', $this->session);
		}

		public function loadSession()
		{
			$this->session = Session::get('mc_qtr');
		}

		public function clearSession()
		{
			Session::uset('mc_qtr');
		}

		public function generateMarkup($id)
		{
			if(!isset($this->session[$id])) {
				echo "\t<mcsel id=\"0\" />\n";
				return;
			}
			$val = $this->session[$id][0];

			echo "\t<mcsel id=\"$val\" />\n";
		}
	}
?>
