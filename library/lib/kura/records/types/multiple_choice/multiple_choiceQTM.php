<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("mc_qtmphandler.php");
	class multiple_choiceQTM extends MC_QTmpHandler implements iQuizTypeManager
	{
		protected $session = array();
		protected $tagStack = array();
		public function generateMarkup($id)
		{
			$i = 1;
			foreach($this->session[$id] as $k => $v) {
				if($k == 0)
					echo "\t\t<question>\n\t\t\t{$v}\n\t\t</question>\n\n";
				else {
					if($v[1] == 1)
						echo "\t\t<choice correct=\"1\">\n";
					else
						echo "\t\t<choice correct=\"0\">\n";

					echo "\t\t\t{$v[0]}\n";
					echo "\t\t</choice>\n";

					if(isset($v['d'])) {
						echo "\t\t<desc>{$v['d']}</desc>\n";
					}
					$i++;
				}
			}
		}

		public function handleComms($id)
		{
			$op = null;
			if(isset($_POST['op']))
				$op = $_POST['op'];
			else
			if(isset($_GET['op']))
				$op = $_GET['op'];
			else
				return;

			switch($op) {
			case 1:
				$this->storeQuestionData($id);
			break;

			case 2:
				$this->addAnswers($id);
			break;

			case 3:
				$this->removeAnswer($id);
			break;

			case 4:
				$this->addAnswerDescription($id);
			break;
			}
		}

		public function pullSession()
		{
			$this->session = Session::get('mc_qtm');
		}

		public function pushSession()
		{
			Session::set('mc_qtm', $this->session);
		}

		public function getSession()
		{
			return $this->session;
		}

		public function addQuestion($id)
		{
			$this->session[$id] = array();
			$this->session[$id][0] = "";
		}

		public function remQuestion($id)
		{
			if(isset($this->session[$id]))
				unset($this->session[$id]);

			$this->pushSession();
		}

		private function storeQuestionData($id)
		{
			foreach($_POST as $k => $p) {
				if(strlen($k) < 6)
					continue;

				if(substr($k, 0, 6) == 'qtmuiq') {
					if(!isset($this->session[$id]))
						$this->session[$id] = array();

					$ans = substr($k, 6);
					if($ans == 0)
						$this->session[$id][0] = $p;
					else {
						// defensive
						if(!isset($this->session[$id][$ans]))
							$this->session[$id][$ans] = array();
						
						$this->session[$id][$ans][0] = $p;
						$str = "qtmuic$ans";
						if(isset($_POST[$str]))
							$this->session[$id][$ans][1] = 1;
						else
							$this->session[$id][$ans][1] = 0;

						$str = "qtmuid$ans";
						if(isset($this->session[$id][$ans]['d']) && isset($_POST[$str])) {
								$this->session[$id][$ans]['d'] = $_POST[$str];
						}
					}

				}
			}

			$this->pushSession();
		}

		public function clearSession()
		{
			Session::uset('mc_qtm');
		}

		private function addAnswers($id)
		{
			$num = $_POST['qmanum'];
			while($num-- > 0)
				$this->session[$id][] = array("Answer", 0);

			$this->pushSession();
		}

		private function removeAnswer($id)
		{
			if(!isset($_GET['qtmuiq']))
				return;

			$ans = $_GET['qtmuiq'];

			if($ans == 0)
				return;
			$na = array();
			foreach($this->session[$id] as $k => $a)
				if($k == $ans)
					continue;
				else
					$na[$k] = $a;
			$this->session[$id] = $na;
			$this->pushSession();
		}

		private function addAnswerDescription($id)
		{
			if(!isset($_GET['qtmuiq']))
				return;

			$desc = "";

			$ans = $_GET['qtmuiq'];
			if(isset($_GET['qtmuid']))
				$desc = $_GET['qtmuid'];

			foreach($this->session[$id] as $k => &$a) {
				if($k == $ans)
					$a['d'] = $desc;
			}

			$this->pushSession();
		}

		private function removeAnswerDescription($id)
		{
			if(!isset($_GET['qtmuiq']))
				return;

			$ans = $_GET['qtmuiq'];

			foreach($this->session[$id] as $k => &$a) {
				if($k == $ans)
					unset($a['d']);
			}

			$this->pushSession();
		}

	}
?>
