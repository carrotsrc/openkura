<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	abstract class QTypeManager
	{
		protected $items;
		public abstract function generateMarkup($id);
		public abstract function pullSession();
		public abstract function pushSession();
		public abstract function clearSession();
		public abstract function getSession();
		public abstract function addQuestion($id);
		public abstract function remQuestion($id);
		public abstract function handleComms($id);
		public abstract function loadSection($tag, $properties, $section);
	}
?>
