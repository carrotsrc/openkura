<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class QTA_Prop
	{
		public function sesTitleProperty($val, &$arr)
		{
			$arr['title'] = $val;
		}

		public function sesTextProperty($val, &$arr, $id = null)
		{
			$arr["text-$id"] = $val;
		}

		public function sesMarkProperty($val, &$arr, $id = null)
		{
			$arr["mark-$id"] = $val;
		}
	}
?>
