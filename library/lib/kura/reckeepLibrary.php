<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class RObj {

		public $type;
		public $parent;

		public $prop = array();
		public $chld = array();
	}

	class reckeepLibrary extends DBAcc
	{
		private $vpxml;
		public function __construct($database)
		{
			$this->db = $database;
			$this->vpxml = null;
		}

		public function addRecordTemplate($xml)
		{
			$xml = StrSan::mysqlSanatize($xml);
			if(!$this->arrayInsert('kura_rectemplates', array(
								'template' => $xml
								) ))
				return false;

			return $this->db->getLastId();
		}

		public function addRecord($template, $xml, $mark, $bound)
		{
			$xml = StrSan::mysqlSanatize($xml);
			if(!$this->arrayInsert('kura_records', array(
								'template_id' => $template,
								'result'=> $xml,
								'mark' => $mark,
								'bound' => $bound
								) ))
				return false;

			return $this->db->getLastId();
		}

		public function getRecord($id)
		{
			$sql = "SELECT r.result, r.mark, r.bound, t.template, t.feedback FROM kura_records as r ";
			$sql .= "JOIN kura_rectemplates AS t ON r.template_id = t.id WHERE r.id='$id';";

			return $this->db->sendQuery($sql, false, false);
		}

		public function getRecordResult($id)
		{
			$res = $this->db->sendQuery("SELECT result FROM kura_records WHERE id='$id';", false, false);
			if(!$res)
				return false;

			return $res[0][0];
		}

		public function getRecords(&$ids, $col = null)
		{
			$sql = "SELECT id, title, time, bound FROM kura_records WHERE";
			$sz = sizeof($ids)-1;
			foreach($ids as $k => $i) {
				if($col === null)
					$sql .= " id='$i'";
				else
					$sql .= " id='{$i[$col]}'";

				if($k < $sz)
					$sql .= " OR";
			}
			return $this->db->sendQuery($sql, false, false);
		}

		public function getTemplate($id)
		{
			$res = $this->db->sendQuery("SELECT template FROM kura_rectemplates WHERE id='$id';", false, false);
			if(!$res)
				return false;
			else
				return $res[0];
		}

		public function updateTemplate($id, $xml)
		{
			$xml = StrSan::mysqlSanatize($xml);
			if(!$this->arrayUpdate('kura_rectemplates', array(
								'template' => $xml),
								"id='$id'"))
				return false;

			return true;
		}

		public function updateRecord($id, $xml)
		{
			$xml = StrSan::mysqlSanatize($xml);
			if(!$this->arrayUpdate('kura_records', array(
								'result' => $xml),
								"id='$id'"))
				return false;

			return true;
		}

		public function getRecordsOf($template)
		{
			$sql = "SELECT result, mark, bound FROM kura_records WHERE template_id='$template';";
			return $this->db->sendQuery($sql, false, false);
		}

		public function getUnboundRecords($ids, $col = null)
		{
			$sql = "SELECT id, title, time FROM kura_records WHERE";
			$sz = sizeof($ids)-1;
			$sql .= "(";
			foreach($ids as $k => $i) {
				if($col === null)
					$sql .= " id='$i'";
				else
					$sql .= " id='{$i[$col]}'";

				if($k < $sz)
					$sql .= " OR";
			}
			$sql.=") AND bound='0'";
			return $this->db->sendQuery($sql, false, false);
		}

		public function getMark($id)
		{
			if(!($res = $this->db->sendQuery("SELECT mark FROM kura_records WHERE id='$id';")))
				return false;
			else
				return $res[0][0];
		}

		public function loadTemplate($id)
		{
			$root = new RObj();
			$xml = $this->getTemplate($id);
			if(!$xml)
				return false;
	
			$xml = StrSan::mysqlDesanatize($xml[0], true);
			$this->vpxml = new VPXML();
			$this->vpxml->init($xml);
		}

		public function setTemplate($xml)
		{
			$this->vpxml = new VPXML();
			$this->vpxml->init($xml);
		}

		public function loadRecord($id)
		{
			$root = new RObj();
			$xml = $this->getRecordResult($id);
			if(!$xml)
				return false;
	
			$xml = StrSan::mysqlDesanatize($xml, true);
			$this->vpxml = new VPXML();
			$this->vpxml->init($xml);
		}

		public function getNextTag()
		{
			if($this->vpxml == null)
				return;

			return $this->vpxml->getNextTag();
		}
	}
?>
