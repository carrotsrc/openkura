<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class articlerfcLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function addArticle($title, $content)
		{
			if(!$this->arrayInsert('article_rfc', array( 'title' => $title,
								'content' => $content )))
				return false;

			return $this->db->getLastId();
		}

		public function getArticle($id)
		{
			return $this->db->sendQuery("SELECT * FROM article_rfc WHERE id='$id';", false, false);
		}

		public function getNextId()
		{
			$res = $this->deb->sendQuery("SELECT id FROM article_rfc ORDER BY id DESC LIMIT 1", false, false);
			if(!$res)
				return 1;

			return $res[0][0]+1;

		}

	}
?>
