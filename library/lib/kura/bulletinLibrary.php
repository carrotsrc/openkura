<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class bulletinLibrary extends DBAcc
	{
		private $instance;
		public function __construct($database, $inst)
		{
			$this->db = $database;
			$this->instance = $inst;
		}

		public function addBulletin($title, $annotation, $user)
		{
			if(!$this->arrayInsert('bulletin_post',
						array('title'=>$title, 'annotation'=>trim($annotation),
						'instance' => $this->instance, 'owner' => $user)))
				return null;

			return $this->db->getLastId();
		}

	}
?>
