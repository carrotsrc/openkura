<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class mentorLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function getQuestions($inst, $open = false, $owner = null, $limit = null, $page = null)
		{
			$sql = "SELECT * FROM kura_mentor_questions WHERE inst='$inst' ";

			// only get the open questions
			if($open)
				$sql .= "AND open='1' ";

			if($owner != null)
				$sql .= "AND owner='$owner' ";

			$sql .= "ORDER BY posted DESC";
			if($limit != null) {
				if($page == null)
					$page = 1;

				$targ = ($limit * ($page-1));
				$sql .= " LIMIT $limit OFFSET $targ";
			}
			return $this->db->sendQuery($sql, false, false);
		}

		public function getOpenUserQuestions($inst, $recv, $limit = null, $page = null)
		{
			$sql = "SELECT * FROM kura_mentor_questions WHERE inst='$inst' AND ";
			$sql.= "( open='1' OR ( recv='$recv' OR owner='$recv' ) )";

			$sql .= "ORDER BY posted DESC";
			if($limit != null) {
				if($page == null)
					$page = 1;

				$targ = ($limit * ($page-1));
				$sql .= " LIMIT $limit OFFSET $targ";
			}
			$sql.= ";";
			return $this->db->sendQuery($sql, false, false);

		}

		public function getQuestion($inst, $id)
		{
			$sql = "SELECT * FROM kura_mentor_questions WHERE inst='$inst' AND id='$id'";
			return $this->db->sendQuery($sql, false, false);
		}

		public function getReplies($inst, $questionid, $limit = null, $page = null)
		{
			// TODO:
			// this is leaky- needs to check the instance is correct as well

			$sql = "SELECT * FROM kura_mentor_replies WHERE qid='$questionid' ORDER BY posted";
			if($limit != null) {
				if($page == null)
					$page = 1;

				$targ = ($limit * ($page-1));
				$sql .= " LIMIT $limit OFFSET $targ";
			}

			return $this->db->sendQuery($sql, false, false);
		}

		public function addQuestion($inst, $question, $details, $userid, $open, $recv)
		{
			
			$question = StrSan::mysqlSanatize($question);
			$details = StrSan::mysqlSanatize($details);
			if(!$this->arrayInsert('kura_mentor_questions', array(
									'inst' => $inst,
									'open' => $open,
									'question' => $question,
									'details' => $details,
									'recv' => $recv,
									'owner' => $userid
									)))
				return false;

			return $this->db->getLastId();
		}

		public function addReply($qid, $parent, $userid, $content)
		{
			$content = StrSan::mysqlSanatize($content);
			if(!$this->arrayInsert('kura_mentor_replies', array(
									'qid' => $qid,
									'parent' => $parent,
									'content' => $content,
									'owner' => $userid
									)))
				return false;

			return $this->db->getLastId();
		}

		public function getNextId()
		{
			$sql = "SELECT id FROM kura_mentor_question ORDER BY id DESC LIMIT 1";
			$id = $this->db->sendQuery($sql, false, false);
			if(!$id)
				return 1;

			return (intval($id[0][0])+1);
		}
	}
?>
