<div class="comments vform-item">
<hr class="hr-dotted" />
<?php
	if($vars->parents != null) {
		foreach($vars->parents as $c) {
			echo "<div class=\"comment-item\">";

				echo "<div class=\"header\">";
				if(isset($c[5])) {
					echo "<img src=\"{$c[6]}\"><br />";
					echo "{$c[5]}";
				}
				echo "</div>";
				echo "<div class=\"content\">";
					echo $c[2];
				echo "</div>";

				echo "<div class=\"vfont-small post-details\">";
					echo "{$c[3]}";
				echo "</div>";

				// check to see if the comment is expanded
				$url = "";
				$open = false;
				if($vars->openRoots != null) {
					$roots = explode(";", $vars->openRoots);
					foreach($roots as $k => $r) {
						if($c[0] == $r) {
							unset($roots[$k]);
							$open = true;
						}
					}

					if($open)
						$url = implode(";", $roots);
					else
						$url = $vars->openRoots.";".$c[0];
				}
				else
					$url = $c[0];


				echo "<div class=\"post-details\" style=\"\">";
				if($open) {
					if($url != "")
						echo "<a href=\"{$vars->_fallback->roots}&{$vars->cprfx}r=$url\">collapse</a>";
					else
						echo "<a href=\"{$vars->_fallback->roots}\">collapse</a>";
				}
				else
					echo "<a href=\"{$vars->_fallback->roots}&{$vars->cprfx}r=$url\">expand</a>";
				
				if($c[0] == $vars->reply) {
					echo " <a href=\"{$vars->_fallback->reply}\">cancel</a>";

					echo "</div>";
					 
					echo "<div class=\"surround\">";
					echo "<div class=\"comment-reply\">";
						echo "<form method=\"post\" action=\"{$vars->_fallback->submit}\">";
						echo "<textarea cols=\"55\" rows=\"7\" class=\"vform-text\" name=\"arfcc\"></textarea>";
						echo"<input type=\"hidden\" value=\"{$c[0]}\" name=\"arfcr\">";
						echo "<input type=\"hidden\" value=\"{$c[0]}\" name=\"arfcp\"><br />";
						echo "<input type=\"submit\" value=\"Comment\" class=\"vform-button vform-item\"/>";
						echo "</form>";
					echo "</div>";
					echo "</div>";
				}
				else {
					echo " <a href=\"{$vars->_fallback->reply}&{$vars->cprfx}d={$c[0]}\">reply</a>";
					echo "</div>";
				}

				if($vars->children == null) {
					echo "</div>";
					echo "<hr/>";
					continue;
				}

				echo "<div class=\"surround\">";
				echo "<ul>";
					foreach($vars->children as $key => $child) {

						if($child[1] == $c[0]) {
							// it is a child of this post
						echo "<li>";
							echo "<div class=\"comment-item child\">";

								echo "<a name=\"C{$child[0]}\"></a>";

								echo "<div class=\"child-header\">";
								if(isset($child[7])) {
									echo "<img src=\"{$child[8]}\"><br />";
									echo "{$child[7]}";
								}
								echo "</div>";

								echo "<div class=\"content\">";
									echo $child[4];
								if($child[2] != $c[0])
									echo "<div class=\"vfont-small\" style=\"margin-top: 10px;\">Replying to <a href=\"#C{$child[2]}\">this comment</a></div>";
								echo "</div>";


								echo "<div class=\"vfont-small post-details\">{$child[5]}</div>";

								if($child[0] == $vars->reply) {
									echo "<div class=\"post-details\">";
									echo "<a href=\"{$vars->_fallback->reply}\">cancel</a>";
									echo "</div>";

									echo "<div class=\"comment-reply\" style=\"clear: left;\">";
										echo "<form method=\"post\" action=\"{$vars->_fallback->submit}\">";
										echo "<textarea cols=\"55\" rows=\"7\" class=\"vform-text\" name=\"arfcc\"></textarea>";
										echo"<input type=\"hidden\" value=\"{$c[0]}\" name=\"arfcr\">";
										echo "<input type=\"hidden\" value=\"{$child[0]}\" name=\"arfcp\"><br />";
										echo "<input type=\"submit\" value=\"Comment\" class=\"vform-button vform-item\"/>";
										echo "</form>";
									echo "</div>";
								}
								else {
									echo "<div class=\"post-details\">";
									echo " <a href=\"{$vars->_fallback->reply}&{$vars->cprfx}d={$child[0]}\">reply</a>";
									echo "</div>";
								}

							echo "</div>";
						}

						echo "</li>";
					}

					echo "</ul>";

				echo "</div>";
			echo "</div>";
			echo "<hr />";
		}
	}
	else {
		echo "<b>No comments yet</b>";
	}

?>
</div>

<div style="color: grey; margin-top: 30px;">
	<form method="post" action="<?php echo $vars->_fallback->submit; ?>">
		<textarea class="vform-text" cols="55" rows="7" name="arfcc"></textarea><br />
		<input type="hidden" value="0" name="arfcr">
		<input type="hidden" value="0" name="arfcp">
		<input type="submit" value="Comment" class="vform-button vform-item"/>
	</form>
</div>
