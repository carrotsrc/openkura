<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class act_articleLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function addArticle($title, $content, $style, $active = 1)
		{
			$title = StrSan::mysqlSanatize($title);
			$content = StrSan::mysqlSanatize($content);

			if(!$this->arrayInsert('kura_article', array (
							'title' => $title,
							'content' => $content,
							'style' => $style
							)))
				return false;

			return $this->db->getLastId();
		}

		public function updateArticle($id, $title, $content, $style)
		{
			return $this->arrayUpdate('kura_article', array (
							'title' => $title,
							'content' => $content,
							'style' => $style
							), "id='$id'");
		}

		public function getArticle($id)
		{
			return $this->db->sendQuery("SELECT * FROM kura_article WHERE id='$id';", false, false);
		}

	}
?>
