<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ModuleLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function getRidModuleDetails(&$rids)
		{
			$sql = "SELECT respool.id, kura_modules.* FROM kura_modules JOIN respool ON respool.handler_ref=kura_modules.id WHERE ";
			$sz = sizeof($rids)-1;
			for($i = 0; $i < $sz; $i++) {
				$sql .= "respool.id='{$rids[$i][0]}' OR ";
			}

			$sql .= "respool.id='{$rids[$sz][0]}';";
			$details = $this->db->sendQuery($sql, false, false);
			foreach($rids as &$rid) {
				foreach($details as &$crs) {
					if($crs[0] == $rid[0]) {
						foreach($crs as $k => $det) {
							if($k == 0)
								continue;
							else
								$rid[] = $det;
						}
					}
				}
			}
		}

		public function getlsModuleDetails(&$ids, $off = null)
		{
			$sql = "SELECT * FROM kura_modules WHERE ";
			$sz = sizeof($ids)-1;
			for($i = 0; $i < $sz; $i++) {
				if($off == null)
					$sql .= "id='{$ids[$i]}' OR ";
				else
					$sql .= "id='{$ids[$i][$off]}' OR ";
			}

			if($off == null)
				$sql .= "id='{$ids[$sz]}';";
			else
				$sql .= "id='{$ids[$sz][$off]}';";

			$res = $this->db->sendQuery($sql);

			$id = null;

			foreach($res as &$row) {
				foreach($ids as &$it) {
					if($off == null)
						$id = $it;
					else
						$id = $it[$off];

					if($row[0] != $id)
						continue;

					foreach($row as $k => $v)
						if($k == 0)
							continue;
						else
							$it[] = $v;
				}
			}
		}

		public function getModuleDetails($id)
		{
			$sql = "SELECT * FROM kura_modules WHERE id='$id';";
			$result = $this->db->sendQuery($sql, false, false);
			if(!$result)
				return null;

			return $result[0];
		}

		public function getActivityTitle($id, $all = true)
		{
			if($all)
				$result = $this->db->sendQuery("SELECT title FROM kura_activities WHERE id='$id';", false, false);
			return $result[0];
		}

		public function getActivityDetails($id, $all = true)
		{
			$result = null;
			if($all)
				$result = $this->db->sendQuery("SELECT title, description, inc, active, UNIX_TIMESTAMP(end) FROM kura_activities WHERE id='$id';", false, false);
			else {
				$sql = "SELECT title, description, inc, UNIX_TIMESTAMP(end) FROM kura_activities WHERE id='$id' AND active='1' AND (";
				$sql .= "(start<=NOW() AND end>=NOW()) OR ";
				$sql .= "(start<=NOW() AND end='0000-00-00 00:00:00') OR ";
				$sql .= "(start<='0000-00-00 00:00:00' AND end='0000-00-00 00:00:00'));";
				$result = $this->db->sendQuery($sql, false, false);
			}

			if(!$result)
				return false;

			return $result[0];
		}

		public function getActivityInc($id)
		{
			$result = $this->db->sendQuery("SELECT inc FROM kura_activities WHERE id='$id';", false, false);
			if(!$result)
				return false;

			return $result[0][0];
		}

		public function addActivity($title, $description, $start, $end, $inc)
		{
			if(!$this->arrayInsert('kura_activities',
						array('title' => $title, 'description' => $description,
							'start' => $start, 'end' => $end,
							'inc' => $inc )))
								return null;

			return $this->db->getLastId();
		}

		public function removeActivity($id)
		{
			return $this->db->sendQuery("DELETE FROM kura_activities WHERE id='$id';");
		}

		public function updateActivity($id, $title, $description, $start, $end)
		{
			return $this->arrayUpdate('kura_activities', array('title' => $title,
									'description' => $description,
									'start' => $start,
									'end' => $end), "id='$id'");
		}

		public function getActivityCountdown($end)
		{
			if($end < 1)
				return null;

			$stamp = time('now');
			$total = $end - $stamp;
			$hours = ($total/60)/60;
			$time = array();
			if($total > 24) {
				$days = floor($hours/24);
				$hours = floor($hours-($days*24));
				$time['days'] = $days;
			}

			$time['hours'] = $hours;
			return $time;
		}
	}
?>
