<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class tutorticketsLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function getTickets($uid, $public, $fg, $fp, $fs)
		{
			$sql = "SELECT `ok_tutor_tickets`.*, `respool`.`label` FROM `ok_tutor_tickets` ";
			$sql .= "JOIN `respool` ON `ok_tutor_tickets`.`gid`=`respool`.`id` WHERE ";

			$sql .= $this->generateFilterArguments($fs, $fg, $fp, $uid, $public);

			$sql .= " ORDER BY `ok_tutor_tickets`.`status` DESC, `ok_tutor_tickets`.`priority` DESC, `ok_tutor_tickets`.`gid`, `ok_tutor_tickets`.`posted` DESC";
			return $this->db->sendQuery($sql, false, false);
		}

		public function getTicketMessages($tid)
		{
			$sql = "SELECT * FROM `ok_tutor_tickets` WHERE `tid`='$tid' ORDER BY `posted`;";
			return $this->db->sendQuery($sql, false, false);
		}

		public function addTicket($gid, $uid, $public, $subject, $body, $priority)
		{
			if(!$this->arrayInsert('ok_tutor_tickets', array(
								'gid' => $gid,
								'owner' => $uid,
								'status' => 1,
								'priority' => $priority,
								'subject' => $subject,
								'public' => $public,
								'body' => $body)))
				return false;

			return $this->db->getLastId();
		}

		public function changeTicketStatus($tid, $uid, $status)
		{
			return $this->arrayUpdate('ok_tutor_tickets', array(
									'status' => $status),
									"`id`='$tid' AND `owner`='$uid'");
		}

		public function getDialogue($ticket)
		{
			return $this->db->sendQuery("SELECT * FROM `ok_tutor_dialog` WHERE `tid`='$ticket';", false, false);
		}

		public function getTicket($ticket)
		{
			$r = $this->db->sendQuery("SELECT * FROM `ok_tutor_tickets` WHERE `id`='$ticket';", false, false);
			if(!$r)
				return null;

			return $r;
		}

		public function addMessageToDialogue($tid, $uid, $body)
		{
			if(!$this->arrayInsert('ok_tutor_dialog', array(
								'tid' => $tid,
								'owner' => $uid,
								'body' => $body)))
				return false;

			return $this->db->getLastId();
		}

		public function generateFilterArguments($status, $groups, $priority, $uid, $public)
		{
			$f = "";
			if($status != null) {
				$f .= "(";
				$sz = sizeof($status);
				foreach($status as $s) {
					$f .= "`ok_tutor_tickets`.`status`='$s'";
					if($sz-- > 1)
						$f .= "OR ";
				}
				$f .= ")";
				if($groups != null || $priority != null)
					$f .= " AND ";
			}

			if($groups != null) {
				$f .= "(";
				$sz = sizeof($groups);
				foreach($groups as $s) {
					if($public) {
						$f .= "(`ok_tutor_tickets`.`gid`='{$s[0][0]}' AND `ok_tutor_tickets`.`public`='1')";
					} else {
						if($s[1])
							$f .= "(`ok_tutor_tickets`.`gid`='{$s[0][0]}' AND `ok_tutor_tickets`.`public`='0')";
						else
							$f .= "(`ok_tutor_tickets`.`gid`='{$s[0][0]}' AND `ok_tutor_tickets`.`public`='0' AND `ok_tutor_tickets`.`owner`='$uid')";
					}
					if($sz-- > 1)
						$f .= " OR ";
				}
				$f .= ")";
				if($priority != null)
					$f .= " AND ";
			}

			if($priority != null) {
				$f .= "(";
				$sz = sizeof($priority);
				foreach($priority as $s) {
					$f .= "`ok_tutor_tickets`.`priority`='{$s}'";
					if($sz-- > 1)
						$f .= " OR ";
				}
				$f .= ")";
			}

			if($f == "")
				return null;

			return $f;
		}

	}
?>
