<?php
	class mentorinboxLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function getMentorInbox($group, $uid)
		{
			$sql = "SELECT `ok_mntquestions`.*, `respool`.`label`FROM `ok_mntquestions` ";
			$sql .= "JOIN `respool` ON `ok_mntquestions`.`group`=`respool`.`id` WHERE ";

			if(sizeof($group) > 1) { 
				$sql .= "(";
				$sz = sizeof($group);
				foreach($group as $g) {
					$sql .= "`group`='{$g[0]}'";
					if($sz-- > 1)
						$sql.=" OR ";
				}
				$sql .= ") ORDER BY `group`, `posted` DESC;";

			}
			else
				$sql .= "`group`='{$group[0][0]}' ORDER BY `posted` DESC;";

			return $this->db->sendQuery($sql, false, false);
		}

		public function getMenteeInbox($group, $uid)
		{
			$sql = "SELECT `ok_mntquestions`.*, `respool`.`label`FROM `ok_mntquestions` ";
			$sql .= "JOIN `respool` ON `ok_mntquestions`.`group`=`respool`.`id` WHERE (";
			$sz = sizeof($group);

			foreach($group as $g) {
				$sql .= "`group`='$g[0]'";
				if($sz-- > 1)
					$sql .= " OR ";
			}
			$sql .= ") AND (";
			$sql .= "`owner`='$uid' OR ";
			$sql .= "`recv`='$uid' OR ";
			$sql .= "`open`='1') ORDER BY `posted` DESC;";

			return $this->db->sendQuery($sql, false, false);
		}

		public function getSuperInbox()
		{
			$sql = "SELECT * FROM `ok_mntquestions` ORDER BY `posted` DESC;";

			return $this->db->sendQuery($sql, false, false);
		}

		public function postMessage($from, $to, $group, $subject, $message)
		{
			$open = 0;
			if($to == 0)
				$open = 1;
			if($subject == "")
				$subject = "- no subject -";

			if(!$this->arrayInsert('ok_mntquestions', array(
								'group' => $group,
								'owner' => $from,
								'recv' => $to,
								'open' => $open,
								'question' => $subject,
								'details' => $message))) {
				return false;
			}


			return true;
		}

		public function getQuestion($qid)
		{
			return $this->db->sendQuery("SELECT * FROM `ok_mntquestions` WHERE `id`='$qid';", false, false);
		}

		public function getReplies($qid)
		{
			return $this->db->sendQuery("SELECT * FROM ok_mntreplies WHERE qid='$qid';", false, false);
		}

		public function postReply($qid, $from, $parent, $message)
		{
			if(!$this->arrayInsert('ok_mntreplies', array(
								'qid' => $qid,
								'owner' => $from,
								'parent' => $parent,
								'content' => $message))) {
				return false;
			}

			return true;
		}
	}
?>
