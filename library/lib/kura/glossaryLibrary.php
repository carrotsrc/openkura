<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class glossaryLibrary extends DBAcc
	{
		public function __construct($database)
		{
			$this->db = $database;
		}

		public function addGlossaryTerm($gid, $term, $meaning)
		{
			$var = strtolower(str_replace(" ", "_", $term));
			if(!$this->arrayInsert('kura_glossary', array(
								'gid' => $gid,
								'term' => $term,
								'var' => $var,
								'meaning' => $meaning,
								'locked' => 0
								)))
				return false;

			return $this->db->getLastId();
		}

		public function modGlossaryTerm($gid, $id, $meaning)
		{
			return $this->arrayUpdate('kura_glossary', array(
									'meaning' => $meaning
									), "gid=$gid AND id='$id'");


		}

		public function getGlossaryTerms($gid, $letter)
		{
			$sql = "SELECT term, meaning, var FROM kura_glossary WHERE gid='$gid' ";
			$sql .="AND term LIKE '$letter%' ORDER BY term;";

			return $this->db->sendQuery($sql, false, false);
		}

		public function getSingleTerm($gid, $term)
		{
			$sql = "SELECT term, meaning, id FROM kura_glossary WHERE gid='$gid' ";
			$sql .="AND var='$term';";

			return $this->db->sendQuery($sql, false, false);
		}

		public function lockTerm($id)
		{
			return $this->arrayUpdate('kura_glossary', array(
									'locked' => 1
									), "id='$id'");
		}
	}
?>
