<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_actlist_uiPanel extends Panel
	{
		private $activities;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_actlist_ui');
			$this->activities = array();
		}

		public function loadTemplate()
		{
			echo "<div class=\"link-group\">";
				echo "<div class=\"header\">";
					echo "Open Activities";
				echo "</div>";

				echo "<div class=\"stack-container\">";

					if(sizeof($this->activities) == 0) {
						echo "<div class=\"item\">";
						echo "- none -";
						echo "</div>";
					}
					else {


						foreach($this->activities as $act) {
							echo "<div class=\"item\">";
							if($act[2] != 0) {
								$qstr = QStringModifier::modifyParams(array('loc' => "{$act[4]}")).$this->globalParamStr();
								echo "<a href=\"$qstr\">{$act[0]}</a>";
							} else
								echo "<font style=\"color: red; text-decoration: line-through;\">{$act[0]}</font>";
							if($act[5] != null) {
								echo "<div style=\"font-weight: normal; font-size: small;\">";
								if(isset($act[5]['days']) && $act[5]['days'] > 0)
									echo "{$act[5]['days']} days, ";

								echo "{$act[5]['hours']} hours left";
								echo "</div>";
							} 
							echo "</div>";
						}
					}

				echo "</div>";
			echo "</div>";


		}

		public function initialize($params = null)
		{
			$this->allActivities();
		}

		public function applyRequest($result)
		{
			foreach($result as $rs) {
				switch($rs['jack']) {
				case 2:
					if($rs['result'] != 104)
						$this->activities = $rs['result'];
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "/.assets/general.css");
		}

		public function allActivities()
		{
			$this->addComponentRequest(2, 101);
		}
	}
?>
