<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_bboard_uiPanel extends Panel
	{
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_bboard_ui');
			$this->jsCommon = "ok_bboardInterface";
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->includeTemplate('templates/board.php');
		}

		public function initialize($params = null)
		{
			$this->addComponentRequest(1, 101);
			$this->addComponentRequest(2, 101);
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					$this->addTParam('title', $rs['result']);
				break;

				case 2:
					$this->addTParam('posts', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', 'templates/bboard.css');
			$this->addAsset("js", "/G/toolset.js");
			$this->addAsset('js', 'templates/bboard_sc.js');
		}

		public function fallback()
		{
			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
	//		$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/1&dbm-redirect=0";
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/3&dbm-redirect=1";
			$this->addFallbackLink('submit', $qstr);

			$img = SystemConfig::relativeLibPath("/media/kura/bulletin");
			$this->addTParam('media', $img);
		}
	}
?>
