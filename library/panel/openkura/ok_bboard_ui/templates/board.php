<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>

<div id="<?php echo $vars->_pmod; ?>-bulletin-header" class="bulletin-header">
		<?php
			echo $vars->title;
			
		?>
		<img id="<?php echo $vars->_pmod; ?>-loading" style="visibility: hidden;" src="<?php echo $vars->media ?>/../general/loading.gif" />
</div>
<form name="<?php echo $vars->_pmod; ?>-compose-form" method="POST" action="<?php echo $vars->_fallback->submit; ?>"  enctype="multipart/form-data">
<div id="<?php echo $vars->_pmod; ?>-bulletin-container" class="bulletin-container">
<div id="<?php echo $vars->_pmod; ?>-bulletin-compose" class="bulletin-compose">
</div>
</form>
<div id="<?php echo $vars->_pmod; ?>-bulletin-compose-button" class="bulletin-compose-tab">
	<a id="<?php echo $vars->_pmod; ?>-bulletin-compose-ln"href="">Compose Bulletin</a>
</div>
<?php
	echo "<div id=\"{$vars->_pmod}-bulletin-list\">";
	if($vars->posts != 104) {
		foreach($vars->posts as $post) {
			echo "<div class=\"bulletin-post\">";
				$title = StrSan::mysqlDesanatize($post[2], true);
				//echo "<div class=\"bulletin-title\">{$title}</div>";
				$anno = StrSan::mysqlDesanatize($post[3], true);
				$anno = StrSan::htmlSanatize($anno);
				echo "<div class=\"bulletin-content\">";
				echo "{$anno}";
				if(isset($post[8])) {
					echo "<div class=\"bulletin-attachment-container\">";
					foreach($post[8] as $att) {
						$title = StrSan::mysqlDesanatize($att[2], true);

						echo "<div class=\"bulletin-attachment-icon\">";
						echo "<img src=\"{$vars->media}/{$att[4]}.png\" />";
						echo "</div>";
						
						echo "<div class=\"bulletin-attachment\">";
						echo "<a target=\"_blank\" href=\"{$att[3]}\">{$title}</a>";
						echo "<div style=\"color: #808080; font-size: 12px;\">{$att[3]}</div></a>";
						echo "<div style=\"color: #808080; font-size: 12px;\">({$att[4]})</div></a>";
						echo "</div>";
						echo "<div id=\"{$vars->_pmod}-embed-{$att[0]}\">";

						echo "</div>";
					}
					echo "</div>";
				}
			echo "<div class=\"vfont-small\" style=\"font-weight: normal; margin-top: 10px;\"><a target=\"_BLANK\" href=\"{$post[7]}\">{$post[6]}</a></div>";
			echo "</div>";

			echo "</div>";
		}
	}
	else {
		echo "<div class=\"bulletin-post\">";
		echo "</div>";
	}
	echo "</div>";
?>
</div>
