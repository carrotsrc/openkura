/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function ok_bboardInterface() {
	var self = this;
	this.media = "APP-MEDIAkura/bulletin";
	this.loadedTypes = new Array();

	this.initialize = function () {
		this.modifyLink("bulletin-compose-ln", this.uio_compose);
		this.modifyLink("click", this.checkYT);
		this.requestPosts();
	}

	this.uio_compose = function () {
		var container = document.getElementById(self._loop._pmod+"-bulletin-compose");
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		u.appendChild('div');
			u.gotoLast();
			u.node.className="bulletin-compose-block";
			u.appendChild('div')
				u.gotoLast();
				u.appendChild('textarea');
					u.gotoLast();
					u.node.style.width = '100%';
					u.node.rows = '5';
					u.node.className = "vform-text";
					u.node.id = self._loop._pmod + "-post-content";
					u.node.name = "anno";
					u.gotoParent();
				u.gotoParent();

			u.appendChild('div')
				u.gotoLast();
				u.node.id = self._loop._pmod+"-attachment";
				u.node.className="vform-item";
				try {
					u.appendText("Attachment: ");
					u.appendChild('input');
						u.gotoLast();
						u.node.type = "hidden";
						u.node.name="MAX_FILE_SIZE";
						u.node.value="3000000";
						u.gotoParent();

					u.appendChild('input');
						u.gotoLast();
						u.node.type = "file";
						u.node.name="bbul";
						u.gotoParent();
				}
				catch(e) {
							u.gotoParent();
							u.node.innerHTML= "Attachment: <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"3000000\" /><input type=\"file\" name=\"bbul\" />";
							u.gotoParent();
				}
				u.appendChild('div')
					u.gotoLast();
					u.node.className = "bulletin-post-div";
					try {
						u.appendChild('input');
							u.gotoLast();
							u.node.type = 'button';
							u.node.className = "vform-button";
							u.node.value="post";
							//u.node.onclick = self._loop.requestPostBulletin;
							u.node.onclick = function () {
								document[self._loop._pmod+"-compose-form"].submit();

							}
							u.gotoParent();
						u.gotoParent();
					}
					catch(e) {
						u.gotoParent();
						var c = u.getChild(-1);
						u.removeChild(c);
						u.node.innerHTML= "<input type=\"submit\" value=\"post\" class=\"vform-button\"/>";
					}
				u.gotoParent();
			u.gotoParent();
		u = KTSet.NodeUtl(container);
		u.appendChild(r);

		var ln = document.getElementById(self._loop._pmod+"-bulletin-compose-ln");
		u = KTSet.NodeUtl(ln);
		u.node.onclick = self._loop.uic_compose;

	}

	this.uic_compose = function () {
		var container = document.getElementById(self._loop._pmod+"-bulletin-compose");
		var u = KTSet.NodeUtl(container);
		u.clearChildren();

		var ln = document.getElementById(self._loop._pmod+"-bulletin-compose-ln");
		u = KTSet.NodeUtl(ln);
		u.clearChildren();
		u.node.onclick = self._loop.uio_compose;
		u.appendText("Compose Bulletin");
	}

	this.requestPosts = function () {
		self._loop.uio_loading();
		self._loop.request(2, null, self._loop.onresponsePosts);
	}

	this.onresponsePosts = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 102) {
			self._loop.refreshPosts(response.data);
		}
		self._loop.uic_loading();
	}

	this.refreshPosts = function (response) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		var data = response.posts;
		var media = response.media;
		var sz = data.length;
		var rtypes = new Array();
		for(var i = 0; i < sz; i++) {
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "bulletin-post";
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "bulletin-content";
					u.appendText(data[i].contents);

					if(data[i].attachments !== undefined) {
						u.appendChild('div');
							u.gotoLast();
							u.node.className = "bulletin-attachment-container";
						
						var sy = data[i].attachments.length;
						for(var j = 0; j < sy; j++) {

								u.appendChild('div');
									u.gotoLast();
									u.node.className = "bulletin-attachment-icon";
									u.appendChild('img');
										u.gotoLast();
										u.node.src = self._loop.media+"/"+data[i].attachments[j].rtype+".png";
										u.gotoParent();
									u.gotoParent();

								u.appendChild('div');
									u.gotoLast();
									u.node.className = 'bulletin-attachment';
									u.appendChild('a');
										u.gotoLast();
										u.node.href = "javascript:void()";
										u.appendText(data[i].attachments[j].title);
										u.node.href = data[i].attachments[j].url;
										u.node.target = "_BLANK";
										u.gotoParent();
									u.appendChild('div');
										u.gotoLast();
										u.node.className = "detail";
										u.appendText(data[i].attachments[j].url);
										u.gotoParent();
									u.appendChild('div');
										u.gotoLast();
										u.node.className = "detail";
										u.appendText("("+data[i].attachments[j].rtype+")");
										u.gotoParent();
									u.gotoParent();


								if(data[i].attachments[j].embed == 1) {
									rtypes.push(data[i].attachments[j].rtype);
									u.appendChild('div')
										u.gotoLast();
										u.node.id=self._loop._pmod+"-embed-expand-"+data[i].attachments[j].id;
										u.node.className = 'bulletin-embed-expand';
										u.node.onclick = self._loop.uio_embed;
										u.node.rtype = data[i].attachments[j].rtype;
										u.node.embedurl = data[i].attachments[j].url;
										u.node.attid = data[i].attachments[j].id;
										u.appendText("\u25BC");
										u.gotoParent();

								}

							u.appendChild('div')
								u.gotoLast();
								u.node.id = self._loop._pmod+"-embed-"+data[i].attachments[j].id;
								u.gotoParent();
							if(j < sy-1) {
								u.appendChild('div');
									u.gotoLast();
									u.node.className = "bulletin-attachment-separator";
									u.gotoParent();
							}
						}
							u.gotoParent();

					}
					u.appendChild('div');
						u.gotoLast();
						u.node.style.marginTop = '10px';
						u.appendChild('a');
							u.gotoLast();
							u.appendText(data[i].username);
							u.node.href=data[i].profile;
							u.node.target = "_BLANK";
							u.gotoParent();
						u.gotoParent();

					u.gotoParent();


				u.gotoParent();


		}

		var container = document.getElementById(this._loop._pmod+"-bulletin-list");

		u = KTSet.NodeUtl(container);
		u.clearChildren();
		u.appendChild(r);
		sz = rtypes.length;
		for(var i = 0; i < sz; i++)
			self._loop.requestEmbeddingScript(rtypes[i]);
	}

	this.requestPostBulletin = function () {
		self._loop.uio_loading();
		var c = document.getElementById(self._loop._pmod+"-post-content");
		var content = c.value;
		self._loop.uic_compose();
		var pstr = "title=&anno="+content;
		self._loop.request(3, null, self._loop.onresponsePostBulletin, pstr);
	}

	this.onresponsePostBulletin = function () {
		self._loop.requestPosts();
	}


	/*
	 * Do not use embedding script within this method because
	 * of latency 
	 */
	this.requestEmbeddingScript = function(type) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);

		u.appendChild('script');
			u.gotoLast();
			u.node.type = "text/javascript";
			u.node.src = self._loop.media+"/embed/"+type+".js";
			u.gotoParent();

		var c = document.getElementsByTagName("head");
		c = c[0];
		u = KTSet.NodeUtl(c);
		u.appendChild(r);
	}

	this.uio_loading = function () {
		var c = document.getElementById(self._loop._pmod+"-loading");
		c.style.visibility = 'visible';
	}

	this.uic_loading = function () {
		var c = document.getElementById(self._loop._pmod+"-loading");
		c.style.visibility = 'hidden';
	}

	this.uio_embed = function () {
		self._loop.uio_loading();
		var obj = window["embed_"+this.rtype];
		var r = obj.generate(this.embedurl);
		var u = undefined;
		if(r === null) {
			r = document.createDocumentFragment();
			u = KTSet.NodeUtl();
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "bulletin-attachment-embedded bulletin-error";
				u.appendText("An error occured when embedding "+this.rtype+" resource");
				u.gotoParent();
		}
		var c = document.getElementById(self._loop._pmod+"-embed-"+this.attid);
		u = KTSet.NodeUtl(c);
		u.appendChild(r);
		c = document.getElementById(self._loop._pmod+"-embed-expand-"+this.attid);

		u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.appendText("\u25B2");
		u.node.onclick = self._loop.uic_embed;
		self._loop.uic_loading();

	}

	this.uic_embed = function () {
		var c = document.getElementById(self._loop._pmod+"-embed-"+this.attid);
		var u = KTSet.NodeUtl(c);
		u.clearChildren();
		c = document.getElementById(self._loop._pmod+"-embed-expand-"+this.attid);

		u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.node.onclick = self._loop.uio_embed;
		u.appendText("\u25BC");
	}

}

ok_bboardInterface.prototype = new KitJS.PanelInterface('ok_bboard_ui');

