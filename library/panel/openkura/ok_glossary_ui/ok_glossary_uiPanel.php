<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_glossary_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_glossary_ui');
			$this->jsCommon = "OK_Glossary_UIInterface";
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$this->fallback();
			switch($this->mode) {
			case 0:
				$this->includeTemplate("template/main.php");
			break;

			case 1:
				$this->includeTemplate("template/word.php");
			break;
			}
		}

		public function initialize($params = null)
		{
			$letter = "a";
			if(isset($_GET['glsi']))
				$letter = $_GET['glsi'];
			
			$this->addTParam('cletter', strtoupper($letter));
			if(!isset($_GET['glsterm']))
				$this->addComponentRequest(1, array(
								'glsindex'=>$letter
								));
			else {
				$this->addComponentRequest(3, array(
								'glsterm'=>$_GET['glsterm']
								));

				$this->mode = 1;
				$this->addTParam('termstr', $_GET['glsterm']);
			}

			if(isset($_GET['glse']))
				$this->addTParam('edit', true);

			if(isset($_GET['glsvd'])) {
				$this->addTParam('discussion', true);
				$this->addComponentRequest(10, array( 'glster' => $_GET['glsterm']));

				if(isset($_GET['glsr'])) {
					$this->addTParam('openRoots', $_GET['glsr']);
					$this->addComponentRequest(12, array('glsr' => $_GET['glsr']));
				}
			}

			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					$terms = array();
					if($rs['result'] != 104)
						$terms = $rs['result'];

					$this->addTParam('terms', $terms);
				break;
				case 3:
					$this->addTParam('term', $rs['result']);
				break;

				case 10:
					if($rs['result'] == 104)
						break;
					$this->addTParam('parents', $rs['result']);
				break;

				case 12:
					if($rs['result'] == 104)
						break;

					$this->addTParam('children', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "template/glossary_style.css");
			$this->addAsset('css', "/.assets/comments_style.css");
			$this->addAsset("js", "/G/toolset.js");
			$this->addAsset('js', "/.assets/comments_sc.js");
			$this->addAsset('js', "template/kglossary.js");
		}




		public function fallback()
		{
			$mgpath = SystemConfig::appRelativePath('library/media/kura/general');
			$this->addTParam('mediag', $mgpath);
			$this->addFallbackLink('mediag',SystemConfig::appRelativePath("library/media/kura/general"));

			$qstr = QStringModifier::modifyParams(array('glsi' => null, 'glsterm' => null, 
									'glse' => null, 'glsvd' => null,
									'glsr' => null, 'glsp' => null,
									'glsd' => null));
			$this->addFallbackLink('index', $qstr);

			$qstr = QStringModifier::modifyParams(array('glsterm' => null, 'glse' => null));
			$this->addFallbackLink('term', $qstr);

			$qstr = QStringModifier::modifyParams(array('glse' => null));
			$this->addFallbackLink('edit', $qstr);

			$qstr = QStringModifier::modifyParams(array('glsvd' => null));
			$this->addFallbackLink('discuss', $qstr);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/2&dbm-redirect=1";
			$this->addFallbackLink('submit', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/4&dbm-redirect=1";
			$this->addFallbackLink('modify', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/11&dbm-redirect=1";
			$this->addFallbackLink('comment', $qstr);

			$qstr = QStringModifier::modifyParams(array('glsr' => null));
			$this->addFallbackLink('roots', $qstr);

			$qstr = QStringModifier::modifyParams(array('glsd' => null));
			$this->addFallbackLink('reply', $qstr);


			if(isset($_GET['glsd']))
				$this->addTParam('reply', $_GET['glsd']);
		}
	}
?>
