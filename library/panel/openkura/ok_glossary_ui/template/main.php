<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="glossary">
<div class="title">
Glossary of Terms
</div>

<div class="index">
	
	<?php
	for($c = 65; $c < 91; $c++) {

		$s = chr($c);
		if($s == $vars->cletter) {
			echo "<div class=\"cletter\">";
			echo $s;
		}
		else {
			echo "<div class=\"nletter\">";
			$lw = chr($c+32);
			echo "<a href=\"{$vars->_fallback->index}&glsi=$lw\">";
			echo $s;
			echo "</a>";
		}
		echo "</div>";
	}

	?>
</div>

<div class="term-container">

<?php
	foreach($vars->terms as $term) {
		echo "<div class=\"entry\">";

			echo "<div class=\"term\">";
			echo "<a href=\"{$vars->_fallback->term}&glsterm={$term[2]}\">";
			echo $term[0];
			echo "</a>";
			echo "</div>";

			echo "<div class=\"meaning\">";
			echo $term[1];
			echo "</div>";

		echo "</div>";
	}
?>

	<div class="add-term">
		<b>Add a new Term</b>
		<form action="<?php echo $vars->_fallback->submit ?>" method="post">
			<input type="text" class="vform-text vfont-large vform-item" name="glsterm" /><br />
			<textarea cols="50" rows="7" name="glsmean" class="vform-text vform-item"></textarea><br />
			<input type="submit" class="vform-button vform-item" value="Add term" />
		</form>
	</div>

</div>

</div>
