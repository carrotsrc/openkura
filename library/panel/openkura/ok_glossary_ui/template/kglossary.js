/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function OK_Glossary_UIInterface() {
	var self = this;
	this.termid = null;
	this.termnid = null;
	var clib = new comments_sc(self, 'gls');

	this.initialize = function () {
		clib.frqAddComment = self.requestAddComment;
		clib.frqChildren = self.requestChildren;
		clib.frqParents = self.requestParents;
		clib.frqRefreshChildren = self.refreshChildren;

		var e = document.getElementById(self._loop._pmod+"-termnid");
		self._loop.termnid = e.value;

		e = document.getElementById(self._loop._pmod+"-open");
		if(e === null)
			return;

		e.termid = e.name;
		e.href="javascript:void(0);";
		e.termnid = self._loop.termnid;
		e.onclick = self.requestParents;
		self._loop.termid = e.name;

	}

	this.closeDiscussion = function () {
		var e = document.getElementById(self._loop._pmod+"-comments");
		var u = KTSet.NodeUtl(e);
		u.clearChildren();
		self._loop.initialize();
	}

	this.requestParents = function () {
		var e = document.getElementById(self._loop._pmod+"-open");
		e.termid = e.name;
		e.href="javascript:void(0);";
		e.onclick = self._loop.closeDiscussion;
		self._loop.termid = e.name;

		var poststr = "glsterm="+this.termid;
		clib.requestParents(poststr);
	}

	this.requestAddComment = function (parentid, rootid, comment) {
		clib.requestAddComment(parentid, rootid, comment, "glsterm="+self._loop.termid);
	}

	this.requestChildren = function () {
		clib.requestChildren("glsterm="+self._loop.termid, this.rootid);
	}



	this.refreshChildren = function (rootid) {
		clib.refreshChildren(rootid, "glsterm="+self._loop.termid);
	}

}
OK_Glossary_UIInterface.prototype = new KitJS.PanelInterface('ok_glossary_ui');
