<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="glossary">
<div class="title">
Glossary of Terms
</div>

<div class="index">
	
	<?php
	for($c = 65; $c < 91; $c++) {

		$s = chr($c);
		if($s == $vars->cletter) {
			$lw = chr($c+32);
			echo "<div class=\"cletter\">";
			echo "<a href=\"{$vars->_fallback->index}&glsi=$lw\">";
			echo $s;
			echo "</a>";
		}
		else {
			echo "<div class=\"nletter\">";
			$lw = chr($c+32);
			echo "<a href=\"{$vars->_fallback->index}&glsi=$lw\">";
			echo $s;
			echo "</a>";
		}
		echo "</div>";
	}

	?>
</div>

<div class="term-container">
<?php
	echo "<div class=\"entry\">";

		echo "<div class=\"term\">\n\t";
		echo $vars->term[0];
		if(!$vars->edit) {
			echo "<a href=\"{$vars->_fallback->edit}&glse=1\" class=\"vfloat-right\" style=\"margin-top: 20px\">\n";
			echo "<img src=\"{$vars->mediag}/gedit.png\" />\n";
			echo "</a>\n";
		}
		echo "\n</div>";

		echo "<div class=\"meaning\">\n";
		if($vars->edit) {
			echo "\t<form action=\"{$vars->_fallback->modify}\" method=\"post\">\n";
			echo "\t<textarea cols=\"65\" rows=\"7\" class=\"vform-text\" name=\"glsmean\">";
			echo $vars->term[1];
			echo "</textarea><br />\n";
			echo "\t<input type=\"hidden\" value=\"{$vars->term[2]}\" name=\"glsid\" />\n";
			echo "\t<input type=\"submit\" value=\"edit\" class=\"vform-button vform-item\" />\n";
			echo "\t</form>\n";
		}
		else
			echo $vars->term[1];
		echo "</div>";
	echo "</div>";
?>

	<div class="discussion">
	
	<?php
		
		if($vars->discussion == true) {
			echo "<div class=\"header\">";
			echo "<a id=\"{$vars->_pmod}-open\" class=\"va-switch\" href=\"{$vars->_fallback->discuss}\">";
			echo "&#9662;";
			echo "</a> ";
		}
		else {
			echo "<div class=\"header-light\">";
			echo "<a id=\"{$vars->_pmod}-open\" name=\"{$vars->termstr}\" href=\"{$vars->_fallback->discuss}&glsvd=1\" class=\"va-switch\">";
			echo "&#9656;";
			echo "</a> ";
		}
	?>
	Discussion</div>
	</div>

<?php
echo "<input type=\"hidden\" id=\"{$vars->_pmod}-termnid\" value=\"{$vars->term[2]}\">";
echo "<div id=\"{$vars->_pmod}-comments\" class=\"comments vform-item\">";
if($vars->discussion) {
?>
<?php
	if($vars->parents != null) {
		foreach($vars->parents as $c) {
			echo "<div class=\"comment-item\">";

				echo "<div class=\"header\">";
				if(isset($c[5])) {
					echo "<img src=\"{$c[6]}\" style=\"width: 48px; height: 48px;\"><br />";
					echo "{$c[5]}";
				}
				echo "</div>";
				echo "<div class=\"content\">";
					echo $c[2];
				echo "</div>";

				echo "<div class=\"vfont-small post-details\">";
					echo "{$c[3]}";
				echo "</div>";

				// check to see if the comment is expanded
				$url = "";
				$open = false;
				if($vars->openRoots != null) {
					$roots = explode(";", $vars->openRoots);
					foreach($roots as $k => $r) {
						if($c[0] == $r) {
							unset($roots[$k]);
							$open = true;
						}
					}

					if($open)
						$url = implode(";", $roots);
					else
						$url = $vars->openRoots.";".$c[0];
				}
				else
					$url = $c[0];


				echo "<div class=\"post-details\" style=\"\">";
				if($open) {
					if($url != "")
						echo "<a href=\"{$vars->_fallback->roots}&glsr=$url\">collapse</a>";
					else
						echo "<a href=\"{$vars->_fallback->roots}\">collapse</a>";
				}
				else
					echo "<a href=\"{$vars->_fallback->roots}&glsr=$url\">expand</a>";
				
				if($c[0] == $vars->reply) {
					echo " <a href=\"{$vars->_fallback->reply}\">cancel</a>";

					echo "</div>";
					 
					echo "<div class=\"surround\">";
					echo "<div class=\"comment-reply\">";
						echo "<form method=\"post\" action=\"{$vars->_fallback->comment}\">";
						echo "<textarea cols=\"55\" rows=\"7\" class=\"vform-text\" name=\"glsc\"></textarea>";
						echo"<input type=\"hidden\" value=\"{$c[0]}\" name=\"glsr\">";
						echo "<input type=\"hidden\" value=\"{$c[0]}\" name=\"glsp\"><br />";
						echo "<input type=\"hidden\" value=\"{$vars->term[2]}\" name=\"glsterm\"><br />";
						echo "<input type=\"submit\" value=\"Comment\" class=\"vform-button vform-item\"/>";
						echo "</form>";
					echo "</div>";
					echo "</div>";
				}
				else {
					echo " <a href=\"{$vars->_fallback->reply}&glsd={$c[0]}\">reply</a>";
					echo "</div>";
				}

				if($vars->children == null) {
					echo "</div>";
					echo "<hr/>";
					continue;
				}

				echo "<div class=\"surround\">";
				echo "<ul>";
					foreach($vars->children as $key => $child) {

						if($child[1] == $c[0]) {
							// it is a child of this post
						echo "<li>";
							echo "<div class=\"comment-item child\">";

								echo "<a name=\"C{$child[0]}\"></a>";

								echo "<div class=\"child-header\">";
								if(isset($child[7])) {
									echo "<img src=\"{$child[8]}\"><br />";
									echo "{$child[7]}";
								}
								echo "</div>";

								echo "<div class=\"content\">";
									echo $child[4];
								if($child[2] != $c[0])
									echo "<div class=\"vfont-small\" style=\"margin-top: 10px;\">Replying to <a href=\"#C{$child[2]}\">this comment</a></div>";
								echo "</div>";


								echo "<div class=\"vfont-small post-details\">{$child[5]}</div>";

								if($child[0] == $vars->reply) {
									echo "<div class=\"post-details\">";
									echo "<a href=\"{$vars->_fallback->reply}\">cancel</a>";
									echo "</div>";

									echo "<div class=\"comment-reply\" style=\"clear: left;\">";
										echo "<form method=\"post\" action=\"{$vars->_fallback->comment}\">";
										echo "<textarea cols=\"55\" rows=\"7\" class=\"vform-text\" name=\"glsc\"></textarea>";
										echo"<input type=\"hidden\" value=\"{$c[0]}\" name=\"glsr\">";
										echo "<input type=\"hidden\" value=\"{$child[0]}\" name=\"glsp\"><br />";
										echo "<input type=\"hidden\" value=\"{$term[2]}\" name=\"glsterm\"><br />";
										echo "<input type=\"submit\" value=\"Comment\" class=\"vform-button vform-item\"/>";
										echo "</form>";
									echo "</div>";
								}
								else {
									echo "<div class=\"post-details\">";
									echo " <a href=\"{$vars->_fallback->reply}&glsd={$child[0]}\">reply</a>";
									echo "</div>";
								}

							echo "</div>";
						}

						echo "</li>";
					}

					echo "</ul>";

				echo "</div>";
			echo "</div>";
			echo "<hr />";
		}
	}
	else {
		echo "<b>No comments yet</b>";
	}

?>
</div>

<div style="color: #808080; margin-top: 30px;">
	<form method="post" action="<?php echo $vars->_fallback->comment; ?>">
		<textarea class="vform-text" cols="55" rows="7" name="glsc"></textarea><br />
		<input type="hidden" value="0" name="glsr">
		<input type="hidden" value="0" name="glsp">
		<input type="hidden" value="<?php echo $vars->term[2]; ?>" name="glsterm">
		<input type="submit" value="Comment" class="vform-button vform-item"/>
	</form>
</div>

<?php
}
?>
</div>

</div>
</div>
