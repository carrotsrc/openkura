<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include("types/qtypemanagerui.php");
	include("qtm_propui.php");
	class ok_quizman_uiPanel extends Panel
	{
		private $qar;
		private $sesd;
		private $tobj;
		private $header;
		private $sechdr;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_quizman_ui');
			$this->jsCommon = "OK_Quizman_UIInterface";
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->loadAllTypeObj();
			$this->setParams();
			$this->includeTemplate('template/main.php');
		}

		public function initialize($params = null)
		{
			$this->qar = null;
			$this->types = null;
			if($this->crud == 3) {
				if(isset($_GET['_ref']) && $_GET['_ref'] > 0) {
					// if this is set then we have just entered the mode
					// so need to load the session data
					$ref = $_GET['_ref'];
					$this->addComponentRequest(4, 101); // clear session

					if(Session::get('nodym') == 1) {
						$this->addComponentRequest(5, array (
										'qmref' => $ref
										));
					}
					else {
						$this->addComponentRequest(7, array (
										'qmref' => $ref
										));
					}
					
					unset($_GET['_ref']);
					$_GET['qmref'] = $ref; // switch it into normal editing mode
				}
			}
			else
			if($this->crud == 1) {
				if(isset($_GET['_ref']) && $_GET['_ref'] == 0) {
					$this->addComponentRequest(4, 101); // clear session
					unset($_GET['_ref']);
				}
			}
			$this->addComponentRequest(1, 101);	// get types
			$this->addComponentRequest(2, 101);	// load the session
			$this->addComponentRequest(3, 101);	// get question scheme
			$this->addComponentRequest(22, 101);	// get quiz header

			if(isset($_GET['qmsh']) && isset($_GET['qmedit']) &&
				$_GET['qmsh'] == 1 && ($id = $_GET['qmedit']) > 0) {
				$this->addComponentRequest(24, array('qmid' => $id)); // get section properties
				$this->addTParam('viewsec', true);
			}

			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					$this->addTParam('qtm', $rs['result']);
				break;
				
				case 2:
					$this->types = $rs['result'];
				break;

				case 3:
					$this->qar = $rs['result'];
				break;

				case 22:
					$this->header = $rs['result'];
				break;

				case 24:
					$this->sechdr = $rs['result'];
					$this->addTParam('sechdr', $this->sechdr);
				break;
				}
			}
		}

		public function fallback()
		{
			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/10&dbm-redirect=1";
			$this->addFallbackLink('submitaddq', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/11&dbm-redirect=1";
			$this->addFallbackLink('comms', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/21&dbm-redirect=1";
			$this->addFallbackLink('submitmodhdr', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/20&dbm-redirect=1";
			$this->addFallbackLink('submitaddp', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/23&dbm-redirect=1";
			$this->addFallbackLink('submitaddsp', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/25&dbm-redirect=1";
			$this->addFallbackLink('submitmodshdr', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/26&dbm-redirect=1";
			$this->addFallbackLink('remsp', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/27&dbm-redirect=1";
			$this->addFallbackLink('remhp', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/12&dbm-redirect=1";
			$this->addFallbackLink('rem', $qstr);

			$activity = $_GET['activity'];
			switch($this->crud) {
			case 1:
				$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/30&dbm-redirect=1&activity=$activity";
			break;

			case 3:
				$ref = $_GET['qmref'];
				$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/31&dbm-redirect=1&activity=$activity&qmref=$ref";
			break;
			}
			$this->addFallbackLink('sendRec', $qstr);

			$qstr = QStringModifier::modifyParams(array('qmedit' => null,
								'qmsh' => null));
			$this->addFallbackLink('edit', $qstr);
		}

		public function setAssets()
		{
			$this->addAsset('css', '/.assets/quizstyle.css');
			$this->addAsset('js', "/G/toolset.js");
			$this->addAsset('js', "template/quizman_sc.js");
		}
		
		public function setParams()
		{
			$this->addTParam('qar', $this->qar);
			$this->addTParam('tobj', $this->tobj);
			$this->addTParam('header', $this->header);

			$mpath = SystemConfig::appRelativePath("library/media/kura/quizman");
			$this->addTParam('media', $mpath);
			if(isset($_GET['qmedit']))
				$this->addTParam('edit', $_GET['qmedit']);
			$propui = new QTM_PropUI($mpath);
			$this->addTParam('propui', $propui);
		}

		private function loadAllTypeObj()
		{
			foreach($this->types as $k => $t) {
				$this->loadTypeObj($k);
				$this->tobj[$k]->setSession($t);
			}
		}
		private function loadTypeObj($type)
		{
			if(isset($this->tobj[$type]))
				return;

			include_once("types/$type/{$type}QTMUI.php");
			$str = "{$type}QTMUI";
			$obj = new $str();
			$this->tobj[$type] = $obj;
		}
	}
?>
