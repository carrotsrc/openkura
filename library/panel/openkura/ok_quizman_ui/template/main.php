<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="quizmanager">
<b>Quiz Manager</b><br />
<?php
	echo "<div class=\"quiz-header\" id=\"{$vars->_pmod}-quiz-header\">\n";
	if($vars->header != null) {
		echo "<div class=\"quiz-header-title\" >";
		echo "<div id=\"{$vars->_pmod}-quiz-header-title\" style=\"display: inline;\">";
		echo $vars->header['title'];
		echo "</div>";
		echo "<div class=\"quiz-tool\" style=\"margin-top: 6px;\">";
		if($vars->edit !== null && $vars->edit == 0) {
			echo "<a href=\"{$vars->_fallback->edit}\">";
			echo "<img src=\"{$vars->media}/unedit.png\" title=\"Stop editing\"/></a>";
		}
		else {
			echo "<a id=\"{$vars->_pmod}-uio_editheader\" href=\"{$vars->_fallback->edit}&qmedit=0\">";
			echo "<img src=\"{$vars->media}/edit.png\" title=\"Edit header\"/></a>";
		}
		echo "</div>";
		echo "</div>";
	}
	echo "<div id=\"{$vars->_pmod}-editheader-container\"></div>";
	// Edit quiz header
	if($vars->edit !== null && $vars->edit == 0) {
		echo "<div class=\"question-edit\" style=\"margin-top: 10px; padding: 10px;\">\n";
		echo "<div style=\"color: orange; font-weight: bold\">Edit Header</div>\n";

		if($vars->header != null) {
		// header edit form
		echo "<form method=\"post\" action=\"{$vars->_fallback->submitmodhdr}\">\n";
		echo "<div class=\"vform-item vfont-small\" style=\"padding: 5px; border-bottom: 1px solid #D8D8D8;\">\n";
			echo "Title:<br />";
			echo "<input type=\"text\" name=\"qmhp_title\" class=\"vform-text vfont-large\" value=\"{$vars->header['title']}\" autocomplete=\"off\"/>";
		echo "</div>";
		// extra properties
		foreach($vars->header as $prop => $val) {
			if($prop == "title")
				continue;
			echo "<div class=\"vform-item vfont-small\" style=\"border-bottom: 1px solid #D8D8D8; padding: 5px;\">";
			$rlink = $vars->_fallback->remhp;
			echo $vars->propui->generateForm('qmhp', $prop, $val, $rlink);
			echo "</div>";
		}
		echo "<div style=\"margin-top: 15px;\">";
			// store button
			echo "<input type=\"submit\" class=\"vform-item vfloat-right vform-button\" value=\"Store\" />";
			echo "</form>";
		}
			// add new property form
			echo "<form class=\"vfloat-left\" action=\"{$vars->_fallback->submitaddp}\" method=\"post\">";
			echo "Add ";
			echo "<select name=\"qmr-nprop\" class=\"vform-text\">";
			echo "<option value=\"grade_boundry\">grade boundry</option>";
			echo "<option value=\"text\">text</option>";
			echo "<option value=\"repeat\">repeat</option>";
			echo "</select> property ";
			echo "<input type=\"submit\" class=\"vform-item vform-button\" value=\"Go\" />";
			echo "</form>";
		echo "</div>";
		echo "</div>\n";
	}
?>
</div>
<?php
	echo "<div class=\"question-area\" id=\"{$vars->_pmod}-question-area\">";
	if($vars->qar == null)
		echo "No questions defined";
	else {
		$i = 1;
		$sz = sizeof($vars->qar);
		foreach($vars->qar as $q) {
			echo "<div class=\"question-title\">";
				echo "Question $i (".str_replace("_", " ", $q[1]).")";
				if($q[0] != $vars->edit) {

					echo "<div class=\"quiz-tool\">";
					echo "<a href=\"{$vars->_fallback->edit}&qmedit={$q[0]}\">";
					echo "<img src=\"{$vars->media}/edit.png\" title=\"Edit question {$q[0]}\"/></a></div>";

					echo "<div class=\"quiz-tool\">";
					echo "<img src=\"{$vars->media}/editsec-disabled.png\" title=\"Edit section header (disabled)\" /></div>";

					echo "<div class=\"quiz-tool\">";
					echo "<img src=\"{$vars->media}/delete-disabled.png\" title=\"Delete question (disabled)\" /></div>";
				}
				else {
					echo "<div class=\"quiz-tool\">";
					echo "<a href=\"{$vars->_fallback->edit}\">";
					echo "<img src=\"{$vars->media}/unedit.png\" title=\"Stop editing\" /></a></div>";

					if($vars->viewsec == true) {
						echo "<div class=\"quiz-tool\">";
						echo "<a href=\"{$vars->_fallback->edit}&qmedit={$q[0]}\">";
						echo "<img src=\"{$vars->media}/uneditsec.png\" title=\"Stop editing\" /></a></div>";
					}
					else {
						echo "<div class=\"quiz-tool\">";
						echo "<a href=\"{$vars->_fallback->edit}&qmedit={$q[0]}&qmsh=1\">";
						echo "<img src=\"{$vars->media}/editsec.png\" title=\"Edit section header\" /></a></div>";
					}

					echo "<div class=\"quiz-tool\">";
					echo "<a href=\"{$vars->_fallback->rem}&qmid={$q[0]}\">";
					echo "<img src=\"{$vars->media}/delete.png\" title=\"Delete question {$q[0]}\"/></a></div>";
				}
			echo "</div>";
			echo "<div class=\"quiz-item\">";
			if($q[0] == $vars->edit) {

				// edit the section header
				if($vars->viewsec != null && $vars->viewsec == true) {
					echo "<div class=\"question-edit\" style=\"margin-bottom: 15px;\">";
					echo "<div class=\"question-edit-form\">";
					echo "<div style=\"color: orange; font-weight: bold;\">Edit Header</div>";
					echo "<div class=\"vform-item\">";
					echo "<form method=\"post\" action=\"{$vars->_fallback->submitmodshdr}&qmid={$q[0]}\">";
						if($vars->sechdr != 104) {
							foreach($vars->sechdr as $prop => $val) {
								$rlink = $vars->_fallback->remsp."&qmid={$q[0]}";
								echo "<div class=\"vform-item vfont-small\" style=\"padding: 5px; border-bottom: 1px solid #D8D8D8;\">";
								echo $vars->propui->generateForm('qmsp', $prop, $val, $rlink);
								echo "</div>";
							}
						}
						else
							echo "Nothing defined in this section header";
						echo "<div style=\"margin-top: 15px\">";
						// store button
						echo "<input type=\"submit\" class=\"vform-item vfloat-right vform-button\" value=\"Store\" />";
						echo "</form>";

						// add now property form
						echo "<div class=\"vform-item\" style=\"margin-top: 15px;\">";
						echo "<form class=\"vfloat-left\" action=\"{$vars->_fallback->submitaddsp}&qmid={$q[0]}\" method=\"post\">";
						echo "Add ";
						echo "<select name=\"qmr-nprop\" class=\"vform-text\">";
							echo "<option value=\"mark\">mark</option>";
							echo "<option value=\"text\">text</option>";
						echo "</select> property ";
						echo "<input type=\"submit\" class=\"vform-item vform-button\" value=\"Go\" />";
						echo "</form>";
						echo "</div>";
						echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}

				$submitstr = $vars->_fallback->comms."&qmid={$q[0]}";
				$vars->tobj[$q[1]]->setStaticSubmit($submitstr);
				$vars->tobj[$q[1]]->setMediaPath($vars->media);
				echo "<div class=\"question-edit\">";
				echo "<div class=\"question-edit-form\">";
					echo $vars->tobj[$q[1]]->loadForm($q[0]);
				echo "</div>";

				echo "</div>";
			}
			else {
				
				echo "<div class=\"question-normal\">";
				echo $vars->tobj[$q[1]]->loadRep($q[0]);
				echo "</div>";
			}
			echo "</div>";

			if($i !=  $sz) {
				echo "<div class=\"question-gap\"></div>";
			}
			$i++;
		}
	}
?>
</div>
<div class="vform-item vfloat-left" id="<?php echo $vars->_pmod; ?>-addSectionForm">
	<form action="<?php echo $vars->_fallback->submitaddq; ?>" method="post">
	Add 
		<select name="qmtype" class="vform-text" id="<?php echo $vars->_pmod; ?>-sectionType">
			<?php
				foreach($vars->qtm as $t)
				echo "<option value=\"{$t[1]}\">{$t[0]}</option>\n";
			?>
		</select>
	question 
	<input type="submit" value="Go" class="vform-button" id="<?php echo $vars->_pmod; ?>-addSection"/>
	</form>
</div>
<div class="vfloat-right" id="<?php echo $vars->_pmod; ?>-save-form">
	<form action="<?php echo $vars->_fallback->sendRec; ?>" method="post">
	<input type="submit" value="Save" class="vform-button"/>
	</form>
</div>
</div>
