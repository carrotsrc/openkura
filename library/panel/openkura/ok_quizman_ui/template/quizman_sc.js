/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function GenProp (type, id, value) {
	this.type = type;
	this.id = id;
	this.value = value;
}
/*
 *  Template for a record, same
 *  as server side
 */
function RecordTemplate(prefixStr) {
	var self = this;
	this.media = "APP-MEDIAkura/quizman";
	this.header = Array();
	this.sectionHeader = Array();
	this.qar = Array();
	this.sechdr = Array();
	this.header = Array(new GenProp("title", undefined, "New Quiz"));
	this.types = Array();
	this.headerUpdate = null;
	this.refreshProperties = null;
	this.refreshQuestions = null;
	this.prefix = prefixStr;

	this.addSection = function (type, mode, noRefresh) {
		var qi = self.qar.length;
		if(mode === undefined)
			mode = 1;
		qi+=1;


		var sz = this.types.length;
		var loaded = false;
		var index = 0
		for(index = 0; index < sz; index++)
			if(self.types[index][0] == type) {
				loaded = true;
				break;
			}

		if(!loaded)
			self.types.push(Array(type, new window[type+"QTM"](self.prefix, self.media)));


		self.qar.push(Array(index, mode));
		self.sectionHeader.push(Array(index, 0, Array()));
		self.types[index][1].addSection(qi);
		var qi = self.qar.length;
		if(noRefresh === undefined)
			self.refreshQuestions();

		return self.qar.length-1;
	}

	this.nextId = function (prefix, arr) {
		var sz = arr.length;
		var id = 0;
		var ex = true;
		while(ex) {
			id++;
			ex = false;
			for(var i = 0; i < sz; i++) {
				if(arr[i].type == prefix && arr[i].id === id)
					ex = true;
			}
		}

		return id;
	}

	this.addHeaderProperty = function (type) {
		var id = this.nextId(type, this.header);
		this.header.push(new GenProp(type, id, ""));
		return this.header.length-1;
	}

	this.removeHeaderProperty = function (index) {
		this.header.splice(index,1);
		this.refreshProperties();
	}

	this.setHeaderProperty = function (type, id, value) {
		var i = this.getPropertyIndex(type, id);
		if(i === undefined)
			i = this.addHeaderProperty(type, id);
		this.header[i].value = value;
		return true;
	}

	this.getHeaderValue = function (type, id) {
		var i = this.getPropertyIndex(type, id);
		if(i === undefined)
			return undefined;

		return this.header[i].value;
	}

	this.addSectionHeaderProperty = function (index, type, value, noRefresh) {
		var id = self.nextId(type, self.sectionHeader[index][2]);
		if(value === undefined)
			value = "";
		self.sectionHeader[index][2].push(new GenProp(type, id, value));

		if(noRefresh === undefined);
			self.refreshQuestions();
	}

	this.getPropertyIndex = function (type, id) {
		var sz = this.header.length;
		for(var i = 0; i < sz; i++) {
			if(this.header[i].type == type && this.header[i].id === id)
				return i;
		}

		return undefined;
	}

	this.uio_headerProperties = function (u) {
		var sz = this.header.length;
		for(var i = 0; i< sz; i++) {
			u.appendChild('div');
				u.gotoLast();
				u.node.style="padding: 5px; border-bottom: 1px solid #d8d8d8; margin-top: 7px;";
				this.uio_headerProperty(i, u);
				u.gotoParent();
		}
	}

	this.uio_headerProperty = function (index, u) {
		var prop = this.header[index];
		switch(prop.type) {
		case "title":
			this.ui_titleProperty(index, prop.value, u);
		break;

		case "text":
			this.ui_textProperty(index, prop.id, prop.value, u);
		break;

		case "repeat":
			this.ui_repeatProperty(index, prop.value, u);
		break;
		}
	}

	this.ui_textProperty = function (index, id, value, u) {
		u.appendChild('img');
			u.gotoLast();
			u.node.src = this.media+"/delete.png";
			u.node.style = "float: right; cursor: pointer;";
			u.node.hindex = index;
			u.node.onclick = function () {
				self.removeHeaderProperty(this.hindex);
			}
			u.gotoParent();
		u.appendText("Text (text-"+id+")");
		u.appendChild('br');
		u.appendChild('textarea');
			u.gotoLast();
			u.node.className = "vform-text";
			u.node.cols = "50";
			u.node.rows = "7";
			var value = this.getHeaderValue("text", id);
			u.appendText(value);
			u.node.hindex = index;
			u.node.onkeyup = function () {
				self.header[this.hindex].value = this.value;
			}
			u.gotoParent(); // r
	}

	this.ui_titleProperty = function (index, value, u) {
		u.appendText("Title:");
		u.appendChild('br');
		u.appendChild('input');
			u.gotoLast();
			u.node.className = "vform-text";
			u.node.value = this.getHeaderValue("title");
			u.node.hindex = index;
			u.node.onkeyup = function () {
				self.header[this.hindex].value = this.value;
				self.headerUpdate();
			}
			u.gotoParent(); // r
	}

	this.ui_repeatProperty = function (index, value, u) {
		u.appendChild('img');
			u.gotoLast();
			u.node.src = this.media+"/delete.png";
			u.node.style = "float: right; cursor: pointer;";
			u.node.hindex = index;
			u.node.onclick = function () {
				self.removeHeaderProperty(this.hindex);
			}
			u.gotoParent();
		u.appendText("Repeat (repeat):");
		u.appendChild('br');
		u.appendText("yes");
	}

	this.uio_sections = function (u) {
		var sz = this.qar.length;
		if(sz == 0) {
			u.appendText("No questions defined");
			return;
		}

		for(var i = 0; i < sz; i++) {
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "question-title";
				var s = self.types[self.qar[i][0]];
				u.appendText("Question " + (i+1) + " ("+s[0]+")");
				u.appendChild('div');
					u.gotoLast();
					u.node.style="float: right;";
					if(self.qar[i][1] == 1) {

						u.appendChild("a");
							u.gotoLast();
							u.node.qid = i;
							u.node.onclick = function () {
								if(self.sectionHeader[this.qid][1] == 0)
									self.sectionHeader[this.qid][1] = 1;
								else
									self.sectionHeader[this.qid][1] = 0;

								self.refreshQuestions();
							}
							u.appendChild('img');
								u.gotoLast();
								if(self.sectionHeader[i][1] == 0) {
									u.node.src = self.media+"/editsec.png";
									u.node.title="Edit section header"
								}
								else {
									u.node.src = self.media+"/uneditsec.png";
									u.node.title="Stop editing section header"
								}

								u.gotoParent();
							u.gotoParent();

						u.appendChild("a");
							u.gotoLast();
							u.node.qid = i;
							u.node.style = "margin-left: 5px;";
							u.node.onclick = function () {
								self.qar[this.qid][1] = 0;
								self.refreshQuestions();
							}
							u.appendChild('img');
								u.gotoLast();
								u.node.src = self.media+"/unedit.png";
								u.node.title="Stop editing"
								u.gotoParent();
							u.gotoParent();
					}
					else {

						u.appendChild('img');
							u.gotoLast();
								u.node.src = self.media+"/editsec-disabled.png";
								u.node.title="Edit section header (disabled)"
							u.gotoParent();

						u.appendChild("a");
							u.gotoLast();
							u.node.style = "margin-left: 5px;";
							u.node.qid = i;
							u.node.onclick = function () {
								self.qar[this.qid][1] = 1;
								self.refreshQuestions();
							}
							u.appendChild('img');
								u.gotoLast();
								u.node.src = self.media+"/edit.png";
								u.node.title="Edit question "+(i+1);
								u.gotoParent();
							u.gotoParent();

					}
					u.gotoParent();
				u.gotoParent();
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "quiz-item";


				if(self.qar[i][1] == 1) {
					if(self.sectionHeader[i][1] == 1)
						self.uio_sectionHeader(i, u);
					s[1].uio_editQuestion(i+1, u);
				}
				else
					s[1].uio_viewQuestion(i+1, u);
				u.gotoParent();
		}
	}

	this.uio_sectionHeader = function (id, u) {
		u.appendChild('div');
			u.gotoLast();
			u.node.className="question-edit";
			u.node.style="margin-bottom: 15px;";
			u.appendChild('div');
				u.gotoLast();
				u.node.className="question-edit-form";
				u.appendChild("div")
					u.gotoLast();
					u.node.style = "color: #FFA500; font-weight: bold;"
					u.appendText("Edit Header");
					u.gotoParent();

				var sz = self.sectionHeader[id][2].length;
				for(var i = 0; i < sz; i++) {
					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vfont-small";
						u.node.style="padding: 5px; border-bottom: 1px solid #D8D8D8;";
						u.appendText(self.sectionHeader[id][2][i].type+" ("+self.sectionHeader[id][2][i].type+"-"+self.sectionHeader[id][2][i].id+"): ");
						switch(self.sectionHeader[id][2][i].type) {
						case 'text':
							u.appendChild('br');
							u.appendChild('textarea');
								u.gotoLast();
								u.node.className = "vform-text";
								u.node.rows = "7";
								u.node.cols = "50";
								u.node.sid = id;
								u.node.pid = i;
								u.node.value = self.sectionHeader[id][2][i].value;
								u.node.onkeyup = function () {
									self.sectionHeader[this.sid][2][this.pid].value = this.value;
								}
								u.gotoParent();
							u.gotoParent();
						break;
						case 'mark':
							u.appendChild('input');
								u.gotoLast();
								u.node.className = "vform-text";
								u.node.style = "width: 35px;";
								u.node.sid = id;
								u.node.pid = i;
								u.node.value = self.sectionHeader[id][2][i].value;
								u.node.onkeyup = function () {
									self.sectionHeader[this.sid][2][this.pid].value = this.value;
								}
								u.gotoParent();
							u.gotoParent();

						break;
						}
				}

				u.appendChild('div');
					u.gotoLast();
					u.node.className="vform-item-spacer";
					u.appendText("Add ");
					u.appendChild('select');
						u.gotoLast();
						u.node.className = "vform-text vform-select";
						u.node.id = this.prefix+"-sprop"+id;
						u.appendChild('option');
							u.gotoLast();
							u.node.value = "mark";
							u.appendText("mark");
							u.gotoParent();

						u.appendChild('option');
							u.gotoLast();
							u.node.value = "text";
							u.appendText("text");
							u.gotoParent();
						u.gotoParent();
					u.appendText(" property ");
					u.appendChild('input');
						u.gotoLast();
						u.node.className="vform-button";
						u.node.type = "button";
						u.node.value = "Go";
						u.node.sid = id;
						u.node.onclick = function () {
							var e = document.getElementById(self.prefix+"-sprop"+id);
							self.addSectionHeaderProperty(this.sid, e.value);
						}
						u.gotoParent();

					u.gotoParent();
				u.gotoParent();
			u.gotoParent();
	}

	this.generateTemplate = function () {
		var xml = "<rtemplate>\n";
		xml += "<header>\n";
		var sz = self.header.length;

		// template header properties
		for(var i = 0; i < sz; i++)
			switch(self.header[i].type) {
			case 'title':
				xml += "<title>"+self.header[i].value+"</title>\n";
			break;

			case 'text':
				xml += "<text id=\""+self.header[i].id+"\">"+self.header[i].value+"</text>\n";
			break;

			case 'repeat':
				xml += "<repeat value=\""+self.header[i].value+"\" />\n";
			break;
			}
		xml += "</header>\n";

		var sz = self.qar.length;
		var s = 1;
		for(var sid = 0; sid < sz; sid++) {

			xml += "<section id=\""+s+"\">\n";

			// Section header properties
			if(self.sectionHeader[sid].length > 0) {
				var sy = self.sectionHeader[sid][2].length;

				for(var j = 0; j < sy; j++)
					switch(self.sectionHeader[sid][2][j].type) {
					case 'text':
						xml += "<text id=\""+self.sectionHeader[sid][2][j].id+"\">"+self.sectionHeader[sid][2][j].value+"</text>\n";
					break;

					case 'mark':
						xml += "<mark id=\""+self.sectionHeader[sid][2][j].id+"\" value=\""+self.sectionHeader[sid][2][j].value+"\" />\n";
					break;
					}
			}
			xml += "<obj type=\""+self.types[self.qar[sid][0]][0]+"\">\n";
			xml += self.types[self.qar[sid][0]][1].generateTemplate(sid+1);
			xml += "</obj>\n";
			xml += "</section>\n";
			s++;
		}
		xml +="</rtemplate>";

		return xml;
	}

	processHeader = function (t) {
		var sz = t.length;

		for(var i = 0; i < sz; i++) {
			switch(t[i].nodeName){
			case '#text':
			break;
			case 'title':
				self.setHeaderProperty('title', undefined, t[i].firstChild.nodeValue);
			break;
			case 'repeat':
				self.setHeaderProperty('repeat', t[i].getAttribute('id'), t[i].getAttribute('id'));
			break;
			case 'text':
				self.setHeaderProperty('text', t[i].getAttribute('id'), t[i].firstChild.nodeValue);
			break;
			}
		}
	}

	processSection = function (t) {
		var type = undefined;
		var sz = t.length;
		for(var i = 0; i < sz; i++)
			if(t[i].nodeName == 'obj')
				type = t[i].getAttribute('type');

		if(type === undefined)
			return;

		var index = self.addSection(type, 0, true);

		for(var i = 0; i < sz; i++) {
			switch(t[i].nodeName) {
			case 'text':
				self.addSectionProperty(index, 'text', t[i].firstChild.nodeValue, true);
			break;

			case 'mark':
				self.addSectionProperty(index, 'mark', t[i].getAttribute('value'), true);
			break;

			case 'obj':
				var r = self.types[self.qar[index][0]][1];
				r.processTemplate(t[i].childNodes, index+1);
			break;
			}
		}

	}

	this.processTemplate = function (t) {
		var sz = t.length;

		for(var i = 0; i < sz; i++) {
			switch(t[i].nodeName){
			case 'header':
				processHeader(t[i].childNodes);
			break;

			case 'section':
				processSection(t[i].childNodes);
			break;
			}

			if(t[i].hasChildNodes())
				this.processTemplate(t[i].childNodes);
		}
	}
}

function OK_Quizman_UIInterface() {
	var self = this;
	this.termid = null;
	this.termnid = null;
	this.rectemplate = null;
	this.media = "APP-MEDIAkura/scripts/records";
	this.types = Array();
	this.quizId = null;

	this.initialize = function () {
		this.rectemplate = new RecordTemplate(self._loop._pmod);
		this.rectemplate.headerUpdate = self._loop.uiu_header;
		this.rectemplate.refreshProperties = self._loop.uiu_editHeader;
		this.rectemplate.refreshQuestions = self._loop.uiu_sections;
		this.modifyLink("uio_editheader", this.uio_editHeader);
		this.requestTypes();

		var n = document.getElementById(self._pmod+"-addSection");
		n.onclick = function () {
			return false;
		}
		n = document.getElementById(self._pmod+"-save-form");
		var u = KTSet.NodeUtl(n);
		u.clearChildren();
		u.appendChild('input');
			u.gotoLast();
			u.node.className = "vform-button";
			u.node.value = "Save";
			u.node.type = "button";
			u.node.onclick = function () {
				self._loop.generateTemplate();
			}
	}

	this.requestTypes = function () {
		self._loop.request(1,null,self._loop.onresponseTypes);
	}

	this.onresponseTypes = function (reply) {
		var response = JSON.parse(reply);
		var sz = response.data.length;
		for(var i = 0; i < sz; i++) {
			self.types.push(Array( response.data[i].value, response.data[i].id));
			self.requestManagerScript(response.data[i].id);
		}

		self.uiu_addSection();
		self.requestTemplate();
	}

	this.requestTemplate = function () {
		self._loop.request(33, null, self._loop.onresponseTemplate);
	}

	this.onresponseTemplate = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 102) {
			self._loop.quizId = response.data.id;
			self._loop.processTemplate(response.data.template);
		}
	}

	this.uio_editHeader = function (data) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		var rt = self.rectemplate;

		u.appendChild('div');
			u.gotoLast();
			u.node.className="question-edit"
			u.node.id = self._loop._pmod+"-edit-header";
			u.node.style = "margin-top: 10px; padding: 10px;";
			u.appendChild('div')
				u.gotoLast();
				u.node.style="color: #FFA500; font-weight: bold;";
				u.appendText("Edit Header");
				u.gotoParent(); // edit-header
			u.appendChild('div');
				u.gotoLast();
				u.node.id = self._loop._pmod+"-header-properties";
				rt.uio_headerProperties(u);
				u.gotoParent();

			u.appendChild('div');
				u.gotoLast();
				u.node.className = "vform-left vform-item";
				u.appendText("Add ");
				u.appendChild('select');
					u.gotoLast();
					u.node.id=self._loop._pmod+"-qmr-nprop";
					u.node.className="vform-text vform-select";
					u.appendChild('option');
						u.gotoLast();
						u.appendText("text");
						u.node.value="text";
						u.gotoParent(); // select
					u.appendChild('option');
						u.gotoLast();
						u.appendText("repeat");
						u.node.value="repeat";
						u.gotoParent(); // select
					u.gotoParent(); // add prop div
				u.appendText(" property ");
				u.appendChild('input');
					u.gotoLast();
					u.node.type="button";
					u.node.value="Go";
					u.node.className = "vform-button";
					u.node.onclick = function () {
						var e = document.getElementById(self._loop._pmod+"-qmr-nprop");
						var rt = self.rectemplate;
						rt.addHeaderProperty(e.value);
						self._loop.uiu_editHeader();
					}
					u.gotoParent()// add prop div
				u.appendChild('input');
					u.gotoLast();
					u.node.type="button";
					u.node.value="Store";
					u.node.className = "vfloat-right vform-button";
					u.gotoParent()// add prop div
				u.gotoParent() // edit-header
			u.gotoParent(); // r

		var c = document.getElementById(self._loop._pmod+"-editheader-container");
		u = KTSet.NodeUtl(c);
		c.appendChild(r);
		self.modifyLink("uio_editheader", self._loop.uic_editHeader);
	}

	this.uic_editHeader = function () {
		var c = document.getElementById(self._loop._pmod+"-editheader-container");
		var u = KTSet.NodeUtl(c);
		u.clearChildren();
		self.modifyLink("uio_editheader", self._loop.uio_editHeader);
	}

	this.uiu_editHeader = function () {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		self.rectemplate.uio_headerProperties(u);

		var c = document.getElementById(self._loop._pmod+"-header-properties");
		u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.appendChild(r);
	}

	this.uiu_header = function () {
		var rt = self.rectemplate;
		var title = rt.getHeaderValue("title", undefined);
		var c = document.getElementById(self._loop._pmod+"-quiz-header-title");
		var u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.appendText(title);
	}

	this.uiu_sections = function () {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		self.rectemplate.uio_sections(u);

		var c = document.getElementById(self._loop._pmod+"-question-area");
		u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.appendChild(r);
	}


	this.addSection = function () {
		var type = document.getElementById(self._loop._pmod+"-sectionType").value;
		self.rectemplate.addSection(type);
	}

	this.uiu_addSection = function () {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		u.appendText("Add ");
		u.appendChild('select');
			u.gotoLast();
			u.node.className = "vform-text vform-select";
			u.node.id = self._loop._pmod+"-sectionType";
			var sz = self.types.length;
			for(var i = 0; i < sz; i++) {
				u.appendChild('option');
					u.gotoLast();
					u.node.value = self.types[i][1];
					u.appendText(self.types[i][0]);
					u.gotoParent();// select
			}
			u.gotoParent(); // r
		u.appendText(" question  ");
			u.appendChild('input');
				u.gotoLast();
				u.node.className = "vform-button";
				u.node.type = "button";
				u.node.value = "Go";
				u.node.onclick = self._loop.addSection;
				u.gotoParent(); // r

		var c = document.getElementById(self._loop._pmod+"-addSectionForm");
		u = KTSet.NodeUtl(c);
		u.clearChildren();
		u.appendChild(r);
	}
	/*
	 * Do not use embedding script within this method because
	 * of latency 
	 */
	this.requestManagerScript = function(type) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		u.appendChild('script');
			u.gotoLast();
			u.node.type = "text/javascript";
			u.node.src = self._loop.media+"/"+type+"/"+type+"QTM.js";
			u.gotoParent();

		var c = document.getElementsByTagName("head");
		c = c[0];
		u = KTSet.NodeUtl(c);
		u.appendChild(r);
	}

	this.generateTemplate = function () {
		var postStr = "qmtemplate="+self.rectemplate.generateTemplate();
		if(self._loop.quizId !== null)
			postStr +="&qmid="+self._loop.quizId;
		self._loop.request(32, null, self._loop.onresponseGenerateTemplate, postStr);
	}

	this.onresponseGenerateTemplate = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 102) {
			self._loop.quizId = response.data;
		}
	}

	this.processTemplate = function (template) {
		var tdoc = undefined;
		
		if(window.DOMParser) {
			var xpar = new DOMParser();
			tdoc = xpar.parseFromString(template, "text/xml");
		}	
		else {
			// internet explorer
			tdoc = new ActiveXObject("Microsoft.XMLDOM");
			tdoc.async=false;
			tdoc.loadXML(template);
		}

		self.rectemplate.processTemplate(tdoc.childNodes);
		self.uiu_header();
		self.uiu_sections();
	}

}
OK_Quizman_UIInterface.prototype = new KitJS.PanelInterface('ok_quizman_ui');
