<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class multiple_choiceQTMUI extends QTypeManagerUI
	{
		protected $session = array();
		public function loadForm($id)
		{
			ob_start();
			echo "<div style=\"color: orange; font-weight: bold;\" class=\"vfloat-left\">Edit</div><br />";

			// question form
			echo "<form action=\"{$this->staticSubmit}\" method=\"post\">\n";
			echo "<div class=\"vform-item vfont-small\">";
			echo "Question:<br />";
			echo "\t<input type=\"hidden\" name=\"op\" value=\"1\">\n";
			echo "\t<input type=\"text\" class=\"vform-text vfont-large\" style=\"width: 100%\" name=\"qtmuiq0\"";
			if(isset($this->session[$id])) {
				if(isset($this->session[$id][0])) {
					$content = StrSan::mysqlDesanatize($this->session[$id][0]);
					$content = StrSan::htmlSanatize($content);
					echo " value=\"$content\"";
				}
			}
			echo " autocomplete=\"off\">\n";
			echo "</div>\n";

			// answers
			echo "<div style=\"margin-left: 15px; margin-top: 15px;\">";

			if(isset($this->session[$id]) && sizeof($this->session[$id]) > 1) {
				$i = 1;
				foreach($this->session[$id] as $k => $f) {
					if($k == 0)
						continue;

					echo "<div style=\"margin-top: 10px\">\n";
					echo "$i. ";
					
					// text
					$content = StrSan::mysqlDesanatize($f[0]);
					$content = StrSan::htmlSanatize($content);
					echo "<input type=\"text\" class=\"vform-text\" ";
					echo "value=\"$content\" style=\"width: 75%;\" name=\"qtmuiq$k\" autocomplete=\"off\" />";
					
					// correct
					echo "<input type=\"checkbox\" class=\"vform-text\" name=\"qtmuic$k\" ";
					if($f[1] == 1)
						echo "checked";
					echo "/>";

					if(!isset($f['d'])) {
						echo " <a href=\"{$this->staticSubmit}&op=4&qtmuiq=$k\" title=\"Add description\"><img src=\"{$this->media}/editsec.png\" /></a>";
					}

					echo " <a href=\"{$this->staticSubmit}&op=3&qtmuiq=$k\"><img src=\"{$this->media}/delete.png\" /></a>";

					if(isset($f['d'])) {
						$content = StrSan::mysqlDesanatize($f['d']);
						$content = StrSan::htmlSanatize($content);
						echo "<textarea class=\"vform-text\" name=\"qtmuid$k\" rows=\"2\" style=\"width: 75%; margin-left: 15px;\">$content</textarea>";
					}

					// remove
					echo "</div>\n";

					$i++;
				}
			}
			echo "</div>";

			echo "<div style=\"height: 15px\"></div>\n";

			// store button
			echo "<div class=\"vfloat-right\">\n";
			echo "<input type=\"submit\" value=\"Store\" class=\"vform-button vfloat-right vform-item\"><br />\n";
			echo "</div>\n";
			echo "</form>\n";

			// add questions button
			echo "<div class=\"vfloat-left margin-form-reverse vform-item\">";
			echo "<form action=\"{$this->staticSubmit}\" method=\"post\">\n";
			echo "\t<input type=\"hidden\" name=\"op\" value=\"2\">\n";
			echo "Add <input type=\"text\" value=\"4\" class=\"vform-text\" style=\"width: 35px\" name=\"qmanum\"> ";
			echo "Answers <input type=\"submit\" value=\"Go\" class=\"vform-button\">\n";
			echo "</form>\n";
			echo "</div>\n";
			$form = ob_get_contents();
			ob_end_clean();
			return $form;
		}

		public function loadRep($id)
		{
			ob_start();
			if(sizeof($this->session) == 0)
				echo "This question is undefined";
			else {
				if(!isset($this->session[$id]) || sizeof($this->session[$id]) == 0) {
					echo "- This question is undefined -";
				}
				else {
					$i = 1;
					foreach($this->session[$id] as $k => $v) {
						if($k == 0) {
							$content = StrSan::mysqlDesanatize($v);
							$content = StrSan::htmlSanatize($content);
							$content = StrSan::sqhtmlSanatize($content);
							echo "<div class=\"vfont-large\">";
							echo "$content";
							echo "</div>";
						}
						else {
							if($v[1] == 1)
								echo "<div style=\"margin-left: 15px; margin-top: 7px; text-decoration: underline;\">";
							else
								echo "<div style=\"margin-left: 15px; margin-top: 7px;\">";
							
							$content = StrSan::mysqlDesanatize($v[0]);
							$content = StrSan::htmlSanatize($content);
							$content = StrSan::sqhtmlSanatize($content);
							echo "$i. {$content}";
							echo "</div>";
							$i++;
						}
					}
				}
			}

			$rep = ob_get_contents();
			ob_end_clean();
			return $rep;
		}

		public function setSession($data)
		{
			$this->session = $data;
		}
	}
?>
