<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	abstract class QTypeManagerUI
	{
		protected $session;
		protected $staticSubmit;
		protected $media;
		public abstract function loadForm($id);
		public abstract function loadRep($id);
		public abstract function setSession($data);

		public function setStaticSubmit($value)
		{
			$this->staticSubmit = $value;
		}

		public function setMediaPath($value)
		{
			$this->media = $value;
		}
	}
?>
