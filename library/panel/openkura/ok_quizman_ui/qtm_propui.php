<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class QTM_PropUI
	{
		private $media;
		public function __construct($mlink)
		{
			$this->media = $mlink;
		}
		public function generateForm($prefix, $prop, $val, $remlink)
		{
			$pn = $prop;
			if(($p = strpos($prop, "-")))
				$pn = substr($prop, 0, $p);

			switch($pn) {
			case 'text':
				return $this->text($prefix, $prop, $val, $remlink);
			break;

			case 'mark':
				return $this->mark($prefix, $prop, $val, $remlink);
			break;

			case 'repeat':
				return $this->repeat($prefix, $prop, $val, $remlink);
			break;
			}
		}

		public function mark($prefix, $prop, $val, $remlink)
		{
			ob_start();
			
			echo "Marks ($prop): ";
			echo "<input type=\"text\" class=\"vform-text\" name=\"{$prefix}_{$prop}\" style=\"width: 40px;\" value=\"$val\" autocomplete=\"off\" />";
			echo " <a style=\"margin-top: 7px;\" class=\"vfloat-right\" href=\"$remlink&qmpid=$prop\"><img src=\"{$this->media}/delete.png\" /></a>";
			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}

		public function text($prefix, $prop, $val, $remlink)
		{
			ob_start();

			echo " <a style=\"\" class=\"vfloat-right\" href=\"$remlink&qmpid=$prop\"><img src=\"{$this->media}/delete.png\" /></a>";
			echo "Text ($prop):<br />";
			echo "<textarea class=\"vform-text\" name=\"{$prefix}_{$prop}\" rows=\"7\" cols=\"50\">$val</textarea>";
			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}

		public function repeat($prefix, $prop, $val, $remlink)
		{
			ob_start();

			echo "<a style=\"margin-top: 7px;\" class=\"vfloat-right\" href=\"$remlink&qmpid=$prop\"><img src=\"{$this->media}/delete.png\" /></a>";
			echo "Repeat ($prop): <br />";
			echo "Yes<br/>";
			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}
	}
?>
