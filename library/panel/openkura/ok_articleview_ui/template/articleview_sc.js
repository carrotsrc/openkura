/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function OK_ArticleViewInterface() {

	var self = this;
	var clib = new comments_sc(self, 'art');
	this.initialize = function () {
		clib.requestParents();
	}


}

OK_ArticleViewInterface.prototype = new KitJS.PanelInterface('ok_articleview_ui');

