<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>
<div style="color: #3FA1F1; font-size: 20px; font-weight: bold; margin-bottom: 15px;">
	Article Activity

	<div style="font-weight: normal;">
		<?php echo date("d/m/Y"); ?>
	</div>
</div>

<div style="color: #808080; font-size: 35px; font-family: 'times new roman'; font-weight: bold;">
	<?php echo $vars->title; ?>
</div>

<div style="color: #808080; font-size: 18px; font-family: 'times new roman'; margin-top: 15px;">
<?php 
	$content = StrSan::mysqlDesanatize($vars->content);
	$content = StrSan::htmlSanatize($content);
	$content = StrSan::sqHtmlSanatize($content);
	echo $content;
?>
</div>
<div style="margin-top: 30px; color: #679B00; font-size: 20px; font-weight: bold; border-bottom: 1px solid #679B00;">
Discussion
</div>
<div id="<?php echo $vars->_pmod; ?>-comments" style="margin-top: 30px;">
	
</div>
