<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
  class ok_articleview_uiPanel extends Panel
	{
		private $style;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_articleview_ui');
			$this->jsCommon = "OK_ArticleViewInterface";
		}

		public function loadTemplate()
		{
			$this->includeTemplate("template/{$this->style}/main.php");
		}

		public function initialize($params = null)
		{
			$this->addComponentRequest(1, 101);
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				$crs = $rs['result'];
				switch($rs['jack']) {
				case 1:
					$this->addTParam('title', $crs[1]);
					$this->addTParam('content', $crs[2]);
					$this->style = $crs[4];
				break;

				}
			}
		}

		public function setAssets()
		{
			$this->addAsset("js", "/G/toolset.js");
			$this->addAsset('js', "/.assets/comments_sc.js");
			$this->addAsset('js', "template/articleview_sc.js");

			$this->addAsset('css', "/.assets/comments_style.css");
		}
	}
?>
