<div class="mntinbox-container">
	<div class="sidebar" id="<?php echo $vars->_pmod; ?>-sidebar">
		<div class="item">
			<a href="<?php echo $vars->_fallback->inbox; ?>">Inbox</a>
		</div>
		<div class="item">
			<a href="<?php echo $vars->_fallback->compose; ?>">Compose</a>
		</div>
		<div class="mentor-list">
		Group Mentors
		<?php
			foreach($vars->mentors as $m) {
				echo "<div>";
				echo "<img src=\"{$m[3]}\" /><br />";
				echo $m[2];
				echo "</div>";
			}
		?>
		</div>
	</div>
	<div class="inbox">
		<form method="post" action="<?php echo $vars->_fallback->post ?>">
		<div class="table" id="<?php echo $vars->_pmod; ?>-table">
		Send to: <select name="uid" id="<?php echo $vars->_pmod; ?>-uid" class="vform-text">
		<?php
		foreach($vars->contacts['mentor'] as $v)
			echo "\t\t<option value=\"{$v[1]}\">{$v[2]} (M)</option>\n";
		?>
		<?php
		foreach($vars->contacts['mentee'] as $v)
			echo "\t\t<option value=\"{$v[1]}\">{$v[2]}</option>\n";
		?>
			<option value="0">Everybody</option>
		</select>
		<input type="text" class="vform-text vfont-x-large composer vform-item" name="subject" autocomplete="off" />
		<textarea class="composer vform-text vform-item" rows="10" name="content"></textarea>
		<input type="submit" value="Send" class="vform-button vfont-x-large vform-item"/>
		</div>
	</div>
</div>

