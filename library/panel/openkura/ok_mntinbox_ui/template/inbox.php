<div class="mntinbox-container">
	<div class="sidebar" id="<?php echo $vars->_pmod; ?>-sidebar">
		<div class="item">
			<a href="<?php echo $vars->_fallback->inbox; ?>">Inbox</a>
		</div>
		<div class="item">
			<a href="<?php echo $vars->_fallback->compose; ?>">Compose</a>
		</div>
		<div class="mentor-list">
		Group Mentors
		<?php
			foreach($vars->mentors as $m) {
				echo "<div>";
				echo "<img src=\"{$m[3]}\" /><br />";
				echo $m[2];
				echo "</div>";
			}
		?>
		</div>
	</div>
	<div class="inbox">
		<div class="table" id="<?php echo $vars->_pmod; ?>-table">
		<?php
			$cgrp = "";

			if($vars->inbox == 104) {
				// user not a part of anything
				echo "<div class=\"message\">";
				echo "<span style=\"font-size: 20px; color: #EF5959;\">Error loading inbox- you may not be a member of a mentor group.</span>";
				echo "</div>";
			}
			else
			if($vars->inbox) {
				foreach($vars->inbox as $m) {
					if($cgrp != $m[8]) {
						$gt = explode("_", $m[8], 2);
						$gt = $gt[1];

						echo "<div class=\"group\">Group {$gt}</div>";;
						$cgrp = $m[8];
					}
					echo "<div class=\"message\">";
					echo "<div class=\"subject\">";
						echo "<a href=\"{$vars->_fallback->focus}&mntqid={$m[0]}\">";
						echo $m[5];
						echo "</a>";
					echo "</div>";
					echo "<div>";
						echo $m[9];
						echo " &gt;&gt; ";
						if($m[3] == 1)
							echo "Everyone";
						else
							echo $m[11];

					echo "</div>";

					echo "<div class=\"content\">";
						echo $m[6];
					echo "</div>";
					echo "</div>";
				}
			}
			else {
				echo "<div class=\"message\">";
				echo "Your inbox is empty";
				echo "</div>";
			}
		?>
		</div>
	</div>
</div>

