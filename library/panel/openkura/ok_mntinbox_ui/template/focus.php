<div class="mntinbox-container">
	<div class="sidebar" id="<?php echo $vars->_pmod; ?>-sidebar">
		<div class="item">
			<a href="<?php echo $vars->_fallback->inbox; ?>">Inbox</a>
		</div>
		<div class="item">
			<a href="<?php echo $vars->_fallback->compose; ?>">Compose</a>
		</div>

		<div class="mentor-list">
		Group Mentors
		<?php
			foreach($vars->mentors as $m) {
				echo "<div>";
				echo "<img src=\"{$m[3]}\" /><br />";
				echo $m[2];
				echo "</div>";
			}
		?>
		</div>
	</div>
	<div class="inbox">
		<div class="table" id="<?php echo $vars->_pmod; ?>-table">
		<?php
			if($vars->inbox == 104) {
				echo "<div class=\"message\">";
				echo "<span style=\"font-size: 20px; color: #EF5959;\">Error accessing private information</span>";
				echo "</div>";
				return;
			}
			$m = $vars->inbox[0];
			echo "<div class=\"message\">";
			echo "<div class=\"subject\">";
				echo $m[5];
			echo "</div>";
			echo "<div>";
				echo $m[8];
				echo " &gt;&gt; ";
				if($m[3] == 1)
					echo "Everyone";
				else
					echo $m[10];

			echo "</div>";

			echo "<div class=\"content\">";
				echo $m[6];
			echo "</div>";

			echo "<div class=\"replies vform-item-spacer\">";
				if($vars->replies == 104) {
					echo "No replies";
				}
				else {
					foreach($vars->replies as $r) {
						echo "<a name=\"c{$r[0]}\"></a><div class=\"reply vform-item-spacer\">";
						$content = StrSan::mysqlDesanatize($r[4], true);
						$content = StrSan::htmlSanatize($content);
						echo "$content";
						echo "<div class=\"detail\"><b>{$r[6]}</b>";
						if($r[2] > 0)
							echo " replying to <a href=\"#c{$r[2]}\">this</a> comment";
						if($vars->crp != $r[0])
							echo "<a style=\"margin-left: 10px; font-weight: bold;\" href=\"{$vars->_fallback->creply}&mntcrp={$r[0]}\">reply</a>";
						else
							echo "<a style=\"margin-left: 10px; font-weight: bold;\" href=\"{$vars->_fallback->creply}\">cancel</a>";

							echo "<span style=\"float: right\">{$r[5]}</span>";
						echo "</div>";
						if($vars->crp == $r[0]) {
							echo "<div class=\"vform-item-spacer\">";
							echo "<form method=\"post\" action=\"{$vars->_fallback->reply}\">";
								echo "<input type=\"hidden\" name=\"pid\" value=\"{$r[0]}\">";
								echo "<input type=\"hidden\" name=\"qid\" value=\"{$vars->qid}\">";
								echo "<textarea class=\"vform-text vfont-large\" cols=\"70\" rows=\"5\" name=\"content\"></textarea><br />";
								echo "<input type=\"submit\" value=\"post\" class=\"vform-button vform-item\" />";
							echo "</form>";
							echo "</div>";
						}
						echo "</div>";
					}
				}
			echo "</div>";

			echo "<div class=\"vform-item-spacer\">";
			echo "<form method=\"post\" action=\"{$vars->_fallback->reply}\">";
				echo "<input type=\"hidden\" name=\"pid\" value=\"0\">";
				echo "<input type=\"hidden\" name=\"qid\" value=\"{$vars->qid}\">";
				echo "<textarea class=\"vform-text vfont-large\" cols=\"70\" rows=\"5\" name=\"content\"></textarea><br />";
				echo "<input type=\"submit\" value=\"post\" class=\"vform-button vform-item\" />";
			echo "</form>";
			echo "</div>";

			echo "</div>";
		?>
		</div>
	</div>
</div>

