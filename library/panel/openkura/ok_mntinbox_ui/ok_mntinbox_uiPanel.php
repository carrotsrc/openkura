<?php
	class ok_mntinbox_uiPanel extends Panel
	{
		private $mode = 0;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_mntinbox_ui');
		}

		public function loadTemplate()
		{
			$this->fallback();
			if(!isset($_GET['mntmode']) || $_GET['mntmode'] == 0)
				$this->includeTemplate("template/inbox.php");
			else
			if(isset($_GET['mntmode']) && $_GET['mntmode'] == 1)
				$this->includeTemplate("template/compose.php");
			else
			if(isset($_GET['mntmode']) && $_GET['mntmode'] == 2)
				$this->includeTemplate("template/focus.php");

		}

		public function initialize($params = null)
		{
			parent::initialize();
			$this->addComponentRequest(5, 101); // get the mentors

			if(!isset($_GET['mntmode']) || (isset($_GET['mntmode']) && $_GET['mntmode'] == 0)) {
				$this->addComponentRequest(1, 101);
			}
			if(isset($_GET['mntmode']) && $_GET['mntmode'] == 1) {
				$this->addComponentRequest(2, 101);
			}
			if(isset($_GET['mntmode']) && $_GET['mntmode'] == 2) {
				$id = $_GET['mntqid'];
				$this->addComponentRequest(4, array('qid' => $id));
				$this->addComponentRequest(3, array('qid' => $id));
				$this->addTParam('qid', $id);
				if(isset($_GET['mntcrp']))
					$this->addTParam('crp', $_GET['mntcrp']);
			}
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				$r = $rs['result'];
				switch($rs['jack']) {
				case 1:
					$this->addTParam('inbox', $r);
				break;
				case 2:
					$this->addTParam('contacts', $r);
				break;

				case 3:
					$this->addTParam('replies', $r);
				break;

				case 4:
					$this->addTParam('inbox', $r);
				break;

				case 5:
					if($r == 104)
						$r = array();
					$this->addTParam('mentors', $r);
				break;

				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "template/mntinbox_style.css");
			$this->addAsset('js', "/G/toolset.js");
			$this->addAsset('js', "template/mntinbox_sc.js");
		}

		public function fallback()
		{
			$qstr = QStringModifier::modifyParams(array('mntmode' => null,
								'mntqid' => null));
			$this->addFallbackLink('inbox', $qstr);

			$qstr = QStringModifier::modifyParams(array('mntmode' => 1,
								'mntqid' => null));
			$this->addFallbackLink('compose', $qstr);

			$qstr = QStringModifier::modifyParams(array('mntmode' => 2,
								'mntqid' => null));
			$this->addFallbackLink('focus', $qstr);

			$qstr = QStringModifier::modifyParams(array('mntcrp' => null));
			$this->addFallbackLink('creply', $qstr);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/10&dbm-redirect=1";
			$this->addFallbackLink('post', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/11&dbm-redirect=1";
			$this->addFallbackLink('reply', $qstr);
		}
	}
?>
