/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
<?php
	class ok_modmanusers_uiPanel extends Panel
	{
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_modmanusers_ui');
			$this->jsCommon = "OK_ModManUserInterface";
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->includeTemplate('templates/view.php');
		}

		public function initialize($params = null)
		{
			$vars = $this->argVar(array('kusum' => 'uopen',
							'kusgm' => 'gopen',
							'kusgo' => 'gop',
							'kusui' => 'userid',
							'kusgi' => 'groupid',
							'kmodule' => 'module'), null);

			if($vars->uopen != null)
				$this->addTParam('umode', true);


			if($vars->gopen != null) {
				$this->addTParam('gmode', true);
				$this->addComponentRequest(10, array('kmodule' => $vars->module));
				if($vars->gop == 2)
					$this->addComponentRequest(11, array('kmodule' => $vars->module,
										'kgroup' => $vars->groupid));
			}

			if($vars->userid != null)
				$this->addComponentRequest(8, array('kuser' => $vars->userid,
									'kmodule' => $vars->module));
			$this->addTParam('gop', $vars->gop);
			$this->addTParam('gid', $vars->groupid);

			$this->addComponentRequest(7, 101);
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 7:
					$this->addTParam('users', $rs['result']);
				break;

				case 8:
					if($rs['result'] != 104)
						$this->addTParam('user', $rs['result']);
				break;

				case 10:
					if($rs['result'] != 104)
						$this->addTParam('groups', $rs['result']);
				break;
				
				case 11:
					if($rs['result'] != 104)
						$this->addTParam('gusers', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', '/.assets/general.css');
		}

		private function fallback()
		{
			$kmodule = 0;
			$kgroup = null;
			if(isset($_GET['kusgi']))
				$kgroup = $_GET['kusgi'];

			$qstr = QStringModifier::modifyParams(array('kusum' => 1));
			$this->addFallbackLink('uopen', $qstr);
			
			$qstr = QStringModifier::modifyParams(array('kusum' => null));
			$this->addFallbackLink('ufold', $qstr);

			$qstr = QStringModifier::modifyParams(array('kusgm' => 1));
			$this->addFallbackLink('gopen', $qstr);
			
			$qstr = QStringModifier::modifyParams(array('kusgm' => null,
									'kusgo' => null,
									'kusgi' => null));
			$this->addFallbackLink('gfold', $qstr);

			$qstr = QStringModifier::modifyParams(array('kusgo' => null, 'kusgi' => null));
			$this->addFallbackLink('gnew', $qstr);

			$qstr = QStringModifier::modifyParams(array('kusui' => null));
			$this->addFallbackLink('user', $qstr);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);

			$this->addTParam('module', $kmodule);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/9&dbm-redirect=0";
			$this->addFallbackLink('submit', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/12&kgroup=$kgroup&kmodule=$kmodule&dbm-redirect=1";
			$this->addFallbackLink('addu', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/13&kgroup=$kgroup&kmodule=$kmodule&dbm-redirect=1";
			$this->addFallbackLink('remu', $qstr);

			$mpath = SystemConfig::appRelativePath("library/media/kura/users");
			$this->addTParam('media', $mpath);
			
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/100&kmodule=$kmodule&dbm-redirect=1";
			$this->addFallbackLink('pickup', $qstr);
		}
	}
?>
