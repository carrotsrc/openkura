<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="manager-title">
	Users and Groups
</div>
<div class="manager-area">
	<div class="cat-title separate">
		<?php if($vars->umode == null){ ?>
			<a href="<?php echo $vars->_fallback->uopen; ?>" class="a-light">Users</a>
		<?php }else{ ?>
			<a href="<?php echo $vars->_fallback->ufold; ?>" class="a-light">Users</a>
		<?php } ?>
	</div>
	<?php
		if($vars->umode != null) {
			foreach($vars->users as $user) { ?>
				<div style="margin-left: 10px">
				<!--<div class="metalcol-detail metalcol-detail-blue metalcol-button-small"> -->
					<?php
						if($vars->user != null && $user[2] == $vars->user[5])
							echo "<a class=\"a-light\" href=\"{$vars->_fallback->user}\">";
						else
							echo "<a class=\"a-light\" href=\"{$vars->_fallback->user}&kusui={$user[2]}\">";
	
						echo "{$user[1]}";
						echo "</a>";
						if($vars->gop == 2) {
							echo "<div style=\"float: right;\">";
							echo "<a title=\"Push into group\" href=\"{$vars->_fallback->addu}&kuser={$user[2]}\"><img alt=\"[Push into group]\" src=\"{$vars->media}/userbin.png\" /></a>";
							echo "</div>";
							echo "<div style=\"float: right; margin-right: 7px;\">";
							echo "<a title=\"Pick up\" href=\"{$vars->_fallback->pickup}&krid={$user[0]}\"><img alt=\"[Pick up]\" src=\"{$vars->media}/pickup.png\" /></a>";
							echo "</div>";
						}
						else {
							echo "<div style=\"float: right;\">";
							echo "<img src=\"{$vars->media}/userbin-inactive.png\" />";
							echo "</div>";
							echo "<div style=\"float: right; margin-right: 7px;\">";
							echo "<a title=\"Pick up\" href=\"{$vars->_fallback->pickup}&krid={$user[0]}\"><img alt=\"Pick up\" src=\"{$vars->media}/pickup.png\" /></a>";
							echo "</div>";
						}
					?>
				</div>

				<?php
				if($vars->user != null && $user[2] == $vars->user[5]) {
					echo "<div style=\"margin-left: 15px; padding-left: 5px; margin-top: 5px; border-left: 1px dashed #d8d8d8;\">";
					echo "<b>".$vars->user[1] . " " . $vars->user[2]."</b><br />";
					echo "<div class=\"vfont-small\">({$user[1]})</div>";
					echo "<div class=\"vform-item\">";
						if($vars->user[6] == 1)
							echo "Module Staff";
						else
							echo "Module Student";

					echo "</div>";
					echo "<div class=\"vfont-x-small\">uid: {$vars->user[5]}</div>";
					echo "</div>";
				}
				?>
	<?php
			} 
		}
	?>


	<div class="cat-title separate vform-item-spacer">
		<?php
			if($vars->gmode == null)
				echo "<a href=\"{$vars->_fallback->gopen}\" class=\"a-light\">Groups</a>";
			else
				echo "<a href=\"{$vars->_fallback->gfold}\" class=\"a-light\">Groups</a>";
		?>
	</div>
	<?php
	if($vars->gmode != null) {
		if($vars->groups != null) {
			foreach($vars->groups as $group) {

				//echo "<div class=\"metalcol-detail metalcol-detail-green metalcol-button-small\">";
				echo "<div class=\"vdiv-auto-overflow\" style=\"margin-left: 10px\">";

					echo "<div style=\"float: left;\">{$group[1]}</div>";
					echo "<div style=\"float: right;\">";
					if($vars->gop == 2 && $vars->gid == $group[0])
						echo "<a title=\"Close Group\" href=\"{$vars->_fallback->gnew}\"><img alt=\"Open group\" src=\"{$vars->media}/groupbin.png\" /></a>";
					else
						echo "<a title=\"Open Group\" href=\"{$vars->_fallback->gnew}&kusgo=2&kusgi={$group[0]}\"><img src=\"{$vars->media}/groupopen.png\" /></a>";
					echo "</div>";
					echo "<div style=\"float: right; margin-right: 7px;\">";
					echo "<a title=\"Pick up\" href=\"{$vars->_fallback->pickup}&krid={$group[0]}\"><img alt=\"Pick up\"  src=\"{$vars->media}/pickup-group.png\" /></a>";
					echo "</div>";
				echo "</div>";
				if($vars->gop == 2 && $vars->gid == $group[0]) {
					if($vars->gusers != null) {
						foreach($vars->gusers as $k => $user) {
							echo "<div style=\"margin-left: 20px; border-top-width: 0px; margin-top: 5px;\">";
							echo "{$user[1]}";
							echo "<div style=\"float: right;\">";
							echo "<a title=\"Pop out of group\" href=\"{$vars->_fallback->remu}&kuser={$user[0]}\"><img alt=\"Pop out of group\" src=\"{$vars->media}/userout.png\" /></a>";
							echo "</div>";
							echo "</div>";
						}

					}
					else {
						echo "<div class=\"metalcol-detail metalcol-detail-blue metalcol-button-small\" style=\"margin-left: 10px; width: 230px; border-top-width: 0px;\">";
						echo "...";
						echo "</div>";
					}
				}
			}
		}

		if($vars->gop != null && $vars->gop == 1) {
			echo "<div class=\"metalcol-detail\">";
				echo "<b>New Group</b>";
				echo "<div class=\"vform-item vfont-small\">";
				echo "<form method=\"post\" action=\"{$vars->_fallback->submit}\">";
					echo "Name:<br />";
					echo "<input type=\"hidden\" name=\"kmodule\" value=\"{$vars->module}\" class=\"vform-text\"/>";
					echo "<input type=\"text\" name=\"kgname\" class=\"vform-text\"/>";
					echo "<input type=\"submit\" class=\"vform-button\" value=\"Add\" style=\"margin-top: 7px;\"/>";
				echo "</form>";
				echo "</div>";
			echo "</div>";
			echo "<div><center><a href=\"{$vars->_fallback->gnew}\" class=\"va-switch\"><b>New Group</b></a></center></div>";
		}
		else
			echo "<div><center><a href=\"{$vars->_fallback->gnew}&kusgo=1\" class=\"va-switch\"><b>New Group</b></a></center></div>";

	}
	?>
</div>
