/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function OK_ModManUserInterface() {

	var self = this;
	this.pcom = new Array();
	// this really needs to be fixed
	this.media = "APP-MEDIA/kura/article";

	this.initialize = function () {
	}

}

ModManUserInterface.prototype = new KitJS.PanelInterface('ok_modmanusers_ui');


