/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function OK_ArticleManInterface() {

	var self = this;
	this.pcom = new Array();
	// this really needs to be fixed
	this.media = "APP-MEDIA/kura/article";

	this.initialize = function () {
		var e = document.getElementById(this._pmod + "-bold");
		e.onclick = this.addBoldStyle;
		var e = document.getElementById(this._pmod + "-italic");
		e.onclick = this.addItalicStyle;
		var e = document.getElementById(this._pmod + "-underline");
		e.onclick = this.addUnderlineStyle;
	}


	this.requestActCmpt = function () {
	}

	this.addItalicStyle = function () {
		var sel = self._loop.getSelection()
		sel.e.value = sel.e.value.substring(0, sel.start) + "[i]" + sel.e.value.substring(sel.start, sel.end) + "[!i]" + sel.e.value.substring(sel.end, sel.e.value.length);
		sel.e.scrollTop = sel.scroll;
	}

	this.addBoldStyle = function () {
		var sel = self._loop.getSelection()
		sel.e.value = sel.e.value.substring(0, sel.start) + "[b]" + sel.e.value.substring(sel.start, sel.end) + "[!b]" + sel.e.value.substring(sel.end, sel.e.value.length);
		sel.e.scrollTop = sel.scroll;
	}

	this.addUnderlineStyle = function () {
		var sel = self._loop.getSelection()
		sel.e.value = sel.e.value.substring(0, sel.start) + "[u]" + sel.e.value.substring(sel.start, sel.end) + "[!u]" + sel.e.value.substring(sel.end, sel.e.value.length);
		sel.e.scrollTop = sel.scroll;
	}

	this.rselection = function () {
		this.e;
		this.start;
		this.end;
		this.scroll;
	}

	this.getSelection = function () {
		/*
		 * this is not cross-browser
		 */
		var e = document.getElementById(self._loop._pmod + "-content");
		var sel = new this.rselection();
		sel.e = e;
		sel.start = e.selectionStart;
		sel.end = e.selectionEnd;
		sel.scroll = e.scrollTop;
		
		return sel;
//		new Array(e, startPos, endPos);
	}

}

OK_ArticleManInterface.prototype = new KitJS.PanelInterface('ok_articleman_ui');

