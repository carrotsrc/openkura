<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
 
<div class="articleman_ui">

<div class="header">
	Article editor
</div>
<form action="<?php echo $vars->_fallback->submit ?>" method="post">
<div class="content">
	<div class="cleft">
		<div class="vform-item">
			&nbsp;Title:
			<input name="artitle" class="vform-text" style="width: 100%" value="<?php
					if($vars->title != null) {
						$content = StrSan::mysqlDesanatize($vars->title);
						$content = StrSan::htmlSanatize($content);
						echo $content;
					}
				?>" autocomplete="off" />
		</div>
		<div class="vform-item">
			&nbsp;Style:
			<select name="arstyle" class="vform-text vform-select" style="width: 100%; margin-top: 3px;">
			<?php
				foreach($vars->styles as $style) {
					
					echo "<option value=\"$style\"";
					if($vars->style != null && $vars->style == $style)
						echo " selected";
					echo ">$style</option>\n";
				}
			?>

			</select>
		</div>

		<div class="vform-item-spacer">
			<input type="submit" value="Save" class="vform-button vfont-large" style="color: #808080;"/>
		</div>
	</div>

	<div class="cright">
		<div class="toolbar">
		<input type="button" id="<?php echo $vars->_pmod ?>-bold" class="vform-button" value="B" style="font-weight: bold;" title="Bold style"/>
		<input type="button" id="<?php echo $vars->_pmod ?>-italic" class="vform-button" value="I" style="font-style: italic;" title="Italic style"/>
		<input type="button" id="<?php echo $vars->_pmod ?>-underline" class="vform-button" value="U" style="text-decoration: underline;" title="Underline style"/>
		<input type="button" class="vform-button nsec" value="&#8226" title="Unordered list"/>
		<input type="button" class="vform-button" value="1" title="Ordered list"/>
		</div>

		<textarea name="arcontent" id="<?php echo $vars->_pmod ?>-content" class="vform-text" ><?php if($vars->content != null) {
				$content = StrSan::mysqlDesanatize($vars->content);
				echo $content;
			}?></textarea>
	</div>
</div>

<div class="footer">
</div>
<?php
	if($vars->ref !== null)
		echo "<input type=\"hidden\" name=\"arref\" value=\"{$vars->ref}\">";
?>
</form>
</div>
