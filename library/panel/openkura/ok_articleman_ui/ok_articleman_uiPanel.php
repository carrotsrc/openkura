<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_articleman_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_articleman_ui');
			$this->jsCommon = 'OK_ArticleManInterface';
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->includeTemplate("template/main.php");
		}

		public function initialize($params = null)
		{
			if(isset($_GET['ref'])) {
				$this->addTParam('ref', $_GET['ref']);
				$this->addComponentRequest(10, array('arref' => $_GET['ref']));
				$this->mode = 1;
			}
			else
			if(isset($_GET['_ref'])) {
				$this->addTParam('ref', $_GET['_ref']);
				$_GET['ref'] = $_GET['_ref'];
				unset($_GET['_ref']);
				$this->addComponentRequest(10, array('arref' => $_GET['ref']));
				$this->mode = 1;
			}

			$this->addComponentRequest(1,101);
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				$crs = $rs['result'];
				switch($rs['jack']) {
				case 1:
					$this->addTParam("styles", $crs);
				break;

				case 10:
					$this->addTParam('title', $crs[1]);
					$this->addTParam('content', $crs[2]);
					$this->addTParam('style', $crs[4]);
				break;
				}
			}
		}

		private function fallback()
		{
			$mpath = SystemConfig::appRelativePath("library/media/kura/general");
			$this->addTParam('mediag', $mpath);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$activity = $_GET['activity'];
			$qstr = "";
			if($this->mode == 0)
				$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/2&dbm-redirect=1&activity=$activity";
			else
				$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/3&dbm-redirect=1&activity=$activity";

			$this->addFallbackLink('submit', $qstr);
		}

		public function setAssets()
		{
			$this->addAsset('css', "/.assets/general.css");
			$this->addAsset('css', "template/articleman_style.css");
			$this->addAsset('js', "template/articleman_sc.js");
		}
	}
?>
