<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_activityman_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_activityman_ui');
			$this->mode = 0;
			$this->jsCommon = 'OK_ActivityManInterface';
		}

		public function loadTemplate()
		{
			$this->fallbackMain();
			if($this->mode == 0)
				$this->includeTemplate("templates/main.php");
		}

		public function initialize($params = null)
		{
			$vars = $this->argVar( array( 'aca' => 'marea',
							'acl' => 'mlayout',
							'aci' => 'mactivity',
							'acp' => 'mpanel'), null);

			$this->addComponentRequest(4,array('kmodule' => 1));
			$this->addTParam('module', 1);
			$this->addTParam('marea', $vars->marea);
			$this->addTParam('mlayout', $vars->mlayout);
			$this->addTParam('mactivity', $vars->mactivity);
			$this->addTParam('mpanel', $vars->mpanel);

			if(isset($_GET['amop']) && $_GET['amop'] == 2 && isset($_GET['amid'])) {
					$this->addComponentRequest(5, array('kmodule' => 1,
										'activity' => $_GET['amid']));
					$this->addComponentRequest(2, 101);
					if(isset($_GET['amii'])) {
						$this->addTParam('iid', $_GET['amii']);
						$this->addComponentRequest(14, array(
										'kmodule' => $_GET['kmodule'],
										'activity' => $_GET['amid'],
										'kinst' => $_GET['amii']));

					}
			}
			else
			if(isset($_GET['amop']) && $_GET['amop'] == 1) {
				$this->addComponentRequest(2, 101);
				$this->addTParam('add', true);

				if(isset($_GET['acret']) && $_GET['acret'] == 102) {
					$this->addTParam('added', true);
				}
			}


			parent::initialize();
		}

		public function applyRequest($result)
		{
			foreach($result as $rs) {
				switch($rs['jack']) {
				case 2:
					$this->addTParam('components', $rs['result']);
				break;
				case 4:
					$this->addTParam('activities', $rs['result']);
				break;

				case 5:
					if($rs['result'] == 104)
						break;
					$this->addTParam('details', $rs['result']);
					if(is_array($rs['result'][6])) {
						$this->addTParam('marea', $rs['result'][6][0]);
						$this->addTParam('mlayout', $rs['result'][6][1]);
						$this->addTParam('mpanel', $rs['result'][6][2]);
					}
				break;

				case 14:
					if($rs['result'] == 104)
						break;
					$this->addTParam('assocs', $rs['result']);
				break;

				}
			}
		}

		public function setAssets()
		{
			$this->addAsset("css", "/.assets/general.css");
			$this->addAsset("css", "templates/actman_style.css");
			$this->addAsset("js", "/G/toolset.js");
			$this->addAsset("js", "/G/resbin_sc.js");
			$this->addAsset("js", "templates/activityman_sc.js");
		}

		public function fallbackMain()
		{
			//$kmodule = $_GET['kmodule'];
			$this->addFallbackLink('media',SystemConfig::appRootPath("library/media/kura/activity"));
			$this->addFallbackLink('mediag',SystemConfig::appRootPath("library/media/kura/general"));

			$qstr = QStringModifier::modifyParams(array('amop' => 2, 'amid' => null, 'acret' => null,
									'aca' => null, 'acl' => null, 'aci' => null, 'acp' => null));
			$this->addFallbackLink('view', $qstr);

			$qstr = QStringModifier::modifyParams(array('amop' => null, 'amid' => null, 'acret' => null, 'amii' => null,
									'aca' => null, 'acl' => null, 'aci' => null, 'acp' => null));
			$this->addFallbackLink('fold', $qstr);

			$qstr = QStringModifier::modifyParams(array('amii' => null));
			$this->addFallbackLink('foldinst', $qstr);

			$qstr = QStringModifier::modifyParams(array('amop' => 1, 'amid' => null, 'acret' => null,
									'aca' => null, 'acl' => null, 'aci' => null, 'acp' => null));
			$this->addFallbackLink('add', $qstr);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/1&dbm-redirect=0";
			$this->addFallbackLink('submit', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/6";
			$this->addFallbackLink('rqtype', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/101&dbm-redirect=1";
			$this->addFallbackLink('dropr', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/15&dbm-redirect=1";
			$this->addFallbackLink('disassoc', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/20&dbm-redirect=1";
			$this->addFallbackLink('chstate', $qstr);

			$this->addTParam('globvar', $this->globalParamStr());
		}

	}
?>
