/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

function OK_ActivityManInterface() {

	var self = this;
	this.pcom = new Array();
	// this really needs to be fixed
	this.media = "APP-MEDIA/kura/activity";

	this.initialize = function () {
		this.modifyLink('new_activity', this.requestActCmpt);
		this.requestUpdateActivities();
	}


	this.requestActCmpt = function () {
		self._loop.request(2, null, self._loop.onresponseActCmpt);
	}


	this.onresponseActCmpt = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;
		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}

		self._loop.uioNewActivity(response.data);
	}

	this.uioNewActivity = function (cmpts) {
		var r = document.createDocumentFragment();
		var u = new KTSet.NodeUtl(r);

		u.appendChild('div');
			u.gotoLast();
			u.node.className = 'ui_actman_detail';
			u.appendChild('b');
				u.gotoLast();
				u.appendText("New Activity");
				u.gotoParent();

			u.appendChild('form');
				u.gotoLast();
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "vfloat-left";

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("name:");
						u.appendChild('br');
						u.appendChild('input');
							u.gotoLast();
							u.node.id = self._pmod+"-acname";
							u.node.className = "metalcol-form-text";
							u.node.type = "text";
							u.node.autocomplete = "off";
							u.gotoParent();
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("description:");
						u.appendChild('br');
						u.appendChild('input');
							u.gotoLast();
							u.node.id = self._pmod+"-acdesc";
							u.node.className = "metalcol-form-text";
							u.node.type = "text";
							u.node.autocomplete = "off";
							u.gotoParent();
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("start:");
						u.appendChild('br');
						u.appendChild('input');
							u.gotoLast();
							u.node.id = self._pmod+"-acstart";
							u.node.className = "metalcol-form-text";
							u.node.type = "text";
							u.node.autocomplete = "off";
							u.gotoParent();
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("end:");
						u.appendChild('br');
						u.appendChild('input');
							u.gotoLast();
							u.node.id = self._pmod+"-acend";
							u.node.className = "metalcol-form-text";
							u.node.type = "text";
							u.node.autocomplete = "off";
							u.gotoParent();
						u.gotoParent();

					u.gotoParent();

				u.appendChild('div');
					u.gotoLast();
					u.node.className = "vfloat-left";

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("type:");
						u.appendChild('br');
						u.appendChild('select');
							u.gotoLast();
							u.node.id = self._pmod+"-actype";
							u.node.className = "metalcol-form-text";
							if(cmpts !== undefined) {
								var sz = cmpts.length;
								for(var i = 0; i < sz; i++) {
									u.appendChild('option');
										u.gotoLast();
										u.node.value=cmpts[i].id;
										u.appendText(cmpts[i].label);
										u.gotoParent();
								}
							}
							else
								u.node.disabled = true;

							u.gotoParent();
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("inclusion:");
						u.appendChild('br');
						u.appendChild('select');
							u.gotoLast();
							u.node.id = self._pmod+"-acinc";
							u.node.className = "metalcol-form-text";
							u.appendChild('option');
								u.gotoLast();
								u.node.value=4;
								u.appendText("Course");
								u.gotoParent();

							u.appendChild('option');
								u.gotoLast();
								u.node.value=3;
								u.appendText("Module");
								u.gotoParent();

							u.appendChild('option');
								u.gotoLast();
								u.node.value=2;
								u.appendText("Group");
								u.gotoParent();

							u.appendChild('option');
								u.gotoLast();
								u.node.value=1;
								u.appendText("User");
								u.gotoParent();

							u.gotoParent();
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "metalcol-form-item";
						u.appendText("Data and time format:");
						u.appendChild('p');
							u.gotoLast();
							u.appendText("DD/MM/YYYY HH:MM");
							u.gotoParent();
						u.appendText("HH:MM is optional");
						u.appendChild('br');
						u.appendText("HH is 24hr format");
						u.gotoParent();
					u.gotoParent();

				u.appendChild('div');
					u.gotoLast();
					u.node.className = "metalcol-form-item vdiv-clear-left";
					u.appendChild('input');
						u.gotoLast();
						u.node.id = self._pmod+'-aclimited';
						try {
							u.node.type='checkbox';
							u.gotoParent();
						}
						catch(e) {
							u.gotoParent();
							u.clearChildren();
							// Horrible
							u.node.innerHTML= "<input type=\"checkbox\" name=\"aclimited\" />";
						}

					u.appendText("activity does end")
					u.gotoParent();

				u.appendChild('div');
					u.gotoLast();
					u.node.className = "metalcol-form-item";
					u.appendChild('input');
						u.gotoLast();
						try {
							u.node.type='button';
							u.node.value="add";
							u.node.className = "vform-button";
							u.node.href="javascript:void(0);";
							u.node.onclick=self._loop.callAddActivity;
							u.gotoParent();
						}
						catch(e) {
							u.gotoParent();
							u.clearChildren();
							// Horrible
							u.node.innerHTML= "<input type=\"button\" value=\"add\" class=\"vform-button\" />";
						}
				u.gotoParent();

			u.gotoParent();

		var e = document.getElementById(self._pmod+"-ui_new_activity");
		u = new KTSet.NodeUtl(e);
		u.appendChild(r);

		self._loop.modifyLink('new_activity', self._loop.uicNewActivity);
	}

	this.uicNewActivity =  function () {
		var e = document.getElementById(self._pmod+"-ui_new_activity");
		u = new KTSet.NodeUtl(e);
		u.clearChildren();

		self._loop.modifyLink('new_activity', self._loop.requestActCmpt);
	}

	this.callAddActivity = function ()
	{
		var name = document.getElementById(self._loop._pmod+"-acname").value;
		var desc = document.getElementById(self._loop._pmod+"-acdesc").value;
		var start = document.getElementById(self._loop._pmod+"-acstart").value;
		var end = document.getElementById(self._loop._pmod+"-acend").value;
		var type = document.getElementById(self._loop._pmod+"-actype").value;
		var inc = document.getElementById(self._loop._pmod+"-acinc").value;
		var limited = document.getElementById(self._loop._pmod+"-aclimited").checked;
		self._loop.requestAddActivity(name, desc, type, start, end, inc, limited);
	}

	this.requestAddActivity = function (name, desc, type, start, end, inc, limited) {
		var postStr = "acname="+name;
		postStr += "&acdesc="+desc;
		postStr += "&actype="+type;
		postStr += "&acstart="+start;
		postStr += "&acend="+end;
		postStr += "&acinc="+inc;
		if(limited)
			postStr += "&aclimited=1";

		this.request(1, null, this.onresponseAddActivity, postStr);
	}

	this.onresponseAddActivity = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}

		self._loop.requestUpdateActivities();
	}

	this.requestUpdateActivities = function () {
		self._loop.request(4, null, self._loop.onresponseUpdateActivities, null);
	}

	this.onresponseUpdateActivities = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}

		self._loop.uiActivityList(response.data);
	}

	this.uiActivityList = function (list) {
		if(list === undefined)
			return;

		var r = document.createDocumentFragment();
		var u = new KTSet.NodeUtl(r);

		var sz = list.length;
		for(var i = 0; i < sz; i++) {
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "cat-title separate";
				u.appendChild('a');
					u.gotoLast();
					u.node.className = "a-light";
					u.node.href="javascript:void(0)";
					u.node.aid = list[i].id;
					u.node.id = self._loop._pmod+"-open_activity_"+list[i].id;
					u.appendText(list[i].title);
					u.node.onclick = function () {
						self._loop.requestActivityDetails(this.aid);
					}
					u.gotoParent();
				u.gotoParent();
			u.appendChild('div');
				u.gotoLast();
				u.node.id = self._loop._pmod+"-act_details_"+list[i].id;
				u.gotoParent();
		}

		var e = document.getElementById(self._loop._pmod+"-ui_activity_list");
		u = new KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}

	this.requestActivityDetails = function (id) {
		this.request(5, "activity="+id, self._loop.onresponseActivityDetails);
	}

	this.onresponseActivityDetails = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;
		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}
		self._loop.uioActivityDetails(response.data)
	}

	this.uicActivityDetails = function (aid) {
		var e = document.getElementById(self._loop._pmod+"-act_details_"+aid)
		var u = KTSet.NodeUtl(e);
		u.clearChildren();
		var  e = document.getElementById(self._loop._pmod+"-open_activity_"+aid);
		e.onclick = function () {
			self._loop.requestActivityDetails(this.aid);
		}
	}

	this.uioActivityDetails = function (data)
	{
		var r = document.createDocumentFragment();
		var u = new KTSet.NodeUtl(r);
		u.appendChild('div');
			u.gotoLast();
			u.node.className = "ui_actman_detail_container";
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "ui_actman_detail";
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "ui_actman_detail_panel";
					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vdiv-auto-overflow vform-item-spacer";
						u.appendChild('b');
							u.gotoLast();
							u.appendText(data.title);
							u.gotoParent();
						u.appendChild('br');
						u.appendText(data.description);
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vdiv-auto-overflow vform-item-spacer";

						u.appendChild('div');
							u.gotoLast();
							u.node.className = "vform-item vfont-small vfloat-left vdiv-auto-overflow";
							u.appendChild('b');
								u.gotoLast();
								u.appendText("type");
								u.gotoParent();
							u.appendChild('br');
							u.appendText(data.type);
							u.gotoParent();

						u.appendChild('div');
							u.gotoLast();
							u.node.className = "vform-item vfloat-left vfont-small vdiv-auto-overflow mleft-15";
							u.appendChild('b');
								u.gotoLast();
								u.appendText("inclusion");
								u.gotoParent();

							u.appendChild('br');
							switch(data.inclusion) {
							case 4:
								u.appendText("Course");
							break;

							case 3:
								u.appendText("Module");
							break;

							case 2:
								u.appendText("Group");
							break;

							case 1:
								u.appendText("User");
							break;

							case 0:
								u.appendText("Undefined");
							break;
							}
							u.gotoParent();
						
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vform-item vdiv-clear-left vfont-small vdiv-auto-overflow vform-item-spacer";
						u.appendChild('b');
							u.gotoLast();
							u.appendText("instances");
							u.gotoParent();

						u.appendChild('div');
							u.gotoLast();
							u.node.className = "vpanel-grey-listbox vfont-large";
							if(data.instances == undefined) {
								u.appendText("no instances");
							} else {
								var sz = data.instances.length;
								for(var i = 0; i < sz; i++) {
									u.appendChild('div');
										u.gotoLast();
										u.node.className = "va-blue";
										u.appendChild('a');
											u.gotoLast();
											u.node.className = "va-switch va-blue";
											u.node.href="javascript:void(0)";
											u.node.id = self._loop._pmod+"-open_assocs_"+data.id+"_"+data.instances[i].instance;
											
											u.node.aid = data.id;
											u.node.area = data.area;
											u.node.aref = data.route.area+"/"+data.route.layout;
											u.node.layout = data.instances[i].layout;
											u.node.panel = data.route.panel;
											u.node.rid = data.instances[i].rid;

											u.node.instance = data.instances[i].instance;
											u.node.aid = data.id;

											u.node.onclick = function () {
												self._loop.requestAssociations(this.aid, this.instance);
											}

											u.appendText("\u25B6 ");
											u.appendText(data.instances[i].label);
											u.gotoParent();

										u.appendChild('div');
											u.gotoLast();
											u.node.id = self._loop._pmod+"-assocs_"+data.id+"_"+data.instances[i].instance;
											u.gotoParent();
										u.gotoParent();
								}
							}
							u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vform-item vdiv-clear-left vfont-small vdiv-auto-overflow vform-item-spacer";
						try {
						u.appendChild('input');
							u.gotoLast();
							u.node.className="vform-button vfloat-right";
							u.node.value="new instance";
							u.node.type="button";
							u.node.aid = data.id;
							u.node.aref = data.route.area+"/"+data.route.layout;
							u.node.panel = data.route.panel;
							u.node.onclick = function () {
								
								window.open("?loc="+this.aref+"&activity="+this.aid+"&_cml="+this.panel+"/1"+KitJS.printGlobalParams(), '_blank');
							}
							u.gotoParent();
						}
						catch(e) {

						}

						u.appendChild('b');
							u.gotoLast();
							u.appendText("state: ");
							u.gotoParent();

						u.appendChild('select');
							u.gotoLast();
							u.node.id=self._loop._pmod+"-active_"+data.id
							u.node.className = "vform-text vform-select";
							u.appendChild('option');
								u.gotoLast();
								u.node.value="0";
								if(data.active == 0)
									u.node.selected = true;
								u.appendText("Inactive");
								u.gotoParent();
							u.appendChild('option');
								u.gotoLast();
								u.node.value="1";
								if(data.active == 1)
									u.node.selected = true;
								u.appendText("Active");
								u.gotoParent();
							u.gotoParent();
						u.appendText("\u00A0");
						try {
						u.appendChild('input');
							u.gotoLast();
							u.node.type="button";
							u.node.value="update";
							u.node.className = "vform-button";
							u.node.aid = data.id;
							u.node.onclick = function () {
								var e = document.getElementById(self._loop._pmod+"-active_"+data.id);
								self._loop.requestStateUpdate(this.aid, e.value);

							}
						}
						catch(e) {

						}
						
						u.gotoParent();

					u.appendChild('div');
						u.gotoLast();
						u.node.className = "vform-item vdiv-clear-left vfont-small vdiv-auto-overflow vform-item-spacer vfont-x-small";
						u.appendText("AID: "+data.id);
						u.gotoParent();
					u.gotoParent();
				u.gotoParent();
			u.gotoParent();
		var e = document.getElementById(self._loop._pmod+"-act_details_"+data.id)
		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
		e = document.getElementById(self._loop._pmod+"-open_activity_"+data.id);

		e.onclick = function () {
			self._loop.uicActivityDetails(this.aid);
		}
	}

	this.requestAssociations = function (aid, inst) {
		this.request(14, "activity="+aid+"&kinst="+inst, self._loop.onresponseAssociations);
	}

	this.onresponseAssociations = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104) {
				self._loop.uioAssociations(response.data, true);
				return;
			}

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}

		self._loop.uioAssociations(response.data, false);
	}

	this.uioAssociations = function (data, empty) {
		e = document.getElementById(self._loop._pmod+"-open_assocs_"+data.aid+"_"+data.instance);
		
		var aref = e.aref;
		var area = e.area;
		var layout = e.layout;
		var panel = e.panel;
		var rid = e.rid;


		e.onclick = function () {
			self._loop.uicAssociations(this.aid, this.instance);
		}

		var r = document.createDocumentFragment();

		e = document.getElementById(self._loop._pmod+"-assocs_"+data.aid+"_"+data.instance);
		var u = KTSet.NodeUtl(e);
		u.clearChildren();

		u = KTSet.NodeUtl(r);
		u.appendChild('div');
			u.gotoLast();
			u.node.className = "ui_actman_assoc_list";
			
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "item toolbar";
				u.appendChild('img');
					u.gotoLast();
					u.node.className="tool";
					u.node.src = self._loop.media+"/view.png";
					u.node.title = "View instance";
					u.node.style.cursor = 'pointer';
					u.node.vref = area+"/"+layout;
					u.node.onclick = function () {
						window.open("?loc="+this.vref+KitJS.printGlobalParams(), '_blank');
					}
					u.gotoParent();

				u.appendChild('img');
					u.gotoLast();
					u.node.className="tool";
					u.node.src = self._loop.media+"/edit.png";
					u.node.title = "Edit instance";
					
					u.node.aref = aref;
					u.node.panel = panel;
					u.node.instance = data.instance;
					u.node.onclick = function () {
						window.open("?loc="+this.aref+"&_cml="+this.panel+"/3&activity="+data.aid+"&_ref="+this.instance+KitJS.printGlobalParams(), '_blank');
					}

					u.node.style.cursor = 'pointer';
					u.gotoParent();

				u.appendChild('img');
					u.gotoLast();
					u.node.className="tool";
					u.node.src = self._loop.media+"/putdown.png";
					u.node.title = "Drop resource here";
					u.node.rid = rid;
					u.node.instance = data.instance;
					u.node.aid = data.aid;
					u.node.onclick = function () {
						self._loop.requestDropResource(this.aid, this.instance, this.rid);
					}
					u.node.style.cursor = 'pointer';
					u.gotoParent();

				u.gotoParent();

			if(empty) {
				u.appendText("Nothing associated with instance");
				u = KTSet.NodeUtl(e);
				u.appendChild(r);
				return;
			}
			var sz = data.assocs.length;

			for(var i = 0; i < sz; i++) {
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "item";
					u.appendText(data.assocs[i].label);
					u.gotoParent();
			}
		u = KTSet.NodeUtl(e);
		u.appendChild(r);
		return;
	}

	this.uicAssociations = function (aid, instance) {
		var e = document.getElementById(self._loop._pmod+"-assocs_"+aid+"_"+instance);

		var u = KTSet.NodeUtl(e);
		u.clearChildren();

		e = document.getElementById(self._loop._pmod+"-open_assocs_"+aid+"_"+instance);
		e.onclick = function () {
			self._loop.requestAssociations(this.aid, this.instance);
		}
	}

	this.requestStateUpdate = function (aid, state) {
		this.request(20, null, self._loop.onresponseStateUpdate, "aid="+aid+"&amacti="+state);
	}

	this.onresponseStateUpdate = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104) {
				console.log("Failed to update state");
				return;
			}

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}
	}

	this.requestDropResource = function (aid, inst, rid)  {
		this.request(101, "kid="+rid, function () { self._loop.onresponseDropResource(aid, inst) });
	}

	this.onresponseDropResource = function (aid, inst) {

		this.requestAssociations(aid, inst);
	}
}

OK_ActivityManInterface.prototype = new KitJS.PanelInterface('ok_activityman_ui');

