<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>
<div class="manager-title">
	Activities
</div>
<div class="manager-area">

<?php
	$did = null;
	$link = "";
	$mpath = SystemConfig::appRelativePath('library/media/kura/activity/');
	$mgpath = SystemConfig::appRelativePath('library/media/kura/general');

	if($vars->details != null) {
		$did = $vars->details[4];
	}
	if($vars->activities != 104) {
		echo "<div id={$vars->_pmod}-ui_activity_list>";
		foreach($vars->activities as $k => $activity) {
			echo "<div class=\"cat-title separate\">\n";
			if($activity[1] == $did)
				echo "<a href=\"{$vars->_fallback->fold}\" class=\"a-light\">".$activity[0]."</a>\n";
			else
				echo "<a href=\"{$vars->_fallback->view}&amid={$activity[1]}\" class=\"a-light\">".$activity[0]."</a>\n";

			echo "</div>\n";
			if($activity[1] == $did) {
			// background-image: url('$mgpath/grad-greytrans-32.png'); 
			echo "<div class=\"ui_actman_detail_container\">\n";
			echo "<div class=\"ui_actman_detail\">";
				echo "<div style=\"margin: 15px; margin-left: 5px;\">\n";
				echo "<div class=\"vdiv-auto-overflow vform-item-spacer\">";
				echo "<b>{$vars->details[0]}</b><br />";
				echo "{$vars->details[1]}<br />";
				echo "</div>";

				echo "<div class=\"vdiv-auto-overflow vform-item-spacer\">";
				echo "<div class=\"vform-item vfloat-left vfont-small vdiv-auto-overflow\">";
				echo "<b>type</b><br />";
				echo "{$vars->details[5]}<br />";
				echo "</div>";

				echo "<div class=\"vform-item vfloat-left vfont-small vdiv-auto-overflow mleft-15\">";
				echo "<b>inclusion</b><br />";
				switch($vars->details[2]) {
				case 4:
					echo "Course"; break;
				case 3: 
					echo "Module"; break;
				case 2: 
					echo "Group"; break;
				case 1: 
					echo "User"; break;
				case 0:
					echo "Undefined"; break;
				}
				echo "</div>";
				echo "</div>";

	/*
				echo "<div class=\"vform-item vfont-small vdiv-clear-left vform-item-spacer\">";
				
				echo "<b>Co-ordinators</b><br />";
				foreach($vars->details[6] as $c)
					echo "{$c[0]}<br />";
				echo "</div>";
	*/
				echo "<div class=\"vform-item vfont-small vdiv-clear-left vform-item-spacer\">";
				echo "<b>instances</b>";
				echo "<div class=\"vpanel-grey-listbox vfont-large\">";
				if(($sz = sizeof($vars->details)) > 8) {
					for($i = 8; $i < $sz; $i++) {
						echo "<div>";
						if($vars->iid != null && $vars->iid == $vars->details[$i][0]) {
							echo "<div class=\"va-blue\">";
							echo "<a class=\"va-switch va-blue\" href=\"{$vars->_fallback->foldinst}\">";
							echo "&#9662;";
							echo "</a> ";
							echo "{$vars->details[$i][1]}";
							echo "<div class=\"vfloat-right\"><a href=\"{$vars->_fallback->dropr}&kid={$vars->details[$i][2]}\"><img src=\"$mpath/putdown.png\"></a></div>";
							
							
							$ref = "{$vars->marea}/{$vars->mlayout}";
							$link = QStringModifier::modifyParams(array('amop' => null, 'amid' => null, 'acret' => null,
												'aca' => null, 'acl' => null, 'aci' => null,
												'loc' => $ref, 'activity' => $vars->details[3],
												'_cml' => "{$vars->mpanel}/3", '_ref' => $vars->details[$i][0]));
							$link .= $vars->globvar;
							$actlink = "?loc=".$vars->details[7]."/".$vars->details[$i][3].$vars->globvar;
							
							echo "<div class=\"vfloat-right\" style=\"margin-right: 7px;\"><a href=\"$link\" target=\"_BLANK\"><img src=\"$mpath/edit.png\" title=\"Edit instance\"></a></div>";
							echo "<div class=\"vfloat-right\" style=\"margin-right: 7px;\"><a href=\"$actlink\" target=\"_BLANK\"><img src=\"$mpath/view.png\" title=\"View instance\"></a></div>";
							echo "</div>";

							echo "<div class=\"ui_actman_assoc_list\" style=\"margin-left: 25px; margin-top: 7px; margin-bottom: 7px;\">";
								if($vars->assocs != null) {
									foreach($vars->assocs as $assoc) {
										echo "{$assoc[1]}";
										echo "<a href=\"{$vars->_fallback->disassoc}&aid={$vars->details[$i][2]}&bid={$assoc[0]}\" class=\"vfloat-right va-switch vfont-small\" style=\"color: red;\">x</a>";
									}
								}
								else
									echo "Nothing associated";

							echo "</div>";
						}
						else {
							echo "<div class=\"va-blue\">";
							echo "<a href=\"{$vars->_fallback->foldinst}&amii={$vars->details[$i][0]}\" class=\"va-switch va-blue\">";
							echo "&#9656;";
							echo "</a> ";
							echo "{$vars->details[$i][1]}";
							echo "</div>";
						}

						//echo "<div class=\"vfloat-right\" style=\"color: red;\">X</div>";
						echo "</div>";
					}
				}
					else
						echo "No Instances";
					echo "</div>";
					echo "</div>";
					echo "<div class=\"vdiv-auto-overflow vfont-item vfloat-right\" style=\"margin-top: 5px\">";
					$ref = "{$vars->marea}/{$vars->mlayout}";
					$link = QStringModifier::modifyParams(array('amop' => null, 'amid' => null, 'acret' => null,
										'aca' => null, 'acl' => null, 'aci' => null,
										'loc' => $ref, 'activity' => $vars->details[4],
										'_cml' => "{$vars->mpanel}/1"));
					$link .= $vars->globvar;
					echo "<form method=\"post\" action=\"$link&_ref=0\" target=\"_BLANK\">";
					echo "<input type=\"submit\" value=\"new instance\" class=\"vform-button\" />";
					echo "</form>";
					echo "</div>";
					
				echo "<div class=\"vform-item vfont-small vdiv-auto-overflow\" style=\"margin-bottom: 15px;\">";
				echo "<b>state:</b><br />";
				echo "<form method=\"post\" action=\"{$vars->_fallback->chstate}\">";
				echo "<select name=\"amacti\" class=\"vform-text vform-select\">";
				echo "<option value=\"0\" ";
					if($vars->details[3] == 0)
						echo " selected";
				echo ">Inactive</option>";

				echo "<option value=\"1\" ";
					if($vars->details[3] == 1)
						echo " selected";
				echo ">Active</option>";

				echo "</select>";
				echo "<input type=\"hidden\" name=\"aid\" value=\"{$vars->details[4]}\" /> ";
				echo "<input type=\"submit\" class=\"vform-button\" value=\"update\" />";
				echo "</form>";
				echo "</div>";
				echo "<div class=\"vfont-x-small vdiv-auto-overflow\" style=\"margin-bottom: 15px;\">AID: {$vars->details[4]}</div>";
				echo "</div>";
			echo "</div>";
			echo "</div>";
			}

		}
		echo "</div>";
	}

	if($vars->add && $vars->added == null) {
		echo "<div class=\"ui_actman_detail\">";
		echo "<div class=\"mbottom-5 vform-item\">\n";
		echo "<b>New Activity</b><br />";
		echo "<form name=\"\" method=\"post\" action=\"{$vars->_fallback->submit}\">\n";

		echo "<div class=\"vfloat-left vdiv-auto-overflow\">";
			echo "<div class=\"metalcol-form-item\">\n";
			echo "name:<br /><input name=\"acname\" class=\"metalcol-form-text\" type=\"text\" autocomplete=off/><br />\n";
			echo "</div>\n";

			echo "<div class=\"metalcol-form-item\">\n";
			echo "description:<br /><input name=\"acdesc\" class=\"metalcol-form-text\" type=\"text\" autocomplete=off/><br />";
			echo "</div>\n";

			echo "<div class=\"metalcol-form-item\" style=\"height: 40px;\">\n";

				echo "start:<br /><input name=\"acstart\" class=\"metalcol-form-text metalcol-form-text-small\" style=\"width: 200px;\" type=\"text\" autocomplete=off/><br />";


			echo "</div>\n";

			echo "<div class=\"metalcol-form-item vform-item-spacer\">\n";
			echo "end:<br /><input name=\"acend\" class=\"metalcol-form-text metalcol-form-text-small\" style=\"width: 200px;\" type=\"text\" autocomplete=off/><br />";
			echo "</div>\n";
		echo "</div>";


		echo "<div class=\"vfloat-left vdiv-auto-overflow\" style=\"margin-left: 10px;\">";

		echo "<div class=\"metalcol-form-item\">\n";
		echo "type:<br /><select name=\"actype\">\n";
			foreach($vars->components as $cmpt)
				echo "<option name=\"actype\" value=\"{$cmpt['handler']}\">{$cmpt['label']}</option>\n";

		echo "</select>\n";
		echo "</div>\n";

		echo "<div class=\"metalcol-form-item vform-item-spacer\">\n";
		echo "inclusion:<br /><select name=\"acinc\">\n";
			echo "<option value=\"4\" />Course</option>\n";
			echo "<option value=\"3\" />Module</option>\n";
			echo "<option value=\"2\" />Group</option>\n";
			echo "<option value=\"1\" />Individual</option>\n";
		echo "</select>";
		echo "</div>";
		echo "<div class=\"metalcol-form-item vform-item-spacer\">\n";
			echo "Date and time format:<p>DD/MM/YYYY HH:MM</p>HH:MM is optional<br >HH is 24hr format";
		echo "</div>";
		echo "</div>";

		echo "<div class=\"metalcol-form-item vdiv-clear-left\">\n";
		echo "<input type=\"checkbox\" name=\"aclimited\"/> activity does end\n";
		echo "</div>\n";

		echo "<input type=\"hidden\" name=\"kmodule\" value=\"{$vars->module}\">\n";

		echo "<div class=\"metalcol-form-item\">\n";
		echo "<input type=\"submit\" class=\"metalcol-form-button\" value=\"add\"/><br />\n";
		echo "</div>\n";
		echo "</form>\n";
		echo "</div>\n";
		echo "</div>\n";

		echo "<div class=\"cat-title centered separate-top\">\n";
		echo "<a href=\"{$vars->_fallback->fold}\" class=\"a-light\" id=\"{$vars->_pmod}-new_activity\" id=\"{$vars->_pmod}-new_activity\">New Activity</a>\n";
		echo "</div>\n";

	} 
	else
	if($vars->add && $vars->added) {
		echo "<div class=\"ui_actman_detail\">\n";
		echo "<b>New Activity</b><br />\n";
		echo "<div class=\"metalcol-success\"></div>\n";
		echo "<center>Added Activity<br /><br />\n";

		$ref = $vars->marea . "/" . $vars->mlayout;
		$cml = "{$vars->mpanel}/1";
		$link = QStringModifier::modifyParams(array('amop' => null, 'amid' => null,
									'loc' => $ref, 'acret' => null,
									'acp' => null, 'aci' => null,
									'aca' => null, 'acl' => null, '_cml' => $cml));
		echo "<a href=\"$link\">Manage Activity</a></center>";
		echo "</div>\n";
		echo "<div class=\"cat-title centered separate-top\">\n";
		echo "<a href=\"{$vars->_fallback->fold}\" class=\"a-light\" id=\"{$vars->_pmod}-new_activity\">New Activity</a>\n";
		echo "</div>\n";

	} else {
		echo "<div id=\"{$vars->_pmod}-ui_new_activity\"></div>";
		echo "<div class=\"cat-title centered\">\n";
		echo "<a href=\"{$vars->_fallback->add}\" class=\"a-light\" id=\"{$vars->_pmod}-new_activity\">New Activity</a>\n";
		echo "</div>\n";
	}
?>
</div>
