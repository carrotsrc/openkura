/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function OK_UMessageInterface() {

	var self = this;

	this.initialize = function () {
		this.modifyLink('compose_link', this.uioCompose);
		var e = document.getElementById(self._loop._pmod + "-mode");
		var mode = e.value;
		var profile = null;

		e = document.getElementById(self._loop._pmod + "-profile")
		if(e != null)
			profile = e.value;
		this.requestInbox(profile, mode);
	}

	this.uioCompose = function () {
		var r = document.createDocumentFragment();
		var e = document.getElementById(self._loop._pmod+"-compose");
		if(e === null)
			return;

		var u = KTSet.NodeUtl(r);
		u.appendChild('textarea');
			u.gotoLast();
			u.node.className = "vform-text";
			u.node.id = self._loop._pmod +"-composed_content";
		u.node.rows = '5';
		u.gotoParent();

		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);

		r = document.createDocumentFragment();
		e = document.getElementById(self._loop._pmod+"-toolbar");
		if(e === null)
			return;

		u = KTSet.NodeUtl(r);

		u.appendChild("a");
			u.gotoLast();
			u.node.href="javascript:void(0);";
			u.node.className = "vfloat-right";
			u.node.onclick = self.uicCompose;
			u.appendText("Cancel");
			u.gotoParent();

		u.appendChild("a");
			u.gotoLast()
			u.node.href="javascript:void(0);";
			u.node.onclick = function () {
				var e = document.getElementById(self._loop._pmod + "-mode");
				var mode = e.value;
				var profile = null;

				e = document.getElementById(self._loop._pmod + "-profile")
				if(e != null)
					profile = e.value;
				e = document.getElementById(self._loop._pmod + "-composed_content")
				var content = e.value;

				self._loop.requestPostMessage(profile, content, mode);
			}
			u.appendText("Post");
			u.gotoParent();


		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}

	this.uicCompose = function () {
		var e = document.getElementById(self._loop._pmod+"-compose");
		var u = KTSet.NodeUtl(e);
		u.clearChildren();

		r = document.createDocumentFragment();
		e = document.getElementById(self._loop._pmod+"-toolbar");
		if(e === null)
			return;

		u = KTSet.NodeUtl(r);
		u.appendChild("a");
			u.gotoLast();
			u.node.href="javascript:void(0);";
			u.node.onclick = self.uioCompose;
			u.appendText("Compose");
			u.gotoParent();

		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}

	this.requestPostMessage = function (to, message, isPrivate) {
		if(to == null)
			to = 0;
		var postStr = "umprofile="+to;
		postStr += "&umcontent="+message;
		postStr += "&umprivate="+isPrivate;

		var getStr = KitJS.printGlobalParams();
		if(isPrivate == 0)
			self._loop.request(2, getStr, self._loop.onresponsePostPublic, postStr);
		else
			self._loop.request(2, getStr, self._loop.onresponsePostPrivate, postStr);
	}

	this.onresponsePostPublic = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}

		var e = document.getElementById(self._loop._pmod + "-mode");
		var mode = e.value;
		var profile = null;

		e = document.getElementById(self._loop._pmod + "-profile")
		if(e != null)
			profile = e.value;
		
		self._loop.requestInbox(profile, mode);
		self._loop.uicCompose();
	}

	this.onresponsePostPrivate = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;

		} catch (e) {
			alert("Error:\n"+e+"\n\n"+reply);
			return;
		}
		var e = document.getElementById(self._loop._pmod + "-mode");
		var mode = e.value;
		var profile = null;

		e = document.getElementById(self._loop._pmod + "-profile")
		if(e != null)
			profile = e.value;



		self._loop.requestInbox(profile, mode);
		self._loop.uicCompose();

		e = document.getElementById(self._loop._pmod + "-toolbar")
		var u = KTSet.NodeUtl(e);
		u.appendChild('div');
			u.gotoLast();
			u.node.className = "message";
			u.appendText("Message sent!");
			u.gotoParent();
	}

	this.requestInbox = function (profile, isPrivate) {
		if(profile == null)
			profile = 0;
		var getStr = "umprofile="+profile;
		getStr += "&umprivate="+isPrivate;
		getStr += KitJS.printGlobalParams();

		self._loop.request(1, getStr, self._loop.onresponseInbox, null);
	}

	this.onresponseInbox = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 104)
				return;

			self._loop.uioInbox(response.data);

		} catch (e) {
			alert("Error inbox:\n"+e+"\n\n"+reply);
			return;
		}
	}

	this.uioInbox = function (inbox) {
		var sz = (inbox.length);
		var r = document.createDocumentFragment();
		var e = document.getElementById(self._loop._pmod+"-inbox");
		if(e === null)
			return;

		var u = KTSet.NodeUtl(r);
		for(var i = 0; i < sz; i++) {
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "umessage-container";
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "avatar";
					u.appendChild('img');
						u.gotoLast();
						u.node.src = inbox[i].avatar;
						u.gotoParent();
					u.gotoParent();

				u.appendChild('div');
					u.gotoLast();
					u.node.className = "username";
					u.appendChild('a');
						u.gotoLast();
						u.node.href="?loc=profiles&profile="+inbox[i].profile;
						u.appendText(inbox[i].from);
						u.gotoParent();
					u.gotoParent();

				u.appendChild('div');
					u.gotoLast();
					u.node.className = "message";
					u.appendText(inbox[i].contents);
					u.gotoParent();
				u.gotoParent();

		}

		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}
}

OK_UMessageInterface.prototype = new KitJS.PanelInterface('ok_umessages_ui');
