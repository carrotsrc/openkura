<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="umsg-surround">

<div class="tab-bar">
<div class="umsg-tab tunsel">
<a href="<?php echo $vars->_fallback->view; ?>">Public</a>
</div>
<div class="umsg-tab tsel rtab">
Private
</div>
<div class="caption">Messages</div>
</div>
<div class="umsg-content">
	<input type="hidden" id="<?php echo $vars->_pmod ?>-mode" value="<?php echo $vars->mode; ?>">

	<?php if($vars->profile != null) { ?>
		<input type="hidden" id="<?php echo $vars->_pmod ?>-profile" value="<?php echo $vars->profile; ?>">
	<?php } ?>

	<div id="<?php echo $vars->_pmod ?>-compose">
	</div>
	<div class="umessage-toolbar" id="<?php echo $vars->_pmod ?>-toolbar">
		<a href="" id="<?php echo $vars->_pmod ?>-compose_link">Compose</a>
	</div>
	<div id="<?php echo $vars->_pmod; ?>-inbox">
	<?php 
		if($vars->inbox != null) {
			foreach($vars->inbox as $msg) {
				echo "<div class=\"umessage-container\">";
					echo "<div class=\"avatar\">";
						echo "<img src=\"$msg[7]\" />";
					echo "</div>";
					echo "<div class=\"username\">";
						echo $msg[6];
					echo "</div>";
					echo "<div class=\"message\">";
							$content =  StrSan::mysqlDesanatize($msg[4]);
							$content = StrSan::htmlSanatize($content);
							echo $content;
					echo "</div>";
				echo "</div>";
			}
		}
	?>
	</div>
</div>
</div>
