<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_umessages_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_umessages_ui');
			$this->jsCommon = "OK_UMessageInterface";
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->addTParam('mode', $this->mode);
			if(isset($_GET['profile']))
				$this->addTParam('profile', $_GET['profile']);

			switch($this->mode) {
			case 0:
				$this->includeTemplate("template/public.php");
			break;

			case 1:
				$this->includeTemplate("template/private.php");
			break;
			}
		}

		public function initialize($params = null)
		{
			if(isset($_GET['umv'])) {
				$this->mode = 1;
			}
			/* TODO
			*  Pull in the inbox locally
			*/
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				$crs = $rs['result'];
				switch($rs['jack']) {
				case 1:
					if($crs == 104)
						break;

					$this->addTParam('inbox', $crs);
				break;

				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "template/umsg.css");
			$this->addAsset('js', "template/umsg_sc.js");
			$this->addAsset("js", "/G/toolset.js");
		}

		public function fallback()
		{
			$img = SystemConfig::relativeLibPath("/media/kura/profile");
			$this->addFallbackLink('mediag', $img);

			$qstr = QStringModifier::modifyParams(array('umv' => null));
			$this->addFallbackLink('view', $qstr);
		}
	}
?>
