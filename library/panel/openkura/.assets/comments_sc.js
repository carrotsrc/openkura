/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function comments_sc (loopObj, prefixStr) {
	var self = loopObj;
	var local = this;
	local.prefix = prefixStr;

	local.rqjParents = 10;
	local.rqjAddComment = 11;
	local.rqjChildren = 12;




	this.paramToStr = function (extraParams) {
		if(extraParams == null)
			return "";
		var str = "";
		var sz = extraParams.length;
		for(var i = 0; i < sz; i++)
			str += "&"+extraParams[i][0]+"="+extraParams[i][1];

		return str;
	}

	this.requestParents = function (extra) {
		var poststr = "";
		if(extra != undefined) {
			if(typeof extra === 'string')
				poststr += "&"+extra
			else
				poststr += local.paramToStr(extra);
		}
		self._loop.request(local.rqjParents, null, this.onresponseParents, poststr);
	}

	this.onresponseParents = function (reply) {
		//try {
			var response = JSON.parse(reply);
			local.generateParentComments(response.data, response.plink);
		//} catch(e) {
		//	alert(e);
		//}
		
	}

	this.requestAddComment = function (parentid, rootid, comment, extra) {
		var postStr = local.prefix+"r="+rootid;
		postStr += "&"+local.prefix+"c="+comment;
		postStr += "&"+local.prefix+"p="+parentid;
		if(extra != undefined) {
			if(typeof extra === 'string')
				postStr += "&"+extra
			else
				postStr += local.paramToStr(extra);
		}

		var getStr = "";
		if(extra != undefined) {
			if(typeof extra === 'string')
				getStr += "&"+extra
			else
				getStr += local.paramToStr(extra);
		}

		self._loop.request(local.rqjAddComment, getStr, local.onresponseAddComment, postStr);
	}

	this.onresponseAddComment = function (reply) {
		// fix the extras problem
		try {
		var response = JSON.parse(reply);
			if(response.code == 102) {
				local.frqRefreshChildren(response.data);
			}
		} catch(e) {
		}
	}

	this.requestChildren = function (extra, rootid) {
		var getStr = "";
		
		if(rootid === undefined)
			getStr = local.prefix+"r="+this.rootid;
		else
			getStr = local.prefix+"r="+rootid;


		if(extra != undefined) {
			if(typeof extra === 'string')
				getStr += "&"+extra
			else
				getStr += local.paramToStr(extra);
		}

		self._loop.request(local.rqjChildren, getStr, local.onresponseChildren, null);
	}


	this.onresponseChildren = function (reply) {
		try {
			var response = JSON.parse(reply);

			if(response.code == 102) {
				local.generateChildComments(response.data, response.plink);
			} else {
				var container = document.getElementById("C"+response.data+"-surround");
				var u = KTSet.NodeUtl(container);
				u.clearChildren();
				u.appendChild('div');
					u.gotoLast();
					u.node.className="comment-item child";
					u.appendText("No replies");
					u.gotoParent();
			}

		} catch (e) {
		}
	}

	this.refreshChildren = function (rootid, extra) {
		var getStr = local.prefix+"r="+rootid;
		if(extra != undefined) {
			if(typeof extra === 'string')
				getStr += "&"+extra
			else
				getStr += local.paramToStr(extra);
		}
		self._loop.request(local.rqjChildren, getStr, local.onresponseRefreshChildren);
	}

	this.onresponseRefreshChildren = function (reply) {
		try {
			var response = JSON.parse(reply);
			if(response.code == 102) {
				var root = response.data[0].root;
				if(root > 0) {
					var e = document.getElementById("C"+response.data[0].root+"-surround");
					var u = KTSet.NodeUtl(e);
					u.clearChildren();
					local.generateChildComments(response.data, response.plink);
				} else {
					local.generateParentComments(response.data, response.plink);
				}
			}
		} catch(e) {
		}
	}



	this.generateParentComments =  function (comments, plink) {
		var sz = comments.length;
		var id = 0;
		var container = document.getElementById(self._loop._pmod+"-comments");

		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);

		while(id < sz) {
			u.appendChild('div'); // start of comment child
				u.gotoLast();
				u.node.className="comment-item";
				u.node.id="C"+id;

				u.appendChild('div'); // start of post
					u.gotoLast();
					u.node.className = "post";

					u.appendChild('div'); // start of header
						u.gotoLast();
							u.node.className="header";
							u.appendChild("img"); // avatar node
							u.gotoLast();
							u.node.src = comments[id].avatar;
							u.node.style.width = "48px";
							u.node.style.height = "48px";
							u.gotoParent(); // end of avatar
							u.appendChild('br');
							if(plink != undefined) {
								u.appendChild('a');
									u.gotoLast();
									u.node.href=plink+comments[id].uid;
									u.node.target="_BLANK";
									u.appendText(comments[id].username);
									u.gotoParent();
							}
							else
								u.appendText(comments[id].username);

						u.gotoParent(); // end of header
					u.appendChild('div'); // start of content
						u.gotoLast();
							u.node.className = "content";
							var contents = comments[id].contents.split("\n");
							var sy = contents.length;
							for(var i = 0; i < sy; i++) {
								u.appendText(contents[i])
								if(i<(sy-1))
									u.appendChild('br');
							}

						u.gotoParent(); // end of content
					u.gotoParent();

				u.appendChild('div');
					u.gotoLast(); // start of posted
					u.node.className = "vfont-small post-details";
					u.appendText(comments[id].posted);
					u.gotoParent(); // end end of posted

				u.appendChild('div');
					u.gotoLast(); // start of reply to area
					u.node.id = self._loop._pmod + "-actions"+comments[id].id;
					u.node.style.display = 'block';
					u.node.style.clear = 'left';

					u.appendChild('div');
						u.gotoLast(); // start of reply to
						u.node.className = "post-details post-details";
						var parentid = comments[id].id;
						var rootid = comments[id].root;

						u.appendChild('a');
							u.gotoLast(); // expand button to button
							u.appendText("expand");
							u.node.href = "javascript:void(0);";
							u.node.rootid = comments[id].id;
							u.node.onclick = local.frqChildren;

							u.node.id = self._loop._pmod+"-expandr"+comments[id].id;
							u.gotoParent();

						u.appendText(" ");

						u.appendChild('a');
							u.gotoLast(); // reply to button
							u.node.href="javascript:void(0);";
							u.node.parentid = parentid;
							u.node.rootid = parentid;
							u.node.onclick = local.generateReplyTo;
							u.node.id = self._loop._pmod+"-reply"+comments[id].id;
							u.appendText("reply");
							u.gotoParent(); // end of reply to button
						u.gotoParent();
					u.gotoParent();


					u.appendChild('div');
						u.gotoLast();
						u.node.id = self._loop._pmod+"-replyto"+comments[id].id;
						u.node.className="comments-reply";
						u.gotoParent();
					u.gotoParent(); // end of actions to area

				u.gotoParent(); // end of comment child

				u.appendChild('div');
					u.gotoLast(); // div for surround
					u.appendChild('div');
						u.gotoLast(); // surround
						u.node.id="C"+comments[id].id+"-surround";
						u.node.className = "comment-surround";
						u.gotoParent();
					u.gotoParent();

					u.appendChild('br');
					u.appendChild('hr');

			id++;
		}


		u.appendChild('textarea');
			u.gotoLast(); // text area
			u.node.className = "vform-text";
			u.node.rows = "7"
			u.node.cols = "55";
			u.node.id = self._loop._pmod+"-"+local.prefix+"c0";
			u.gotoParent(); // end text area
		u.appendChild('br');
		u.appendChild('input');
			u.gotoLast(); // start of button
			u.node.className = "vform-button vform-item";
			try{
				u.node.type="button";
				u.node.value="Reply";
			} catch(e) {
				u.gotoParent();
				var c = u.getChild(-1);
				u.removeChild(c);
				u.appendChild('a');
				u.gotoLast();
				u.node.href="javascript:void(0);";
				u.appendText("Reply");
			}
			u.node.parentid = 0;
			u.node.rootid = 0;
			u.node.onclick = function () {
				var e = document.getElementById(self._loop._pmod+"-"+local.prefix+"c"+this.parentid);
				var content = e.value;
				e.value = "";
				local.frqAddComment(this.parentid, this.rootid, content);
			}
			u.gotoParent(); // end of button

		u = KTSet.NodeUtl(container);
		u.clearChildren();

		container.insertBefore(r, null); 
	}

	this.generateReplyTo = function () {
		var r = document.createDocumentFragment();

		var e = document.getElementById(self._loop._pmod+"-reply"+this.parentid);
		var u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendText("cancel");
		u.node.href="javascript:void(0);";
		u.node.parentid = this.parentid;
		u.node.rootid = this.rootid;
		u.node.onclick = local.clearReplyTo;

		u = KTSet.NodeUtl(r);

		u.appendChild('textarea');
			u.gotoLast(); // text area
			u.node.className = "vform-text comment-reply-box";
			u.node.rows = "7"
			u.node.cols = "55";
			u.node.id = self._loop._pmod+"-"+local.prefix+"c"+this.parentid;
			u.gotoParent(); // end text area
		u.appendChild('br');
		u.appendChild('input');
			u.gotoLast(); // start of button
			u.node.className = "vform-button vform-item comment-reply-box";
			try{
				u.node.type="button";
				u.node.value="reply";
			} catch(e) {
				u.gotoParent();
				var c = u.getChild(-1);
				u.removeChild(c);
				u.appendChild('a');
				u.gotoLast();
				u.node.className = "reply-offset";
				u.node.href="javascript:void(0);";
				u.appendText("reply");
			}
			u.node.parentid = this.parentid;
			u.node.rootid = this.rootid;
			u.node.onclick = function () {
				var e = document.getElementById(self._loop._pmod+"-"+local.prefix+"c"+this.parentid);
				var content = e.value;
				e.value = "";
				local.frqAddComment(this.parentid, this.rootid, content);
			}
			u.gotoParent(); // end of button

		e = document.getElementById(self._loop._pmod+"-replyto"+this.parentid);
		if(e === null) {
			return;
		}

		e.insertBefore(r, null);
	}

	this.clearReplyTo = function () {
		var e = document.getElementById(self._loop._pmod+"-reply"+this.parentid);
		var u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendText("reply");
		u.node.href="javascript:void(0);";
		u.node.parentid = this.parentid;
		u.node.rootid = this.rootid;
		u.node.onclick = local.generateReplyTo;
		

		var e = document.getElementById(self._loop._pmod+"-replyto"+this.parentid);
		if(e === null) {
			return;
		}
		u = KTSet.NodeUtl(e);
		u.clearChildren();
	}

	this.generateChildComments = function (comments, plink) {
		var sz = comments.length;
		var id = 0;
		var container = null;
		var r = document.createDocumentFragment();

		var root = comments[0].root;
		var e = document.getElementById(self._loop._pmod+"-expandr"+root)
		var u = KTSet.NodeUtl(e);
		
		u.clearChildren();
		u.appendText("collapse");
		u.node.href = "javascript:void(0);";
		u.node.rootid = root;
		u.node.onclick = local.clearChildren;


		u = KTSet.NodeUtl(r);

		u.appendChild('ul');
		u.gotoLast();


		while(id < sz) {
			if(container === null) {
				container = document.getElementById("C"+comments[id].root+"-surround");
			}
			u.appendChild('li');
			u.gotoLast();
			u.appendChild('div'); // start of comment child
				u.gotoLast();
				u.node.className="comment-item child";
				u.appendChild('a');
					u.gotoLast(); // named anchor
					u.node.name="C"+comments[id].id;
					u.gotoParent(); // end of named anchor
				u.appendChild('div'); // start of header
					u.gotoLast();
						u.node.className="child-header";
						u.appendChild("img"); // avatar node
						u.gotoLast();
						u.node.style.width="32px";
						u.node.style.height="32px";
						u.node.src = comments[id].avatar;
						u.gotoParent(); // end of avatar
						u.appendChild('br');
						if(plink != undefined) {
							u.appendChild('a');
								u.gotoLast();
								u.node.href=plink+comments[id].uid;
								u.node.target="_BLANK";
								u.appendText(comments[id].username);
								u.gotoParent();
						}
						else
							u.appendText(comments[id].username);

					u.gotoParent(); // end of header
				u.appendChild('div'); // start of content
					u.gotoLast();
						u.node.className = "content child-content";
						var contents = comments[id].contents.split("\n");
						var sy = contents.length;
						for(var i = 0; i < sy; i++) {
							u.appendText(contents[i])
							if(i<(sy-1))
								u.appendChild('br');
						}

						if(comments[id].parentid != comments[id].root) {
							u.appendChild('div'); // start of 'reply to'
								u.gotoLast();
								u.node.className = "vfont-small";
								u.node.style.marginTop = "10px";
								u.appendText("Replying to ");
								u.appendChild('a');
								u.gotoLast(); // start of link
									u.node.href="#C"+comments[id].parentid;
									u.appendText("this comment");
								u.gotoParent(); // end link
							u.gotoParent(); // end 'reply to'
						}
					u.gotoParent(); // end of content

					u.appendChild('div');
						u.gotoLast(); // start of posted
						u.node.className = "vfont-small post-details child-details";
						u.appendText(comments[id].posted);
						u.gotoParent(); // end end of posted

					u.appendChild('div');
						u.gotoLast(); // start of reply to area
						u.node.id = self._loop._pmod + "-actions"+comments[id].id;

						u.appendChild('div');
							u.gotoLast(); // start of reply to
							u.node.className = "post-details child-details";
							var parentid = comments[id].id;
							var rootid = comments[id].root;
							u.appendChild('a');
								u.gotoLast(); // reply to button
								u.node.href="javascript:void(0);";
								u.node.parentid = parentid;
								u.node.rootid = rootid;
								u.node.onclick = local.generateReplyTo;
								u.node.id = self._loop._pmod +"-reply"+parentid;

								u.appendText("reply");
								u.gotoParent(); // end of reply to button
							
							u.gotoParent(); // end of  reply to

						u.gotoParent(); // end of reply to area
						u.appendChild('div');
							u.gotoLast();
							u.node.id = self._loop._pmod+"-replyto"+parentid;
							u.node.className = "comment-reply";
							u.gotoParent();
				u.gotoParent(); // end of comment child

			u.gotoParent(); //end of li
			
			id++;
		}

		u.gotoParent(); // end of ul
		u.appendChild('br');

		container.insertBefore(r, null); 
	}

	this.clearChildren = function () {
		var e = document.getElementById("C"+this.rootid+"-surround");
		var u = KTSet.NodeUtl(e);
		u.clearChildren();

		var e = document.getElementById(self._loop._pmod+"-expandr"+this.rootid)
		var u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendText("expand");
		u.node.href = "javascript:void(0);";
		u.node.rootid = this.rootid;
		u.node.onclick = local.frqChildren;
	}

	local.frqParents = local.requestParents;
	local.frqChildren = local.requestChildren;
	local.frqAddComment = local.requestAddComment;
	local.frqRefreshChildren = local.refreshChildren;
}
