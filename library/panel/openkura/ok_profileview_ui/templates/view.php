<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="profile">

<?php
	echo "<div class=\"header\">";
	echo "<div class=\"photo\">";
	echo "<img src=\"{$vars->_fallback->media}/{$vars->details[4]}\" />";
	echo "</div>";
	if($vars->details != null && $vars->details[5] != "")
		echo "<div class=\"details\" style=\"background-image: url('{$vars->_fallback->media}/headers/{$vars->details[5]}');\">";
	else
		echo "<div class=\"details\">";
	if($vars->edit) {
		echo "<form method=\"post\" action=\"{$vars->_fallback->update}\">";
		$fname = "first";
		$lname = "surname";

		if($vars->details[1] != "")
			$fname = $vars->details[1];
		if($vars->details[2] != "")
			$lname = $vars->details[2];

		echo "<input type=\"text\" name=\"pr-first\" class=\"vform-text vfont-large\" style=\"width: 30%; font-size: 30px;\" value=\"$fname\">";
		echo "<input type=\"text\" name=\"pr-last\" class=\"vform-text vfont-large\" style=\"font-size: 30px; width: 60%; margin-left: 10px\" value=\"$lname\">";
	}
	else {
		echo $vars->details[1];
		echo " ";
		echo $vars->details[2];
	}
	echo "<div style=\"font-size: 15px\">{$vars->details[6]}</div>";
	echo "</div>";
?>
</div>
<div class="content">
<?php
	$sz = sizeof($vars->details);
	if(!$vars->edit) {
		if($sz > 8) {
			for($i = 8; $i < $sz; $i++) { 
				echo "<div class=\"section\">";
				echo "<div class=\"section-title\">";
				echo $vars->details[$i][0];
				echo "</div>";
				echo $vars->details[$i][1];
				echo "</div>";
			}
		}
		else {
			echo "<div class=\"vfont-xx-large\" style=\"text-align: center;\">nothing here</div>";
			if($vars->details[7]) {
				echo "<div style=\"text-align: center;\">edit the profile";
				echo "<div class=\"edit-button\" style=\"float: none;\"><a href=\"{$vars->_fallback->edit}&predit=1\">";
				echo "<img src=\"{$vars->_fallback->media}/switch.png\"><br />edit</a></div>";
				echo "</div>";
			}
		}
	}
	else {
		$items =  array();
		// edit mode
		if($sz > 8) {
			for($i = 8; $i < $sz; $i++) { 
				$items[$vars->details[$i][0]] = 1;
				echo "<div class=\"section\">";
				echo "<div class=\"section-title\">";
				echo $vars->details[$i][0];
				echo "<a href=\"{$vars->_fallback->remitem}&pr-profile={$vars->details[0]}&pr-item={$vars->details[$i][0]}\"><img src=\"{$vars->_fallback->mediag}/delete.png\" title=\"Remove section\" class=\"vfloat-right\"></a>";
				echo "</div>";
				echo "<textarea rows=\"5\" class=\"vform-text\" name=\"pr-{$vars->details[$i][0]}\" style=\"width: 100%;\">";
				echo $vars->details[$i][1];
				echo "</textarea>";
				echo "</div>";
			}
		}

		echo "<input type=\"submit\" value=\"save changes\" class=\"vform-button vform-item-spacer vfont-large\" />";
		echo "<input type=\"hidden\" value=\"{$vars->details[0]}\" name=\"pr-profile\" />";
		echo "</form>";

		//	header previews
		echo "<div style=\"overflow: auto; margin-bottom: 15px;\">";
		echo "<form action=\"\" method=\"post\" style=\"margin-top: 15px;\">";
		if($vars->headers != null) {
			foreach($vars->headers as $header) {
				echo "<a href=\"{$vars->_fallback->setheader}&pr-profile={$vars->details[0]}&pr-header={$header[0]}\">";
				echo "<img  style=\"float: left; margin-right: 10px; margin-bottom: 10px;\" src=\"{$vars->_fallback->media}/headers/{$header[1]}\">";
				echo "</a>";
			}
			echo "<div style=\"clear: left; border: 1px solid #D8D8D8; padding: 3px;\" class=\"vfont-small\">";
			echo "<b>Creative Commons Attributions:</b><br />";
			echo "Bubble, butterfly, star and leaf brushes by <a target=\"_BLANK\" href=\"http://hawksmont.com\">Hawksmont</a><br />";
			echo "Swirl brushes by <a target=\"_BLANK\" href=\"http://szuia.deviantart.com\">Szuia</a></div>";
		}
		echo "</form>";
		echo "</div>";
		
		echo "<div style=\"clear: left;\">";
		echo "<form action=\"{$vars->_fallback->additem}\" method=\"post\" style=\"margin-top: 5px;\">";
		echo "<input type=\"hidden\" value=\"{$vars->details[0]}\" name=\"pr-profile\" />";
		echo "<div class=\"section\">Add ";
		echo "<select name=\"pr-item\" class=\"vform-text vform-select\">";
		echo "<option value=\"About\">About</option>";
		echo "</select> section <input type=\"submit\" value=\"go\" class=\"vform-button\">";
		echo "</form>";
		echo "</div>";

		echo "</div>";
	}
?>
</div>

<?php
	if($vars->details[7]) {
		if($vars->edit) {
			echo "<div class=\"edit-button button-on\"><a href=\"{$vars->_fallback->edit}\">";
			echo "<img src=\"{$vars->_fallback->media}/switch-on.png\"><br />edit</a></div>";
		}
		else {
			echo "<div class=\"edit-button\"><a href=\"{$vars->_fallback->edit}&predit=1\">";
			echo "<img src=\"{$vars->_fallback->media}/switch.png\"><br />edit</a></div>";
		}

	}
?>

</div>
