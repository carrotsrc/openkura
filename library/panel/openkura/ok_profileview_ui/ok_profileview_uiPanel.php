<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_profileview_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_profileview_ui');
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$this->fallback();
			$this->includeTemplate('templates/view.php');
		}

		public function initialize($params = null)
		{
			parent::initialize();
			$profile = null;
			if(isset($_GET['profile']))
				$profile = $_GET['profile'];

			if(isset($_GET['predit']) && $_GET['predit'] == 1) {
				$this->addComponentRequest(20, 101);
				$this->addTParam('edit', true);
			}
			else
				$this->addTParam('edit', false);

			if($profile == null)
				$this->addComponentRequest(1, 101);
			else
				$this->addComponentRequest(2, array('profile' => $profile));

		}

		public function applyRequest($result)
		{
			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
				case 2:
					$details = $rs['result'];
					if($details == 104) {
						$this->addTParam('unk', true);
						break;
					}
					
					$this->addTParam('details', $details[0]);
				break;

				case 20:
					$this->addTParam('headers', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', 'templates/profile.css');
		}

		public function fallback()
		{
			$qstr = QStringModifier::modifyParams(array('predit' => null));
			$this->addFallbackLink('edit', $qstr);

			$img = SystemConfig::relativeLibPath("/media/kura/profile");
			$this->addFallbackLink('media', $img);

			$img = SystemConfig::relativeLibPath("/media/kura/general");
			$this->addFallbackLink('mediag', $img);

			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/10&dbm-redirect=1";
			$this->addFallbackLink('additem', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/11&dbm-redirect=1";
			$this->addFallbackLink('remitem', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/12&dbm-redirect=1";
			$this->addFallbackLink('update', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/21&dbm-redirect=1";
			$this->addFallbackLink('setheader', $qstr);
		}

	}
?>
