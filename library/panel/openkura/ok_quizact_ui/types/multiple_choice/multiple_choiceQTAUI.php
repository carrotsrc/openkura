<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 class multiple_choiceQTAUI extends QType_ActUI
	{
		protected $session;
		public function setSession($ses)
		{
			$this->session = $ses;
		}

		public function generateForm($id)
		{
			echo "<b>";
			$content = $this->session[$id][0];
			$content = StrSan::htmlSanatize($content);
			$content = StrSan::sqhtmlSanatize($content);

			echo $content;
			echo "</b>";
			$i = 1;
			echo "<div style=\"margin-left: 15px; margin-top: 7px\">";
			echo "<table>";
			foreach($this->session[$id] as $k => $q) {

				if($k == 0)
					continue;

				$content = $q[0];
				$content = StrSan::htmlSanatize($content);
				$content = StrSan::sqhtmlSanatize($content);

				echo "<tr style=\"border-top-spacing: 50px;\">";
				echo "<td style=\"vertical-align: top;\">";
					echo "<input type=\"radio\" name=\"qmc$id\" value=\"$k\"/>";
				echo "</td>";

				echo "<td style=\"vertical-align: top;\">";
				echo "$i.";
				echo "</td>";
				echo "<td>{$content}<br />";
				$i++;

				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>";

		}
	}
?>
