<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include("qtype_actui.php");
	class ok_quizact_uiPanel extends Panel
	{
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_quizact_ui');
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$this->fallback();
			switch($this->mode) {
			case 0:
				$this->includeTemplate("template/main.php");
			break;

			case 1:
				$this->includeTemplate("template/complete.php");
			break;
			}
		}

		public function initialize($params = null)
		{
			if(isset($_GET['qtamcc'])) {
				$this->mode = 1;
				$this->addTParam('title', "Quiz Complete!");
			}
			else {
				$this->addComponentRequest(1, 101);
				$this->addComponentRequest(2, 101);
				$this->addComponentRequest(3, 101);
				$this->addComponentRequest(4, 101);
			}
			parent::initialize();
		}

		public function loadUIObj($type)
		{
			include_once("types/{$type}/{$type}QTAUI.php");
			$str = "{$type}QTAUI";
			$obj = new $str();
			$this->types[$type] = $obj;
			return $obj;
		}

		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					if($rs['result'] == 104) {
						$this->mode = 1;
						$this->addTParam('title', "Quiz already completed");
					}
					$this->addTParam('header', $rs['result']);
				break;

				case 2:
					$this->addTParam('scheme', $rs['result']);
				break;

				case 3:
					$types = array();
					foreach($rs['result'] as $k => $v) {
						$obj = $this->loadUIObj($k);
						$obj->setSession($v);
						$types[$k] = $obj;
					}

					$this->addTParam('types', $types);
				break;

				case 4:
					$this->addTParam('sechdr', $rs['result']);
				break;
				}
			}
		}

		public function fallback()
		{
			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/5&dbm-redirect=1";
			$this->addFallbackLink('submit', $qstr);

			$qstr = QStringModifier::modifyParams(array('qtamcc' => null, 'loc' => "my-records"));
			$this->addFallbackLink('records', $qstr);
		}

		public function setAssets()
		{
			$this->addAsset('css', '/.assets/quizstyle.css');
		}
	}
?>
