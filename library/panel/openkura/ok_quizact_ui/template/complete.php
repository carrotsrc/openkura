<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="quizarea">
<b>Quiz Activity</b>
</div>
<?php
	echo "<div class=\"quiz-header-title\" style=\"border-width: 0px;\">";
	echo $vars->title;
	echo "</div>";

	echo "<div class=\"quiz-item\">";
	echo "<div class=\"question-title\" style=\"color: #5F5F5F; border-width: 0px;\">";
	echo "Check your <a href=\"{$vars->_fallback->records}\">records</a>";
	echo "</div>";
	echo "</div>";
?>

