<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="quizarea">
<b>Quiz Activity</b><br />
<div class="vform-item">
<?php
	$timestamp = strtotime("now"); 
	echo date('d/m/y', $timestamp);
?>
</div>
<div class="quiz-header">
<div class="quiz-header-title">
<?php
	echo $vars->header['title'];
?>
</div>
</div>

<?php
	echo "<form action=\"{$vars->_fallback->submit}\" method=\"post\">";
	$i = 1;
	foreach($vars->scheme as $k => $q) {
		$mark = 1;
		foreach($vars->sechdr[$k] as $pt => $p) {
			$ptitle = $pt;
			
			if(($pos = strpos($pt, '-')))
				$ptitle = substr($pt, 0, $pos);

			switch($ptitle){
			case 'mark':
				$mark = $p;
			break;
			}
		}
		echo "<div class=\"quiz-item\" style=\"overflow: auto;\">";

		echo "<div class=\"question-title\" style=\"overflow: auto; height: auto;\">";
			echo "Question $i";
			//echo "<div class=\"quiz-tool\" style=\"display: inline; overflow: auto; height: auto;\">[ $mark ]</div>";
		echo "</div>";
		echo "<div class=\"quiz-item\">";
		echo "<div class=\"quiz-area\">";
		foreach($vars->sechdr[$k] as $pt => $p) {
			$ptitle = $pt;
			
			if(($pos = strpos($pt, '-')))
				$ptitle = substr($pt, 0, $pos);

			switch($ptitle){
			case 'text':
				echo "<div class=\"quiz-info\">$p</div>";
			break;
			}
		}
			$vars->types[$q]->generateForm($k);
		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "<div class=\"question-gap\"></div>";
		$i++;
	}
?>
<input type="submit" value="Submit answers" class="vform-button vfloat-right" />
</form>
</div>
