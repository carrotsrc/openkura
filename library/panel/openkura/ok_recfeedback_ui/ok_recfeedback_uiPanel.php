<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("types/qtyperecordui.php");
	include_once("qtr_propui.php");
	class ok_recfeedback_uiPanel extends Panel
	{
		private $types;
		private $mode;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_recfeedback_ui');
			$this->types = array();
			$this->mode = 0;
		}

		public function loadTemplate()
		{
			$mpath = SystemConfig::appRelativePath("library/media/kura");
			$this->addTParam('mpath', $mpath);

			switch($this->mode) {
			case 0:
				$this->addTParam('types', $this->types);
				$mpath = SystemConfig::appRelativePath("library/media/kura/quizman");
				$qtrpui = new QTR_PropUI($mpath);
				$this->addTParam('qtrprop', $qtrpui);

				$this->fallback();
				$this->includeTemplate("template/main.php");
			break;

			case 1:
				$this->includeTemplate("template/done.php");
			break;
			}
		}

		public function initialize($params = null)
		{
			parent::initialize();
			$id = null;
			if(isset($_GET['rfbd'])) {
				$this->mode = 1;
				return;
			}
			else
			if(isset($_GET['_ref'])) {
				$id = $_GET['_ref'];
				$this->addComponentRequest(12, array('recid' => $id)); // clear
				$this->addComponentRequest(10, array('recid' => $id)); // load record
				unset($_GET['_ref']);
				$_GET['rfref'] = $id;
			}
			else {
				$id = $_GET['rfref'];
				$this->addComponentRequest(11, array('recid' => $id)); // pull session
			}

			$this->addComponentRequest(20, 101); // get header
			$this->addComponentRequest(21, 101); // get scheme
			$this->addComponentRequest(22, 101); // get type sessions
			$this->addComponentRequest(23, 101); // get section headers
			$this->addComponentRequest(24, 101); // get record session
			$this->addComponentRequest(25, 101); // get record headers
		}

		private function loadUIObj($type)
		{
			include_once("types/{$type}/{$type}QTRUI.php");
			$str = "{$type}QTRUI";
			$obj = new $str();
			return $obj;
		}

		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 20:
					$this->addTParam('theader', $rs['result']);
				break;

				case 21:
					$this->addtparam('scheme', $rs['result']);
				break;

				case 22:
					foreach($rs['result'] as $k => $v) {
						$obj = $this->loadUIObj($k);
						$obj->setSession($v);
						$this->types[$k] = $obj;
					}
					$this->addtparam('tsession', $rs['result']);
				break;

				case 23:
					$this->addtparam('sheader', $rs['result']);
				break;

				case 24:
					foreach($rs['result'] as $t => $v)
						$this->types[$t]->setRecord($v);
				break;

				case 25:
					$this->addTParam('rheader', $rs['result']);
				break;
				}
			}
		}

		public function fallback()
		{
			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/30&dbm-redirect=1";
			$this->addFallbackLink('addfeedback', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/32&dbm-redirect=1";
			$this->addFallbackLink('storefeedback', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/31&dbm-redirect=1";
			$this->addFallbackLink('rem', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/33&dbm-redirect=1";
			$this->addFallbackLink('save', $qstr);

			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/40&dbm-redirect=1";
			$this->addFallbackLink('bind', $qstr);
		}

		public function setAssets()
		{
			$this->addAsset('css', '/.assets/quizstyle.css');
		}
	}
?>
