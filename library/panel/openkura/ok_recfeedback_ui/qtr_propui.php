<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class QTR_PropUI
	{
		private $media;
		public function __construct($mlink)
		{
			$this->media = $mlink;
		}
		public function generateForm($prefix, $prop, $val, $remlink)
		{
			$pn = $prop;

			if(($p = strpos($prop, "-")))
				$pn = substr($prop, 0, $p);

			switch($pn) {
			case 'feedback':
				return $this->feedback($prefix, $prop, $val, $remlink);
			break;

			case 'mark':
				return $this->mark($prefix, $prop, $val, $remlink);
			break;
			}

			return false;
		}

		public function feedback($prefix, $prop, $val, $remlink)
		{
			ob_start();

			echo " <a style=\"\" class=\"vfloat-right\" href=\"$remlink&qrftype=$prop\"><img src=\"{$this->media}/delete.png\" /></a>";
			echo "Textual Feedback:<br />";
			echo "<textarea class=\"vform-text\" name=\"{$prefix}_{$prop}\" rows=\"7\" cols=\"50\">$val</textarea>";
			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}

		public function feedbackRep($val)
		{
			
		}
	}
?>
