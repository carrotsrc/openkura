<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="quizarea">
<b>Activity Feedback</b><br />
<div class="quiz-header">
<div class="quiz-header-title">
<?php
	echo $vars->theader['title'];
?>
</div>
</div>

<div class="question-area">
<?php
	$q = 1;
	foreach($vars->scheme as $sid => $type) {
		echo "<div class=\"question-title\">";
		echo "Question $q";
		echo "</div>";

		echo "<div class=\"quiz-item\">";
		$vars->types[$type]->genRecordUI($sid);
		echo "</div>";
		if(!isset($vars->rheader[$sid]) || !isset($vars->rheader[$sid]['feedback'])) {
			echo "<form method=\"post\" action=\"{$vars->_fallback->addfeedback}\" class=\"vform-item\">";
			echo "<input type=\"hidden\" name=\"qrftype\" value=\"feedback\">";
			echo "<input type=\"hidden\" name=\"qrfsid\" value=\"$sid\">";
			echo "<input type=\"submit\" class=\"vform-button vfloat-right\" value=\"Add feedback\">";
			echo "</form>";
		}
		else {
			echo "<form method=\"post\" action=\"{$vars->_fallback->storefeedback}\" class=\"vform-item\">";
			echo "<div class=\"question-edit\">";
			echo "<div class=\"question-edit-form\">";

			echo "<div style=\"color: orange; font-weight:bold; margin-bottom: 15px;\">Feedback</div>";
			foreach($vars->rheader[$sid] as $k => $v) {
				$rlink = $vars->_fallback->rem."&qrfsid=$sid";
				echo $vars->qtrprop->generateForm("qrfsp", $k, $v, $rlink);
			}

			echo "<div class=\"vform-item\" style=\"overflow: auto;\">";
			echo "<input type=\"hidden\" name=\"qrfsid\" value=\"$sid\">";
			echo "<input type=\"submit\" class=\"vform-button vfloat-right\" value=\"Store feedback\">";
			echo "</div>";
			
			echo "</div>";
			echo "</div>";
			echo "</form>";
		}
		echo "</font>";

		echo "<div class=\"question-gap\"></div>";

	}

	echo "<div style=\"border-top: 2px dashed #D8D8D8; margin-top: 20px;\">";
	//echo "<div style=\"\">";
	echo "<form method=\"post\" action=\"{$vars->_fallback->save}\" class=\"vform-item\">";
	echo "<input type=\"submit\" class=\"vform-button vfloat-right\" value=\"Save\">";
	echo "</form>";
	//echo "</div>";

	echo "<form method=\"post\" action=\"{$vars->_fallback->bind}\" style=\"margin-top: -8px;\">";
	echo "<input type=\"submit\" class=\"vform-button vfloat-left\" value=\"Bind record\">";
	echo "</form>";
	echo "</div>";
?>
</div>
</div>


