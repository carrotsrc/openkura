<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="quizarea">
<b>Activity Feedback</b><br />
<div class="question-area">

<div class="vfloat-left">
	<img src="<?php echo $vars->mpath; ?>/general/gtick-96.png" />
</div>

<div class="vfloat-left vfont-x-large" style="font-weight: bold; margin-left: 30px; color: #7D9E05;">
	<br />
	Record bound successfully
</div>

</div>
</div>

