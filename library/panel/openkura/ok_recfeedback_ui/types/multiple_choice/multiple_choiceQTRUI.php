<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class multiple_choiceQTRUI extends QTypeRecordUI
	{
		public function __construct()
		{
		}

		public function setRecord($record)
		{
			$this->record = $record;
		}

		public function setSession($session)
		{
			$this->session = $session;
		}

		public function genRecordUI($id)
		{
			$correct = false;

			echo "<div class=\"light-blue vfont-large\" style=\"margin-bottom: 10px;\">";
				echo $this->session[$id][0];
			echo "</div>";

			if($this->record[$id][1]) {
				$correct = true;
			}
			else {
				$ch = $this->record[$id][0];
				echo "Selected Answer:";
				echo "<div style=\"margin-left: 25px; margin-top: 5px;\">";
				echo "{$this->session[$id][$ch][0]}";
				echo "</div>";

				echo "<br />Correction:";
				echo "<div style=\"margin-left: 25px; margin-top: 5px;\">";
				
				foreach($this->session[$id] as $ch)
					if($ch[1] == 1)
						echo $ch[0];
				echo "</div>";
			}

			return $correct;
		}
	}
?>

