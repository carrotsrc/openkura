<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_linkgroup_uiPanel extends Panel
	{
		private $group;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_linkgroup_ui');
		}

		public function loadTemplate()
		{
			if($this->group == false) {
				echo "<span class=\"vp-error\">Error!</span>";
				return;
			}

			echo "<div class=\"link-group\">";
			echo "<div class=\"header\">";
				foreach($this->group as $g)
					if($g[2] == 0)
						echo $g[4];
			echo "</div>";
			echo "<div class=\"stack-container\">";
			foreach($this->group as $g) {
				if($g[2] == 0)
					continue;
				
				echo "<div class=\"item\">";
				if($g[3] == 0) {
					$grp = $this->globalParamStr();
					echo "<a href=\"?loc={$g[5]}{$grp}\">{$g[4]}</a>";
				} else
				if($g[3] == 1) {
					echo "<a href=\"?loc={$g[5]}\">{$g[4]}</a>";
				} else
				if($g[3] == 2) {
					echo "<a href=\"{$g[5]}\">{$g[4]}</a>";
				} else
				if($g[3] == 3) {
					echo "<a href=\"{$g[5]}\" target=\"_BLANK\">{$g[4]}</a>";
				}
				echo "</div>";
			}
			echo "</div>";
			echo "</div>";
		}

		public function initialize($params = null)
		{
			$this->addComponentRequest(1, 101);
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					$this->group = $rs['result'];
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "/.assets/general.css");
		}
	}
?>
