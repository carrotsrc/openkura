<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="tutor-container">
	<div class="toolbar-container">
		<div class="h-toolbar">
			<div class="h-button h-button-top-left" id="<?php echo $vars->_pmod ?>-button-genq">
				<a href="" id="<?php echo $vars->_pmod ?>-link-genq">General Questions</a>
			</div>
			<div class="h-button" id="<?php echo $vars->_pmod ?>-button-myq">
				<a href="" id="<?php echo $vars->_pmod ?>-link-myq">My Questions</a>
			</div>
			<div class="h-button" id="<?php echo $vars->_pmod ?>-button-newq">
				<a href="" id="<?php echo $vars->_pmod ?>-link-newq">New Question</a>
			</div>

			<div class="h-title">
				Tutor Dialogue
			</div>
		</div>
		<div class="h-toolbar h-toolbar-separator" id="<?php echo $vars->_pmod ?>-filter-tools">
			<div class="h-label">Filters</div>
			<div class="h-filter-collection">
				<div class="h-filter h-filter-selected">
					<a href="" id="<?php echo $vars->_pmod ?>-fstateActive">active</a>
				</div>
				<div class="h-filter">
					<a href="" id="<?php echo $vars->_pmod ?>-fstateClosed">closed</a>
				</div>
			</div>

			<div class="h-filter-collection">
				<div class="h-filter">
					<a href="" id="<?php echo $vars->_pmod ?>-fpriorityHigh">High</a>
				</div>
				<div class="h-filter">
					<a href="" id="<?php echo $vars->_pmod ?>-fpriorityMedium">Medium</a>
				</div>
				<div class="h-filter h-filter-selected">
					<a href="" id="<?php echo $vars->_pmod ?>-fpriorityLow">Low</a>
				</div>
			</div>
			<div class="h-filter-collection">
				<div class="h-filter h-filter-selected">
					Year 1
				</div>
				<div class="h-filter">
					Year 2
				</div>
				<div class="h-filter">
					Year 3
				</div>
			</div>
		</div>
	</div>
	<div class="h-inbox-container" id="<?php echo $vars->_pmod ?>-inbox-container">
	</div>
</div>
