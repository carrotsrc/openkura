/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

function OK_TutorInterface() {

	this.media = "APP-MEDIAkura/tutor";
	var self = this;
	var state = 1;
	this.filter = {
		stateActive: 1,
		stateClosed: undefined,

		priorityHigh: undefined,
		priorityMedium: undefined,
		priorityLow: 1,

		groups: new Array(),

		generateFilterString: function () {
			var str = "";

			var filterStr = "";

			if(self.filter.stateActive !== undefined)
				str += self.filter.stateActive;

			if(self.filter.stateClosed !== undefined) {
				if(str.length > 0)
					str += ";";
				str += self.filter.stateClosed;
			}

			if(str.length > 0) {
				filterStr += "&fstatus="+str;
				str = "";
			}

			if(self.filter.priorityLow !== undefined)
				str += self.filter.priorityLow;

			if(self.filter.priorityMedium !== undefined) {
				if(str.length > 0)
					str += ";";
				str += self.filter.priorityMedium;
			}

			if(self.filter.priorityHigh !== undefined) {
				if(str.length > 0)
					str += ";";
				str += self.filter.priorityHigh;
			}

			if(str.length > 0) {
				filterStr += "&fpriority="+str;
				str = "";
			}

			if(self.filter.groups.length > 0) {
				var sz = self.filter.groups.length;
				for(var i =0; i < sz; i++) {
					str +=self.filter.groups[i];
					if(i < sz-1)
						str += ";";
				}
				filterStr += "&fgroups="+str;
			}

			return filterStr;
		}
	}

	this.initialize = function () {
		self._loop.modifyLink("link-newq", self._loop.uio_compose);
		self._loop.modifyLink("link-genq", function() {
			self.ui_highlight("genq");
			self.uio_filterBar();
			self.state = 1;
			self._loop.requestQuestions(1);
		});

		self._loop.modifyLink("link-myq", function() {
			self.ui_highlight("myq");
			self.uio_filterBar();
			self.state = 0;
			self._loop.requestQuestions(0);
		});

		self.organiseHFilters();
		self.ui_highlight("genq");
		self._loop.requestQuestions(1);
		self.requestUserGroups();
	}

	this.organiseHFilters = function () {
		self._loop.modifyLink("fstateActive", function () {
			var e = this.parentNode;
			if(self.filter.stateActive == undefined) {
				self.filter.stateActive = 1;
				e.className = "h-filter h-filter-selected";
			} else {
				self.filter.stateActive = undefined;
				e.className = "h-filter";
			}

			self._loop.requestQuestions(self.state);
		});

		self._loop.modifyLink("fstateClosed", function () {
			var e = this.parentNode;
			if(self.filter.stateClosed == undefined) {
				self.filter.stateClosed = 0;
				e.className = "h-filter h-filter-selected";
			} else {
				self.filter.stateClosed = undefined;
				e.className = "h-filter";
			}

			self._loop.requestQuestions(self.state);
		});


		self._loop.modifyLink("fpriorityHigh", function () {
			var e = this.parentNode;
			if(self.filter.priorityHigh == undefined) {
				self.filter.priorityHigh = 3;
				e.className = "h-filter h-filter-selected";
			} else {
				self.filter.priorityHigh = undefined;
				e.className = "h-filter";
			}

			self._loop.requestQuestions(self.state);
		});

		self._loop.modifyLink("fpriorityMedium", function () {
			var e = this.parentNode;
			if(self.filter.priorityMedium == undefined) {
				self.filter.priorityMedium = 2;
				e.className = "h-filter h-filter-selected";
			} else {
				self.filter.priorityMedium = undefined;
				e.className = "h-filter";
			}

			self._loop.requestQuestions(self.state);
		});

		self._loop.modifyLink("fpriorityLow", function () {
			var e = this.parentNode;
			if(self.filter.priorityLow == undefined) {
				self.filter.priorityLow = 1;
				e.className = "h-filter h-filter-selected";
			} else {
				self.filter.priorityLow = undefined;
				e.className = "h-filter";
			}

			self._loop.requestQuestions(self.state);
		});
	}

	this.requestPostMessage = function (priority, type, subject, body, group) {
		var postStr = "tdp="+priority+"&tdt="+type+"&tds="+subject+"&tdb="+body;
		if(group != undefined)
			postStr += "tdg="+group;

		self._loop.request(10, null, self._loop.onresponsePostMessage, postStr);
	}

	this.onresponsePostMessage = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104) {
			var e = document.getElementById(self._loop._pmod+"-error-msg");
			var u = KTSet.NodeUtl(e);
			u.clearChildren();
			u.appendText("Error occured posting question");
			return;
		}
		
		var type = document.getElementById(self._loop._pmod+"-type").value;
		self._loop.requestQuestions(type);
	}

	this.requestPostDialogue = function (tid, body) {
		var postStr = "&tdi="+tid+"&tdb="+body;

		self._loop.request(11, null, self._loop.onresponsePostDialogue, postStr);
	}

	this.onresponsePostDialogue = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104)
			return;

		self._loop.requestDialogue(response.data);
	}

	this.requestQuestions = function (type) {
		str = self.filter.generateFilterString();
		str += "&fpublic="+type;

		self._loop.request(1, str, self._loop.onresponseQuestions);
	}

	this.onresponseQuestions = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104) {
			self._loop.uio_questions(null);
			return;
		}

		self._loop.uio_questions(response.data);
	}

	this.requestDialogue = function (tid) {
		self._loop.request(3, "tdti="+tid, self._loop.onresponseDialogue);
	}

	this.onresponseDialogue = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104)
			return;
		self._loop.uio_dialogue(response.ticket, response.data);
	}

	this.requestChangeState = function (tid, state) {
		self._loop.request(20, null, self._loop.onresponseChangeState,"tdi="+tid+"&tds="+state);
	}

	this.onresponseChangeState = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104)
			return;
		self._loop.requestDialogue(response.data);
	}

	this.requestUserGroups = function () {
		self.request(2, null, self.onresponseUserGroups);
	}

	this.onresponseUserGroups = function (reply) {
		var response = JSON.parse(reply);
		if(response.code == 104)
			return;
	
	}

	this.uio_compose = function () {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);

		u.appendChild('div');
			u.gotoLast();
			u.node.className = "compose-form";
			u.appendChild('div');
				u.gotoLast();
				u.appendText("Priority: ");
				u.appendChild('select');
					u.gotoLast();
					u.node.id = self._loop._pmod+"-priority";
					u.node.style="margin-right: 10px";
					u.node.className = "vform-text vform-select";

					u.appendChild('option');
						u.gotoLast();
						u.node.value = "1";
						u.appendText("Low");
						u.gotoParent();

					u.appendChild('option');
						u.gotoLast();
						u.node.value = "2";
						u.appendText("Medium");
						u.gotoParent();

					u.appendChild('option');
						u.gotoLast();
						u.node.value = "3";
						u.appendText("High");
						u.gotoParent();

					u.gotoParent();

				u.appendText(" Type: ");
				u.appendChild('select');
					u.gotoLast();
					u.node.id = self._loop._pmod+"-type";
					u.node.className = "vform-text vform-select";

					u.appendChild('option');
						u.gotoLast();
						u.node.value = "1";
						u.appendText("Public");
						u.gotoParent();

					u.appendChild('option');
						u.gotoLast();
						u.node.value = "0";
						u.appendText("Private");
						u.gotoParent();

					u.gotoParent();
				u.gotoParent();


			u.appendChild('div');
				u.gotoLast();
				u.node.className = "vform-item-spacer";
				u.appendChild('input');
					u.gotoLast();
					u.node.id = self._loop._pmod+"-subject";
					u.node.style="width: 75%";
					u.node.className = "vform-text vfont-large";
					u.node.value="Question subject";
					u.node.maxLength = "256";
					u.node.isFresh = true;
					u.node.style.color = "#D8D8D8";
					u.node.onfocus = function () {
						if(this.isFresh) {
							this.value = "";
							this.isFresh = false;
							this.style.color = "#808080";
						}
					}

					u.node.onblur = function () {
						if(!this.isFresh && this.value == "") {
							this.value = "Question subject";
							this.isFresh = true;
							this.style.color = "#D8D8D8";
						}
					}
					u.gotoParent();
				u.gotoParent();
			
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "vform-item-spacer";
				u.appendChild('textarea');
					u.gotoLast();
					u.node.id = self._loop._pmod+"-body";
					u.node.className = "vform-text";
					u.node.isFresh = true;
					u.node.value="Question details";
					u.node.onfocus = function () {
						if(this.isFresh) {
							this.value = "";
							this.isFresh = false;
							this.style.color = "#808080";
						}
					}

					u.node.onblur = function () {
						if(!this.isFresh && this.value == "") {
							this.value = "Question details";
							this.isFresh = true;
							this.style.color = "#D8D8D8";
						}
					}
					u.node.style="width: 100%; color: #D8D8D8;";
					u.node.rows = "8";
					u.gotoParent();
				u.gotoParent();

			u.appendChild('div');
				u.gotoLast();
				u.appendChild('span');
					u.gotoLast();
					u.node.id = self._loop._pmod+"-error-msg";
					u.node.style="red";
					u.gotoParent();
				u.node.className = "vform-item-spacer";
				u.appendChild('input');
					u.gotoLast();
					u.node.type="button";
					u.node.className="vform-button vfont-large vfloat-right";
					u.node.value="Post Question";

					u.node.onclick = function () {
						var priority = document.getElementById(self._loop._pmod+"-priority").value;
						var type = document.getElementById(self._loop._pmod+"-type").value;
						var subject = document.getElementById(self._loop._pmod+"-subject").value;
						var body = document.getElementById(self._loop._pmod+"-body").value;
						var e = document.getElementById(self._loop._pmod+"-group");
						var group = undefined;
						if(e !== null)
							group = e.value;

						self._loop.requestPostMessage(priority, type, subject, body, group);
					}

					u.gotoParent();
				u.gotoParent();

			u.gotoParent();

		self.uic_filterBar();
		self.ui_highlight("newq");
		e = document.getElementById(self._loop._pmod+"-inbox-container");
		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}

	this.uio_questions = function (questions) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		u.appendChild('div');
			u.gotoLast();
		if(questions != null) {
			var img = Array("closed.png", "low.png", "medium.png", "high.png");
			var sz = questions.length;
			for(var i =0; i < sz; i++) {
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "h-post";
					u.appendChild('img');
						u.gotoLast();
						if(questions[i].status == 0)
							questions[i].priority = 0;

						u.node.src = self.media+"/"+img[questions[i].priority];
						u.gotoParent();
					u.appendChild('a');
						u.gotoLast();
						u.node.href="javascript:void(0)";
						u.appendText(questions[i].subject);
						u.node.tid = questions[i].id;
						u.node.onclick = function () {
							self._loop.requestDialogue(this.tid);
						}
						u.gotoParent();
					u.gotoParent();
			}
		}
		else {
			u.appendText("No questions found");
		}
			u.gotoParent();

		self._loop.uio_filterBar();
		e = document.getElementById(self._loop._pmod+"-inbox-container");
		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);

	}

	this.uio_dialogue = function (ticket, dialogue) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		var img = Array("closed.png", "low.png", "medium.png", "high.png");
		u.appendChild('div');
			u.gotoLast();

			u.appendChild('div');
				u.gotoLast();
				u.node.className = "h-dialogue-ticket";
				u.appendChild('img')
					u.gotoLast();
					u.node.src = ticket.avatar;
					u.gotoParent();

				u.appendChild('img')
					u.gotoLast();
					if(ticket.status == 0)
						ticket.priority = 0;


					u.node.src = self.media+"/"+img[ticket.priority];
					u.node.style="float: right;";
					u.gotoParent();

				u.appendChild('div')
					u.gotoLast();
					u.node.className = "question";
					u.appendText(ticket.subject);
					u.gotoParent();
				u.appendChild('div')
					u.gotoLast();
					u.node.className = "body";
					u.appendText(ticket.body);
					u.gotoParent();
				u.appendChild('div')
					u.gotoLast();
					u.node.style = "clear: left; margin-top: 5px;";
					u.appendText(ticket.name +" @ "+ticket.posted);
					u.gotoParent();
				u.gotoParent();

				if(ticket.status == 0) {
					u.appendChild('div');
						u.gotoLast();
						u.node.className = "h-dialogue-tool h-dialogue-green";
						u.appendText("Question Answered!");
						u.gotoParent();
				}
				else
				if(ticket.isOwner == 1) {
					u.appendChild('div');
						u.gotoLast();
						u.node.className = "h-dialogue-tool";
						u.appendText("Once this question has been answered to your satisfaction:");
						u.appendChild('br');
						u.appendChild('input');
							u.gotoLast();
							u.node.type="button";
							u.node.className = "vform-button vfont-large vform-item";
							u.node.style="cursor: pointer;";
							//u.node.style = "border-color: #6A9F06; background-color: #B1DA62; color: #6A9F06;";
							u.node.value="Answered!";
							u.node.tid = ticket.id;
							u.node.onclick = function () {
								self._loop.requestChangeState(this.tid, 0);
							}
							u.gotoParent();
						u.gotoParent();
				}

		if(dialogue != 0) {
			var sz = dialogue.length;
			for(var i =0; i < sz; i++) {
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "h-dialogue-message";
					u.appendText(dialogue[i].body);
					u.gotoParent();
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "h-dialogue-detail";
					u.appendChild('span');
						u.gotoLast();
						u.node.className = "name-detail";
						u.appendText(dialogue[i].name);
						u.gotoParent();
					u.appendText(" @ "+dialogue[i].posted);
					u.gotoParent();
			}
		}

			u.appendChild('div');
				u.gotoLast();
				u.node.className = "vform-item-spacer";
				u.appendChild('textarea');
					u.gotoLast();
					u.node.className = "vform-text";
					u.node.id = self._loop._pmod +"-dialogue-compose";
					u.node.cols = "80";
					u.node.rows = "8";
					u.gotoParent();
				u.gotoParent();
				u.appendChild('input');
					u.gotoLast();
					u.node.type="button";
					u.node.className="vform-button vfont-large vform-item";
					u.node.value="Post Reply";
					u.node.tid = ticket.id;
					u.node.onclick = function () {
						var e = document.getElementById(self._loop._pmod+"-dialogue-compose");
						self._loop.requestPostDialogue(this.tid, e.value);
					}
			u.gotoParent();

		self.uic_filterBar();
		e = document.getElementById(self._loop._pmod+"-inbox-container");
		u = KTSet.NodeUtl(e);
		u.clearChildren();
		u.appendChild(r);
	}

	this.ui_highlight = function (button) {
		var e = document.getElementById(self._loop._pmod+"-button-genq");
		e.style.backgroundColor = "#dfdfdf";

		e = document.getElementById(self._loop._pmod+"-button-myq");
		e.style.backgroundColor = "#dfdfdf";

		e = document.getElementById(self._loop._pmod+"-button-newq");
		e.style.backgroundColor = "#dfdfdf";

		e = document.getElementById(self._loop._pmod+"-button-"+button);
		e.style.backgroundColor = "#efefef";
	}

	this.uio_filterBar = function () {
		var e = document.getElementById(self._loop._pmod+"-filter-tools");
		e.style.display = "block";
	}

	this.uic_filterBar = function () {
		var e = document.getElementById(self._loop._pmod+"-filter-tools");
		e.style.display = "none";
	}
}

OK_TutorInterface.prototype = new KitJS.PanelInterface('ok_tutor_ui');

