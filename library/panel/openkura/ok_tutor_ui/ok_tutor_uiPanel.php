<?php
	class ok_tutor_uiPanel extends Panel
	{
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_tutor_ui');
			$this->jsCommon = "OK_TutorInterface";
		}

		public function loadTemplate()
		{
			$this->fallback();

			$this->includeTemplate("template/inbox.php");
		}

		public function initialize($params = null)
		{
			parent::initialize();
		}
		public function applyRequest($result)
		{
			foreach($result as $rs) {
				$r = $rs['result'];
				switch($rs['jack']) {
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('js', "/G/toolset.js");
			$this->addAsset('js', "template/tutor_sc.js");
			$this->addAsset('css', "template/tutor_style.css");
		}

		public function fallback()
		{
			$img = SystemConfig::relativeLibPath("/media/kura/tutor");
			$this->addTParam('media', $img);
		}
	}
?>
