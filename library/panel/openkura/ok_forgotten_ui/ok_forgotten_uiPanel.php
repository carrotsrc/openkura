<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_forgotten_uiPanel extends Panel
	{
		private $mode = 0;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_forgotten_ui');
		}

		public function loadTemplate()
		{
			$this->fallback();
			if($this->mode == 0) {
				$this->includeTemplate("template/main.php");
			}
			else 
			if($this->mode == 1) {
				$this->includeTemplate("template/form.php");
			}
			else
			if($this->mode == 2) {
				$this->includeTemplate("template/success.php");
			}
		}

		public function initialize($params = null)
		{
			if(isset($_GET['fpcode']) && $_GET['fpcode'] == 0)
				$this->addTParam("error", "An error occurred please contact admin (at) openkura (dot) org");
			else
			if(isset($_GET['fpcode']) && $_GET['fpcode'] == 1)
				$this->addTParam("msg", "Successfully filed request for password reset. Please check your email");

			if(isset($_GET['fpkey'])) {
				if(isset($_POST['fppw'])) {
					if($_POST['fppw'] == $_POST['fppchk']) {
						$this->addComponentRequest(2, array('hash' => $_GET['fpkey'], 'password' => $_POST['fppw']));
						$this->mode = 2;
					} else {
						$this->addTParam('error', "Passwords do not match");
						$this->mode = 1;
					}
				}
				else
					$this->mode = 1;
			}
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 2:
					if($rs['result'] == 104) {
						$this->addTParam('error', "Something went wrong!");
						$this->mode = 1;
						break;
					}
				break;

				}
			}
		}

		public function setAssets()
		{

		}

		private function fallback()
		{
			$spath = SystemConfig::appRelativePath(Managers::AppConfig()->setting('submitrequest'));
			$aid = Session::get('aid');
			$area = Managers::ResourceManager()->getHandlerRef($aid);
			$qstr = "$spath?cpl=$area/{$this->componentId}/{$this->instanceId}/1&dbm-redirect=1";
			$this->addFallbackLink('submit', $qstr);

			$qstr = QStringModifier::modifyParams(array());
			$this->addFallbackLink('script', $qstr);
		}
	}
?>
