<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div style="color: #909090;">
<div style="color: #7D9E05; font-size: 30px; font-weight: bold; margin-bottom: 30px;">Success!</div>
Please login with your new password<br />

<a href="?loc=web" class="vfont-x-large">Login</a>
</div>
