<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div style="color: #909090;">
<div style="color: #7D9E05; font-size: 30px; font-weight: bold; margin-bottom: 30px;">Forgotten password</div>

Please enter your username and an email will be sent to the address you used to register.<br />
Follow the instructions in the email.

<form method="post" class="vform-item" action="<?php echo $vars->_fallback->submit; ?>">
Username: <input type="text" name="user" class="vform-text" />
<div>
<input type="submit" value="Send" class="vform-button vform-item" />
</div>
</form>
<div style="color: red;">
<?php
	if($vars->error != null)
		echo $vars->error;
?>
</div>
<div style="color: #7D9E05;">
<?php
	if($vars->msg != null)
		echo $vars->msg;
?>
</div>
</div>
