<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div style="color: #909090;">
<div style="color: #7D9E05; font-size: 30px; font-weight: bold; margin-bottom: 30px;">Password reset</div>
Please enter a new password:
<form method="post" action="<?php echo $vars->_fallback->script; ?>">
<input type="password" name="fppw" class="vform-text" /><br />
<div class="vform-item">
Please re-enter the password:<br />
<input type="password" name="fppchk" class="vform-text" />
</div>
<input type="submit" value="Send" class="vform-button vform-item" />
</form>
<div style="color: red;">
<?php

if($vars->error != null) {
	echo $vars->error;
}
?>
</div>
</div>

