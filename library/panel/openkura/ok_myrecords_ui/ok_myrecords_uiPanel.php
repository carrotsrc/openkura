<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	include_once("types/qtyperecordui.php");
	class ok_myrecords_uiPanel extends Panel
	{
		private $mode;
		private $types;
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_myrecords_ui');
			$mode = 0;
		}

		public function loadTemplate()
		{
			$this->setParams();
			if($this->mode == 0)
				$this->includeTemplate("template/main.php");
			else
			if($this->mode == 1)
				$this->includeTemplate("template/view.php");

		}

		public function initialize($params = null)
		{
			if(isset($_GET['recid'])) {
				$this->addComponentRequest(4, array(
								'recid' => $_GET['recid']
								));
				$this->addComponentRequest(2, array(
								'recid' => $_GET['recid']
								));
				$this->addComponentRequest(3, array(
								'recid' => $_GET['recid']
								));
				$this->addComponentRequest(5, array(
								'recid' => $_GET['recid']
								));
				$this->mode = 1;
			}
			else
				$this->addComponentRequest(1, 101);

			$this->fallback();
			parent::initialize();
		}

		private function loadUIObj($type)
		{
			include_once("types/{$type}/{$type}QTRUI.php");
			$str = "{$type}QTRUI";
			$obj = new $str();
			return $obj;
		}

		public function applyRequest($result)
		{
			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					$this->addTParam('records', $rs['result']);
				break;

				case 2:
					if($rs['result'] == 104)
						break;

					foreach($rs['result'] as $t => $v)
						$this->types[$t]->setRecord($v);
				break;

				case 3:
					$this->addTParam('scheme', $rs['result']);
				break;

				case 4:
					if($rs['result'] == 104)
						break;

					foreach($rs['result'] as $k => $v) {
						$obj = $this->loadUIObj($k);
						$obj->setSession($v);
						$this->types[$k] = $obj;
					}
				break;

				case 5:
					$this->addTParam('header', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "/.assets/recordstyle.css");
		}

		public function setParams()
		{
			$this->addTParam('types', $this->types);
		}

		public function fallback()
		{
			$mpath = SystemConfig::appRelativePath('library/media/kura/record-keeper');
			$this->addFallbackLink('media',$mpath);

			$mpath = SystemConfig::appRelativePath('library/media/kura/general');
			$this->addFallbackLink('mediag',$mpath);

			$qstr = QStringModifier::modifyParams(array('recid' => null));
			$this->addFallbackLink('view', $qstr);
		}
	}
?>
