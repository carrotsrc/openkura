<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="vfont-large vfont-grey">
</div>
<div class="vfont-grey vform-item" style="overflow: auto; position: relative;">
<?php
	if(!$vars->records || $vars->records == 104) {
		echo "You have no records";
	}
	else {
		foreach($vars->records as $r) {
			echo "<div class=\"vform-item\" style=\"vertical-align: text-top; overflow: auto; margin-right: 20px; border-bottom: 0px solid #D8D8D8;\">";
			echo "<div class=\"vfloat-left\" style=\"overflow: auto;\">";
				if($r[6] == 1) {
					echo "<a href=\"{$vars->_fallback->view}&recid={$r[0]}\">\n";
					echo "<img src=\"{$vars->_fallback->media}/lblue-folder.png\" />\n";
					echo "</a>";
				}
				else {
					echo "<img src=\"{$vars->_fallback->media}/grey-folder.png\" />\n";
				}

			echo "</div>";
			echo "<div class=\"vfloat-left vfont-large\" style=\"overflow: auto; margin-left: 20px; padding-top: 10px;\">";
				echo "<div class=\"vfont-lblue\">";
				echo $r[4];
				echo "</div>";

				echo "<div class=\"vfont-small vform-item\">";
					echo "<b>{$r[2]}</b>";
				echo "</div>";

				echo "<div class=\"vfont-small\">";
				echo substr($r[5], 0, 10);
				echo "</div>";

				if($r[6] == 0) {
					echo "<div class=\"vfont-small\"><br />";
					echo "<b>Pending marks<br />...</b>";
					echo "</div>";
				}
			echo "</div>";

			echo "</div>";
		}
	}
?>
</div>
