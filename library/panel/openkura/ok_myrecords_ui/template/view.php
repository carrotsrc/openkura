<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="vfont-x-large" style="color: #808080; margin-bottom: 10px;">
<?php
	foreach($vars->header as $k => $p)
		if($k == 'title')
			echo $p;
?>
</div>
<?php
	foreach($vars->scheme as $sid => $type) {
			$correct = false;
			echo "<div>";
			echo "<div class=\"record-sec-part record-secq font-orange\" style=\"width: 70px; text-align: right; display: block; overflow: hidden; font-size: 35px;\">\n";

			echo "Q$sid";
			echo "</div>";

			echo "<div class=\"record-sec-part record-sec-border\" style=\"padding-left: 15px; width: 450px; display: inline;\">\n";
			$correct = $vars->types[$type]->genRecordUI($sid);
			echo "</div>\n";

			echo "<div class=\"record-sec-icon\">\n";
			if($correct)
				echo "<img src=\"{$vars->_fallback->mediag}/gtick-basic-96.png\">";
			else
				echo "<img src=\"{$vars->_fallback->mediag}/pcross-basic-96.png\">";
			echo "</div>\n";
			echo "</div>";
	}
?>
