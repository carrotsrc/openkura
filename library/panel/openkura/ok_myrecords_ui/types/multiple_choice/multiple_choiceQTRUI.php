<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class multiple_choiceQTRUI extends QTypeRecordUI
	{
		public function __construct()
		{
		}

		public function setRecord($record)
		{
			$this->record = $record;
		}

		public function setSession($session)
		{
			$this->session = $session;
		}

		public function genRecordUI($id)
		{
			$correct = false;
			echo "<div class=\"light-blue vfont-large\" style=\"margin-bottom: 10px;\">";
				$content = StrSan::sqhtmlSanatize($this->session[$id][0]);
				echo $content;
			echo "</div>";

			if($this->record[$id][1]) {
				$ch = $this->record[$id][0];
				echo "You selected the correct answer:";
				echo "<div style=\"margin-left: 25px; margin-top: 5px;\">";

				$content = StrSan::mysqlDesanatize($this->session[$id][$ch][0]);
				$content = StrSan::htmlSanatize($content);
				$content = StrSan::sqhtmlSanatize($content);
				echo $content;

				echo "</div>";
				if(isset($this->session[$id][$ch]['d'])) {
					echo "<div style=\"margin-left: 25px; margin-top: 7px; font-style: italic\">";
					echo $this->session[$id][$ch]['d'];
					echo "</div>";
				}
				$correct = true;
			}
			else {
				$ch = $this->record[$id][0];
				echo "You selected:";
				echo "<div style=\"margin-left: 25px; margin-top: 5px;\">";

				$content = StrSan::mysqlDesanatize($this->session[$id][$ch][0]);
				$content = StrSan::htmlSanatize($content);
				$content = StrSan::sqhtmlSanatize($content);

				echo $content;
				echo "</div>";
				if(isset($this->session[$id][$ch]['d'])) {
					echo "<div style=\"margin-left: 25px; margin-top: 7px; font-style: italic\">";
					echo $this->session[$id][$ch]['d'];
					echo "</div>";
				}


				echo "<br />Correction:";
				
				foreach($this->session[$id] as $ch) {
					if($ch[1] == 1) {
						echo "<div style=\"margin-left: 25px; margin-top: 5px;\">";
						$content = StrSan::mysqlDesanatize($ch[0]);
						$content = StrSan::htmlSanatize($content);
						$content = StrSan::sqhtmlSanatize($content);

						echo $content;
						echo "</div>";
						if(isset($ch['d'])) {
							echo "<div style=\"margin-left: 25px; margin-top: 7px; font-style: italic\">";
							echo $ch['d'];
							echo "</div>";
						}
					}
				}
			}

			return $correct;
		}
	}
?>

