<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div class="record-queue">
<div class="title">
<b>Record Inbox</b>
</div>
<div class="content">
<?php
	echo "<table>\n";
		echo "<tr class=\"title\">";
			echo "<td>Record</td>";
			echo "<td>User</td>";
			echo "<td>Activity</td>";
		echo "</tr>";
		if($vars->records == null) {
			echo "<tr>";
				echo "<td class=\"content-row content-alt\" colspan=\"3\">";
					echo "No records pending";
				echo "</td>";
			echo "</tr>";
		}
		else {
			for($i = 0; $i < 3; $i++)
			foreach($vars->records as $k => $r) {
				if($i%2 != 0)
					echo "<tr class=\"content-row\">";
				else
					echo "<tr class=\"content-row content-alt\">";
				echo "<td style=\"border-right: 2px dashed #D8D8D8;\"><a target=\"_BLANK\" href=\"{$vars->_fallback->route}{$r[1]}\">{$r[2]}</a></td>";
				echo "<td>{$r[5]}</td>";
				echo "<td>{$r[6]}</td>";
				echo "</tr>";
			}
		}
	echo "</table>";
?>
</div>
</div>
