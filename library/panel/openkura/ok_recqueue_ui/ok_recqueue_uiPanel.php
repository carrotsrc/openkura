<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class ok_recqueue_uiPanel extends Panel
	{
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('ok_recqueue_ui');
		}

		public function loadTemplate()
		{
			$mpath = SystemConfig::appRelativePath("library/media/kura/users");
			$this->addTParam('media', $mpath);

			$this->includeTemplate("template/main.php");
		}

		public function initialize($params = null)
		{
			$this->addComponentRequest(1, 101);
			$this->addComponentRequest(2, 101);
			parent::initialize();
		}
		public function applyRequest($result)
		{
			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					if($rs['result'] == 104)
						break;

					$this->addTParam('records', $rs['result']);
				break;

				case 2:
					$this->setRoute($rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', "template/rqueue.css");
		}

		public function setRoute($rt)
		{
			$qstr = "?loc={$rt[0]}/{$rt[1]}&_cml=recfeedback_ui/3&_ref=";
			$this->addFallbackLink('route', $qstr);
		}
	}
?>
