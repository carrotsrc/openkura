<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/*
*  OpenKura modification for including attachments in a post
*  Updated: 2013-02-06
*/
define('OKUI_STATEMENT', 1);
define('OKUI_TYPE', 2);
define('OKUI_ATTRIBUTE', 3);
define('OKUI_VALUE', 4);
define('OKUI_STR', 5);

class OkuiFormat
{
	private $rdir;
	public function __construct($path)
	{
		$this->rdir = $path;
	}
	public function format(&$body, &$attp)
	{
		$slen = 0; // statement length
		$i = 0; // index
		$str = ""; // current str
		$ctype = array();
		$mode = array();
		$cm = false;
		$this->switchNormal($i, $cm, $body, $mode, $attp);
	}

	private function switchNormal(&$i, &$cm, &$body, &$mode, &$attp)
	{
		while(1) {
			if(!isset($body[$i]))
				break;

			$ch = $body[$i];
			switch($ch) {
			case '!':
				echo $body[++$i];
			break;

			case '[':
				$mode[] = OKUI_STATEMENT;
				$cm = OKUI_STATEMENT;
				$i++;
				$this->switchExp($i, $cm, $body, $mode, $attp);
			break;

			case ']':
				echo $ch;
				break;
			break;

			case "\n":
				echo "<br />";
			break;

			default:
				echo $ch;
			break;
			}

			$i++;
		}
	}

	private function switchExp(&$i, &$cm, &$body, &$mode, &$attp)
	{
		$str = "";
		$stm = array(null, array());
		$attr = array();
		$cattr = "";
		while(1) {
			if(!isset($body[$i]))
				break;

			$ch = $body[$i];
			switch($ch) {
			case ' ':
				if($cm == OKUI_STR) {
					$str .= $ch;
					break;
				}

				if($cm == OKUI_TYPE)
					break;

				if($cm == OKUI_STATEMENT) {
					$stm[0] = $str;
					$str = "";
					$cm = OKUI_TYPE;
					$mode[] = OKUI_TYPE;
				}
			break;

			case '=':
				if($cm == OKUI_TYPE) {
					$cm = OKUI_ATTRIBUTE;
					$mode[] = OKUI_ATTRIBUTE;
					$cattr = $str;
					$str = "";
				}
			break;

			case '"':
				if($cm == OKUI_ATTRIBUTE) {
					$cm = OKUI_STR;
					$mode[] = OKUI_STR;
					break;
				}

				if($cm == OKUI_STR) {
					array_pop($mode);
					array_pop($mode);
					$cm = end($mode);

					$stm[1][$cattr] = $str;
					$cattr = "";
					$str = "";
				}
			break;

			case ']':
				array_pop($mode); // pop type
				array_pop($mode); // pop statement
				$cm = end($mode);
				$this->processExp($stm, $attp);
				return;
			break;

			case "\n":
				echo "<br />";
			break;

			default:
				$str .= $ch;
			break;
			}

			$i++;
		}

		echo "<span style=\"color: red;\">Formatting error!</span>";
	}

	private function processExp(&$stm, &$attp)
	{
		switch($stm[0]){
		case "img":
			$this->generateImg($stm[1], $attp);
		break;
		case "link":
			$this->generateLink($stm[1], $attp);
		break;
		}
	}

	private function generateImg(&$attr, &$attp)
	{
		if(!isset($attr['ref']))
			return;
		$path = "unknown";
		if(is_array($attp))
			foreach($attp as $k =>$a)
				if($a[0] == $attr['ref'])
					$path = $a[3];

		$path = $this->rdir . $path;
		echo "<img src=\"$path\"";

		if(isset($attr['style']))
			echo "style=\"{$attr['style']}\"";

		echo " />";
	}

	private function generateLink(&$attr, &$attp)
	{
		if(!isset($attr['ref']))
			return;

		if(!isset($attr['title']))
			return;

		echo "<a href=\"{$attr['ref']}\">{$attr['title']}</a>";
	}
}
/*
*  End mod
*/
	class vp_blogview_uiPanel extends Panel
	{
		public function __construct()
		{
			parent::__construct();
			$this->setModuleName('vp_blogview_ui');
		}

		public function loadTemplate()
		{
			$this->includeTemplate("template/main.php");
		}

		public function initialize($params = null)
		{
			$this->addComponentRequest(1, 101);
			$this->addComponentRequest(2, 101);
			$this->addTParam('okuif', new OkuiFormat(SystemConfig::appRelativePath("")));
			parent::initialize();
		}
		public function applyRequest($result)
		{

			foreach($result as $rs) {
				switch($rs['jack']) {
				case 1:
					if($rs['result'] == 104) {
						$this->addTParam('posts', array());
						break;
					}
					$this->addTParam('posts', $rs['result']);
				break;

				case 2:
					$this->addTParam('blog', $rs['result']);
				break;
				}
			}
		}

		public function setAssets()
		{
			$this->addAsset('css', '/.assets/general.css');
		}
	}
?>
