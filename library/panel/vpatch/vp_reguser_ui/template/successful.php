<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>
<div class="register">
<div class="rform vfloat-left">
	<center style="color: #7D9E05; font-weight: bold;">
	<img src="<?php echo $vars->_fallback->mediag; ?>/ngtick-128.png" /><br /><br />
	Successfully registered and activated!
	</center>
	<div style="text-align: center; margin-top: 50px;">
	Please <a href="?loc=web/login">login</a>
	</div>
</div>

<div class="rdata vfloat-left">
	<h3>Welcome to OpenKura registration!</h3>

	<p>
	We need you to register again because we have migrated over to the new
	server.
	</p>

	<p>
	It's best to use your <b>university account name</b> here because then there is consistency across the systems and save for confusion later on.
	</p>

	<p>
	Once you register, your account will be activated automatically.
	</p>
</div>
</div>
