<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>
<div class="register">
<div class="rform vfloat-left">
	<form action="<?php echo $vars->_fallback->submit; ?>" method="post">
	<table>
	<tr>
		<td>username </td>
	</tr>
	<tr>
		<td>
			<input name="uruser" type="text" class="vform-text vfont-x-large" autocomplete="off" value=""/>
		</td>
	</tr>

	<tr class="tr-info">
		<td class="td-info">
			Please use your university account name (ad42 or similar)
		</td>
	</tr>

	<tr><td colspan="2">&nbsp;</td></tr>

	<tr>
	<tr>
		<td>password</td>
	</tr>
		<td>
			<input name="urpass" type="password" class="vform-text vfont-x-large" autocomplete="off"/>
		</td>
	</tr>
	<tr class="tr-info">
		<td class="td-info">
		Please user a different password to your university account
		</td>
	</tr>

	<tr><td colspan="2">&nbsp;</td></tr>

	<tr>
		<td>email</td>
	</tr>
	<tr>
		<td>
			<input name="uremail" type="text" class="vform-text vfont-x-large" autocomplete="off"/>
		</td>
	</tr>
	<tr class="tr-info">
		<td class="td-info">
		Only your university email is valid here.<br />
		</td>
	</tr>

	<tr><td colspan="2">&nbsp;</td></tr>

	</table>
	<input type="submit" class="vform-button vfont-large" value="Register" style="width: 50%;">
	</form>
	<div class="error">
	<?php
		if($vars->error != null) {
			switch($vars->error) {
			case 1:
				echo "*You missed entering some data.";
			break;

			case 2:
				echo "*Sorry, your email is invalid";
			break;

			case 3:
				echo "*Sorry, your username is already in use";
			break;
			}
		}
	?>
	</div>
</div>

<div class="rdata vfloat-left">
	<h3>Welcome to OpenKura registration!</h3>

	<p>
	We need you to register again because we have migrated over to the new
	server.
	</p>

	<p>
	It's best to use your <b>university account name</b> here because then there is consistency across the systems.
	</p>

	<p>
	Once you register, your account will be activated automatically.
	</p>
</div>
</div>
