<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class coursecfg extends StrapBase
	{
		public function process(&$xml)
		{
			while(($tag = $xml->getNextTag()) != null) {
				if($tag->element == "module")
					$this->handleModule($tag, $xml);
				else
				if($tag->element == "/obj")
					break;
			}
		}

		public function handleCourse($tag)
		{
			
		}

		private function handleModule($tag, &$xml)
		{
			$name = $code = $desc = $rout = $out = null;
			foreach($tag->attributes as $a => $v) {
				if($a == "name")
					$name = $v;
				else
				if($a == "code")
					$code = $v;
				if($a == "out")
					$out = $v;
				else
				if($a == "rout")
					$rout = $v;
			}

			while(($tag = $xml->getNextTag()) != null) {
				if($tag->element == "_text_" && isset($tag->attributes['content'])) {
					if($tag->attributes['content'] != "")
						$desc = $tag->attributes['content'];
				}
				else
				if($tag->element == "/module")
					break;
			}

			$sql = "INSERT INTO `kura_modules` ";
			$sql .= "(`name`,  `code`, `desc`) VALUES ";
			$sql .= "('$name', '$code', '$desc');";

			if(!$this->db->sendQuery($sql, false, false)) {
				if($rout != null)
					setVariable($rout, null);

				if($out != null)
					setVariable($out, null);

				return;
			}
			$id = $this->db->getLastId();
			$rid = $this->resManager->addResource("KModule", $id, $code);
			
			if($rout != null)
				setVariable($rout, $rid);

			if($out != null)
				setVariable($out, $id);
		}

	}
?>
