<!--
	Strap: OpenKura / Demo Module
	Type: Sub/Init
	Version: 0.1
	VPatch: 0.2
	OpenKura: 0.2
	Author: carrotsrc.org / cfg

	Course module Demo

	Area Register:
		ok001-students
		ok001-staff

	Module Register:
		ok_bulletin 	=> component / instance
		ok_bboard_ui	=> panel
		ok_linkgroup	=> component / instance
		ok_linkgroup_ui => panel
		ok_glossary 	=> component / instance
		ok_glossary_ui	=> panel

	Populates ok_linkgroup:
		OpenKura menu
		OK-001 menu
		OK-001 staff menu

	Generates layout wireframe:
		ok001-home
		ok001-staff-home
		ok001-glossary
		( ok001-activity-template )

	REQUIRES
	kura
	ok_initkura
	ok_glossary
	ok_linkgroup

-->
<obj name="coursecfg" space="openkura">
	<module name="OpenKura Demo" code="OK001" rout="ridm">This is a module based on new things</module>
</obj>

<obj name="areacfg" space="vpatch">
	<area name="ok001-staff" template="1" surround="1" out="areaid_staff" rout="rida_staff" />
	<area name="ok001-students" template="1" surround="1" out="areaid_students" rout="rida_students" />
</obj>

<relationship parent="{ridm}" child="{rida_staff}" edge="cadmin" />
<relationship parent="{ridm}" child="{rida_students}" edge="cstudent" />


<!-- Student stuff -->
<obj name="modreg" space="vpatch">
	<!-- setup the bulletin component -->
	<module type="component" name="ok_bulletin" space="openkura" out="cid" rout="ridb" />
	<module type="panel" name="ok_bboard_ui" space="openkura" />
	<instance id="{cid}" label="okbb_ok001" out="bbid" rout="ridi">
		<param name="title" value="OK-001 Bulletin" />
	</instance>

	<!-- setup the link groups component -->
	<module type="component" name="ok_linkgroup" space="openkura" out="cidlg" rout="ridlg" />
	<instance id="{cidlg}" label="oklg_openkura" out="reflg_a">
		<param name="title" value="OpenKura" />
	</instance>
	
	<instance id="{cidlg}" label="oklg_ok001" out="reflg_b">
		<param name="title" value="OK-001" />
	</instance>

	<instance id="{cidlg}" label="oklg_ok001-staff" out="reflg_c">
		<param name="title" value="Module" />
	</instance>

	<module type="panel" name="ok_linkgroup_ui" space="openkura" />
</obj>

<!-- populate the link groups -->
<obj name="db" space="vpatch">
	<insert table="kura_linkgroup">
		<col name="inst" value="1" />
		<col name="item" value="1" />
		<col name="setting" value="1" />
		<col name="value" value="Home" />
		<col name="link" value="home" />
	</insert>
	<insert table="kura_linkgroup">
		<col name="inst" value="1" />
		<col name="item" value="2" />
		<col name="setting" value="1" />
		<col name="value" value="Profile" />
		<col name="link" value="profile" />
	</insert>
	<insert table="kura_linkgroup">
		<col name="inst" value="1" />
		<col name="item" value="3" />
		<col name="setting" value="1" />
		<col name="value" value="Logout" />
		<col name="link" value="logout" />
	</insert>

	<insert table="kura_linkgroup">
		<col name="inst" value="2" />
		<col name="item" value="2" />
		<col name="setting" value="0" />
		<col name="value" value="Glossary" />
		<col name="link" value="ok001-students/ok001-glossary" />
	</insert>

	<insert table="kura_linkgroup">
		<col name="inst" value="3" />
		<col name="item" value="1" />
		<col name="setting" value="0" />
		<col name="value" value="Activities" />
		<col name="link" value="ok001-staff" />
	</insert>
</obj>

<!-- setup the layout for the module home -->
<obj name="wireframecfg" space="vpatch">
	<layout name="ok001-home" rout="ridl">
		<node type="1" style="float: left; width: 150px;">
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_a}" grp="1" style="float: none; display: block;" />
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_b}" grp="1" style="float: none; display: block;" />
		</node>
		<node type="0" style="float: none; display: block; margin-left: 160px; clear: none;">
			<leaf type="1" pid="ok_bboard_ui" cid="{cid}" ref="{bbid}" grp="1" style="float: none; display: block; clear: none;" />
		</node>
	</layout>
</obj>

<relationship parent="{rida_students}" child="{ridl}" edge="index" />

<obj name="modreg" space="vpatch">
	<!-- setup the glossary component -->
	<module type="component" name="ok_glossary" space="openkura" out="cid" rout="ridg" />
	<instance id="{cid}" label="okg_ok001" out="gid" rout="ridgi">
	</instance>
	<module type="panel" name="ok_glossary_ui" space="openkura" />
</obj>

<!-- setup a layout for the glossary -->
<obj name="wireframecfg" space="vpatch">
	<layout name="ok001-glossary" rout="ridl">
		<node type="1" style="float: left; width: 150px;">
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_a}" grp="1" style="float: none; display: block;" />
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_b}" grp="1" style="float: none; display: block;" />
		</node>
		<node type="1" style="float: none; display: block; clear: none">
			<leaf type="1" pid="ok_glossary_ui" cid="{cid}" ref="{gid}" grp="1" style="float: none; display: block; clear: none;" />
		</node>
	</layout>
</obj>
<relationship parent="{rida_students}" child="{ridl}" edge="index" />

<!-- staff stuff -->
<obj name="modreg" space="vpatch">
	<!-- setup the course manager and activity manager component component -->
	<module type="component" name="ok_courseman" space="openkura" out="cid" rout="ridb" />
	<module type="panel" name="ok_activityman_ui" space="openkura" />
	<instance id="{cid}" label="okc_ok001_cm" out="refcm" rout="ridi">
	</instance>
</obj>

<!-- staff home layout -->
<obj name="wireframecfg" space="vpatch">
	<layout name="ok001-staff-home" rout="ridl">
		<node type="1" style="float: left; width: 150px;">
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_a}" grp="1" style="float: none; display: block;" />
			<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_c}" grp="1" style="float: none; display: block;" />
		</node>
		<node type="0" style="float: none; display: block; margin-left: 160px; clear: none;">
			<leaf type="1" pid="ok_activityman_ui" cid="{cid}" ref="{refcm}" grp="1" style="float: none; display: block; clear: none;" />
		</node>
	</layout>
</obj>
<relationship parent="{rida_staff}" child="{ridl}" edge="index" />

<!-- ok001 activity template -->
<obj name="wireframecfg" space="vpatch">
	<layout name="ok001-template-act" rout="ridl">
		<node type="1" style="width: 180px;">
		<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_a}" grp="1" style="float: none; display: block;" />
		<leaf type="1" pid="ok_linkgroup_ui" cid="{cidlg}" ref="{reflg_b}" grp="1" style="float: none; display: block;" />
		</node>

		<node type="0" style="float: none; margin-left: 200px; clear: none; display: block;">
		<leaf type="1" grp="0" />
		</node>
	</layout>
</obj>
<relationship parent="{ridm}" child="{ridl}" />
