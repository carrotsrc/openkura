/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
function multiple_choiceQTM (prefixStr, mediaStr) {
	var self = this;
	this.prefix = prefixStr;
	this.media = mediaStr;
	this.session = Array();

	this.addSection = function (id) {
		this.session.push(Array(id, Array("")));
		return (this.session.length);
	}

	this.removeSection = function (id) {
		var sz = this.session.length;
		for(var i = 0; i < sz; i++) {
			if(this.session[i][0] == id) {
				this.session.splice(i, 1);
				return;
			}
		}
	}

	this.addAnswer = function (id, n, v, c, noRefresh) {
		var sz = this.session.length;

		if(v === undefined)
			v = "";
		if(c === undefined)
			c = 0;

		for(var i = 0; i < sz; i++) {
			if(this.session[i][0] == id) {
				while(n-- > 0)
					this.session[i][1].push(Array(c,v));
	
				if(noRefresh === undefined)
					this.uiu_editQuestion(id);

				return this.session[i][1].length;
			}
		}
	}

	this.removeAnswer = function (sid, n, id) {
		this.session[sid][1].splice(n,1);
		this.uiu_editQuestion(id);
	}

	this.addTextProperty = function (sid, n, id) {
		this.session[sid][1][n].push("");
		this.uiu_editQuestion(id);
	}

	this.uiu_editQuestion = function (id) {
		var r = document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		u = this.uio_editQuestion(id, u);
		var e = document.getElementById(this.prefix+"-qedit"+id);
		u = KTSet.NodeUtl(e);
		u.gotoParent();
		u.clearChildren();
		u.appendChild(r);
	}

	this.uio_editQuestion = function (id, u) {

		var index = 0;
		u.appendChild('div');
			u.gotoLast();
			u.node.className = "question-edit";
			u.node.id = this.prefix+"-qedit"+id;
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "question-edit-form";
				u.appendChild('span');
					u.gotoLast();
					u.node.style = "color: #FFA500; font-weight: bold;";
					u.appendText("Edit");
					u.gotoParent();
	
				u.appendChild('div');
					u.gotoLast();
					u.node.className = "vform-item vfont-small";
					u.appendText("Question:");
					u.appendChild('br');
					try {
						u.appendChild('input');
							u.gotoLast();
							u.node.className = "vform-text vfont-large";
							u.node.style="width: 100%";
							var sz = self.session.length;
							for(index = 0; index < sz; index++) {
								if(self.session[index][0] === id) {
									u.node.value = self.session[index][1][0];
									break;
								}
							}
							u.node.qid = id;
							u.node.sid = index;
							// update the question as the user types
							u.node.onkeyup = function () {
								self.session[this.sid][1][0]=this.value;
							}
							u.gotoParent();
					}
					catch(e) {

					}
					u.appendChild('div');
						u.gotoLast();
						u.node.style="margin-left: 15px; margin-top: 15px;";

					try {
						var sz = self.session[index][1].length;
						if(sz  > 1)
							for(var i = 1; i < sz; i++) {
								u.appendChild('div');
									u.gotoLast();
									u.node.className="vform-item-spacer vfont-large";
									u.appendText(i+". ");
									u.appendChild('input');
										u.gotoLast();
										u.node.className = "vform-text";
										u.node.style = "width: 75%;";
										u.node.value = self.session[index][1][i][1];
										u.node.sid = index;
										u.node.aid = i;
										u.node.onkeyup = function () {
												self.session[this.sid][1][this.aid][1] = this.value;
										}
										u.gotoParent();
									u.appendChild('input');
										u.gotoLast();
										u.node.type = "checkbox";
										u.node.style="margin-left: 5px;";
										u.node.sid = index;
										u.node.aid = i;
										if(self.session[index][1][i][0] == 1)
											u.node.checked = true;
										u.node.onclick = function () {
											if(this.checked)
												self.session[this.sid][1][this.aid][0] = 1;
											else
												self.session[this.sid][1][this.aid][0] = 0;
										}
										u.gotoParent();
									if(self.session[index][1][i][2] === undefined) {
										u.appendChild('img');
											u.gotoLast();
											u.node.style="margin-left: 5px;";
											u.node.src = self.media+"/editsec.png";
											u.node.sid = index;
											u.node.aid = i;
											u.node.qid = id;
											u.node.onclick = function () {
												self.addTextProperty(this.sid, this.aid, this.qid);
											}
											u.gotoParent();
									}
									u.appendChild('img');
										u.gotoLast();
										u.node.style="margin-left: 5px; cursor: pointer;";
										u.node.src = self.media+"/delete.png";
										u.node.sid = index;
										u.node.aid = i;
										u.node.qid = id;
										u.node.onclick = function () {
											self.removeAnswer(this.sid, this.aid, this.qid);
										}
										u.gotoParent();
									if(self.session[index][1][i][2] !== undefined) {
										u.appendChild('textarea');
											u.gotoLast();
											u.node.className = "vform-text";
											u.node.aid = i;
											u.node.sid = index;
											u.node.qid = id;
											u.node.value = self.session[index][1][i][2];
											u.node.onkeyup = function () {
												self.session[this.sid][1][this.aid][2] = this.value;
											}
											u.node.style="margin-left: 15px; width: 75%";
											u.node.rows = 2;
											u.gotoParent();
									}
									u.gotoParent();
							}
					}
					catch(e) {}

						u.gotoParent();// question form

					u.appendChild('div');
						u.gotoLast();
						u.node.className="vform-item-spacer";
						u.appendText("Add ");
						u.appendChild('input');
							u.gotoLast();
							u.node.className = "vform-text";
							u.node.id = self.prefix+"-nans"+id;
							u.node.value = "4";
							u.node.style = "width: 35px;";
							u.gotoParent();
						u.appendText(" answers ");
						u.appendChild('input');
							u.gotoLast();
							u.node.type = "button";
							u.node.className = "vform-button";
							u.node.value = "Go";
							u.node.qid = id;
							u.node.onclick = function () {
								var e = document.getElementById(self.prefix+"-nans"+this.qid);
								self.addAnswer(this.qid, e.value);
							}
							u.gotoParent();
						u.gotoParent(); // question form
					u.gotoParent(); // edit-form
				u.gotoParent(); // questionEdit
			u.gotoParent(); // r
	}

	this.uio_viewQuestion = function (id, u) {
		u.appendChild('div');
			u.gotoLast();
			u.node.className = "question-normal";
			u.appendChild('div');
				u.gotoLast();
				u.node.className = "vfont-large";
				var sz = self.session.length;
				for(var i = 0; i < sz; i++) {
					if(self.session[i][0] == id) {
						u.appendText(self.session[i][1][0])
						break;
					}
				}
				u.gotoParent();

			var sz = self.session[i][1].length;
			if(sz > 1) {
				for(var j = 1; j < sz; j++) {
					u.appendChild('div');
						u.gotoLast();
						if(self.session[i][1][j][0] == 1)
							u.node.style = "margin-left: 15px; margin-top: 7px; font-weight: bold;";
						else
							u.node.style = "margin-left: 15px; margin-top: 7px;";
						u.appendText(j+". "+self.session[i][1][j][1]);
						u.gotoParent();
				}
			}
			u.gotoParent();
	}

	this.generateTemplate = function (qid) {
		var index;
		var sz = self.session.length;

		for(index = 0; index < sz; index++)
			if(self.session[index][0] === qid)
				break;

		var xml = "<question>";
		xml += self.session[index][1][0];
		xml += "</question>\n";

		sz = self.session[index][1].length;
		if(sz == 1)
			return xml;

		for(var j = 1; j < sz; j++) {
			xml += "<choice correct=\""+self.session[index][1][j][0]+"\">\n";

			xml += self.session[index][1][j][1];
			xml += "\n</choice>\n";
			if(self.session[index][1][j][2] !== undefined)
				xml += "<desc>"+self.session[index][1][j][2]+"</desc>\n";
		}

		return xml;

	}

	this.processTemplate = function (t, id) {
		var sz = t.length;
		index = self.session.length-1;
		var ca = null;
		for(var i = 0; i < sz; i++) {
			switch(t[i].nodeName){

			case 'question':
				var question  = t[i].firstChild.nodeValue.replace(/^\s+|\s+$|\n+|\n+$/g,'');
				self.session[index][1][0] = question;
			break;

			case 'choice':
				var c = t[i].getAttribute('correct');
				var answer  = t[i].firstChild.nodeValue.replace(/^\s+|\s+$|\n+|\n+$/g,'');
				ca = self.addAnswer(id, 1, answer, c, true);
			break;

			case 'desc':
				self.session[index][1][ca-1].push(t[i].firstChild.nodeValue);
			break;
			}
		}
	}
}
