function EmbedYouTube() {
	this.generate = function (value) {
		var r = window.document.createDocumentFragment();
		var u = KTSet.NodeUtl(r);
		var params = value.split("?");
		var code = undefined;
		var p = undefined;
		params = params[params.length-1];
		params = params.split("&");
		for(var i = 0; i < params.length; i++) {
			p = params[i].split("=");
			if(p[0] == "v")
				code = p[1];
		}

		if(code == undefined)
			return null;

		u.appendChild('div');
			u.gotoLast();
			u.node.className = "bulletin-attachment-embedded";
			u.appendChild('iframe');
				u.gotoLast();
				u.node.style.width = "560px";
				u.node.style.height = "315px";
				u.node.src="http://www.youtube.com/embed/"+code;
				u.node.frameBorder = "0";
				u.node.allowfullscreen;
				u.gotoParent();
			u.gotoParent();

		return r;
	}
}

var embed_YouTube = new EmbedYouTube();
