<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class activity_crudPlugin extends Plugin
	{
		private $resManager;
		public function init($instance)
		{
			if($instance == null)
				return false;

			$this->setInstance($instance);
			$this->resManager = Managers::ResourceManager();
		}

		public function process(&$params)
		{
			if(!isset($params['rid']))
				return $params;

			if(!isset($_GET['activity']))
				return $params;

			$aid = $_GET['activity'];
			$rida = $this->resManager->queryAssoc("Activity('$aid');");
			if(!$rida)
				return $params;
			$rida = $rida[0][0];


			// create relationship between activity and instance
			// of the activity component (eg. qmultichoice)
			if($params['rid'][0] == RIO_INS) {
				$ridi = $params['rid'][1];

				$this->setupActivity($aid, $params['cmpt'], $ridi);
				
				$this->resManager->createRelationship($rida, $ridi);

				// add the exit channel so any resources are put into the
				// module's search tree
				$ridg = $this->resManager->queryAssoc("CrudOps('kmod_sgraph');");
				$this->resManager->createRelationship($ridi, $ridg[0][0]);
			}
			return $params;
		}

		private function setupActivity($aid, $cmpt, $ridi)
		{
			// get the panel associated with the component
			$rq = "Panel()<Component('$cmpt');";
			$ridp = $this->resManager->queryAssoc($rq);
			if(!$ridp)
				return false;
			$ridp = $ridp[0][0];
			$pid = $this->resManager->getResourceFromId($ridp);
			$pid = $pid['handler'];

			// get the activity work area
			$ridw = $this->resManager->queryAssoc("Area()>Activity('$aid');");
			if(!$ridw)
				return false;
			$ridw = $ridw[0][0];

			//get the layout template for module activitys
			$lid = $this->resManager->queryAssoc("Layout()<KModule('{$_GET['kmodule']}');");
			if(!$lid)
				return;
			$lid = $lid[0][0];

			$lid = $this->resManager->getResourceFromId($lid);
			if(!$lid)
				return false;

			$lid = $lid['handler'];

			// get the component for the activity worker
			$cid = $this->resManager->queryAssoc("Component()<Component('$cmpt');");
			if(!$cid)
				return false;


			$cid = $this->resManager->getResourceFromId($cid[0][0]);
			$cid = $cid['handler'];

			include_once(LibLoad::shared('stdint', 'layout'));

			// create a new layout from the template
			$llib = new layoutLibrary($this->db);
			$cml = $llib->getLayout($lid);
			$cml = $cml[2];

			$res = $this->resManager->getResourceFromId($ridi);
			$ref = $res['handler'];
			$leaf = "pid=\"$pid\" cid=\"$cid\" ref=\"$ref\"";
			$cml = str_replace("<!--leaf-->", $leaf, $cml);
			$lid = $llib->addLayout("{$res['label']}_act$aid", $cml);

			$ridl = $this->resManager->addResource('Layout', $lid, "{$res['label']}_act$aid");

			$this->resManager->createRelationship($ridw, $ridl, $this->resManager->getEdge('index'));
			$this->resManager->createRelationship($ridi, $ridl);

			return true;
		}
	}

?>
