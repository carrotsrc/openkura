<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class addprofilePlugin extends Plugin
	{
		public function init($instance)
		{
			$this->instance = $instance;
		}

		public function process(&$params)
		{
			if(!isset($params['rid']))
				return $params;

			$resManager = Managers::ResourceManager();
			if($params['rid'][0] == RIO_INS) {
				$ridu = $params['rid'][1];
				include_once(LibLoad::shared('kura', 'profile'));
				include_once(LibLoad::shared('vpatch', 'users'));

				$plib = new profileLibrary($this->db);
				$ulib = new usersLibrary($this->db);
				$ref = $resManager->getHandlerRef($ridu);
				$account = $ulib->getAccount($ref);
				$pid = $plib->addProfile($account[0][1], "", "", "unknown.png");
				$ridp = $resManager->addResource("Profile", $pid, str_replace(" ", "_", $account[0][1]));

				$resManager->createRelationship($ridu, $ridp);
			}
			return $params;
		}

	}
?>
