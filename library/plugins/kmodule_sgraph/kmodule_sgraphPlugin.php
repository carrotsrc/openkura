<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class kmodule_sgraphPlugin extends Plugin
	{
		private $resManager;
		public function init($instance)
		{
			$this->instance = $instance;
			$this->resManager = Managers::ResourceManager();
		}

		public function process(&$params)
		{
			if(!isset($params['rid']))
				return $params;

			$kmodule = $_GET['kmodule'];
			
			/* TODO:
			*  Make this specifically for activities and source the
			*  graph from parents rather than from the a query string
			*/
			$ridg = $this->resManager->queryAssoc("Graph()<KModule('$kmodule');");

			if(!$ridg)
				return $params;

			$ridg = $ridg[0][0];
			// create relationship between the module search graph
			// and the new resource

			if($params['rid'][0] == RIO_INS) {
				$ridi = $params['rid'][1];
				$this->resManager->createRelationship($ridg, $ridi, $this->resManager->getEdge('gchild'));
			}
			return $params;
		}

	}
?>
