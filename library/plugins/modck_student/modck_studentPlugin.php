<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class modck_studentPlugin extends Plugin
	{
		private $resManager;
		public function init($instance)
		{
			$this->instance = $instance;
			$this->resManager = Managers::ResourceManager();
		}

		public function process(&$params)
		{
			$user = Session::get('uid');
				
			if(!isset($_GET['kmodule']))
				die("No module specified");

			$module = $_GET['kmodule'];
			$res = $this->resManager->queryAssoc("User('$user')<KModule('$module');");
			if(!$res) {
				if($user == 0)
					HttpHeader::redirect("?loc=web");
				else
					HttpHeader::redirect("?loc=home");
				return false;
			}

			return $params;
		}

		public function redirect()
		{

		}

	}
?>
