<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class mentor_accessPlugin extends Plugin
	{
		private $resManager;
		public function init($instance)
		{
			$this->instance = $instance;
			$this->resManager = Managers::ResourceManager();
		}

		public function process(&$params)
		{
			$uid = Session::get('uid');
			// check to see if the user is activity coordinator
			if(($rid = $this->resManager->queryAssoc("User('$uid')<Area('mentors'):admin;"))) {
				return $params;
			}
			// other wise find out which group the user is in

			$ridg = $this->resManager->queryAssoc("Group()>User('$uid')<Area('mentors');");
			if(!$ridg) {
				echo "You have not been assigned to a group.<br />";
				return false;
			}
			
			$ridl = $this->resManager->queryAssoc("Layout()<Group({$ridg[0][0]});");
			if(!$ridl) {
				$params['layout'] = 0;
				return $params;
			}
			$ridl = $ridl[0][0];
			$ref = $this->resManager->getHandlerRef($ridl);
			$params['layout'] = $ref;
			return $params;
		}

	}
?>
