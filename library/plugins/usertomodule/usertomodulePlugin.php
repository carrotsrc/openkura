<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class usertomodulePlugin extends Plugin
	{
		private $resManager;
		public function init($instance)
		{
			$this->instance = $instance;
			$this->resManager = Managers::ResourceManager();
		}

		public function process(&$params)
		{
			if(!isset($params['rid']))
				return $params;
			$resManager = Managers::ResourceManager();
			if($params['rid'][0] == RIO_INS) {
				$ridu = $params['rid'][1];
				$ridm = $resManager->queryAssoc("KModule('SS435');");
				if(!$ridm)
					return $params;

				$this->resManager->createRelationship($ridm[0][0], $ridu, $this->resManager->getEdge('cstudent'));
			}
			return $params;
		}

	}
?>
