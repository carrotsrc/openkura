<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class kmod_globalPlugin extends Plugin
	{
		public function init($instance)
		{
			$this->instance = $instance;
		}

		public function process(&$params)
		{
			$resManager = Managers::ResourceManager();
			if(!isset($params['globvar']))
				$params['globvar'] = array();

			$ref = null;
			$mod = $resManager->queryAssoc("KModule()>Area('{$params['area']}');");
			if(!$mod) {
				if(!isset($_GET['kmodule']))
					die("Major Malfunction: No area access to kmodule");
				else
					$ref = $_GET['kmodule'];
			}
			else
				$ref = $resManager->getHandlerRef($mod[0][0]);

			$params['globvar']['kmodule']=$ref;
			

			$onloadParams = "KitJS.addGlobalParam(\"kmodule\",\"$ref\");\n";
			if(isset($params['onload']))
				if(!is_array($params['onload']))
					$params['onload'] = array();

			$params['onload'][]= $onloadParams;


			if(isset($_GET['activity'])) {
				$ref = $_GET['activity'];
				// sometimes we are going to be working with activities
				// so make sure that activity is added to the global params
				$onloadParams = "KitJS.addGlobalParam(\"activity\",\"$ref\");\n";
				$params['onload'][]= $onloadParams;
			}
			return $params;
		}

	}
?>
