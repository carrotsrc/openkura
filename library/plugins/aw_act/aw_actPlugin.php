<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
	class aw_actPlugin extends Plugin
	{
		public function init($instance)
		{
			$this->instance = $instance;
		}


		public function process(&$params)
		{
			if(!isset($params['cmpt']) || !isset($params['inst']))
				return $params;
	
			$resManager = Managers::ResourceManager();
			if(!isset($params['rid']))
				return $params;

			$cmpt = $params['cmpt'];
			$inst = $params['inst'];
			$rida = $resManager->queryAssoc("Activity()>(Instance('$inst')<Component('$cmpt'));");
			if(!$rida)
				return $params;
			$rida = $rida[0][0];

			if($params['rid'][0] == RIO_INS) {
				$ride = $params['rid'][1];
				$resManager->createRelationship($rida, $ride);
			}
			return $params;
		}

	}
?>
